#version 300 es

precision mediump float;

out vec3 fFragColor;

void main() {
    fFragColor = vec3(0.5f,0.3f,0.1f);
}
