#version 300 es

precision mediump float;

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in float aVertexSize;

uniform mat4 uMVMatrix;
uniform mat4 uMVPMatrix;
uniform vec3 uColor;

void main() {
  vec4 position_vs = uMVMatrix * vec4(aVertexPosition, 1);
  gl_Position = uMVPMatrix * vec4(aVertexPosition, 1);
  gl_PointSize = aVertexSize * 10.f / length(position_vs);
}
