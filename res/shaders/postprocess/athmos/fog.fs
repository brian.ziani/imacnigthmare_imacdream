#version 300 es

precision mediump float;

in vec2 vCoord_s;

out vec4 fFragColor;

uniform sampler2D gWorldPos;
uniform sampler2D gZbuffer;

uniform vec3 uEyePosition_w;
uniform float uHorizon;

uniform vec3 uFogColor;
uniform float uFogHeight;

uniform float uNear;
uniform float uFar;

float linearizedDepth() {
  float depth = texture(gZbuffer, vCoord_s).r;
  return pow(depth, (uFar - uNear) * 0.1f);
}

float angularDistanceAgainstHorizon() {
  return clamp(vCoord_s.y - uHorizon, 0.1f, 1.f);
}

float bias(float x, float b) {
  return (1.f - b) * (x + b);
}

vec3 sky(float d) {
  float redCoef = pow(bias(1.f - d, 0.3f), 0.3f);
  float greenCoef = pow(bias(d, 0.3f), 0.1f);
  float blueCoef = bias(pow(d, 0.2f), 0.3f);
  return uFogColor * vec3(redCoef,greenCoef, blueCoef);
}


float F(float y) {
  return -exp(-y / uFogHeight -5.f);
}

void main() {
  vec3 fragPos = texture(gWorldPos, vCoord_s).xyz;
  vec3 U = fragPos - uEyePosition_w;
  float scaleFactor = length(U) / (fragPos.y - uEyePosition_w.y);

  float depth = linearizedDepth();
  float height = fragPos.y / (uFogHeight * 10.f);
  float heightFactor = pow(abs(1.f - height), 0.7f);
  float depthFactor = depth;
  float angle = angularDistanceAgainstHorizon();
  float hasfog = clamp(angle - 0.1f, 0.f, 0.1f) * 10.f;
  hasfog = (1.f - hasfog);
  hasfog = hasfog * hasfog;

  fFragColor =
    0.7f * vec4(sky(angle), 1.f - hasfog * depthFactor * heightFactor)
    + 0.3f * vec4(uFogColor, exp(-scaleFactor * (F(fragPos.y) - F(uEyePosition_w.y))));
}
