#version 300 es

precision mediump float;

in vec2 vCoord_s;

layout(location = 0) out vec3 fFragColor;

uniform sampler2D gDiffuse;
uniform sampler2D gEmission;
uniform sampler2D gBlurIn;

uniform int uMode;
uniform float uExposure;
uniform float uRadius;

const float weight[5] = float[] (0.227027f, 0.1945946f, 0.1216216f, 0.054054f, 0.016216f);

void pickEmissionColor() {
  fFragColor = 2.f * texture(gEmission, vCoord_s).r * texture(gDiffuse, vCoord_s).rgb;
}
void horizontalBlur() {
  float texelSize = uRadius / float(textureSize(gBlurIn, 0).x);
  vec3 result = texture(gBlurIn, vCoord_s).rgb * weight[0];
  for(int i = 1; i < 5; ++i) {
    result += texture(gBlurIn, vCoord_s + vec2(texelSize * float(i), 0.f)).rgb * weight[i];
    result += texture(gBlurIn, vCoord_s - vec2(texelSize * float(i), 0.f)).rgb * weight[i];
  }
  fFragColor = result;
}
void verticalBlur() {
  float texelSize = uRadius / float(textureSize(gBlurIn, 0).y);
  vec3 result = texture(gBlurIn, vCoord_s).rgb * weight[0];
  for(int i = 1; i < 5; ++i) {
    result += texture(gBlurIn, vCoord_s + vec2(0.f, texelSize * float(i))).rgb * weight[i];
    result += texture(gBlurIn, vCoord_s - vec2(0.f, texelSize * float(i))).rgb * weight[i];
  }
  fFragColor = result;
}
void combine() {
  fFragColor = texture(gBlurIn, vCoord_s).rgb;
}

void main() {
  if (0 == uMode) {
    pickEmissionColor();
  } else if (1 == uMode) {
    horizontalBlur();
  } else if (2 == uMode) {
    verticalBlur();
  } else {
    combine();
  }
}
