#version 300 es

precision mediump float;

in vec2 vCoord_s;

layout(location = 0) out vec3 fFragColor;
layout(location = 2) out vec3 AmbiantOut;

uniform sampler2D gWorldPos;
uniform sampler2D gNormal;

uniform sampler2D gShadowMap;

uniform float uBias;
uniform int uMode;

uniform mat4 uLightMatrix;
uniform vec3 uLightDir;

float isInShadow(vec4 fragPos_l) {
  vec3 projCoords = fragPos_l.xyz / fragPos_l.w;
  projCoords = projCoords * 0.5 + 0.5;
  if (projCoords.z > 1.f) {
    return 0.f;
  } else {
    ivec2 size = textureSize(gShadowMap, 0);
    vec2 texelSize = 1.0f / vec2(size.x, size.y);
    float bias = max(uBias * (1.f - dot(texture(gNormal, vCoord_s).xyz, uLightDir)), uBias / 10.f);
    float shadow = 0.f;
    for(int x = -1; x <= 1; ++x) {
      for(int y = -1; y <= 1; ++y) {
        float pcfDepth = texture(gShadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
        shadow += projCoords.z - bias > pcfDepth ? 1.0f : 0.0f;        
      }    
    }
    return shadow / 9.f;
    float pcfDepth = texture(gShadowMap, projCoords.xy).r;
    return projCoords.z - bias > pcfDepth ? 1.0f : 0.0f;
  }
}

void fillShadow() {

  vec4 fragPos_w = vec4(texture(gWorldPos, vCoord_s).xyz, 1.f);
  vec4 fragPos_l = uLightMatrix * fragPos_w;
  fFragColor = vec3(isInShadow(fragPos_l));
}

void main() {

  if (0 == uMode) {
    fillShadow();
  } else if (1 == uMode) {
    AmbiantOut = 0.5f * texture(gShadowMap, vCoord_s).rrr;
  }
}
