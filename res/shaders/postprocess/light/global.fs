#version 300 es

precision mediump float;

in vec2 vCoord_s;

layout(location = 0) out vec3 fFragColor;

uniform sampler2D gWorldPos;
uniform sampler2D gNormal;
uniform sampler2D gAmbiamt;
uniform sampler2D gDiffuse;
uniform sampler2D gSpecular;

uniform vec3 uEyePosition_w;
uniform float uHorizon;

uniform vec3 uLightDir_w;
uniform vec3 uLightColor;

uniform vec3 uAmbiant;
uniform vec3 uSkyColor;

uniform int uMode;

float samplehold(float x, float steps) {
  x = pow(x, 0.5f);
  x = float(int(x * steps)) / steps;
  return pow(x, 2.f);
}

vec3 blinnPhon(vec3 position_w, vec3 lightDir, vec3 lightColor) {
  /* retrieve informations from gbuffer */
  vec3 normal = texture(gNormal, vCoord_s).xyz;
  vec3 uKd = texture(gDiffuse, vCoord_s).rgb;
  vec3 uKs = vec3(1.f,1.f,1.f);
  float shinyness = texture(gSpecular, vCoord_s).r;
  /* compute context */
  vec3 halfVector = (normalize(uEyePosition_w - position_w) + lightDir) / 2.f;
  /* compute each component */
  vec3 diffuse =  uKd * samplehold(clamp(dot(-lightDir, normal), 0.f, 1.f), 6.f);
  vec3 specular = uKs * samplehold(clamp(pow(dot(halfVector, -normal), shinyness), 0.f, 1.f), 6.f);
  /* summ components */
  return lightColor * (diffuse + specular);
}

float angularDistanceAgainstHorizon() {
  return clamp(vCoord_s.y - uHorizon, 0.1f, 1.f);
}

float bias(float x, float b) {
  return (1.f - b) * (x + b);
}

vec3 sky() {
  float dist = angularDistanceAgainstHorizon();
  float redCoef = pow(bias(1.f - dist, 0.3f), 0.3f);
  float greenCoef = pow(bias(dist, 0.3f), 0.1f);
  float blueCoef = bias(pow(dist, 0.2f), 0.3f);
  return uSkyColor * vec3(redCoef,greenCoef, blueCoef);
}

vec3 ambiantColor() {

  return texture(gAmbiamt, vCoord_s).rgb * uAmbiant;
}

vec3 farObjectColor() {

  vec3 position_w = texture(gWorldPos, vCoord_s).xyz;
  vec3 lightdir = vec3(0.f, 1.f, 0.f);
  vec3 antilightdir = vec3(0.f, -1.f, 0.f);

  return 
    ambiantColor()
    + blinnPhon(position_w, lightdir, uLightColor);
    + blinnPhon(position_w, antilightdir, uLightColor);
}

vec3 worldObjectColor() {

  vec3 position_w = texture(gWorldPos, vCoord_s).xyz;
  return ambiantColor() + blinnPhon(position_w, uLightDir_w, uLightColor);
}

void main() {

  if (0 == uMode) {
    fFragColor = farObjectColor();
  } else if (1 == uMode) {
    fFragColor = worldObjectColor();
  } else if (2 == uMode) {
    fFragColor = sky();
  }
}
