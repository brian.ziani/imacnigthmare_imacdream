#version 300 es

precision mediump float;

in vec3 vLightCenter_w;
in vec3 vLightColor;

layout(location = 0) out vec3 fFragColor;

uniform sampler2D gWorldPos;
uniform sampler2D gNormal;
uniform sampler2D gAmbiamt;
uniform sampler2D gDiffuse;
uniform sampler2D gSpecular;
uniform sampler2D gZbuffer;

uniform vec3 uEyePosition_w;
uniform vec2 uScreenSize;

uniform float ucst;
uniform float ulin;
uniform float uquad;

vec2 screenCoords() {
  return vec2(
    float(gl_FragCoord.x) / uScreenSize.x, 
    float(gl_FragCoord.y) / uScreenSize.y);
}

float samplehold(float x, float steps) {
  x = pow(x, 0.5f);
  x = float(int(x * steps)) / steps;
  return pow(x, 2.f);
}

vec3 blinnPhon() {
  vec2 vCoord_s = screenCoords();
  vec3 position_w = texture(gWorldPos, vCoord_s).xyz;
  vec3 lightDir = normalize(position_w - vLightCenter_w);
  /* retrieve informations from gbuffer */
  vec3 normal = texture(gNormal, vCoord_s).xyz;
  vec3 uKa = texture(gAmbiamt, vCoord_s).rgb;
  vec3 uKd = texture(gDiffuse, vCoord_s).rgb;
  vec3 uKs = vec3(1.f,1.f,1.f);
  float shinyness = texture(gSpecular, vCoord_s).r;
  /* compute context */
  vec3 halfVector = (normalize(uEyePosition_w - position_w) + lightDir) / 2.f;
  /* compute each component */
  vec3 ambiant = uKa;
  vec3 diffuse =  uKd * samplehold(clamp(dot(-lightDir, normal), 0.f, 1.f), 6.f);
  vec3 specular = uKs * samplehold(clamp(pow(dot(halfVector, -normal), shinyness), 0.f, 1.f), 6.f);
  /* summ components */
  float distance = length(vLightCenter_w - position_w);
  float attenuation = 1.0 / (ucst + ulin * distance + uquad * (distance * distance));    
  return attenuation * vLightColor * (ambiant + diffuse + specular);
}

void main() {
  fFragColor = blinnPhon();
}