#version 300 es

precision mediump float;

layout(location = 0) in vec3 aVertexPosition;
layout(location = 8) in vec3 aLightCenter;
layout(location = 9) in vec3 aLightColor;
layout(location = 10) in float aRadius;

uniform mat4 uVPmatrix;

out vec3 vLightCenter_w;
out vec3 vLightColor;

void main() {
  vec3 position = aVertexPosition * aRadius + aLightCenter;
  gl_Position = uVPmatrix * vec4(position, 1.f);
  vLightCenter_w = aLightCenter;
  vLightColor = aLightColor;
}