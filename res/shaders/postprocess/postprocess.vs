#version 300 es

precision mediump float;

layout(location = 0) in vec3 aVertexPosition;

out vec2 vCoord_s;

void main() {
  gl_Position = vec4(aVertexPosition, 1.0f);
  vCoord_s = aVertexPosition.xy / 2.f + vec2(0.5f,0.5f);
}