#version 310 es

precision mediump float;

in vec3 vPosition_w;
in vec3 vNormal_w;
in vec2 vTextureUV;
in float emissiveRot;

uniform sampler2D uDiffuse;
uniform sampler2D uSpecular;

uniform float uSpecularScale;
uniform float uSpecularPow;

uniform int uMode;

layout(location = 0) out vec3 WorldPosOut;
layout(location = 1) out vec3 NormalOut;
layout(location = 2) out vec3 AmbiantOut;
layout(location = 3) out vec3 DiffuseOut;
layout(location = 4) out float SpecularOut;
layout(location = 5) out float EmissiveOut;

void main() {
  if (0 == uMode) {
    WorldPosOut = vPosition_w;
    NormalOut = vNormal_w;
    vec3 color = texture(uDiffuse, vTextureUV).rgb;
    AmbiantOut = color * 0.8f;
    DiffuseOut = color;
    SpecularOut = uSpecularScale * pow(texture(uSpecular, vTextureUV).r, uSpecularPow);
    EmissiveOut = DiffuseOut.r > 0.7f ? 0.7f : 0.05f;
  }
}
