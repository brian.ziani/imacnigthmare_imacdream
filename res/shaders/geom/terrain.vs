#version 300 es

precision mediump float;

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec3 aVertexTangent;
layout(location = 2) in vec3 aVertexBitangent;
layout(location = 3) in vec2 aVertexNormalUV;

uniform mat4 uMVPmatrix;

uniform int uMode;

out vec3 vPosition_w;
out vec3 vTangent_w;
out vec3 vBitangent_w;
out vec2 vNormalUV;

void main() {
  if (uMode == 0) {
    gl_Position = uMVPmatrix * vec4(aVertexPosition, 1.f);
    vPosition_w = aVertexPosition;
    vTangent_w = aVertexTangent;
    vBitangent_w = aVertexBitangent;
    vNormalUV = aVertexNormalUV;
  } else {
    gl_Position = uMVPmatrix * vec4(aVertexPosition, 1.f);
    gl_Position += vec4(0.f, -1.f, 0.f, 0.f);
  }
}