#version 300 es

precision mediump float;

layout(location = 0) in vec3 aVertexPosition;

uniform mat4 uMVPmatrix;
uniform vec3 uCenter;
uniform vec3 uTranslate;
uniform float uTime;

out vec3 vPosition_w;

void main() {
  vec3 position = aVertexPosition + uTranslate;
  float d = distance(uCenter, position);
  position.y = uTranslate.y / 4.f * pow(d, 0.4f) * cos(d * 0.1f);
  position.y += 3.f * cos(uTime + d * 0.1f);
  gl_Position = uMVPmatrix * vec4(position, 1.f);
  vPosition_w = position;
}