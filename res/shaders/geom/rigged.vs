#version 300 es

precision mediump float;

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec3 aVertexNormal;
layout(location = 2) in vec2 aVertexTextureUV;
layout(location = 3) in ivec4 aBoneIDs;
layout(location = 4) in vec4 aBoneWeights;

const int MAX_BONES = 128;

uniform mat4 uMmatrix;
uniform mat4 uNormalMatrix;
uniform mat4 uMVPmatrix;

uniform mat4 uBoneTransforms[MAX_BONES];

uniform int uMode;

out vec3 vPosition_w;
out vec3 vNormal_w;
out vec2 vTextureUV;

void main() {

  mat4 transform = uBoneTransforms[aBoneIDs[0]] * aBoneWeights[0]
                + uBoneTransforms[aBoneIDs[1]] * aBoneWeights[1]
                + uBoneTransforms[aBoneIDs[2]] * aBoneWeights[2]
                + uBoneTransforms[aBoneIDs[3]] * aBoneWeights[3];
  vec4 position_m = transform * vec4(aVertexPosition, 1.f);
  vec4 normal_m = transform * vec4(aVertexNormal, 0.0f);

  if (0 == uMode) {
    gl_Position = uMVPmatrix * position_m;
    vPosition_w = (uMmatrix * position_m).xyz;
    vNormal_w = (uNormalMatrix * normal_m).xyz;
    vTextureUV = aVertexTextureUV;
  } else {
    gl_Position = uMVPmatrix * (position_m * vec4(vec3(1.f), 1.f));
  }
}