#version 300 es

precision mediump float;

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec3 aVertexNormal;
layout(location = 2) in vec2 aVertexTextureUV;

uniform mat4 uMmatrix;
uniform mat4 uNormalMatrix;
uniform mat4 uMVPmatrix;

uniform int uMode;
uniform float uTime;

uniform int uAnimMode;
uniform float uSubTime;
uniform float uAnimTime;
uniform float uSize;
uniform float uAmplitude;

out vec3 vPosition_w;
out vec3 vNormal_w;
out vec2 vTextureUV;
out float emissiveRot;

void main() {
  if (0 == uMode) {    
    vec3 position = uSize * (aVertexPosition + uAmplitude * aVertexNormal * sin(uTime * 3.1415f * 2.f + float(gl_VertexID) / 150.f));
    position = position + uAmplitude * sin(uAnimTime * 3.1415f) * sin(aVertexNormal * 0.1f + float(gl_VertexID) / 150.f);
    gl_Position = uMVPmatrix * vec4(position, 1.f);
    vPosition_w = (uMmatrix * vec4(position, 1.f)).xyz;
    vNormal_w = normalize((uNormalMatrix * vec4(aVertexNormal, 0.0f)).xyz);
    vNormal_w.y = vNormal_w.y * (1.f - 0.2f * sin(uTime * 3.1415f * 2.f + float(gl_VertexID) / 300.f));
    emissiveRot = sin(uTime * 3.1415f * 2.f + float(gl_VertexID) / 300.f);
    vTextureUV = aVertexTextureUV;
  } else {
    gl_Position = uMVPmatrix * vec4(aVertexPosition * 1.f, 1.f);
  }
}