#version 300 es

precision mediump float;

in vec3 vPosition_w;

uniform vec3 uColor;

layout(location = 0) out vec3 WorldPosOut;
layout(location = 1) out vec3 NormalOut;
layout(location = 2) out vec3 AmbiantOut;
layout(location = 3) out vec3 DiffuseOut;
layout(location = 4) out float SpecularOut;
layout(location = 5) out float EmissiveOut;

void main() {
  WorldPosOut = vPosition_w;
  NormalOut = vec3(0.f, 1.f, 0.f);
  AmbiantOut = uColor;
  DiffuseOut = uColor;
  SpecularOut = 1.f;
  EmissiveOut = 0.5f;
}
