#version 300 es

precision mediump float;

in vec3 vPosition_w;
in vec3 vTangent_w;
in vec3 vBitangent_w;
in vec2 vNormalUV;

uniform sampler2D uNormalMap;
uniform vec3 uAmbiantColor;
uniform vec3 uDiffuseColor;
uniform float uShinyness;

uniform int uMode;

layout(location = 0) out vec3 WorldPosOut;
layout(location = 1) out vec3 NormalOut;
layout(location = 2) out vec3 AmbiantOut;
layout(location = 3) out vec3 DiffuseOut;
layout(location = 4) out float SpecularOut;
layout(location = 5) out float EmissiveOut;

vec3 sampleNormalMap() {
  return normalize(texture(uNormalMap, vNormalUV).xyz * 2.0f - vec3(1.f,1.f,1.f));
}

vec3 computeNormalVector() {
  vec3 baseNormal = cross(vTangent_w, vBitangent_w);
  vec3 bumpNormal = sampleNormalMap();
  mat3 basis = mat3(vTangent_w, baseNormal, vBitangent_w);
  return basis * bumpNormal;
}

void main() {
  if (0 == uMode) {
    WorldPosOut = vPosition_w;
    NormalOut = computeNormalVector();
    AmbiantOut = uAmbiantColor;
    DiffuseOut = uDiffuseColor;
    SpecularOut = uShinyness;
    EmissiveOut = 0.f;
  }
}
