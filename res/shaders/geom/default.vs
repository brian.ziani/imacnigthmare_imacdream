#version 300 es

precision mediump float;

layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec3 aVertexNormal;
layout(location = 2) in vec2 aVertexTextureUV;

uniform mat4 uMmatrix;
uniform mat4 uNormalMatrix;
uniform mat4 uMVPmatrix;

uniform int uMode;

out vec3 vPosition_w;
out vec3 vNormal_w;
out vec2 vTextureUV;

void main() {
  if (0 == uMode) {
    gl_Position = uMVPmatrix * vec4(aVertexPosition, 1.f);
    vPosition_w = (uMmatrix * vec4(aVertexPosition, 1.f)).xyz;
    vNormal_w = normalize((uNormalMatrix * vec4(aVertexNormal, 0.0f)).xyz);
    vTextureUV = aVertexTextureUV;
  } else {
    gl_Position = uMVPmatrix * vec4(aVertexPosition * 1.f, 1.f);
  }
}