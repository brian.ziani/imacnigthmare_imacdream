class Vector {
  private final float x;
  private final float y;
  private final float z;
  public Vector(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  public float x() {
    return x;
  }
  public float y() {
    return y;
  }
  public float z() {
    return z;
  }
  public Vector plus(Vector b) {
    return new Vector(x+b.x, y+b.y, z+b.z);
  }
  public Vector minus(Vector b) {
    return new Vector(x-b.x, y-b.y, z-b.z);
  }
  public Vector multiply(float f) {
    return new Vector(x*f, y*f, z*f);
  }
  public float dot(Vector b) {
    return x*b.x + y*b.y + z * b.z;
  }
  public Vector vec(Vector b) {
    return new Vector(y*b.z - z*b.y, z*b.x - x*b.z, x*b.y - y*b.x);
  }
}

class Matrix {
  public final Vector vals[];
  Matrix(float a, float b, float c, float d, float e, float f, float g, float h, float i) {
    vals = new Vector[3];
    vals[0] = new Vector(a, b, c);
    vals[1] = new Vector(d, e, f);
    vals[2] = new Vector(g, h, i);
  }
  Matrix(Vector a, Vector b, Vector c) {
    vals = new Vector[3];
    vals[0] = a;
    vals[1] = b;
    vals[2] = c;
  }
  Matrix plus(Matrix M) {
    return new Matrix(vals[0].plus(M.vals[0]), vals[1].plus(M.vals[1]), vals[2].plus(M.vals[2]));
  }
}

Vector mult(Matrix M, Vector V) {
  return new Vector(V.dot(M.vals[0]), V.dot(M.vals[1]), V.dot(M.vals[2]));
}

float norm(Vector u) {
  return sqrt(u.dot(u));
}
float dist(Vector u, Vector v) {
  return norm(u.minus(v));
}

Vector randVector(float min, float max) {
  return new Vector(random(min, max), random(min, max), random(min, max));
}

int cptr = 0;

void arrow(int x1, int y1, int z1, int x2, int y2, int z2) {
  line(x1, y1, z1, x2, y2, z2);
  pushMatrix();
  translate(x2, y2, z2);
  float a = atan2(x1-x2, y2-y1);
  rotate(a);
  line(0, 0, -10, -10);
  line(0, 0, 10, -10);
  popMatrix();
}
void varrow(Vector p, Vector v, float f) {
  //f = f*10;
  //arrow(int(p.x), int(p.y), int(p.z), int(p.x+v.x()*f), int(p.y+v.y()*f), int(p.z+v.z*f));
}

/* Paramètres physiques */

public final float kd = 0.5;
public final float kr = 0.15;
public final float alpha = -0.6;
public final float betha = 0.9;
public final float kt = 0.1;
public final Vector gravity = new Vector(0, 0.5, 0);


class Particle {
  public Vector pos;
  public Vector vel;
  public Vector acc;
  public final float radius;
  public final float mass;
  public final boolean physic;
  public final int hash;
  Particle(float mass, float radius, boolean s, Vector pos, Vector vel, Vector acc) {
    this.physic = s;
    this.mass = mass;
    this.radius = radius;
    this.pos = pos;
    this.vel = vel;
    this.acc = acc;
    this.hash = cptr++;
  }
  @Override
    public int hashCode() {
    return hash;
  }
  Vector computeForcesOn(ArrayList<Particle> list, float dt) {
    Vector forces = physic ? gravity.multiply(mass) : new Vector(0,0,0);
    stroke(255, 125, 0);
    varrow(pos, forces, 1);
    stroke(255, 0, 0);
    for (var u : list) {
      if (this == u) {
        continue;
      }
      float e = max(0, radius + u.radius - dist(u.pos, pos));
      //Contact c = contacts.get(new Key(u,p));
      if (e == 0) {
        //c.tc = 0;
        continue;
      }
      //c.tc += dt;
      Vector N = u.pos.minus(pos).multiply(1.0f / dist(u.pos, pos));
      Vector V = vel.minus(u.vel);
      float E = V.dot(N);
      Vector Vt = V.minus(N.multiply(E));
      //float epsilon = 0.9999;
      //float meff = (u.mass + p.mass) / (u.mass * p.mass);
      float fn = -kd * pow(e, alpha) * E - kr * pow(e, betha);
      Vector F = N.multiply(fn);
      varrow(pos, F, 1);
      Vector Ft = Vt.multiply(-kt*mass);
      forces = forces.plus(F).plus(Ft);
    }
    strokeWeight(3);
    stroke(255, 255, 255);
    varrow(pos, forces, 1);
    strokeWeight(1);
    return forces;
  }
}

class Body {
  public Vector pos;
  public Vector vel;
  public Vector acc;
  public Matrix rot;
  public Vector ang;
  public Vector forces;
  public final float mass;
  Body(float m, Vector pos, Vector vel, Vector acc, Matrix rot) {
    this.mass = m;
    this.pos = pos;
    this.vel = vel;
    this.acc = acc;
    this.rot = rot;
    this.ang = new Vector(0, 0, 0);
    this.forces = new Vector(0, 0, 0);
  }
}

class BodyParticle extends Particle {
  public final Body body;
  public final Vector offset;
  public Vector ang;
  BodyParticle(Body b, Vector off, float mass, float radius, boolean s, Vector vel, Vector acc) {
    super(mass, radius, s, new Vector(0, 0, 0).plus(off).plus(b.pos), vel, acc);
    this.body = b;
    this.offset = off;
  }
  Vector computeForcesOn(ArrayList<Particle> list, float dt) {
    Vector forces = physic ? gravity.multiply(mass) : new Vector(0,0,0);
    body.forces = body.forces.plus(forces);
    body.ang = body.ang.plus(pos.minus(body.pos).vec(forces));
    stroke(255, 125, 0);
    varrow(pos, forces, 1);
    stroke(255, 0, 0);
    for (var u : list) {
      if (this == u) {
        continue;
      }
      float e = max(0, radius + u.radius - dist(u.pos, pos));
      //Contact c = contacts.get(new Key(u,p));
      if (e == 0) {
        //c.tc = 0;
        continue;
      }
      if (u instanceof BodyParticle) {
        BodyParticle v = (BodyParticle) u;
        if (v.body == body) {
          continue;
        }
      }
      //c.tc += dt;
      Vector N = u.pos.minus(pos).multiply(1.0f / dist(u.pos, pos));
      Vector V = vel.minus(u.vel);
      float E = V.dot(N);
      Vector Vt = V.minus(N.multiply(E));
      //float epsilon = 0.9999;
      //float meff = (u.mass + p.mass) / (u.mass * p.mass);
      float fn = -kd * pow(e, alpha) * E - kr * pow(e, betha);
      Vector F = N.multiply(fn);
      varrow(pos, F, 1);
      Vector Ft = Vt.multiply(-kt);
      Vector Res = F.plus(Ft);
      forces = forces.plus(Res);
      body.forces = body.forces.plus(Res);
      body.ang = body.ang.plus(pos.plus(N.multiply(radius)).minus(body.pos).vec(Res));
    }
    {
      Vector P = mult(body.rot, pos.minus(body.pos.plus(offset)));
      forces = forces.plus(P.multiply(-0.1 * mass));
    }
    strokeWeight(3);
    stroke(255, 255, 255);
    varrow(pos, forces, 1);
    strokeWeight(1);
    return forces;
  }
}

class Key {
  private final int val;
  Key(Particle a, Particle b) {
    val = a.hash | (b.hash << 16);
  }
  public int hashCode() {
    return val;
  }
  public boolean equals(Object o) {
    Key k = (Key) o;
    return (o instanceof Key) && k.val == val;
  }
}
class Contact {
  public float tc;
  Contact() {
    tc = 0;
  }
}

ArrayList<Body> bodies;
ArrayList<Particle> particles;
HashMap<Key, Contact> contacts;
float prevt, curt;
float last;
int frames;

void createParticle(Particle p) {
  for (var u : particles) {
    Contact c = new Contact();
    contacts.put(new Key(p, u), c);
    contacts.put(new Key(u, p), c);
  }
  particles.add(p);
}

void CreateRectBody(int x, int y, int z, int w) {
  Body b = new Body(8*5, new Vector(x, y, z), new Vector(0, 0, 0), new Vector(0, 0, 0), new Matrix(1, 0, 0, 0, 1, 0, 0, 0, 1));
  bodies.add(b);
  float r = (w * 3)/4;
  createParticle(new BodyParticle(b, new Vector( 0.5, 0.5, 0.5).multiply(w), 5, r, true, new Vector(0, 0, 0), new Vector(0, 0, 0)));
  createParticle(new BodyParticle(b, new Vector(-0.5, 0.5, 0.5).multiply(w), 5, r, true, new Vector(0, 0, 0), new Vector(0, 0, 0)));
  createParticle(new BodyParticle(b, new Vector( 0.5, -0.5, 0.5).multiply(w), 5, r, true, new Vector(0, 0, 0), new Vector(0, 0, 0)));
  createParticle(new BodyParticle(b, new Vector( 0.5, 0.5, -0.5).multiply(w), 5, r, true, new Vector(0, 0, 0), new Vector(0, 0, 0)));

  createParticle(new BodyParticle(b, new Vector(-0.5, -0.5, 0.5).multiply(w), 5, r, true, new Vector(0, 0, 0), new Vector(0, 0, 0)));
  createParticle(new BodyParticle(b, new Vector( 0.5, -0.5, -0.5).multiply(w), 5, r, true, new Vector(0, 0, 0), new Vector(0, 0, 0)));
  createParticle(new BodyParticle(b, new Vector(-0.5, 0.5, -0.5).multiply(w), 5, r, true, new Vector(0, 0, 0), new Vector(0, 0, 0)));
  createParticle(new BodyParticle(b, new Vector(-0.5, -0.5, -0.5).multiply(w), 5, r, true, new Vector(0, 0, 0), new Vector(0, 0, 0)));
}

void createPlanBody() {
  int ww = 200;
  float mass = 1;
  int N = 2;
  int w = ww/N;
  int offy = 70;
  Body b = new Body(mass*N*N*4, new Vector(ww, ww+offy, ww/2), new Vector(0, 0, 0), new Vector(0, 0, 0), new Matrix(1, 0, 0, 0, 1, 0, 0, 0, 1));
  bodies.add(b);
  for (int x = w/2, i=0; i<N*2; x += w/2, i++) {
    for (int z = w/2, j=0; j<N*2; z += w/2, j++) {
      createParticle(new BodyParticle(b, new Vector(x, 0, z - ww/2), mass, w/2, true, new Vector(0, 0, 0), new Vector(0, 0, 0)));
    }
    //for (int y = (w*3)/4, j=0; j<1; y += w/2, j++) {
    //  createParticle(new Particle(1000, w/2, false, new Vector(x, height - y+offy, -width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    //  createParticle(new Particle(1000, w/2, false, new Vector(x, height- y+offy, width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    //  createParticle(new Particle(1000, w/2, false, new Vector(0, height- y+offy, x - width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    //  createParticle(new Particle(1000, w/2, false, new Vector(width, height- y+offy, x - width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    //}
  }
}

void createFloor() {
  float mass = 10000000;
  int N = 3;
  int w = width/N;
  int offy = 70;
  Body b = new Body(mass*N*N, new Vector(0, height+offy, 0), new Vector(0, 0, 0), new Vector(0, 0, 0), new Matrix(1, 0, 0, 0, 1, 0, 0, 0, 1));
  bodies.add(b);
  for (int x = w/2, i=0; i<N*2; x += w/2, i++) {
    for (int z = w/2, j=0; j<N*2; z += w/2, j++) {
      createParticle(new BodyParticle(b, new Vector(x, 0, z - width/2), mass, w/2, false, new Vector(0, 0, 0), new Vector(0, 0, 0)));
    }
    //for (int y = (w*3)/4, j=0; j<1; y += w/2, j++) {
    //  createParticle(new Particle(1000, w/2, false, new Vector(x, height - y+offy, -width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    //  createParticle(new Particle(1000, w/2, false, new Vector(x, height- y+offy, width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    //  createParticle(new Particle(1000, w/2, false, new Vector(0, height- y+offy, x - width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    //  createParticle(new Particle(1000, w/2, false, new Vector(width, height- y+offy, x - width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    //}
  }
}

void setup() {
  size(720, 720, P3D);
  frameRate(30);
  particles = new ArrayList<Particle>();
  contacts = new HashMap<Key, Contact>();
  bodies = new ArrayList<Body>();
  madded = false;
  prevt = millis();
  last = millis();
  createFloor();
  /*
  int N = 4;
  int w = width/N;
  int offy = 100;
  for (int x = w/2, i=0; i<N*2; x += w/2, i++) {
    for (int z = w/2, j=0; j<N*2; z += w/2, j++) {
      createParticle(new Particle(1000, w/2, false, new Vector(x, height+offy, z - width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    }
    for (int y = (w*3)/4, j=0; j<1; y += w/2, j++) {
      createParticle(new Particle(1000, w/2, false, new Vector(x, height - y+offy, -width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
      createParticle(new Particle(1000, w/2, false, new Vector(x, height- y+offy, width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
      createParticle(new Particle(1000, w/2, false, new Vector(0, height- y+offy, x - width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
      createParticle(new Particle(1000, w/2, false, new Vector(width, height- y+offy, x - width/2), new Vector(0, 0, 0), new Vector(0, 0, 0)));
    }
    //createParticle(new Particle(1000, w/2, false, new Vector(x, 0), new Vector(0, 0), new Vector(0, 0)));
    //createParticle(new Particle(1000, w/2, false, new Vector(x, height), new Vector(0, 0), new Vector(0, 0)));
    //createParticle(new Particle(1000, w/2, false, new Vector(0, x), new Vector(0, 0), new Vector(0, 0)));
    //createParticle(new Particle(1000, w/2, false, new Vector(width, x), new Vector(0, 0), new Vector(0, 0)));
  }
  */
}

boolean madded, kadded;

void draw() {
  background(0, 0, 0);
  if (mousePressed) {
    if (!madded) {
      createParticle(new Particle(5, 40, true, new Vector(mouseX, mouseY, random(-50, 50)), randVector(0, 0), randVector(0, 0)));
      //CreateRectBody(mouseX, mouseY, int(random(-50, 50)), 50);
      madded = true;
    }
  } else {
    madded = false;
  }
  if (keyPressed) {
    if (!kadded) {
      //createParticle(new Particle(5, 40, true, new Vector(mouseX, mouseY, random(-50, 50)), randVector(0, 0), randVector(0, 0)));
      CreateRectBody(mouseX, mouseY, int(random(-50, 50)), 50);
      //createPlanBody();
      kadded = true;
    }
  } else {
    kadded = false;
  }
  curt = millis();
  stroke(0, 0, 0);
  camera(mouseX, 0, (height/2) / tan(PI/6), width/2, height/2, 0, 0, 1, 0);
  directionalLight(255, 255, 255, 0, 1, 0);
  for (var p : particles) {
    float vel = norm(p.vel)*50;
    fill(vel/25, vel, 255);
    pushMatrix();
    translate(p.pos.x(), p.pos.y(), p.pos.z());
    noStroke();
    sphere(p.radius);
    popMatrix();
    //ellipse(p.pos.x(), p.pos.y(), p.radius*2, p.radius*2);
  }
  for (var b : bodies) {
    b.forces = new Vector(0, 0, 0);
    b.ang = new Vector(0, 0, 0);
  }
  computePhysics(particles, (curt-prevt) / 10);
  for (var b : bodies) {
    float dt = (curt-prevt) / 10;
    Vector acc = b.forces.multiply(1 / b.mass);
    b.vel = b.vel.plus(acc.multiply(dt));
    b.pos = b.pos.plus(b.vel.multiply(dt).plus(b.acc.multiply(0.5*dt*dt)));
    Vector w = b.ang.multiply(dt);
    //b.rot = b.rot.plus(new Matrix(0, -w.z, w.y,  w.z, 0, -w.x,   -w.y, w.x, 0));
    stroke(0, 255, 0);
    strokeWeight(3);
    varrow(b.pos, b.forces, 1);
    //println("(", b.ang.x, ",", b.ang.y, ",", b.ang.z, ")");
  }
  prevt = millis();
  if (prevt - last > 1000) {
    println("Particles count : ", particles.size());
    println(frameCount - frames);
    frames = frameCount;
    last = prevt;
  }
}

void computePhysics(ArrayList<Particle> list, float dt) {
  for (var p : list) {
    Vector acc = p.acc.plus(p.computeForcesOn(list, dt)).multiply(1 / p.mass);
    p.vel = p.vel.plus(acc.multiply(dt));
    p.pos = p.pos.plus(p.vel.multiply(dt).plus(p.acc.multiply(0.5*dt*dt)));
    stroke(0, 255, 0);
    varrow(p.pos, p.vel, 1);
  }
}
