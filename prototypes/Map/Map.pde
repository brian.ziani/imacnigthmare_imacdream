final int N = 128;
final float[][] Grid = new float[N][N];


void setup() {
  size(900, 900);
  frameRate(30);
  for (int i=0; i<N; ++i) {
    for (int j=0; j<N; ++j) {
      Grid[i][j] = random(0, 1);
    }
  }
}

boolean clickok = false;
void draw() {
  background(100);
  noStroke();
  int dx = width/N;
  int dy = height/N;
  for (int i=0; i<N; ++i) {
    for (int j=0; j<N; ++j) {
      fill(Grid[i][j] * 255);
      rect(dx*i, dy*j, dx, dy);
    }
  }
  if (mousePressed) {
    if (!clickok) {
      smooth();
      clickok = true;
    }
  } else {
    clickok = false;
  }
}

void smooth() {
  float[] runner = new float[3];
  float summ = 0;
  int index = 0;
  for (int i=0; i<N; ++i) {
    runner[0] = Grid[i][0];
    runner[1] = Grid[i][1];
    runner[2] = Grid[i][2];
    Grid[i][1] = summ = runner[0] + runner[1] + runner[2];
    for (int j=2; j<N-1; ++j) {
      summ -= runner[index];
      runner[index] = Grid[i][j+1];
      summ += runner[index];
      index = (index+1) % 3;
      Grid[i][j] = summ;
    }
  }
  for (int i=0; i<N; ++i) {
    for (int j=0; j<N; ++j) {
      Grid[i][j] /= 3;
    }
  }
  ///*
  for (int i=0; i<N; ++i) {
    runner[0] = Grid[0][i];
    runner[1] = Grid[1][i];
    runner[2] = Grid[2][i];
    index = 0;
    Grid[i][1] = summ = runner[0] + runner[1] + runner[2];
    for (int j=2; j<N-1; ++j) {
      summ -= runner[index];
      runner[index] = Grid[j+1][i];
      summ += runner[index];
      index = (index+1) % 3;
      Grid[j][i] = summ;
    }
  }
  //*/
  for (int i=0; i<N; ++i) {
    for (int j=0; j<N; ++j) {
      Grid[i][j] /= 3;
    }
  }
}
