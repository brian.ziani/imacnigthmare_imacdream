class Particle {
  public Vector pos;
  public Body body;
  public final float mass;
  public final float radius;
  public final int uid;

  Particle(int uid, Body body, Vector pos, float mass, float radius) {
    this.body = body;
    this.pos = pos;
    this.mass = mass;
    this.radius = radius;
    this.uid = uid;
  }
}

class Body {
  public HashMap<Integer, Particle> particles;
  public Vector position;
  public Vector velocity;
  
  public Vector force;
  public Vector repulsions;
  public Vector torque;

  public Vector angular;
  public Matrix rotation;

  public float imomentum;
  public float mass;
  public float imass;

  public boolean active;
  Body() {
    particles = new HashMap<Integer, Particle>();
    position = new Vector();
    velocity = new Vector();
    force = new Vector();
    repulsions = new Vector();
    torque = new Vector();
    angular = new Vector();
    rotation = new Matrix();
    imomentum = 0;
    mass = 0;
    imass = 0;
    active = true;
  }
  
  
  int size() {
    return particles.size();
  }

  void addParticle(Particle p) {
    particles.put(p.uid, p);
  }
  
  void removeParticle(Particle p) {
    particles.remove(p.uid);
    rebuild();
  }

  void rebuild() {
    mass = 0;
    position = new Vector();
    for (var id : particles.keySet()) {
      var p = particles.get(id);
      mass += p.mass;
      position = plus(position, mult(p.pos, p.mass));
    }
    if (mass != 0) {
      imass = 1/mass;
      position = mult(position, imass);
    } else {
      imass = 0;
    }
    setAngularSpeed(angular);
    force = new Vector();
    torque = new Vector();
  }

  void setAngularSpeed(Vector v) {
    angular = v;
    imomentum = 0;
    Vector u = normalise(angular);
    for (var id : particles.keySet()) {
      var p = particles.get(id);
      Vector OA = minus(p.pos, position);
      Vector OH = mult(u, dot(u, OA));
      Vector HA = minus(OA, OH);
      imomentum += p.mass * norm2(HA);
    }
    if (imomentum != 0) {
      imomentum = 1 / imomentum;
    }
  }

  void move(Vector v) {
    position = plus(position, v);
    for (var id : particles.keySet()) {
      var p = particles.get(id);
      //print(p.pos, " -> ");
      p.pos = plus(p.pos, v);
      //println(p.pos);
    }
  }
  void rotate(Vector w) {
    Vector u = normalise(w);
    Matrix Q = new Matrix(0, -u.z, u.y, u.z, 0, -u.x, -u.y, u.x, 0);
    Matrix I = new Matrix();
    Matrix Q2 = mult(Q, Q);
    float a = TAU;
    float theta = a * norm(w);
    Matrix R = plus(I, plus(mult(Q, sin(theta)), mult(Q2, 1 - cos(theta))));
    //println(R);
    rotation = mult(R, rotation);
    for (var id : particles.keySet()) {
      var p = particles.get(id);
      p.pos = plus(position, mult(R, minus(p.pos, position)));
    }
  }
}
