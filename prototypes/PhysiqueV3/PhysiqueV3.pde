
Engine engine;
float prevt, curt;
float last;
int frames;
boolean madded, kadded;

final float radius = 30;

void setup() {
  size(720, 900, P3D);
  frameRate(30);
  engine = new Engine();
  var body = engine.createBody();
  float ww = 1000;
  engine.createParticle(body, new Vector(0, 0, 0), 5, ww);
  body.rebuild();
  body.move(new Vector(width/2, height+ww, 0));
  body.active = false;
  engine.earth = body;
}

void draw() {
  /* begin tick */
  curt = millis();
  background(0, 0, 0);
  camera(mouseX, 0, (height/2) / tan(PI/6), width/2, height/2, 0, 0, 1, 0);
  directionalLight(255, 255, 255, 0, 1, 0);
  /* compute things */
  if (mousePressed) {
    if (!madded) {
      var body = engine.createBody();
      float w = 20;
      float mass = 10;
      engine.createParticle(body, new Vector(w, w, w), mass, radius);
      engine.createParticle(body, new Vector(w, -w, -w), mass, radius);
      engine.createParticle(body, new Vector(-w, w, -w), mass, radius);
      engine.createParticle(body, new Vector(-w, -w, w), mass, radius);
      body.rebuild();
      body.move(new Vector(mouseX, mouseY, 0));
      madded = true;
    }
  } else {
    madded = false;
  }
  if (keyPressed) {
    if (!kadded) {
      var body = engine.createBody();
      float w = 30;
      float mass = 10;
      engine.createParticle(body, new Vector(w, w, w), mass, radius);
      body.rebuild();
      body.move(new Vector(mouseX, mouseY, 0));
      kadded = true;
    }
  } else {
    kadded = false;
  }
  engine.computePhysics((curt-prevt) / 1000);
  /* draw */
  for (var body : engine.bodies) {
    fill(255, 0, 0);
    pushMatrix();
    translate(body.position.x, body.position.y, body.position.z);
    noStroke();
    sphere(10);
    popMatrix();
  }
  /* draw */
  for (var id : engine.particles.keySet()) {
    var p = engine.particles.get(id);
    fill(0, 255, 255);
    pushMatrix();
    translate(p.pos.x, p.pos.y, p.pos.z);
    noStroke();
    sphere(p.radius);
    popMatrix();
    //println(p.pos);
  }
  //println(" ");
  /* end */
  prevt = curt;
}
