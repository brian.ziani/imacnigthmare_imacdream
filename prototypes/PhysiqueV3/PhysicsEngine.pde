class Collision {
  public final Particle A;
  public final Particle B;

  Vector FA;
  Vector FB;
  Vector TA;
  Vector TB;

  Vector contactA;
  Vector contactB;

  Collision(Particle A, Particle B) {
    this.A = A;
    this.B = B;
  }
  @Override
    public boolean equals(Object o) {
    Collision c = (Collision) o;
    return (o instanceof Collision)
      && c.A.uid == A.uid
      && c.B.uid == B.uid;
  }
}

void arrow(int x1, int y1, int z1, int x2, int y2, int z2) {
  line(x1, y1, z1, x2, y2, z2);
  pushMatrix();
  translate(x2, y2, z2);
  float a = atan2(x1-x2, y2-y1);
  rotate(a);
  line(0, 0, -10, -10);
  line(0, 0, 10, -10);
  popMatrix();
}

void varrow(Vector p, Vector v, float f) {
  f = f*0.01;
  arrow(int(p.x), int(p.y), int(p.z), int(p.x+v.x*f), int(p.y+v.y*f), int(p.z+v.z*f));
}

class Engine {
  public final ArrayList<Body> bodies;
  public final HashMap<Integer, Particle> particles;
  public final ArrayList<Collision> activesCollisions;
  public final ArrayList<ArrayList<Collision>> collisionsHash;

  //private final Vector gravity = new Vector(0, 900, 0);
  private Body earth;
  private final float timeGranularity = 0.025;
  private float timeScale = 1;
  private final float collisionTimeScale = 0.001;
  private final float frictionMu = 0;
  private final int resolutionSubstep = 1;
  private final float energyRestitutionFactor = 0.05;
  private final float penetrationNonLinearity = 0;

  Engine() {
    bodies = new ArrayList<Body>();
    particles = new HashMap<Integer, Particle>();
    activesCollisions = new ArrayList<Collision>();
    collisionsHash = new ArrayList<ArrayList<Collision>>();
  }

  Body createBody() {
    Body b = new Body();
    bodies.add(b);
    return b;
  }

  Vector gravity(Vector p) {
    return mult(minus(earth.position, p), 1000000/norm2(minus(p, earth.position)));
  }

  void createParticle(Body body, Vector pos, float mass, float radius) {
    Particle p = new Particle(particles.size(), body, pos, mass, radius);
    collisionsHash.add(new ArrayList<Collision>(particles.size()+1));
    for (int i=0; i<particles.size(); ++i) {
      Collision c = new Collision(particles.get(i), p);
      collisionsHash.get(i).add(c);
      collisionsHash.get(p.uid).add(c);
    }
    collisionsHash.get(particles.size()).add(null);
    particles.put(p.uid, p);
    body.addParticle(p);
  }

  void computePhysics(float deltaTime) {
    /* update physical bodies */
    deltaTime *= timeScale;
    for (float dt=min(deltaTime, timeGranularity); 0 < deltaTime; deltaTime -= timeGranularity) {
      for (var body : bodies) {
        if (timeScale != 1) {
          println("");
        }
        strokeWeight(3);
        stroke(255, 255, 255);
        varrow(body.position, body.force, 1);
        stroke(255, 0, 255);
        varrow(body.position, body.torque, 1);
        strokeWeight(1);

        if (body.mass == 0 || ! body.active) {
          body.force = new Vector();
          body.repulsions = new Vector();
          body.torque = new Vector();
          body.velocity = new Vector();
          body.angular = new Vector();
          continue;
        }
        
        body.velocity = mult(body.velocity, 0.99);
        body.angular = mult(body.angular, 0.99);

        Vector acc = new Vector();
        if (norm(body.force) > 0.01) {
          acc = mult(body.force, body.imass);
          body.velocity = plus(body.velocity, mult(acc, dt));
        }
        if (timeScale != 1) {
          print("Acc : ", acc);
          print("  vel : ", body.velocity);
          println("  force : ", body.force);
        }
        if (norm(body.velocity) < 0.1) {
          body.velocity = new Vector();
        }
        body.move(plus(mult(body.velocity, dt), mult(acc, 0.5*dt*dt)));

        Vector angacc = new Vector();
        if (norm(body.torque) > 0.01) {
          angacc = mult(body.torque, body.imomentum);
          body.setAngularSpeed(plus(body.angular, mult(angacc, dt)));
        }
        if (timeScale != 1) {
          print("aAcc : ", angacc);
          print("  avel : ", body.angular);
          println("  torque : ", body.torque);
        }
        if (norm(body.angular) < 0.1) {
          body.angular = new Vector();
        }
        body.rotate(plus(mult(body.angular, dt), mult(angacc, 0.5*dt*dt)));

        body.force = mult(gravity(body.position), body.mass);
        body.repulsions = new Vector();
        body.torque = new Vector();
      }
      if (timeScale != 1) {
        println("");
      }
      /* update collisions statuses */
      for (int i=0; i<particles.size(); ++i) {
        for (int j=i+1; j<particles.size(); ++j) {
          var collision = collisionsHash.get(i).get(j);
          /* skip objects in the same body */
          if (collision.A.body == collision.B.body) {
            continue;
          }
          /* compute collision overlap */
          Vector AB = minus(collision.B.pos, collision.A.pos);
          float epsilon = max(0, collision.A.radius + collision.B.radius - norm(AB));
          /* check if collision is active */
          if (0 < epsilon) {
            /* compute contact point */
            Vector N = normalise(minus(collision.A.pos, collision.B.pos));
            Vector OaC = minus(plus(collision.A.pos, mult(opposite(N), collision.A.radius)), collision.A.body.position);
            Vector ObC = minus(plus(collision.B.pos, mult(N, collision.B.radius)), collision.B.body.position);
            Vector va = plus(collision.A.body.velocity, vect(collision.A.body.angular, OaC));
            Vector vb = plus(collision.B.body.velocity, vect(collision.B.body.angular, ObC));
            /* this collision is already active */
            if (!activesCollisions.contains(collision)) {
              activesCollisions.add(collision);
              collision.contactA = plus(collision.A.body.position, OaC);
              collision.contactB = plus(collision.B.body.position, ObC);
            } else {
              float k = 0.1*dt;
              float thrsh = 1000;
              Vector tmp = minus(plus(collision.A.body.position, OaC), collision.contactA);
              if (norm(tmp) > thrsh) {
                collision.contactA = plus(collision.contactA, mult(tmp, k));
              }
              tmp = minus(plus(collision.B.body.position, ObC), collision.contactB);
              if (norm(tmp) > thrsh) {
                collision.contactB = plus(collision.contactB, mult(tmp, k));
              }
            }
            /* update collision */
            Vector dV = minus(vb, va);
            float Vn = dot(dV, N);
            Vector Vt = minus(dV, mult(N, Vn));

            float meff = (collision.A.body.mass + collision.B.body.mass) * (collision.A.body.imass * collision.B.body.imass);
            float tc = 0.1;
            float e = 0.01;
            float kd = -2 * meff * log(e) / tc;
            float kr = meff * (log(e)*log(e) + PI*PI) / (tc*tc);
            float fn = -kd * pow(epsilon, 0.5) * Vn - kr * pow(epsilon, 1.5);
            Vector F = mult(N, fn);

            /*
            float kt = 10;
             float mu = 1;
             float fa = kt * norm(DA), fb = kt * norm(DB);
             float coulomb = mu * abs(fn);
             Vector Fta, Ftb;
             if (fa < coulomb) {
             Fta = mult(DA, -fa);// * (collision.B.mass));// * max(0, dot(normalise(gravity), N))));
             } else {
             Fta = mult(DA, -coulomb / norm(DA));
             }
             if (fb < coulomb) {
             Ftb = mult(DB, -fb);// * (collision.B.mass));// * max(0, dot(normalise(gravity), N))));
             } else {
             Ftb = mult(DB, -coulomb / norm(DB));
             }*/
             float kt = 100;
             float mu = 0;
             Vector DA = mult(minus(collision.contactA, plus(collision.A.body.position, OaC)), mu);
             Vector DB = mult(minus(collision.contactB, plus(collision.B.body.position, ObC)), mu);
             Vector Fta = mult(plus(opposite(va), DA), kt);
             Vector Ftb = mult(plus(opposite(vb), DB), kt);

            if (timeScale != 1) {
              println("OA : ", OaC, " OB : ", ObC, " wA : ", collision.A.body.angular, " wB : ", collision.B.body.angular);
              println("va : ", va, " vb : ", vb);
              println("dv : ", dV, " vn : ", Vn);
              println(epsilon, meff, kd, kr, fn);
              println("F : ", F, " Ft : ", Fta);
            }

            strokeWeight(1);
            stroke(255, 0, 0);
            varrow(collision.B.pos, F, 1);
            varrow(collision.A.pos, opposite(F), 1);
            stroke(0, 255, 0);
            varrow(collision.B.pos, Ftb, 1);
            varrow(collision.A.pos, Fta, 1);

            collision.FA = plus(opposite(F), Fta);
            collision.FB =  plus(F, Ftb);

            collision.TA = vect(OaC, collision.FA);
            collision.TB = vect(ObC, collision.FB);

            float breakTreshold = 1000 * (collision.A.body.mass + collision.B.body.mass);
            if (false && collision.A.body.active && collision.A.body.size() > 1 && norm(collision.FA) > breakTreshold) {
              var body = collision.A.body;
              body.removeParticle(collision.A);
              body.force = mult(gravity(body.position), body.mass);

              body = createBody();
              body.addParticle(collision.A);
              body.rebuild();
              body.force = mult(gravity(body.position), body.mass);
              collision.A.body = body;

              timeScale = collisionTimeScale;
            } else {
              collision.A.body.force = plus(collision.A.body.force, collision.FA);
              collision.A.body.torque = plus(collision.A.body.torque, collision.TA);
            }
            if (false && collision.B.body.active && collision.B.body.size() > 1 && norm(collision.FB) > breakTreshold) {
              var body = collision.B.body;
              body.removeParticle(collision.B);
              body.force = mult(gravity(body.position), body.mass);

              body = createBody();
              body.addParticle(collision.B);
              body.rebuild();
              body.force = mult(gravity(body.position), body.mass);
              collision.B.body = body;

              timeScale = collisionTimeScale;
            } else {
              collision.B.body.force = plus(collision.B.body.force, collision.FB);
              collision.B.body.torque = plus(collision.B.body.torque, collision.TB);
            }
          }
          /* else invalid collision */
          else {
            if (activesCollisions.contains(collision)) {
              float correction = -0.0;
              collision.B.body.force = plus(collision.B.body.force, mult(collision.FB, correction));
              collision.A.body.force = plus(collision.A.body.force, mult(collision.FA, correction));
              collision.A.body.torque = plus(collision.A.body.torque, mult(collision.TA, correction));
              collision.B.body.torque = plus(collision.B.body.torque, mult(collision.TB, correction));
            }
            activesCollisions.remove(collision);
          }
        } /* for j */
      } /* for i */
      /* update active collisions */
      for (var collision : activesCollisions) {
        for (int i=0; i<resolutionSubstep; ++i) {
        }
      }
    }
  }
}
