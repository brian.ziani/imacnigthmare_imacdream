class Vector {
  public final float x;
  public final float y;
  public final float z;
  Vector() {
    this(0, 0, 0);
  }
  Vector(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  String toString() {
    return "(" + x + "," + y + "," + z + ")";
  }
}

class Matrix {
  public final Vector a;
  public final Vector b;
  public final Vector c;
  Matrix() {
    this(1, 0, 0,  0, 1, 0,  0, 0, 1);
  }
  Matrix(float a, float b, float c, float d, float e, float f, float g, float h, float i) {
    this.a = new Vector(a, b, c);
    this.b = new Vector(d, e, f);
    this.c = new Vector(g, h, i);
  }
  Matrix(Vector a, Vector b, Vector c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }
  String toString() {
    return "" + a + b + c;
  }
  Matrix transpose() {
    return new Matrix(a.x, b.x, c.x, a.y, b.y, c.y, a.z, b.z, c.z);
  }
}

Vector mult(Vector a, float f) {
  return new Vector(a.x * f, a.y * f, a.z * f);
}
Vector plus(Vector a, Vector b) {
  return new Vector(a.x+b.x, a.y+b.y, a.z+b.z);
}
Vector minus(Vector a, Vector b) {
  return new Vector(a.x-b.x, a.y-b.y, a.z-b.z);
}
Vector opposite(Vector a) {
  return new Vector(-a.x, -a.y, -a.z);
}
Vector vect(Vector a, Vector b) {
  return new Vector(a.y * b.z - a.z * b.y,
    a.z * b.x - a.x * b.z,
    a.x * b.y - a.y * b.x);
}
float dot(Vector a, Vector b) {
  return a.x * b.x + a.y * b.y + a.z * b.z;
}
float norm2(Vector a) {
  return dot(a, a);
}
float norm(Vector a) {
  return sqrt(norm2(a));
}

Vector normalise(Vector v) {
 float n = norm(v);
 if (n == 0)  {
   return v;
 }
 return mult(v, 1 / norm(v));
}

Vector mult(Matrix M, Vector v) {
  return new Vector(dot(v, M.a), dot(v, M.b), dot(v, M.c));
}
Matrix plus(Matrix A, Matrix T) {
  return new Matrix(plus(A.a, T.a), plus(A.b, T.b), plus(A.c, T.c));
}

Matrix mult(Matrix A, Matrix B) {
  Matrix T = B.transpose();
  return new Matrix(dot(A.a, T.a), dot(A.a, T.b), dot(A.a, T.c),
    dot(A.b, T.a), dot(A.b, T.b), dot(A.b, T.c),
    dot(A.c, T.a), dot(A.c, T.b), dot(A.c, T.c));
}

Matrix mult(Matrix A, float f) {
return new Matrix(mult(A.a, f), mult(A.b, f), mult(A.c, f));
}
