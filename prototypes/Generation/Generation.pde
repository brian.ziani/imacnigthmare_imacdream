HScrollbar iterationScroll;
HScrollbar lengthScroll;
HScrollbar smallLengthScroll;
HScrollbar smallAngleScroll;
HScrollbar angleScroll;
LSystem pattern;

void setup() {
  size(900, 900);
  frameRate(30);

  smallAngleScroll = new HScrollbar(0, height-150, width, 30, 1);
  smallLengthScroll = new HScrollbar(0, height-120, width, 30, 1);
  angleScroll = new HScrollbar(0, height-90, width, 30, 1);
  lengthScroll = new HScrollbar(0, height-60, width, 30, 1);
  iterationScroll = new HScrollbar(0, height-30, width, 30, 1);
  pattern = new LSystem();
  pattern.rules.put('T', "T");
  pattern.rules.put('N', "N");
  pattern.rules.put('L', "NTNTLFKTK");
  pattern.rules.put('K', "NTLFK");
  pattern.rules.put('F', "F");
}

void draw() {
  background(100);
  fill(255);

  smallAngleScroll.update();
  smallAngleScroll.display();
  smallLengthScroll.update();
  smallLengthScroll.display();
  angleScroll.update();
  angleScroll.display();
  lengthScroll.update();
  lengthScroll.display();
  iterationScroll.update();
  iterationScroll.display();

  stroke(100, 255, 0);
  var sequency = pattern.iterate("TL", int(iterationScroll.value(0.0, 10.0)));
  drawStep(width/2, height-150, 0, 0, sequency);
}

int drawStep(float x, float y, float r, int index, String sequency) {
  if (index < sequency.length()) {
    switch(sequency.charAt(index)) {
    case 'T' :
      {
        float l = lengthScroll.value(0.0, 100.0);
        float nx = x + l*sin(r);
        float ny = y - l*cos(r);
        stroke(100, 255, 0);
        fill(255, 0, 0);
        line(x, y, nx, ny);
        index = drawStep(nx, ny, r, index+1, sequency);
        break;
      }
    case 'F' :
      {
        float l = smallLengthScroll.value(0.0, 100.0);
        float nx = x + l*sin(r);
        float ny = y - l*cos(r);
        stroke(100, 255, 0);
        fill(255, 255, 0);
        line(x, y, nx, ny);
        index = drawStep(nx, ny, r, index+1, sequency);
        break;
      }
    case 'N' :
      {
        stroke(100, 255, 0);
        circle(x, y, 5);
        float dr = angleScroll.value(0, PI/2);
        index = drawStep(x, y, r+dr, index+1, sequency);
        dr = smallAngleScroll.value(0, PI/2);
        index = drawStep(x, y, r-dr, index+1, sequency);
        break;
      }
    case 'L' :
      {
        stroke(255, 255, 0);
        fill(255, 0, 0);
        circle(x, y, 20);
        break;
      }
    case 'K' :
      {
        stroke(255, 255, 0);
        fill(255, 255, 0);
        circle(x, y, 20);
        break;
      }
    }
  }
  return index;
}
