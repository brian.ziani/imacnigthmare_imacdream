class LSystem {
   private final ArrayList<Character> alphabet;
   private final HashMap<Character,String> rules;
   LSystem() {
     this.alphabet = new ArrayList<Character>();
     this.rules = new HashMap<Character,String>();
   }
   String step(String axiom) {
      StringBuilder builder = new StringBuilder();
      for (char c : axiom.toCharArray()) {
         builder.append(rules.getOrDefault(c, "")); 
      }
      return builder.toString();
   }
   String iterate(String axiom, int N) {
      for ( ; 0 < N ; --N) {
        axiom = step(axiom);
      }
      return axiom;
   }
}

class StochasticLSystem {
  
}
