class Collision {
  public final Particle A;
  public final Particle B;

  Vector AB;
  Vector BA;
  Vector dirBA;
  Vector dirAB;
  Vector tanAB;
  Vector tanBA;

  Vector torqueA;
  Vector torqueB;

  Vector frictionA;
  Vector frictionB;

  float baseEnergy;

  Collision(Particle A, Particle B) {
    this.A = A;
    this.B = B;
  }
  @Override
    public boolean equals(Object o) {
    Collision c = (Collision) o;
    return (o instanceof Collision)
      && c.A.uid == A.uid
      && c.B.uid == B.uid;
  }
}

class Engine {
  public final ArrayList<Body> bodies;
  public final HashMap<Integer, Particle> particles;
  public final ArrayList<Collision> activesCollisions;
  public final ArrayList<ArrayList<Collision>> collisionsHash;

  private final Vector gravity = new Vector(0, 300, 0);
  private final float timeGranularity = 1;
  private final float collisionDamp = 0.1;
  private final float frictionMu = 0.9;
  private final int resolutionSubstep = 10;

  Engine() {
    bodies = new ArrayList<Body>();
    particles = new HashMap<Integer, Particle>();
    activesCollisions = new ArrayList<Collision>();
    collisionsHash = new ArrayList<ArrayList<Collision>>();
  }

  Body createBody() {
    Body b = new Body();
    bodies.add(b);
    return b;
  }

  void createParticle(Body body, Vector pos, float mass, float radius) {
    Particle p = new Particle(particles.size(), body, pos, mass, radius);
    collisionsHash.add(new ArrayList<Collision>(particles.size()+1));
    for (int i=0; i<particles.size(); ++i) {
      Collision c = new Collision(particles.get(i), p);
      collisionsHash.get(i).add(c);
      collisionsHash.get(p.uid).add(c);
    }
    collisionsHash.get(particles.size()).add(null);
    particles.put(p.uid, p);
    body.addParticle(p);
  }

  void computePhysics(float dt) {
    dt *= timeGranularity;
    /* update physical bodies */
    for (var body : bodies) {
      if (body.mass == 0 || ! body.active) {
        continue;
      }
      body.forces = plus(body.forces, mult(gravity, dt * body.mass));
      body.velocity = plus(body.velocity, mult(body.forces, 1 / body.mass));
      body.forces = new Vector();
    }
    /* update collisions statuses */
    for (int i=0; i<particles.size(); ++i) {
      for (int j=i+1; j<particles.size(); ++j) {
        var collision = collisionsHash.get(i).get(j);
        /* skip objects in the same body */
        if (collision.A.body == collision.B.body) {
          continue;
        }
        /* compute collision overlap */
        collision.BA = minus(collision.A.pos, collision.B.pos);
        collision.AB = minus(collision.B.pos, collision.A.pos);
        collision.dirBA = normalise(collision.BA);
        collision.dirAB = normalise(collision.AB);
        float epsilon = max(0, collision.A.radius + collision.B.radius - norm(collision.BA));
        /* check if collision is active */
        if (0 < epsilon) {
          /* this collision is already active */
          if (!activesCollisions.contains(collision)) {
            activesCollisions.add(collision);
          }
          /* update collision */
          Vector OaC = minus(minus(collision.A.pos, mult(collision.dirBA, collision.A.radius)), collision.A.body.position);
          Vector ObC = minus(plus(collision.B.pos, mult(collision.dirBA, collision.B.radius)), collision.B.body.position);
          collision.torqueA = vect(OaC, collision.dirBA);
          collision.torqueB = mult(vect(ObC, collision.dirBA), -1);

          Vector va = plus(collision.A.body.velocity, vect(collision.A.body.angular, minus(collision.A.body.position, OaC)));
          Vector vb = plus(collision.B.body.velocity, vect(collision.B.body.angular, minus(collision.B.body.position, ObC)));
          Vector deltaV = normalise(minus(vb, va));
          Vector tmp = minus(deltaV, mult(deltaV, dot(deltaV, collision.dirBA)));
          collision.tanBA = mult(normalise(tmp), norm(collision.dirBA));
          collision.tanAB = mult(normalise(tmp), norm(collision.dirAB));
          collision.frictionA = vect(OaC, collision.tanBA);
          collision.frictionB = vect(ObC, collision.tanAB);

          collision.baseEnergy = max(0, dot(mult(normalise(gravity), epsilon), collision.dirBA)) * collisionDamp;
        }
        /* else invalid collision */
        else {
          activesCollisions.remove(collision);
        }
      } /* for j */
    } /* for i */
    /* update active collisions */
    for (var collision : activesCollisions) {
      //println("_");
      float energy = 0;
      float storedEnergy = 0;
      for (int i=0; i<resolutionSubstep; ++i) {
        /* Normal forces */
        float e = -(dot(collision.A.body.velocity, collision.dirBA)
          + dot(collision.B.body.velocity, collision.dirAB)
          + dot(collision.A.body.angular, collision.torqueA)
          + dot(collision.B.body.angular, collision.torqueB)
          + collision.baseEnergy)
          / ( collision.A.body.imass * norm2(collision.dirBA)
          + collision.A.body.imomentum * norm2(collision.torqueA)
          + collision.B.body.imass * norm2(collision.dirAB)
          + collision.B.body.imomentum * norm2(collision.torqueB));
        if (storedEnergy + e < 0) {
          e = -storedEnergy;
        }
        storedEnergy += e;
        energy = e;
        collision.A.body.velocity = plus(collision.A.body.velocity, mult(mult(collision.dirBA, energy), collision.A.body.imass * int(collision.A.body.active)));
        collision.B.body.velocity = plus(collision.B.body.velocity, mult(mult(collision.dirAB, energy), collision.B.body.imass * int(collision.B.body.active)));
        collision.A.body.angular = plus(collision.A.body.angular, mult(mult(collision.torqueA, energy), collision.A.body.imomentum * int(collision.A.body.active)));
        collision.B.body.angular = plus(collision.B.body.angular, mult(mult(collision.torqueB, energy), collision.B.body.imomentum * int(collision.B.body.active)));
        
        /* Friction Forces */
        float tanEnergy = -(dot(collision.A.body.velocity, collision.tanBA)
          + dot(collision.B.body.velocity, collision.tanAB)
          + dot(collision.A.body.angular, collision.frictionA)
          + dot(collision.B.body.angular, collision.frictionB))
          / ( collision.A.body.imass * norm2(collision.tanBA)
          + collision.A.body.imomentum * norm2(collision.frictionA)
          + collision.B.body.imass * norm2(collision.tanAB)
          + collision.B.body.imomentum * norm2(collision.frictionB));
        tanEnergy = constrain(tanEnergy, -frictionMu * energy, frictionMu * energy);
        collision.A.body.velocity = plus(collision.A.body.velocity, mult(mult(collision.tanBA, tanEnergy), collision.A.body.imass * int(collision.A.body.active)));
        collision.B.body.velocity = plus(collision.B.body.velocity, mult(mult(collision.tanAB, tanEnergy), collision.B.body.imass * int(collision.B.body.active)));
        collision.A.body.angular = plus(collision.A.body.angular, mult(mult(collision.frictionA, tanEnergy), collision.A.body.imomentum * int(collision.A.body.active)));
        collision.B.body.angular = plus(collision.B.body.angular, mult(mult(collision.frictionB, tanEnergy), collision.B.body.imomentum * int(collision.B.body.active)));
      }
    }
    /* update objects positions */
    for (var body : bodies) {
      if (body.active) {
        body.move(mult(body.velocity, dt));
        body.rotate(mult(body.angular, dt));
      } else {
        body.velocity = new Vector();
        body.angular = new Vector();
      }
    }
  }
}
