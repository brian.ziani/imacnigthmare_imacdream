
Engine engine;
float prevt, curt;
float last;
int frames;
boolean madded, kadded;

final float radius = 30;

void setup() {
  size(720, 720, P3D);
  frameRate(30);
  engine = new Engine();
  var body = engine.createBody();
  float ww = width;
  float mass = 1;
  int N = 4;
  float w = ww/N;
  for (float x = -ww/2, i=0; i<N*2; x += w/2, i++) {
    for (float z = -ww/2, j=0; j<N*2; z += w/2, j++) {
      engine.createParticle(body, new Vector(x, 0, z), 5, w);
    }
  }
  body.rebuild();
  body.move(new Vector(width/2, width, 0));
  body.active = false;
}

void draw() {
  /* begin tick */
  curt = millis();
  background(0, 0, 0);
  camera(mouseX, 0, (height/2) / tan(PI/6), width/2, height/2, 0, 0, 1, 0);
  directionalLight(255, 255, 255, 0, 1, 0);
  /* compute things */
  if (mousePressed) {
    if (!madded) {
      var body = engine.createBody();
      float w = 30;
      engine.createParticle(body, new Vector(w, 0, w), 5, radius);
      engine.createParticle(body, new Vector(-w, 0, w), 5, radius);
      engine.createParticle(body, new Vector(w, 0, -w), 5, radius);
      engine.createParticle(body, new Vector(-w, 0, -w), 5, radius);
      body.rebuild();
      body.move(new Vector(mouseX, mouseY, 0));
      madded = true;
    }
  } else {
    madded = false;
  }
  engine.computePhysics((curt-prevt) / 1000);
  /* draw */
  for (var body : engine.bodies) {
    fill(255, 0, 0);
    pushMatrix();
    translate(body.position.x, body.position.y, body.position.z);
    noStroke();
    sphere(10);
    popMatrix();
  }
  /* draw */
  for (var id : engine.particles.keySet()) {
    var p = engine.particles.get(id);
    fill(0, 255, 255);
    pushMatrix();
    translate(p.pos.x, p.pos.y, p.pos.z);
    noStroke();
    sphere(p.radius);
    popMatrix();
    //println(p.pos);
  }
  //println(" ");
  /* end */
  prevt = curt;
}
