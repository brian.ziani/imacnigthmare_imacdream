
class Vector {
  private final float x;
  private final float y;
  public Vector(float x, float y) {
    this.x = x;
    this.y = y;
  }
  public float x() {
    return x;
  }
  public float y() {
    return y;
  }
  public Vector plus(Vector b) {
    return new Vector(x+b.x, y+b.y);
  }
  public Vector minus(Vector b) {
    return new Vector(x-b.x, y-b.y);
  }
  public Vector multiply(float f) {
    return new Vector(x*f, y*f);
  }
  public float dot(Vector b) {
    return x*b.x + y*b.y;
  }
}

float norm(Vector u) {
  return sqrt(u.dot(u));
}
float dist(Vector u, Vector v) {
  return norm(u.minus(v));
}

Vector randVector(float min, float max) {
  return new Vector(random(min, max), random(min, max));
}

int cptr = 0;

class Particle {
  public Vector pos;
  public Vector vel;
  public Vector acc;
  public final float radius;
  public final float mass;
  public final boolean physic;
  public final int hash;
  Particle(float mass, float radius, boolean s, Vector pos, Vector vel, Vector acc) {
    this.physic = s;
    this.mass = mass;
    this.radius = radius;
    this.pos = pos;
    this.vel = vel;
    this.acc = acc;
    this.hash = cptr++;
  }
  @Override
    public int hashCode() {
    return hash;
  }
}

class Key {
  private final int val;
  Key(Particle a, Particle b) {
    val = a.hash | (b.hash << 16);
  }
  public int hashCode() {
    return val;
  }
  public boolean equals(Object o) {
    Key k = (Key) o;
    return (o instanceof Key) && k.val == val;
  }
}
class Contact {
  public float tc;
  Contact() {
    tc = 0;
  }
}

ArrayList<Particle> particles;
HashMap<Key, Contact> contacts;
boolean added;
float prevt, curt;
float last;
int frames;
Vector gravity;

void createParticle(Particle p) {
  for (var u : particles) {
    Contact c = new Contact();
    contacts.put(new Key(p, u), c);
    contacts.put(new Key(u, p), c);
  }
  particles.add(p);
}

void setup() {
  size(720, 720, P3D);
  frameRate(30);
  particles = new ArrayList<Particle>();
  contacts = new HashMap<Key, Contact>();
  added = false;
  prevt = millis();
  last = millis();
  gravity = new Vector(0, 50);
  int N = 5;
  int w = width/N;
  for (int x = w/2, i=0; i<N*2; x += w/2, i++) {
    createParticle(new Particle(1000, w/2, false, new Vector(x, 0), new Vector(0, 0), new Vector(0, 0)));
    createParticle(new Particle(1000, w/2, false, new Vector(x, height), new Vector(0, 0), new Vector(0, 0)));
    createParticle(new Particle(1000, w/2, false, new Vector(0, x), new Vector(0, 0), new Vector(0, 0)));
    createParticle(new Particle(1000, w/2, false, new Vector(width, x), new Vector(0, 0), new Vector(0, 0)));
  }
}

void draw() {
  background(0, 0, 0);
  if (mousePressed) {
    if (!added) {
      createParticle(new Particle(random(0.5, 10), random(10, 50), true, new Vector(mouseX, mouseY), randVector(0, 0), randVector(0, 0)));
      added = true;
    }
  } else {
    added = false;
  }
  curt = millis();
  stroke(0, 0, 0);
  for (var p : particles) {
    float vel = norm(p.vel)*10;
    fill(vel/255, vel, 255);
    pushMatrix();
    translate(p.pos.x(), p.pos.y(), 0);
    sphere(p.radius);
    popMatrix();
    //ellipse(p.pos.x(), p.pos.y(), p.radius*2, p.radius*2);
  }
  computePhysics(particles, (curt-prevt) / 1000);
  prevt = millis();
  if (prevt - last > 1000) {
    println(frameCount - frames);
    frames = frameCount;
    last = prevt;
  }
}

void arrow(int x1, int y1, int x2, int y2) {
  line(x1, y1, x2, y2);
  pushMatrix();
  translate(x2, y2);
  float a = atan2(x1-x2, y2-y1);
  rotate(a);
  line(0, 0, -10, -10);
  line(0, 0, 10, -10);
  popMatrix();
}
void varrow(Vector p, Vector v, float f) {
  arrow(int(p.x), int(p.y), int(p.x+v.x()*f), int(p.y+v.y()*f));
}

Vector computeForcesOn(Particle p, ArrayList<Particle> list, float dt) {
  Vector forces = gravity.multiply(p.mass);
  stroke(255, 125, 0);
  varrow(p.pos, forces, 1);
  stroke(255, 0, 0);
  for (var u : list) {
    if (p == u) {
      continue;
    }
    /* compute contact situation */
    float e = max(0, p.radius + u.radius - dist(u.pos, p.pos));
    //Contact c = contacts.get(new Key(u,p));
    if (e == 0) {
      //c.tc = 0;
      continue;
    }
    //c.tc += dt;
    Vector N = u.pos.minus(p.pos).multiply(1.0f / dist(u.pos, p.pos));
    Vector V = p.vel.minus(u.vel);
    float E = V.dot(N);
    Vector Vt = V.minus(N.multiply(E));
    float epsilon = 0.9999;
    float meff = (u.mass + p.mass) / (u.mass * p.mass);
    float kd = 3;//2 * meff * -log(epsilon) / c.tc;
    float kr = 15;//meff * (log(epsilon) * log(epsilon) + PI*PI) / (c.tc * c.tc);
    float alpha = 0.7;
    float betha = 1.5;
    float fn = -kd * pow(e, alpha) * E - kr * pow(e, betha);
    Vector F = N.multiply(fn);
    varrow(p.pos, F, 1);
    float mu = 0.1;
    forces = forces.plus(F);
  }
  strokeWeight(3);
  stroke(255, 255, 255);
  varrow(p.pos, forces, 1);
  strokeWeight(1);
  return forces;
}

void computePhysics(ArrayList<Particle> list, float dt) {
  for (var p : list) {
    if (p.physic) {
      Vector acc = p.acc.plus(computeForcesOn(p, list, dt)).multiply(1 / p.mass);
      p.vel = p.vel.plus(acc.multiply(dt));
      p.pos = p.pos.plus(p.vel.multiply(dt).plus(p.acc.multiply(0.5*dt*dt)));
      stroke(0, 255, 0);
      varrow(p.pos, p.vel, 1);
    }
  }
}
