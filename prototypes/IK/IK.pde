
Vector control;
Vector root;

ArrayList<Vector> chain;
ArrayList<Vector> positions;

void drawVec(Vector v) {
    fill(255, 0, 0);
    circle(v.x, v.y, 30);
}
void drawChain() {
  for (int i=0 ; i<positions.size() ; ++i) {
    drawVec(positions.get(i));
  }
}

float chainlength() {
float l = 0;
  for (int i=0 ; i<chain.size() ; ++i) {
    l += norm(chain.get(i));
  }
  return l;
}

void setup() {
  size(900, 900);
  frameRate(30);
  root = new Vector(width/2, height/2, 0);
  chain = new ArrayList<Vector>();
  chain.add(new Vector(50, 50, 0));
  chain.add(new Vector(50, 50, 0));
  positions = new ArrayList<Vector>();
  Vector tmp = root;
  positions.add(tmp);
  for (int i=0 ; i<chain.size() ; ++i) {
    tmp = plus(tmp, chain.get(i));
    positions.add(tmp);
  }
}

void draw() {
  background(100);
  fill(255);
  stroke(100, 255, 0);
  control = new Vector(mouseX, mouseY, 0);
  solveIK();
  drawChain();
  drawVec(control);
}

void solveIK() {
  Vector V = minus(control, root);
  if (chainlength() < norm(V)) {
    makeLine(normalise(V));
    //constrain();
  } else {
     for (int i=0 ; i<5 ; ++i) {
       backward();
       forward();
        //constrain();
     }
  }
}

void constrain() {
  for (int i=0 ; i<chain.size()-1 ; ++i) {
    Vector v0 = normalise(chain.get(i));
    Vector v1 = normalise(chain.get(i+1)); 
    Vector w = vect(v0, v1);
    float d = dot(v0, v1);
    if (d < 0) {
       w = mult(normalise(w), 0.25f);
       Matrix R = rotationMatrix(w);
       v1 = mult(mult(R, v0), norm(chain.get(i+1)));
       chain.set(i+1, v1);
    }
  }
  Vector tmp = root;
  positions.set(0, tmp);
  for (int i=0 ; i<chain.size() ; ++i) {
    tmp = plus(tmp, chain.get(i));
    positions.set(i+1, tmp);
  }
}

void makeLine(Vector dir) {
  Vector tmp = root;
  for (int i=0 ; i<chain.size() ; ++i) {
    chain.set(i, mult(dir, norm(chain.get(i))));
    tmp = plus(chain.get(i), tmp);
    positions.set(i+1, tmp);
  }
}

void forward() {
  Vector tmp = root;
  positions.set(0, tmp);
  for (int i=0 ; i < chain.size() ; ++i) {
    Vector v = mult(normalise(minus(positions.get(i+1), tmp)), norm(chain.get(i)));
    tmp = plus(tmp, v);
    positions.set(i+1, tmp);
  }
  for (int i=0 ; i<chain.size() ; ++i) {
     chain.set(i, minus(positions.get(i+1), positions.get(i))); 
  }
}

void backward() {
  Vector tmp = control;
  positions.set(chain.size(), tmp);
  for (int i=chain.size()-1 ; 0 <= i ; --i) {
    Vector v = mult(normalise(minus(positions.get(i), tmp)), norm(chain.get(i)));
    tmp = plus(tmp, v);
    positions.set(i, tmp);
  }
  for (int i=0 ; i<chain.size() ; ++i) {
     chain.set(i, minus(positions.get(i+1), positions.get(i))); 
  }
}
