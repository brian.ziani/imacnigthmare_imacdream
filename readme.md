## IMAC Dream
# Requirements
- gcc >= 9.3 (c++20)
- OpenGL 3.3
- GLEW
- glm
- Assimp
- SDL2, SDL2_Mixer, SDL2_image
# Compilation
From project's root :
- mkdir build && cd build
- cmake -DCMAKE_BUILD_TYPE=Release ../
- make -j4 main *(or -j6, -j8, -j16 depending on the machine capacity)*
- ./main

**"main"** can be replaced by **"demo"** in previous comands to compile a version with a small test structure near spawn
# Settings
Settings are available in directory **"res/conf/"**
Window size is editable at **"res/conf/global/Hard"** → **GUI_WINDOW_WIDTH** and **GUI_WINDOW_HEIGHT**
# Controls
| Control | Keyboard | Controller |
| --- | --- | --- |
| Move | ZQSD | Left Joystick |
| Camera | AE RF | Right Joystick |
| Jump | Space | A |
| Fly | Space (in air) | Y (in air) |
| Grab | LShift | B |
| Reload Config | F3 | - |
| Dump GBuffer | F1 | - |
# Documentation
Documentation is available for generation with doxygen, From project root :
- doxygen doxyconfig

It will be generated in **"doc/html"**