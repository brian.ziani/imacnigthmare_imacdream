/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Sand.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-24
 ///

#ifndef _SRC_IMPL_PARTICLES_SAND_H
#define _SRC_IMPL_PARTICLES_SAND_H

#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <vector>

namespace particles {
  struct Sand {
    glm::vec3 velocity;
  };
  struct Vertex {
    glm::vec3 position;
    float size;
  };

  struct System {
    std::vector<Sand> particles;
    std::vector<Vertex> vertices;
    GLuint vbo;
    GLuint vao;

    System(int particlesCount = 0) noexcept;
    ~System() noexcept;
    System(System&& m) noexcept;
    System& operator= (System&& m) noexcept;
    // Delete copy to prevent bad destruction of buffers
    System(const System&) = delete;
    System& operator= (const System&) = delete;

    /// \brief Used to manually fill a mesh, and built it afterwards
    void build() noexcept;
    /// \brief Send updated buffer to the GPU
    void send() noexcept;
  };
}

#endif /* _SRC_IMPL_PARTICLES_SAND_H */