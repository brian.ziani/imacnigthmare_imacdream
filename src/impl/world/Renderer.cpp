/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Renderer.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-11
 ///

#include "Renderer.hpp"
#include <libs/utils/log.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <future>
#include <vector>

namespace world {

  Renderer::Renderer(
    const std::shared_ptr<World>& w,
    const conf::SettingsPtr& settings) noexcept :
    _terrainPgm(nullptr), _world(w),
    _settings(settings),
    _visibleSpan(w)
  {
  }

  void Renderer::loadTerrainProgram(const std::string& vs, const std::string& fs) noexcept
  {
    _terrainPgm = std::make_shared<TerrainProgram>(_settings, vs, fs);
  }

  void Renderer::renderTerrain(const glm::mat4& VP) const noexcept
  {
    renderTerrain(VP, _visibleSpan);
  }
  void Renderer::renderTerrain(const glm::mat4& VP, const ChunkSpan& span) const noexcept
  {
    _terrainPgm->bind(VP);
    for (const auto& chunk : span) {
      chunk.mesh.draw();
    }
    _terrainPgm->unbind();
  }

  void Renderer::updateVisibleSpan(
    const glm::vec3& playerPosition,
    const glm::vec3 cameraFrontAxis,
    float)
    noexcept
  {
    Chunk::ID center(chunkAt(playerPosition, *_settings));
    glm::vec2 front(cameraFrontAxis.x, cameraFrontAxis.z);
    float angle = glm::orientedAngle(front, glm::vec2(1.f, 0.f));
    angle = angle < 0 ? 2.f * float(M_PI) + angle : angle;
    int viewDistance = _settings->Soft.World.Chunk.preloadDistance / 2;
    Chunk::ID dirOffsetA;
    Chunk::ID dirOffsetB;
    if (M_PI_4 < angle && angle <= 3.f * M_PI_4) {
      dirOffsetA = Chunk::ID(-1, -1) * viewDistance;
      dirOffsetB = Chunk::ID(+1, 0) * viewDistance;
      dirOffsetB.y = +2;
    }
    else if (3.f * M_PI_4 < angle && angle < 5.f * M_PI_4) {
      dirOffsetA = Chunk::ID(-1, +1) * viewDistance;
      dirOffsetB = Chunk::ID(0, -1) * viewDistance;
      dirOffsetB.x = +2;
    }
    else if (5.f * M_PI_4 < angle && angle < 7.f * M_PI_4) {
      dirOffsetA = Chunk::ID(+1, +1) * viewDistance;
      dirOffsetB = Chunk::ID(-1, 0) * viewDistance;
      dirOffsetB.y = -2;
    }
    else {
      dirOffsetA = Chunk::ID(+1, -1) * viewDistance;
      dirOffsetB = Chunk::ID(0, +1) * viewDistance;
      dirOffsetB.x = -2;
    }
    _visibleSpan = ChunkSpan(
      _world,
      center + dirOffsetA,
      center + dirOffsetB);
  }
}