/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file World.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-22
 ///

#include "World.hpp"
#include <libs/glm/Helpers.hpp>
#include <libs/glwrap/Helpers.hpp>
#include <libs/glwrap/Mesh.hpp>
#include <libs/utils/log.hpp>
#include <chrono>

namespace {
  int LODAt(
    const world::Chunk::ID chunk,
    const world::Chunk::ID player,
    const conf::SettingsPtr& settings) noexcept
  {
    world::Chunk::ID tmp(chunk - player);
    return std::max(
      settings->Soft.World.Chunk.minLOD,
      settings->Soft.World.Chunk.maxLOD + 1 - std::max(1, std::max(abs(tmp.x), abs(tmp.y))));
  }
}

namespace world {

  World::World(
    const conf::SettingsPtr& settings,
    const gen::SimplexNoise& generator) noexcept :

    _settings(settings),

    _notPushedRequests(),
    _loadedChunksPipe(settings->Soft.World.Chunk.preloadDistance* settings->Soft.World.Chunk.preloadDistance),
    _loadedChunks(),

    _chunkLoader(_loadedChunksPipe, generator, settings),
    _loadingThread(&ChunkLoader::loadingThreadCallback, std::ref(_chunkLoader)),

    _EmptyChunk(Chunk::ID(std::numeric_limits<int>::max()))
  {
    _EmptyChunk.mesh.build();
    _EmptyChunk.curentLOD = 0;
  }
  World::~World() noexcept
  {
    _chunkLoader.kill();
    _loadingThread.join();
  }
  const World::ChunkTable& World::loadedChunks() const noexcept
  {
    return _loadedChunks;
  }
  const Chunk& World::chunk(const Chunk::ID& id) const noexcept
  {
    if (auto itr = _loadedChunks.find(id); itr != _loadedChunks.end()) {
      return *itr->second;
    }
    else {
      return _EmptyChunk;
    }
  }

  void World::update() noexcept
  {
    updateChunks();
  }
  void World::updateLoadedChunks(const glm::vec3& player) noexcept
  {
    /* list all chunks that must be loaded */
    Chunk::ID playerChunk(chunkAt(player, *_settings));
    std::set<Chunk::ID, ChunkIDComp> dirs;
    int halfload(_settings->Soft.World.Chunk.preloadDistance / 2);
    for (int i = 0; i < _settings->Soft.World.Chunk.preloadDistance; ++i) {
      for (int j = 0; j < _settings->Soft.World.Chunk.preloadDistance; ++j) {
        dirs.emplace(
          i - halfload + playerChunk.x, 
          j - halfload + playerChunk.y);
      }
    }
    eraseOldChunks(dirs);
    launchNewChunksLoading(dirs, playerChunk);
  }

  void World::eraseOldChunks(const std::set<Chunk::ID, ChunkIDComp>& actives) noexcept
  {
    auto isOldChunk = [&actives](const auto& item) -> bool {
      auto& [chunk, _] = item;
      return !actives.contains(chunk);
    };
    std::erase_if(_loadedChunks, isOldChunk);
  }
  void World::launchNewChunksLoading(const std::set<Chunk::ID, ChunkIDComp>& actives, Chunk::ID center) noexcept
  {
    for (auto itr(_notPushedRequests.begin()); itr != _notPushedRequests.end();) {
      if (trylaunchLoading(itr->first, itr->second.first, itr->second.second)) {
        itr = _notPushedRequests.erase(itr);
      }
      else {
        ++itr;
      }
    }
    for (Chunk::ID newchunk : actives) {
      int lod = LODAt(newchunk, center, _settings);
      auto itr(_loadedChunks.find(newchunk));
      if (itr == _loadedChunks.end()) {
        trylaunchLoading(newchunk, lod, nullptr);
      }
      else if (lod != itr->second->curentLOD) {
        trylaunchLoading(newchunk, lod, itr->second);
      }
    }
  }
  void World::updateChunks() noexcept
  {
    while (!_loadedChunksPipe.empty()) {
      auto ptr(_loadedChunksPipe.read());
      ptr->mesh.build();
      _loadedChunks.erase(ptr->id);
      _loadedChunks.emplace(ptr->id, ptr);
    }
  }

  bool World::trylaunchLoading(
    Chunk::ID id, int lod,
    const std::shared_ptr<Chunk>& base) noexcept
  {
    if (!_chunkLoader.requestChunk(id, lod, base)) {
      _notPushedRequests.insert_or_assign(
        id, std::pair<int, std::shared_ptr<Chunk>>(lod, base));
      return false;
    }
    else {
      return true;
    }
  }

  World::ChunkLoader::ChunkLoader(
    ChunkPipe& outputbus,
    const gen::SimplexNoise& generator,
    const conf::SettingsPtr& settings) noexcept :
    _requestPipe(settings->Soft.World.Chunk.preloadDistance* settings->Soft.World.Chunk.preloadDistance),
    _pendingRequests(),
    _chunkGenerator(generator),
    _settings(settings),
    _output(outputbus),
    _pendingResults(),
    _run(true)
  {
  }


  bool World::ChunkLoader::requestChunk(
    Chunk::ID id,
    int lod,
    const std::shared_ptr<Chunk>& base) noexcept
  {
    if (_requestPipe.full()) {
      return false;
    }
    else {
      _requestPipe.write(id, lod, base);
      return true;
    }
  }
  void World::ChunkLoader::kill()
  {
    _run.clear();
  }

  void World::ChunkLoader::loadingThreadCallback(ChunkLoader& loader) noexcept
  {
    while (loader._run.test_and_set()) {
      while (!loader._requestPipe.empty()) {
        auto [id, lod, base] = loader._requestPipe.read();
        loader._pendingRequests.insert_or_assign(
          id, std::pair<int, std::shared_ptr<Chunk>>(lod, base));
      }
      if (!loader._pendingRequests.empty()) {
        auto itr(loader._pendingRequests.begin());
        auto [id, pair] = *itr;
        auto [lod, base] = pair;
        /// \bug Memory leak here ???
        auto chunk(std::make_shared<Chunk>(
          generateRawChunk(id, lod, base.get(), *loader._settings,
            std::bind_front(&gen::SimplexNoise::noiseAt, &loader._chunkGenerator))));
        loader._pendingResults.insert_or_assign(id, chunk);
        loader._pendingRequests.erase(itr);
      }
      for (auto itr(loader._pendingResults.begin()); itr != loader._pendingResults.end();) {
        if (loader._output.full()) {
          break;
        }
        else {
          loader._output.write(itr->second);
          itr = loader._pendingResults.erase(itr);
        }
      }
      if (loader._pendingRequests.empty()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
      }
    }
  }
}
