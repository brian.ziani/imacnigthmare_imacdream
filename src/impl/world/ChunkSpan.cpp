/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file ChunkSpan.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-27
 ///

#include "ChunkSpan.hpp"
#include "World.hpp"
#include <cassert>

namespace world {

  ChunkSpan::Iterator::Iterator(
    const ChunkSpan* cs, Chunk::ID pos) noexcept :
    _span(cs), _pos(pos)
  {
  }

  ChunkSpan::Iterator& ChunkSpan::Iterator::operator++ () noexcept
  {
    assert(nullptr != _span && "Invalid Iterator");
    if (_pos.y < _span->_end.y) {
      _pos.x += 1;
      if (_span->_end.x <= _pos.x) {
        _pos = Chunk::ID(_span->_begin.x, _pos.y + 1);
        if (_span->_end.y <= _pos.y) {
          _pos = _span->_end;
        }
      }
    }
    else {
      _pos = _span->_end;
    }
    return *this;
  }
  const Chunk& ChunkSpan::Iterator::operator* () const noexcept
  {
    assert(nullptr != _span && "Invalid Iterator");
    assert(nullptr != _span->_world && "Invalid Span");
    assert(*this != _span->end() && "Dereferencing past end iterator");
    return _span->_world->chunk(_pos);
  }
  const Chunk& ChunkSpan::Iterator::operator-> () const noexcept
  {
    assert(nullptr != _span && "Invalid Iterator");
    assert(nullptr != _span->_world && "Invalid Span");
    assert(*this != _span->end() && "Dereferencing past end iterator");
    return _span->_world->chunk(_pos);
  }
  ChunkSpan::Iterator::operator bool() const noexcept
  {
    return nullptr != _span
      && _span->_begin.x <= _pos.x && _span->_begin.y <= _pos.y
      && _pos.x < _span->_end.x&& _pos.y < _span->_end.y;
  }

  ChunkSpan::ChunkSpan(
    const std::shared_ptr<World>& world,
    const Chunk::ID& cornerA,
    const Chunk::ID& cornerB)
    noexcept :
    _world(world),
    _begin(std::min(cornerA.x, cornerB.x), std::min(cornerA.y, cornerB.y)),
    _end(std::max(cornerA.x, cornerB.x), std::max(cornerA.y, cornerB.y))
  {
  }

  ChunkSpan::Iterator ChunkSpan::begin() const noexcept
  {
    return Iterator(this, _begin);
  }
  ChunkSpan::Iterator ChunkSpan::end() const noexcept
  {
    return Iterator(this, _end);
  }

  ChunkSpan::Iterator begin(const ChunkSpan& cs) noexcept
  {
    return cs.begin();
  }
  ChunkSpan::Iterator end(const ChunkSpan& cs) noexcept
  {
    return cs.end();
  }

  bool operator!= (
    const ChunkSpan::Iterator& a,
    const ChunkSpan::Iterator& b) noexcept
  {
    return a._span != b._span || bool(a) != bool(b) ||
      (bool(a) && a._pos != b._pos);
  }
}