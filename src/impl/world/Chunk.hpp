/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Chunk.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-22
 ///

#ifndef _SRC_IMPL_WORLD_CHUNK_H
#define _SRC_IMPL_WORLD_CHUNK_H

#include <impl/Settings.hpp>
#include <libs/glwrap/Mesh.hpp>
#include <libs/generator/Simplex.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <functional>

namespace world {

  struct Vertex {
    glm::vec3 position;
    glm::vec3 tangent;
    glm::vec3 bitangent;
    glm::vec2 normaluv;

    static void bindAttributes();
  };

  using ChunkMesh = glhelp::Mesh<Vertex>;

  struct Chunk {

    using ID = glm::vec<2, int>;

    Chunk(const ID id) noexcept;

    ChunkMesh mesh;
    int curentLOD;
    const ID id;
  };

  struct ChunkIDComp {
    bool operator() (const Chunk::ID& u, const Chunk::ID& v) const noexcept;
  };

  /// \brief Return ID of chunk containing given position
  const Chunk::ID chunkAt(
    const glm::vec3& pos,
    const conf::GlobalSettings& settings) noexcept;

  Chunk generateRawChunk(
    const Chunk::ID chunkid,
    const int levelOfDetail,
    const Chunk* base,
    const conf::GlobalSettings& settings,
    const std::function<float(glm::vec2)>& terrain) noexcept;
}

#endif /* _SRC_IMPL_WORLD_CHUNK_H */