/*
 * Copyright (C) 2021 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file VoidMesh.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2021-01-02
 ///

#ifndef _SRC_IMPL_WORLD_VOID_MESH_H
#define _SRC_IMPL_WORLD_VOID_MESH_H

#include <impl/Settings.hpp>
#include <libs/glwrap/Mesh.hpp>
#include <libs/glwrap/Program.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace world {
  namespace voiddim {

    struct Vertex {
      glm::vec3 position;

      static void bindAttributes();
    };

    class FloorProgram : public glhelp::Program {
    public:

      FloorProgram(const std::string& vs, const std::string& fs);

      void setMVPMatrix(const glm::mat4& m) noexcept;
      void setColor(const glm::vec3& c) noexcept;
      void setCenter(const glm::vec3& c) noexcept;
      void setTranslate(const glm::vec3& t) noexcept;

      void addTime(float t) noexcept;
      void sendTime() const noexcept;

    private:

      GLuint uMVPmatrix, uColor, uCenter, uTranslate, uTime;
      float _time;
    };

    using VoidMesh = glhelp::Mesh<Vertex>;

    VoidMesh generateRawMesh(const conf::GlobalSettings& settings);
  }
}

#endif /* _SRC_IMPL_WORLD_VOID_MESH_H */