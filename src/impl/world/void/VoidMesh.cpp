/*
 * Copyright (C) 2021 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file VoidMesh.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2021-01-02
 ///

#include "VoidMesh.hpp"

namespace world {
  namespace voiddim {


    FloorProgram::FloorProgram(const std::string& vs, const std::string& fs) :
      glhelp::Program(vs, fs),
      uMVPmatrix(uniform("uMVPmatrix")),
      uColor(uniform("uColor")),
      uCenter(uniform("uCenter")),
      uTranslate(uniform("uTranslate")),
      uTime(uniform("uTime")),
      _time(0.f)
    {
    }

    void FloorProgram::setMVPMatrix(const glm::mat4& m) noexcept
    {
      setMat4(uMVPmatrix, m);
    }
    void FloorProgram::setColor(const glm::vec3& c) noexcept
    {
      setVec3(uColor, c);
    }
    void FloorProgram::setCenter(const glm::vec3& c) noexcept
    {
      setVec3(uCenter, c);
    }
    void FloorProgram::setTranslate(const glm::vec3& t) noexcept
    {
      setVec3(uTranslate, t);
    }

    void FloorProgram::addTime(float t) noexcept
    {
      _time += t;
    }
    void FloorProgram::sendTime() const noexcept
    {
      setFloat(uTime, _time);
    }

    void Vertex::bindAttributes()
    {
      // POSITION //
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
      /* END */
      glhelp::checkGLError();
    }

    VoidMesh generateRawMesh(const conf::GlobalSettings& settings)
    {
      int size = settings.Hard.World.Void.viewDistance;
      VoidMesh mesh((size + 1) * (size + 1), size * size * 2);
      mesh.vertices.assign((size + 1) * (size + 1), Vertex{});
      for (int i = 0; i <= size; ++i) {
        for (int j = 0; j <= size; ++j) {
          mesh.vertices[(size + 1) * i + j] = Vertex{
            glm::vec3(i, 0, j) * float(settings.Hard.World.Void.tileSize) };
        }
      }
      mesh.indices.assign(size * size * 2 * 3, 0);
      for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
          mesh.indices[6 * (size * i + j) + 0] = (i * (size + 1) + j);
          mesh.indices[6 * (size * i + j) + 1] = (i * (size + 1) + (j + 1));
          mesh.indices[6 * (size * i + j) + 2] = ((i + 1) * (size + 1) + j);

          mesh.indices[6 * (size * i + j) + 3] = (i * (size + 1) + (j + 1));
          mesh.indices[6 * (size * i + j) + 4] = ((i + 1) * (size + 1) + (j + 1));
          mesh.indices[6 * (size * i + j) + 5] = ((i + 1) * (size + 1) + j);
        }
      }
      mesh.build();
      return mesh;
    }
  }
}