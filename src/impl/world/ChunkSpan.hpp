/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file ChunkSpan.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-27
 ///

#ifndef SRC_IMPL_WORLD_CHUNK_H
#define SRC_IMPL_WORLD_CHUNK_H

#include "Chunk.hpp"
#include <impl/Settings.hpp>
#include <memory>

namespace world {

  class World;

  class ChunkSpan {
  public:

    class Iterator {
    public:

      Iterator(
        const ChunkSpan* cs = nullptr,
        Chunk::ID pos = Chunk::ID(0, 0)) noexcept;
      ~Iterator() noexcept = default;

      Iterator& operator++ () noexcept;
      const Chunk& operator* () const noexcept;
      const Chunk& operator-> () const noexcept;

      explicit operator bool() const noexcept;

    private:

      friend bool operator!= (
        const ChunkSpan::Iterator& a,
        const ChunkSpan::Iterator& b) noexcept;

      const ChunkSpan* _span;
      Chunk::ID _pos;
    };

    ChunkSpan(
      const std::shared_ptr<World>& world = nullptr,
      const Chunk::ID& cornerA = Chunk::ID(0, 0),
      const Chunk::ID& cornerB = Chunk::ID(0, 0))
      noexcept;

    Iterator begin() const noexcept;
    Iterator end() const noexcept;

  private:

    std::shared_ptr<World> _world;
    Chunk::ID _begin;
    Chunk::ID _end;
  };

  ChunkSpan::Iterator begin(const ChunkSpan& cs) noexcept;
  ChunkSpan::Iterator end(const ChunkSpan& cs) noexcept;

  /// \brief invalid iterators on the same span are always equals
  /// valid iterators are equals only if their chunk is the same
  bool operator!= (
    const ChunkSpan::Iterator& a,
    const ChunkSpan::Iterator& b) noexcept;
}

#endif /* SRC_IMPL_WORLD_CHUNK_H */