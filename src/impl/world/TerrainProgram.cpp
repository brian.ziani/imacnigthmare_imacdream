/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file MapMesh.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-16
 ///

#include "TerrainProgram.hpp"
#include <libs/glwrap/Helpers.hpp>
#include <random>

namespace world {

  TerrainProgram::TerrainProgram(
    const conf::SettingsPtr& settings,
    const std::string& vs, const std::string& fs) :

    Program(vs, fs),

    _settings(settings),

    sampler({
      {GL_TEXTURE_MIN_FILTER, GL_LINEAR},
      {GL_TEXTURE_MAG_FILTER, GL_LINEAR},
      {GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE},
      {GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE},
      {GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE}
      }),

    uMVPmatrix(uniform("uMVPmatrix")),
    uAmbiantColor(uniform("uAmbiantColor")),
    uDiffuseColor(uniform("uDiffuseColor")),
    uShinyness(uniform("uShinyness"))
  {
    generateNormalMap();
  }

  void TerrainProgram::bind(const glm::mat4& VP) const
  {
    use();
    setMVPMatrix(VP);
    setAmbiantColor(_settings->Vol.World.Render.Dark.color());
    setDiffuseColor(_settings->Vol.World.Render.Bright.color());
    setShinynessIntensity(_settings->Vol.World.Render.shinyness);
    setNormalMap(_normalMap.glid());
    glhelp::checkGLError();
  }
  void TerrainProgram::setMVPMatrix(const glm::mat4& m) const
  {
    setMat4(uMVPmatrix, m);
    glhelp::checkGLError();
  }
  void TerrainProgram::setAmbiantColor(const glm::vec3& color) const
  {
    setVec3(uAmbiantColor, color);
    glhelp::checkGLError();
  }
  void TerrainProgram::setDiffuseColor(const glm::vec3& color) const
  {
    setVec3(uDiffuseColor, color);
    glhelp::checkGLError();
  }
  void TerrainProgram::setShinynessIntensity(float shiny) const
  {
    setFloat(uShinyness, shiny);
    glhelp::checkGLError();
  }

  void TerrainProgram::setNormalMap(GLuint id) const
  {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, id);
    glBindSampler(0, sampler.glid());
    glhelp::checkGLError();
  }

  void TerrainProgram::unbind() const
  {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindSampler(0, 0);
    glhelp::checkGLError();
  }

  void TerrainProgram::generateNormalMap()
  {
    size_t sandNormalSize(_settings->Soft.World.Render.resolution);
    float amplitude(_settings->Soft.World.Render.amplitude);
    glhelp::Texture sandNormalMap(sandNormalSize, sandNormalSize, 3);
    std::mt19937 gen(0);
    std::uniform_int_distribution<int> disor(-amplitude, amplitude);

    for (size_t i = 0; i < sandNormalSize; ++i) {
      for (size_t j = 0; j < sandNormalSize; ++j) {
        sandNormalMap.pixels.emplace_back(128 + disor(gen));
        sandNormalMap.pixels.emplace_back(255);
        sandNormalMap.pixels.emplace_back(128 + disor(gen));
      }
    }
    sandNormalMap.build();
    _normalMap = std::move(sandNormalMap);
  }
}