/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file World.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-22
 ///

#ifndef _SRC_IMPL_WORLD_WORLD_H
#define _SRC_IMPL_WORLD_WORLD_H

#include "Chunk.hpp"
#include <impl/Settings.hpp>
#include <libs/generator/Simplex.hpp>
#include <libs/utils/Pipe.hpp>
#include <functional>
#include <atomic>
#include <thread>
#include <tuple>
#include <map>
#include <set>

namespace world {
  class World {
  public:

    using ChunkTable = std::map<Chunk::ID, std::shared_ptr<Chunk>, ChunkIDComp>;

    World(
      const conf::SettingsPtr& settings,
      const gen::SimplexNoise& generator) noexcept;
    ~World() noexcept;

    void update() noexcept;
    void updateLoadedChunks(const glm::vec3& player) noexcept;

    const ChunkTable& loadedChunks() const noexcept;

    const Chunk& chunk(const Chunk::ID& id) const noexcept;

  private:

    void eraseOldChunks(const std::set<Chunk::ID, ChunkIDComp>& actives) noexcept;
    void launchNewChunksLoading(const std::set<Chunk::ID, ChunkIDComp>& actives, Chunk::ID center) noexcept;
    void updateChunks() noexcept;

    bool trylaunchLoading(
      Chunk::ID id, int lod,
      const std::shared_ptr<Chunk>& base) noexcept;

    using ChunkPipe = utils::Pipe<std::shared_ptr<Chunk>>;

    class ChunkLoader {
    public:

      ChunkLoader(
        ChunkPipe& outputbus,
        const gen::SimplexNoise& generator,
        const conf::SettingsPtr& settings) noexcept;
      ~ChunkLoader() noexcept = default;

      bool requestChunk(
        Chunk::ID id,
        int lod,
        const std::shared_ptr<Chunk>& base) noexcept;

      static void loadingThreadCallback(ChunkLoader& loader) noexcept;

      void kill();

    private:

      utils::Pipe<std::tuple<
        Chunk::ID,
        int,
        std::shared_ptr<Chunk>>> _requestPipe;
      std::map<
        Chunk::ID,
        std::pair<int, std::shared_ptr<Chunk>>,
        ChunkIDComp> _pendingRequests;

      const gen::SimplexNoise& _chunkGenerator;
      conf::SettingsPtr _settings;
      ChunkPipe& _output;
      ChunkTable _pendingResults;
      std::atomic_flag _run;
    };

    conf::SettingsPtr _settings;

    std::map<
      Chunk::ID,
      std::pair<int, std::shared_ptr<Chunk>>,
      ChunkIDComp> _notPushedRequests;

    ChunkPipe _loadedChunksPipe;
    ChunkTable _loadedChunks;

    ChunkLoader _chunkLoader;
    std::thread _loadingThread;

    Chunk _EmptyChunk;
  };
};

#endif /* _SRC_IMPL_WORLD_WORLD_H */