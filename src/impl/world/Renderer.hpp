/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Renderer.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-11
 ///

#ifndef _SRC_IMPL_WORLD_RENDERER_H
#define _SRC_IMPL_WORLD_RENDERER_H

#include "Chunk.hpp"
#include "World.hpp"
#include "ChunkSpan.hpp"
#include "TerrainProgram.hpp"
#include <memory>

namespace world {

  class Renderer {
  public:

    Renderer(
      const std::shared_ptr<World>& w,
      const conf::SettingsPtr& settings) noexcept;
    ~Renderer() noexcept = default;

    void loadTerrainProgram(const std::string& vs, const std::string& fs) noexcept;

    void renderTerrain(const glm::mat4& VP) const noexcept;
    void renderTerrain(const glm::mat4& VP, const ChunkSpan& span) const noexcept;

    void updateVisibleSpan(
      const glm::vec3& playerPosition,
      const glm::vec3 cameraFrontAxis,
      float) noexcept;

  private:

    std::shared_ptr<TerrainProgram> _terrainPgm;
    std::shared_ptr<World> _world;
    const conf::SettingsPtr& _settings;
    ChunkSpan _visibleSpan;
  };

}

#endif /* _SRC_IMPL_WORLD_RENDERER_H */