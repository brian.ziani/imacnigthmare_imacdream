/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file MapMesh.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-12
 ///

#ifndef _SRC_IMPL_WORLD_MAPMESH_H
#define _SRC_IMPL_WORLD_MAPMESH_H

#include <GL/glew.h>

#include "Chunk.hpp"

#include <impl/Settings.hpp>

#include <libs/glwrap/Program.hpp>
#include <libs/glwrap/Texture.hpp>
#include <libs/glwrap/Sampler.hpp>

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <memory>
#include <vector>

namespace world {

  class TerrainProgram : public glhelp::Program {
  public:

    TerrainProgram(
      const conf::SettingsPtr& settings,
      const std::string& vs, const std::string& fs);

    void bind(const glm::mat4& VP) const;
    void unbind() const;

  private:

    void setMVPMatrix(const glm::mat4& m) const;
    void setAmbiantColor(const glm::vec3& color) const;
    void setDiffuseColor(const glm::vec3& color) const;
    void setShinynessIntensity(float shiny) const;

    void setNormalMap(GLuint id) const;

    void generateNormalMap();

    const conf::SettingsPtr& _settings;
    glhelp::Texture<uint8_t> _normalMap;
    glhelp::Sampler sampler;
    GLuint uMVPmatrix, uAmbiantColor, uDiffuseColor, uShinyness;
  };
}

#endif /* _SRC_IMPL_WORLD_MAPMESH_H */