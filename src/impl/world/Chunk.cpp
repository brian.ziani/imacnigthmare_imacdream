/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Chunk.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-22
 ///

#include "Chunk.hpp"
#include <libs/glwrap/Helpers.hpp>
#include <libs/utils/log.hpp>
#include <glm/glm.hpp>
#include <cmath>

namespace world {

  Chunk::Chunk(const ID id) noexcept : mesh(), curentLOD(1), id(id)
  {
  }

  bool ChunkIDComp::operator() (const Chunk::ID& u, const Chunk::ID& v) const noexcept
  {
    return u.x < v.x || (u.x == v.x && u.y < v.y);
  }

  const Chunk::ID chunkAt(
    const glm::vec3& pos,
    const conf::GlobalSettings& settings) noexcept
  {
    return Chunk::ID(
      floor(pos.x / (settings.Hard.World.Chunk.tilesPerChunk * settings.Hard.World.Chunk.tileSize)),
      floor(pos.z / (settings.Hard.World.Chunk.tilesPerChunk * settings.Hard.World.Chunk.tileSize)));
  }

  void Vertex::bindAttributes()
  {
    // POSITION //
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
    // TANGENT //
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, tangent));
    // BITANGENT //
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, bitangent));
    // NORMALMAP //
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normaluv));
    /* END */
    glhelp::checkGLError();
  }

  inline glm::vec2 positionOf(
    float i, float j, float scale, const glm::vec2& anchor) noexcept
  {
    return anchor + glm::vec2(j, i) * scale;
  }
  inline glm::vec3 sampleAt(
    float i, float j, float scale,
    const glm::vec2& anchor, const std::function<float(glm::vec2)>& terrain) noexcept
  {
    glm::vec2 pos(positionOf(j, i, scale, anchor));
    return glm::vec3(pos.x, terrain(pos), pos.y);
  }

  void generateNormals(
    const int size,
    const float scale,
    const glm::vec2& anchor,
    const std::function<float(glm::vec2)>& terrain,
    std::vector<Vertex>& vertices) noexcept
  {

    for (int i = 0; i <= size; ++i) {

      vertices[(size + 1) * i].bitangent = glm::normalize(
        glm::vec3(
          2.f,
          vertices[(size + 1) * i + 1].position.y
          - sampleAt(i, -1, scale, anchor, terrain).y,
          0.f
        ));

      vertices[(size + 1) * i + size].bitangent = glm::normalize(
        glm::vec3(
          2.f,
          sampleAt(i, size + 1, scale, anchor, terrain).y
          - vertices[(size + 1) * i + (size - 1)].position.y,
          0.f
        ));

      for (int j = 1; j < size; ++j) {
        vertices[(size + 1) * i + j].bitangent = glm::normalize(
          glm::vec3(
            2.f,
            vertices[(size + 1) * i + (j + 1)].position.y
            - vertices[(size + 1) * i + (j - 1)].position.y,
            0.f
          ));
      }
    }

    for (int i = 1; i < size; ++i) {
      for (int j = 0; j <= size; ++j) {
        vertices[(size + 1) * i + j].tangent = glm::normalize(
          glm::vec3(
            0.f,
            vertices[(size + 1) * (i + 1) + j].position.y
            - vertices[(size + 1) * (i - 1) + j].position.y,
            2.f
          ));
      }
    }
    for (int j = 0; j <= size; ++j) {

      vertices[j].tangent = glm::normalize(
        glm::vec3(
          0.f,
          vertices[(size + 1) + j].position.y
          - sampleAt(-1, j, scale, anchor, terrain).y,
          2.f
        ));

      vertices[(size + 1) * size + j].tangent = glm::normalize(
        glm::vec3(
          0.f,
          sampleAt(size + 1, j, scale, anchor, terrain).y
          - vertices[(size + 1) * (size - 1) + j].position.y,
          2.f
        ));
    }
  }

  void generateIndices(int size, std::vector<uint32_t>& indices) noexcept
  {
    indices.assign(size * size * 2 * 3, 0);
    for (int i = 0; i < size; ++i) {
      for (int j = 0; j < size; ++j) {
        indices[6 * (size * i + j) + 0] = (i * (size + 1) + j);
        indices[6 * (size * i + j) + 1] = (i * (size + 1) + (j + 1));
        indices[6 * (size * i + j) + 2] = ((i + 1) * (size + 1) + j);

        indices[6 * (size * i + j) + 3] = (i * (size + 1) + (j + 1));
        indices[6 * (size * i + j) + 4] = ((i + 1) * (size + 1) + (j + 1));
        indices[6 * (size * i + j) + 5] = ((i + 1) * (size + 1) + j);
      }
    }
  }

  Chunk upScaleChunk(
    const int levelOfDetail,
    const Chunk* base,
    const conf::GlobalSettings& settings,
    const std::function<float(glm::vec2)>& terrain) noexcept
  {
    assert(nullptr != base && "Null pointer");
    /* create the chunk and fill the mesh */
    Chunk chunk(base->id);
    /* create basic mesh */
    const float oldlod = powf(2.f, base->curentLOD);
    const float newlod = powf(2.f, levelOfDetail);
    const float scale = float(settings.Hard.World.Chunk.tileSize) / newlod;
    const int oldsize = settings.Hard.World.Chunk.tilesPerChunk * oldlod;
    const int newsize = settings.Hard.World.Chunk.tilesPerChunk * newlod;
    const int step = (newsize + 1) / oldsize;

    chunk.mesh = ChunkMesh((newsize + 1) * (newsize + 1), newsize * newsize * 2);

    glm::vec2 chunkpos(
      base->id
      * settings.Hard.World.Chunk.tilesPerChunk
      * settings.Hard.World.Chunk.tileSize);

    chunk.mesh.vertices.assign((newsize + 1) * (newsize + 1), Vertex{});
    for (int i = 0, y = 0; i <= newsize; i += step, ++y) {
      for (int j = 0, x = 0; j < newsize; j += step, ++x) {

        chunk.mesh.vertices[(newsize + 1) * i + j] = Vertex{ base->mesh.vertices[(oldsize + 1) * y + x] };
        chunk.mesh.vertices[(newsize + 1) * i + j].normaluv = glm::vec2(float(i % 2), float(j % 2));

        for (int jj = 1; jj < step; ++jj) {
          glm::vec3 position(sampleAt(i, j + jj, scale, chunkpos, terrain));
          glm::vec2 uv(float(i % 2), float((j + jj) % 2));
          chunk.mesh.vertices[(newsize + 1) * i + j + jj] = Vertex{ position, glm::vec3(0.f), glm::vec3(0.f), uv };
        }
      }

      chunk.mesh.vertices[(newsize + 1) * i + newsize] = Vertex{ base->mesh.vertices[(oldsize + 1) * y + oldsize] };
      chunk.mesh.vertices[(newsize + 1) * i + newsize].normaluv = glm::vec2(float(i % 2), float(oldsize % 2));

      for (int ii = 1; ii < step && (i + ii) <= newsize; ++ii) {
        for (int j = 0; j <= newsize; ++j) {
          glm::vec3 position(sampleAt(i + ii, j, scale, chunkpos, terrain));
          glm::vec2 uv(float((i + ii) % 2), float(j % 2));
          chunk.mesh.vertices[(newsize + 1) * (i + ii) + j] = Vertex{ position, glm::vec3(0.f), glm::vec3(0.f), uv };
        }
      }
    }

    generateNormals(newsize, scale, chunkpos, terrain, chunk.mesh.vertices);
    generateIndices(newsize, chunk.mesh.indices);

    chunk.curentLOD = levelOfDetail;
    return chunk;
  }

  Chunk downScaleChunk(
    const int levelOfDetail,
    const Chunk* base,
    const conf::GlobalSettings& settings) noexcept
  {
    assert(nullptr != base && "Null pointer");
    /* create the chunk and fill the mesh */
    Chunk chunk(base->id);
    /* create basic mesh */
    const float oldlod = powf(2.f, base->curentLOD);
    const float newlod = powf(2.f, levelOfDetail);
    const int oldsize = settings.Hard.World.Chunk.tilesPerChunk * oldlod;
    const int newsize = settings.Hard.World.Chunk.tilesPerChunk * newlod;
    const int step = (oldsize + 1) / newsize;
    /* too lazy to get exact an expression of this */
    chunk.mesh = ChunkMesh((newsize + 1) * (newsize + 1), newsize * newsize * 2);

    chunk.mesh.vertices.assign((newsize + 1) * (newsize + 1), Vertex{});
    for (int i = 0, y = 0; i <= oldsize; i += step, ++y) {
      for (int j = 0, x = 0; j <= oldsize; j += step, ++x) {
        chunk.mesh.vertices[(newsize + 1) * y + x] =
          Vertex(base->mesh.vertices[(oldsize + 1) * i + j]);
        chunk.mesh.vertices[(newsize + 1) * y + x].normaluv =
          glm::vec2(float(y % 2), float(x % 2));
      }
    }

    generateIndices(newsize, chunk.mesh.indices);

    chunk.curentLOD = levelOfDetail;
    return chunk;
  }

  /*
  * With emplace_back :
  *   18.7% runtime
  *   1.82 s / 2290 calls
  *   0.79 ms / call
  *
  * With assign + direct access :
  *   12.4% runtime
  *   0.84 s / 1537 calls
  *   0.55 ms / call
  */
  Chunk generateNewChunk(
    const Chunk::ID chunkid,
    const int levelOfDetail,
    const conf::GlobalSettings& settings,
    const std::function<float(glm::vec2)>& terrain) noexcept
  {
    /* create the chunk and fill the mesh */
    Chunk chunk(chunkid);
    /* create basic mesh */
    const float lod = powf(2.f, levelOfDetail);
    const float scale = float(settings.Hard.World.Chunk.tileSize) / lod;
    const int size = settings.Hard.World.Chunk.tilesPerChunk * lod;
    /* too lazy to get exact an expression of this */
    chunk.mesh = ChunkMesh((size + 1) * (size + 1), size * size * 2);
    /* convert heightmap to a beautiful mesh */
    glm::vec2 chunkpos(
      chunkid
      * settings.Hard.World.Chunk.tilesPerChunk
      * settings.Hard.World.Chunk.tileSize);

    chunk.mesh.vertices.assign((size + 1) * (size + 1), Vertex{});
    for (int i = 0; i <= size; ++i) {
      for (int j = 0; j <= size; ++j) {
        glm::vec3 position(sampleAt(i, j, scale, chunkpos, terrain));
        glm::vec2 uv(float(i % 2), float(j % 2));
        chunk.mesh.vertices[(size + 1) * i + j] = Vertex{ position, glm::vec3(0.f), glm::vec3(0.f), uv };
      }
    }

    generateNormals(size, scale, chunkpos, terrain, chunk.mesh.vertices);
    generateIndices(size, chunk.mesh.indices);

    chunk.curentLOD = levelOfDetail;
    return chunk;
  }

  Chunk generateRawChunk(
    const Chunk::ID chunkid,
    const int levelOfDetail,
    const Chunk* base,
    const conf::GlobalSettings& settings,
    const std::function<float(glm::vec2)>& terrain) noexcept
  {
    if (nullptr != base) {
      if (base->curentLOD < levelOfDetail) {
        return upScaleChunk(levelOfDetail, base, settings, terrain);
      }
      else if (levelOfDetail < base->curentLOD) {
        return downScaleChunk(levelOfDetail, base, settings);
      }
      else {
        assert(false && "Regenerating at same LOD");
        return Chunk(Chunk::ID(0));
      }
    }
    else {
      return generateNewChunk(chunkid, levelOfDetail, settings, terrain);
    }
  }
}