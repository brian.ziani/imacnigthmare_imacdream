/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Settings.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#include "Settings.hpp"

namespace conf {

  using namespace utils::serial;

  const SerialisationTable& HardSettings::serialTable() noexcept
  {
    static SerialisationTable __table{
      {"WORLD_TILE_PER_CHUNK", {offsetof(HardSettings, World.Chunk.tilesPerChunk), Entry::Type::Int}},
      {"WORLD_TILE_SIZE", {offsetof(HardSettings, World.Chunk.tileSize), Entry::Type::Int}},

      {"WORLD_VOID_TILE_SIZE", {offsetof(HardSettings, World.Void.tileSize), Entry::Type::Int}},
      {"WORLD_VOID_VIEWDISTANCE", {offsetof(HardSettings, World.Void.viewDistance), Entry::Type::Int}},

      {"GUI_WINDOW_NAME", {offsetof(HardSettings, Gui.Window.name), Entry::Type::String}},
      {"GUI_WINDOW_WIDTH", {offsetof(HardSettings, Gui.Window.width), Entry::Type::Int}},
      {"GUI_WINDOW_HEIGHT", {offsetof(HardSettings, Gui.Window.height), Entry::Type::Int}},

      {"GLOBAL_FRAMERATE", {offsetof(HardSettings, Global.framerate), Entry::Type::Int}},
      {"GLOBAL_FPS_SAMPLETIME", {offsetof(HardSettings, Global.FPSSampleTime), Entry::Type::Int}}
    };
    return __table;
  }

  const SerialisationTable& SoftSettings::serialTable() noexcept
  {
    static SerialisationTable __table{
      {"WORLD_VIEW_DISTANCE", {offsetof(SoftSettings, World.Chunk.preloadDistance), Entry::Type::Int}},
      {"WORLD_MAX_LOD", {offsetof(SoftSettings, World.Chunk.maxLOD), Entry::Type::Int}},
      {"WORLD_MIN_LOD", {offsetof(SoftSettings, World.Chunk.minLOD), Entry::Type::Int}},

      {"WORLD_RENDER_NORMALMAP_RES", {offsetof(SoftSettings, World.Render.resolution), Entry::Type::Int}},
      {"WORLD_RENDER_NORMALMAP_AMP", {offsetof(SoftSettings, World.Render.amplitude), Entry::Type::Int}}
    };
    return __table;
  }

  const SerialisationTable& VolatileSettings::serialTable() noexcept
  {
    static SerialisationTable __table{
      {"WORLD_RENDER_DARK_R", {offsetof(VolatileSettings, World.Render.Dark.red), Entry::Type::Float}},
      {"WORLD_RENDER_DARK_G", {offsetof(VolatileSettings, World.Render.Dark.green), Entry::Type::Float}},
      {"WORLD_RENDER_DARK_B", {offsetof(VolatileSettings, World.Render.Dark.blue), Entry::Type::Float}},

      {"WORLD_RENDER_BRIGHT_R", {offsetof(VolatileSettings, World.Render.Bright.red), Entry::Type::Float}},
      {"WORLD_RENDER_BRIGHT_G", {offsetof(VolatileSettings, World.Render.Bright.green), Entry::Type::Float}},
      {"WORLD_RENDER_BRIGHT_B", {offsetof(VolatileSettings, World.Render.Bright.blue), Entry::Type::Float}},

      {"WORLD_RENDER_SHINYNESS", {offsetof(VolatileSettings, World.Render.shinyness), Entry::Type::Float}},


      {"PLAYER_BASE_GRAVITY", {offsetof(VolatileSettings, Player.baseGravity), Entry::Type::Float}},
      {"PLAYER_BASE_VELOCINESS", {offsetof(VolatileSettings, Player.baseVelociness), Entry::Type::Float}},

      {"PLAYER_WALK_SPEED", {offsetof(VolatileSettings, Player.walkSpeed), Entry::Type::Float}},
      {"PLAYER_GRAB_SPEED", {offsetof(VolatileSettings, Player.grabSpeed), Entry::Type::Float}},
      {"PLAYER_JUMP_FORCE", {offsetof(VolatileSettings, Player.jumpForce), Entry::Type::Float}},
      {"PLAYER_WALLJUMP_FORCE", {offsetof(VolatileSettings, Player.wallJumpForce), Entry::Type::Float}},
      
      {"PLAYER_FOLLOW_VELDIR_SPEED", {offsetof(VolatileSettings, Player.followVelocityDirectionSpeed), Entry::Type::Float}},
      {"PLAYER_FOLLOW_UPDIR_SPEED", {offsetof(VolatileSettings, Player.standUpCorrectionSpeed), Entry::Type::Float}},

      {"PLAYER_CAMERA_ROTY_SPEED", {offsetof(VolatileSettings, Player.cameraRotSpeedY), Entry::Type::Float}},
      {"PLAYER_CAMERA_ROTXZ_SPEED", {offsetof(VolatileSettings, Player.cameraTorSpeedXZ), Entry::Type::Float}},
      {"PLAYER_CAMERA_FLIGHT_FOLLOW_SPEED", {offsetof(VolatileSettings, Player.flightCameraAutolockForce), Entry::Type::Float}},

      {"PLAYER_FLIGHT_PITCH_SPEED", {offsetof(VolatileSettings, Player.flightPitchSpeed), Entry::Type::Float}},
      {"PLAYER_FLIGHT_ROLL_SPEED", {offsetof(VolatileSettings, Player.flightRollSpeed), Entry::Type::Float}},
      {"PLAYER_FLIGHT_BOOST", {offsetof(VolatileSettings, Player.flightSpeedBoost), Entry::Type::Float}},
      {"PLAYER_FLIGHT_MASS", {offsetof(VolatileSettings, Player.flightMass), Entry::Type::Float}}

    };
    return __table;
  }
}