/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Events.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-18
 ///

#ifndef _DEF_IMPL_PLAYER_EVENTS_H
#define _DEF_IMPL_PLAYER_EVENTS_H

#include <glm/vec3.hpp>
#include <cstddef>
#include <ostream>

/// \brief Player related stuff namespace
namespace player {

  /// \brief Player Status Events
  namespace ievents {

    struct CtrlEvent {
      enum class Kind {
        Invalid,
        PressJump, ReleaseJump,
        PressFly, ReleaseFly,
        PressGrab, ReleaseGrab
      };
      Kind kind;

      CtrlEvent(Kind k) noexcept;
    };

    struct PhxEvent {
      enum class Kind {
        Invalid,
        HitGround, NoGround,
        HitWall, NoWall,
        GrabbableEdgeFound
      };
      Kind kind;

      union {
        struct {
          glm::vec3 edgeOrigin;
          glm::vec3 edgeVector;
        } grab;
      };

      PhxEvent(Kind k) noexcept;
    };

    struct TimeEvent {
      unsigned long int delay;
      unsigned long int timestamp;
    };
  }

  struct InputEvent {

    struct Epsilon {};

    enum class Kind {
      Invalid, Controller, Physics, Time, Epsilon
    } kind;

    union {
      ievents::CtrlEvent ctrl;
      ievents::PhxEvent physics;
      ievents::TimeEvent time;
    };

    InputEvent() noexcept;
    InputEvent(Epsilon) noexcept;
    InputEvent(const ievents::CtrlEvent& ctrl);
    InputEvent(const ievents::PhxEvent& phx);
    InputEvent(unsigned long int delay, unsigned long int timestamp = 0);

    struct KindHasher {
      size_t operator() (const InputEvent& e) const noexcept;
    };
    struct KindEquals {
      bool operator() (const InputEvent& a, const InputEvent& b) const noexcept;
    };
    struct TimeEventOrder {
      bool operator() (const InputEvent& a, const InputEvent& b) const noexcept;
    };
  };

  std::ostream& operator<< (std::ostream& s, const InputEvent& e) noexcept;

  namespace oevent {
    struct EnterFloorMode {};
    struct EnterWallMode {};
    struct EnterFallMode {};
    struct EnterFlyMode {};
    struct EnterGrabMode {};
    struct SearchGrab { bool doSearch; };
    struct Jump {};
    struct Climb {};
  }
}

#endif /* _DEF_IMPL_PLAYER_EVENTS_H */