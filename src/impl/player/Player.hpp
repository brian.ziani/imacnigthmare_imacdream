/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Body.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-21
 ///

#ifndef _SRC_LIBS_PYSICS_BODY_H
#define _SRC_LIBS_PYSICS_BODY_H

#include <libs/physics/Primitives.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

namespace player {
  class Player {
  public:

    Player(const glm::vec3& pos) noexcept;

    glm::mat3 orientation() const noexcept;
    void alignUp() noexcept;
    void rotate(const glm::mat3& R) noexcept;

    const physics::Ellipsoid& lowHitbox() const noexcept;
    const physics::Ellipsoid& highHitbox() const noexcept;
    void position(const glm::vec3& pos) noexcept;

    void velocity(const glm::vec3& vec) noexcept;
    const glm::vec3& velocity() const noexcept;

    glm::mat4 transformation() const noexcept;

    glm::vec3 frontDirection() const noexcept;
    glm::vec3 transverseDirection() const noexcept;
    glm::vec3 upDirection() const noexcept;

  private:

    glm::mat3 _orientation;
    glm::vec3 _velocity;
    physics::Ellipsoid _lowHitbox;
    physics::Ellipsoid _highHitbox;
  };
}

#endif /* _SRC_LIBS_PYSICS_BODY_H */