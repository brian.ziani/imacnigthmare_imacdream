/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Body.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-21
 ///

#include "Player.hpp"
#include <libs/glm/Helpers.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <functional>

namespace {
  constexpr glm::vec3 lowHitboxSize = glm::vec3(0.75f, 1.4f, 0.75f);
  constexpr glm::vec3 highHitboxSize = glm::vec3(1.4f, 1.1f, 1.4f);
  constexpr glm::vec3 highHitboxOffset = glm::vec3(0.f, 1.5f, 0.f);
}

namespace player {

  Player::Player(const glm::vec3& pos) noexcept :
    _orientation(glm::identity<glm::mat3>()),
    _velocity(0),
    _lowHitbox(pos, lowHitboxSize),
    _highHitbox(pos + highHitboxOffset, highHitboxSize) {
  }

  /// \brief Return Body's orientation
  glm::mat3 Player::orientation() const noexcept {
    return _orientation;
  }
  void Player::alignUp() noexcept {
    _orientation = glm::identity<glm::mat3>();
  }
  void Player::rotate(const glm::mat3& R) noexcept {
    _orientation = R * _orientation;
  }

  /// \brief Return Body's position
  const physics::Ellipsoid& Player::lowHitbox() const noexcept {
    return _lowHitbox;
  }
  const physics::Ellipsoid& Player::highHitbox() const noexcept {
    return _highHitbox;
  }
  void Player::position(const glm::vec3& pos) noexcept {
    _lowHitbox.position = pos;
    _highHitbox.position = pos + highHitboxOffset;
  }

  void Player::velocity(const glm::vec3& vec) noexcept {
    _velocity = vec;
  }
  const glm::vec3& Player::velocity() const noexcept {
    return _velocity;
  }

  glm::vec3 Player::frontDirection() const noexcept {
    return -_orientation[2];
  }
  glm::vec3 Player::transverseDirection() const noexcept {
    return -_orientation[0];
  }
  glm::vec3 Player::upDirection() const noexcept {
    return _orientation[1];
  }

  /// \brief Return body's transformation matrix, with offset given in local coordinate system
  glm::mat4 Player::transformation() const noexcept {
    glm::mat4 transform(_orientation);
    transform[3] = glm::vec4(
      _lowHitbox.position.x,
      _lowHitbox.position.y - _lowHitbox.size.y,
      _lowHitbox.position.z,
      1.0f);
    return transform;
  }

}