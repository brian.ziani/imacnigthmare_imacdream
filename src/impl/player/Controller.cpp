/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Controller.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-17
 ///

#include "Controller.hpp"
#include <libs/utils/log.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <libs/utils/Math.hpp>
#include <libs/utils/log.hpp>
#include <libs/glm/Helpers.hpp>
#include <SDL2/SDL.h>
#include <exception>
#include <functional>

namespace player {

  const int joystickTreshold = 8000;
  float joystickAxisToFloat(int value)
  {
    value = joystickTreshold < abs(value) ? value : 0;
    return (2.0f * (value + 32768.0f) / (65535.0f)) - 1.0f;
  }

  ctrl::InputHandler::KeyMap& Controller::__ControllerX360KeyMap() noexcept
  {
    static bool _builded = false;
    static ctrl::InputHandler::KeyMap _instance;
    if (!_builded) {

      /* LAxis */
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERAXISMOTION, SDL_CONTROLLER_AXIS_LEFTX),
        [&](const SDL_Event& e) -> void {
          _playerTargetRightward = joystickAxisToFloat(e.caxis.value);
        });
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERAXISMOTION, SDL_CONTROLLER_AXIS_LEFTY),
        [&](const SDL_Event& e) -> void {
          _playerTargetForward = joystickAxisToFloat(-e.caxis.value);
        });

      /* RAxis */
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERAXISMOTION, SDL_CONTROLLER_AXIS_RIGHTX),
        [&](const SDL_Event& e) -> void {
          _cameraYRotationSpeed = joystickAxisToFloat(e.caxis.value);
        });
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERAXISMOTION, SDL_CONTROLLER_AXIS_RIGHTY),
        [&](const SDL_Event& e) -> void {
          _cameraXZRotationSpeed = joystickAxisToFloat(e.caxis.value);
        });

      /* JUMP */
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERBUTTONDOWN, SDL_CONTROLLER_BUTTON_A),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::PressJump }));
        });
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERBUTTONUP, SDL_CONTROLLER_BUTTON_A),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::ReleaseJump }));
        });

      /* FLY */
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERBUTTONDOWN, SDL_CONTROLLER_BUTTON_Y),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::PressFly }));
        });
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERBUTTONUP, SDL_CONTROLLER_BUTTON_Y),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::ReleaseFly }));
        });

      /* GRAB */
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERBUTTONDOWN, SDL_CONTROLLER_BUTTON_B),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::PressGrab }));
        });
      _instance.addCallback(
        ctrl::InputHandler::GenericFilter(SDL_CONTROLLERBUTTONUP, SDL_CONTROLLER_BUTTON_B),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::ReleaseGrab }));
        });

      _builded = true;
    }
    return _instance;
  }

  ctrl::InputHandler::KeyMap& Controller::__KeyboardMouseKeyMap() noexcept
  {
    static bool _builded = false;
    static ctrl::InputHandler::KeyMap _instance;
    if (!_builded) {

      /* LAxis */
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_z),
        [&](const SDL_Event&) -> void {
          _playerTargetForward = 1;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_z),
        [&](const SDL_Event&) -> void {
          _playerTargetForward = 0;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_s),
        [&](const SDL_Event&) -> void {
          _playerTargetForward = -1;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_s),
        [&](const SDL_Event&) -> void {
          _playerTargetForward = 0;
        });

      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_q),
        [&](const SDL_Event&) -> void {
          _playerTargetRightward = -1;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_q),
        [&](const SDL_Event&) -> void {
          _playerTargetRightward = 0;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_d),
        [&](const SDL_Event&) -> void {
          _playerTargetRightward = 1;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_d),
        [&](const SDL_Event&) -> void {
          _playerTargetRightward = 0;
        });

      /* RAxis */
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_a),
        [&](const SDL_Event&) -> void {
          _cameraYRotationSpeed = -0.5;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_a),
        [&](const SDL_Event&) -> void {
          _cameraYRotationSpeed = 0;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_e),
        [&](const SDL_Event&) -> void {
          _cameraYRotationSpeed = 0.5;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_e),
        [&](const SDL_Event&) -> void {
          _cameraYRotationSpeed = 0;
        });

      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_r),
        [&](const SDL_Event&) -> void {
          _cameraXZRotationSpeed = -0.5;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_r),
        [&](const SDL_Event&) -> void {
          _cameraXZRotationSpeed = 0;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_f),
        [&](const SDL_Event&) -> void {
          _cameraXZRotationSpeed = 0.5;
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_f),
        [&](const SDL_Event&) -> void {
          _cameraXZRotationSpeed = 0;
        });

      /* JUMP */
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_SPACE),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::PressJump }));
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_SPACE),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::ReleaseJump }));
        });

      /* FLY */
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_SPACE),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::PressFly }));
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_SPACE),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::ReleaseFly }));
        });

      /* GRAB */
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_LSHIFT),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::PressGrab }));
        });
      _instance.addCallback(
        ctrl::InputHandler::KeyBoardFilter(SDL_KEYUP, SDLK_LSHIFT),
        [&](const SDL_Event&) -> void {
          _flowchart.handle(InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::ReleaseGrab }));
        });

      _builded = true;
    }
    return _instance;
  }
  const player::FlowchartHandler::TriggersTemplatesTable& Controller::__TriggerEvents() noexcept
  {
    static bool _builded = false;
    static player::FlowchartHandler::TriggersTemplatesTable _instance;
    if (!_builded) {

      _instance["PressJump"] = InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::PressJump });
      _instance["ReleaseJump"] = InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::ReleaseJump });
      _instance["PressFly"] = InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::PressFly });

      _instance["HitGround"] = InputEvent(ievents::PhxEvent{ ievents::PhxEvent::Kind::HitGround });
      _instance["HitWall"] = InputEvent(ievents::PhxEvent{ ievents::PhxEvent::Kind::HitWall });
      _instance["NoGround"] = InputEvent(ievents::PhxEvent{ ievents::PhxEvent::Kind::NoGround });
      _instance["NoWall"] = InputEvent(ievents::PhxEvent{ ievents::PhxEvent::Kind::NoWall });

      _instance["PressGrab"] = InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::PressGrab });
      _instance["ReleaseGrab"] = InputEvent(ievents::CtrlEvent{ ievents::CtrlEvent::Kind::ReleaseGrab });
      _instance["GrabbableEdgeFound"] = InputEvent(ievents::PhxEvent{ ievents::PhxEvent::Kind::GrabbableEdgeFound });

      _builded = true;
    }
    return _instance;
  }
  const player::FlowchartHandler::ActionsTable& Controller::__ActionTable() noexcept
  {
    static bool _builded = false;
    static player::FlowchartHandler::ActionsTable _instance;
    if (!_builded) {

      _instance["ActionFloorJump"] = std::bind_front(&player::Controller::ActionFloorJump, this);
      _instance["ActionWallJump"] = std::bind_front(&player::Controller::ActionWallJump, this);

      _instance["HitGround"] = std::bind_front(
        &player::Controller::ActionHitGround, this);
      _instance["HitWall"] = std::bind_front(
        &player::Controller::ActionHitWall, this);
      _instance["StartFalling"] = std::bind_front(
        &player::Controller::ActionStartFalling, this);
      _instance["EnterFlyMode"] = std::bind_front(
        &player::Controller::ActionEnterFlyMode, this);

      _instance["EnableSearchGrab"] = std::bind_front(
        &player::Controller::ActionSetSearchGrab, this, true);
      _instance["DisableSeachGrab"] = std::bind_front(
        &player::Controller::ActionSetSearchGrab, this, false);
      _instance["EnterGrabMode"] = std::bind_front(
        &player::Controller::ActionEnterGrabMode, this);
      _instance["Climb"] = std::bind_front(
        &player::Controller::ActionClimb, this);

      _builded = true;
    }
    return _instance;
  }

  Controller::Controller(
    const conf::SettingsPtr& settings,
    ctrl::InputHandler& inputs,
    gui::TrackballCamera& cam,
    Player& player) :

    _settings(settings),

    _inputHandler(inputs), _flowchart(),
    _joystick(nullptr),
    _camera(cam), _player(player),

    _mover(std::bind_front(&player::Controller::moverWalk, this)),
    _rotater(std::bind_front(&player::Controller::rotateCameraFree, this)),

    _wallNormal(0.f), _grabEdgeOrigin(0.f), _grabEdgeVector(0.f),

    _cameraYRotationSpeed(0), _cameraXZRotationSpeed(0),
    _playerTargetForward(0), _playerTargetRightward(0),
    _velociness(1.f), _gravityFactor(1.f), _directionFollowness(1.f),

    _floorMaterial(physics::Material::Invalid)
  {
    if (0 == SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER)) {
      for (int i = 0; i < SDL_NumJoysticks(); ++i) {
        if (SDL_IsGameController(i)) {
          _joystick = SDL_GameControllerOpen(i);
          if (_joystick) {
            break;
          }
          else {
            LOG::warning("Failed Open Controller :", i, ":", SDL_GetError());
          }
        }
      }
      if (_joystick) {
        LOG::info("Controller Found : ", SDL_GameControllerName(_joystick));
      }
      else {
        LOG::info("No controller");
      }
    }
    else {
      LOG::warning("Can't open controller device :", SDL_GetError());
    }
    _inputHandler.bindKeyMap(__ControllerX360KeyMap());
    _inputHandler.bindKeyMap(__KeyboardMouseKeyMap());
  }


  float Controller::targetVelocity() const noexcept
  {
    return _playerTargetForward * _playerTargetForward
      + _playerTargetRightward * _playerTargetRightward;
  }
  float Controller::gravityFactor() const noexcept
  {
    return _gravityFactor;
  }

  physics::Material Controller::currentFloorMaterial() const noexcept
  {
    return _floorMaterial;
  }


  void Controller::update(unsigned long int time, float dt) noexcept
  {
    _flowchart.update(time);
    updateOrientation(dt);
    upadteVelocity(dt);
  }


  void Controller::handleMotion(const physics::motion::Solution& motion, float dt) noexcept
  {
    if (motion.hasContact) {
      _floorMaterial = motion.contactMaterial;
      glm::vec3 normal(glm::normalize(motion.contactNormal));
      float vertical(glm::dot(normal, glm::vec3(0.f, 1.f, 0.f)));
      if (M_SQRT1_2 <= vertical) {
        _flowchart.handle(InputEvent(ievents::PhxEvent(ievents::PhxEvent::Kind::HitGround)));
      }
      else if (fabs(vertical) <= M_SQRT1_2) {
        _flowchart.handle(InputEvent(ievents::PhxEvent(ievents::PhxEvent::Kind::HitWall)));
        _wallNormal = normal;
      }
    }
    else {
      _flowchart.handle(InputEvent(ievents::PhxEvent(ievents::PhxEvent::Kind::NoGround)));
      _flowchart.handle(InputEvent(ievents::PhxEvent(ievents::PhxEvent::Kind::NoWall)));
    }
    /* apply solution */
    if (dt != 0.f) {
      _player.velocity((motion.finalPosition - _player.lowHitbox().position) / dt);
      _player.position(motion.finalPosition);
    }

  }
  void Controller::grabbableFound(const glm::vec3& origin, const glm::vec3& vector) noexcept
  {
    InputEvent event(ievents::PhxEvent(ievents::PhxEvent::Kind::GrabbableEdgeFound));
    event.physics.grab.edgeOrigin = origin;
    event.physics.grab.edgeVector = vector;
    _flowchart.handle(event);
  }
  std::pair<glm::vec2, glm::vec2> Controller::XZAxis() const noexcept
  {
    glm::vec2 velocityXZ(glm::vec2(_player.velocity().x, _player.velocity().z));
    glm::vec2 playerXZ(glm::normalize(glm::vec2(_player.frontDirection().x, _player.frontDirection().z)));
    if (0.01f <= glm::length2(velocityXZ)) {
      velocityXZ = glm::normalize(velocityXZ);
      velocityXZ = glm::vec2(velocityXZ.x, velocityXZ.y);
    }
    else {
      velocityXZ = playerXZ * glm::sign(_player.upDirection().y);
    }
    return { velocityXZ, playerXZ };
  }
  void Controller::updateOrientation(float dt) noexcept
  {
    _rotater(dt);
    _camera.angley += _cameraYRotationSpeed * _settings->Vol.Player.cameraRotSpeedY;
    _camera.anglex += _cameraXZRotationSpeed * _settings->Vol.Player.cameraTorSpeedXZ;
    // constexpr float PI_8 = M_PI_4 / 2.f;
    constexpr float xmin = -M_PI_2;
    constexpr float xmax = M_PI_2;
    _camera.anglex = std::max(xmin, std::min(_camera.anglex, xmax));
  }
  void Controller::rotateCameraFree(float dt) noexcept
  {
    auto [velocityXZ, playerXZ] = XZAxis();
    float deltaagnle(glm::orientedAngle(playerXZ, velocityXZ));
    deltaagnle *= _settings->Vol.Player.followVelocityDirectionSpeed * dt * _directionFollowness;
    deltaagnle = math::nullifySmallValue(deltaagnle, 0.001f);
    if (deltaagnle != 0.f) {
      glm::mat3 Q = glm::mat3(
        0.f, 0.f, 1.f,
        0.f, 0.f, 0.f,
        -1.f, 0.f, 0.f);
      glm::mat3 I = glm::identity<glm::mat3>();
      glm::mat3 R = I + (sinf(deltaagnle) * Q) + ((1.f - cosf(deltaagnle)) * Q * Q);
      _player.rotate(R);
    }
    glm::vec3 rot(
      glm::cross(_player.upDirection(), glm::vec3(0, 1, 0))
      * _settings->Vol.Player.standUpCorrectionSpeed * dt);
    float r = math::nullifySmallValue(glm::length(rot), 0.001f);
    if (r != 0) {
      glm::mat3 R = helpers::rotationMatrix(-rot);
      _player.rotate(R);
    }
  }
  void Controller::rotateCameraFlight(float dt) noexcept
  {
    auto [velocityXZ, playerXZ] = XZAxis();
    float playerY(glm::orientedAngle(glm::vec2(0.f, -1.f), velocityXZ));
    float delta(playerY - _camera.angley);
    delta = fmod(delta, M_PI * 2.f);
    if (fabs(delta - M_PI * 2.f) < fabs(delta)) {
      delta -= M_PI * 2.f;
    }
    else if (fabs(delta + M_PI * 2.f) < fabs(delta)) {
      delta += M_PI * 2.f;
    }
    delta = math::nullifySmallValue(delta * _settings->Vol.Player.flightCameraAutolockForce * dt, 0.001f);
    _camera.angley += delta;
  }
  void Controller::rotateCameraGrab(float dt) noexcept
  {
    auto [_, playerXZ] = XZAxis();
    glm::vec2 wallXZ(-_grabEdgeVector.z, _grabEdgeVector.x);
    float deltaagnle(glm::orientedAngle(playerXZ, wallXZ));
    deltaagnle *= _settings->Vol.Player.cameraRotSpeedY * dt;
    deltaagnle = math::nullifySmallValue(deltaagnle, 0.0001f);
    if (deltaagnle != 0.f) {
      glm::mat3 Q = glm::mat3(
        0.f, 0.f, 1.f,
        0.f, 0.f, 0.f,
        -1.f, 0.f, 0.f);
      glm::mat3 I = glm::identity<glm::mat3>();
      glm::mat3 R = I + (sinf(deltaagnle) * Q) + ((1.f - cosf(deltaagnle)) * Q * Q);
      _player.rotate(R);
    }
    glm::vec3 rot(
      glm::cross(_player.upDirection(), glm::vec3(0, 1, 0))
      * _settings->Vol.Player.standUpCorrectionSpeed * dt);
    float r = math::nullifySmallValue(glm::length(rot), 0.001f);
    if (r != 0) {
      glm::mat3 R = helpers::rotationMatrix(-rot);
      _player.rotate(R);
    }
  }

  void Controller::upadteVelocity(float dt) noexcept
  {
    /* update velocity and status */
    _mover(dt);
  }

  void Controller::moverFlight(float dt) noexcept
  {
    glm::mat3 pitch(helpers::rotationMatrix(
      _playerTargetForward * _settings->Vol.Player.flightPitchSpeed * _player.transverseDirection() * dt));
    glm::mat3 roll(helpers::rotationMatrix(
      _playerTargetRightward * _settings->Vol.Player.flightRollSpeed * _player.upDirection() * dt));
    _player.rotate(pitch * roll);
    /* motion parameters */
    glm::vec3 liftnormal(-_player.frontDirection());
    glm::vec3 velvect(_player.velocity());
    glm::vec3 veldir(helpers::safeNormalize(velvect));
    float vel2(glm::length2(velvect));
    float vel(sqrtf(vel2));
    float attackAngle(glm::dot(_player.upDirection(), veldir));
    float small(0.001f), big(9.f); // Size when front area is small or big
    float a(5.f);
    float fluidDensity(0.6f * exp(-_player.highHitbox().position.y / 800.f));
    /* drag coefs */
    float attackAngleDrag(fabs(attackAngle));
    float theta(M_PI_2 * attackAngleDrag);
    float dragB(cos(theta) * big + sin(theta) * small);
    float crossSectionArea(a * dragB);
    float roughness(50.f);
    float dragFactor(fluidDensity * vel * 1.3f * pow(a + dragB, 0.625f - 0.25f) / roughness);
    /* lift */
    float attackAngleLift(std::max(0.f, attackAngle) * fabs(liftnormal.y));
    float betha = M_PI_2 * attackAngleLift;
    float liftB(sin(betha) * big + cos(betha) * small);
    float wingsArea(a * liftB);
    float smallLift(0.001f);
    float bigLift(0.013f);
    float liftFactor(sin(betha) * bigLift + cos(betha) * smallLift);
    /* forces */
    glm::vec3 Fd(-veldir * 0.5f * fluidDensity * vel2 * dragFactor * crossSectionArea);
    glm::vec3 Fl(liftnormal * 0.5f * fluidDensity * vel2 * liftFactor * wingsArea);
    /* final velocity update */
    float mass(_settings->Vol.Player.flightMass);
    if (mass == 0) assert(false && "Invalid mass");
    glm::vec3 acceleration((Fd + Fl) / mass);
    _player.velocity(
      _player.velocity()
      + acceleration * dt
      + _player.upDirection() * pow(fluidDensity, 2.f) * attackAngleLift * dt * _settings->Vol.Player.flightSpeedBoost);
    /* done */
  }
  void Controller::moverWalk(float dt) noexcept
  {
    glm::mat3 cameraY(
      glm::rotate(
        glm::identity<glm::mat4>(),
        -_camera.angley + float(M_PI_2),
        glm::vec3(0, 1, 0)));
    glm::vec3 velocity(_player.velocity());
    /* update velocity according to controller inputs */
    float forwardDiff(_playerTargetForward * _settings->Vol.Player.walkSpeed - glm::dot(velocity, cameraY[0]));
    float rightwardDiff(_playerTargetRightward * _settings->Vol.Player.walkSpeed - glm::dot(velocity, cameraY[2]));
    glm::vec3 deltaV(forwardDiff * cameraY[0] + rightwardDiff * cameraY[2]);
    velocity += deltaV * _velociness * dt;
    velocity = helpers::nullifySmallComponents(velocity, 0.0001f);
    /* done */
    _player.velocity(velocity);
  }
  void Controller::moverGrab(float dt) noexcept
  {
    glm::vec3 edgeDir(glm::normalize(_grabEdgeVector));
    glm::vec3 edgeToPlayer(_player.lowHitbox().position - _grabEdgeOrigin);
    glm::vec3 playerOnEdge(edgeDir * glm::dot(edgeDir, edgeToPlayer));
    glm::mat3 cameraXZMatrix(
      glm::rotate(
        glm::identity<glm::mat4>(),
        -_camera.angley + float(M_PI_2),
        glm::vec3(0, 1, 0)));
    glm::vec3 cameraTransverseAxis(cameraXZMatrix[2]);
    glm::vec3 screenEdgeDir = edgeDir;
    if (glm::dot(cameraTransverseAxis, screenEdgeDir) < 0) {
      screenEdgeDir = -edgeDir;
    }
    glm::vec3 velocity(_player.velocity());
    float rightwardDiff(_playerTargetRightward * _settings->Vol.Player.grabSpeed - glm::dot(velocity, screenEdgeDir));
    velocity += screenEdgeDir * rightwardDiff * _velociness * dt;
    if (0 < glm::dot(velocity, edgeDir)) {
      if (glm::length2(_grabEdgeVector) < glm::length2(playerOnEdge) + 4.f) {
        velocity = glm::vec3(0.f);
      }
    }
    else {
      if (glm::dot(playerOnEdge, edgeDir) < 0) {
        velocity = glm::vec3(0.f);
      }
    }
    velocity = helpers::nullifySmallComponents(velocity, 0.0001f);
    _player.velocity(velocity);
  }

  void Controller::loadFlowchart(const std::string& path) noexcept
  {
    _flowchart.load(path, __TriggerEvents(), __ActionTable());
  }

  void Controller::ActionFloorJump(const InputEvent&) noexcept
  {
    glm::vec3 v(_player.velocity());
    v.y = _settings->Vol.Player.jumpForce;
    _player.velocity(v);
    _velociness = _settings->Vol.Player.baseVelociness * 0.01f;
    _directionFollowness = 1.f;
    _gravityFactor = 0.6f;
    slot<oevent::Jump>().notify();
  }

  void Controller::ActionWallJump(const InputEvent&) noexcept
  {
    glm::vec3 velocity(_player.velocity());
    /* we only keep horizontal tangential component of original velocity */
    velocity -= glm::vec3(0.f, 1.f, 0.f) * velocity.y;
    velocity -= _wallNormal * glm::dot(velocity, _wallNormal);
    velocity +=
      glm::normalize(glm::vec3(0.f, 1.f, 0.f) + _wallNormal)
      * _settings->Vol.Player.wallJumpForce;
    _player.velocity(velocity);
    _velociness = _settings->Vol.Player.baseVelociness * 0.01f;
    _directionFollowness = 1.f;
    _gravityFactor = 0.7f;
    slot<oevent::Jump>().notify();
  }

  void Controller::ActionHitGround(const InputEvent&) noexcept
  {
    _velociness = _settings->Vol.Player.baseVelociness * 0.6f;
    _gravityFactor = 1.f;
    _directionFollowness = 1.f;
    _rotater = std::bind_front(&player::Controller::rotateCameraFree, this);
    _mover = std::bind_front(&player::Controller::moverWalk, this);
    slot<oevent::EnterFloorMode>().notify();
  }
  void Controller::ActionHitWall(const InputEvent&) noexcept
  {
    _velociness = _settings->Vol.Player.baseVelociness * 0.3f;
    _gravityFactor = 0.6f;
    _directionFollowness = 0.2f;
    _rotater = std::bind_front(&player::Controller::rotateCameraFree, this);
    _mover = std::bind_front(&player::Controller::moverWalk, this);
    slot<oevent::EnterWallMode>().notify();
  }
  void Controller::ActionEnterFlyMode(const InputEvent&) noexcept
  {
    _velociness = _settings->Vol.Player.baseVelociness * 0.3f;
    _gravityFactor = 0.4f;
    _directionFollowness = 1.f;
    _rotater = std::bind_front(&player::Controller::rotateCameraFlight, this);
    _mover = std::bind_front(&player::Controller::moverFlight, this);
    slot<oevent::EnterFlyMode>().notify();
  }
  void Controller::ActionStartFalling(const InputEvent&) noexcept
  {
    _velociness = _settings->Vol.Player.baseVelociness * 0.01f;
    _gravityFactor = 1.f;
    _directionFollowness = 1.f;
    _rotater = std::bind_front(&player::Controller::rotateCameraFree, this);
    _mover = std::bind_front(&player::Controller::moverWalk, this);
    slot<oevent::EnterFallMode>().notify();
  }
  void Controller::ActionSetSearchGrab(bool search, const InputEvent&) noexcept
  {
    slot<oevent::SearchGrab>().notify(search);
  }
  void Controller::ActionEnterGrabMode(const InputEvent& e) noexcept
  {
    _velociness = _settings->Vol.Player.baseVelociness * 0.1f;
    _gravityFactor = 0.f;
    _directionFollowness = 1.f;
    _player.velocity(glm::vec3(0.f));
    _rotater = std::bind_front(&player::Controller::rotateCameraGrab, this);
    _mover = std::bind_front(&player::Controller::moverGrab, this);
    _grabEdgeOrigin = e.physics.grab.edgeOrigin;
    _grabEdgeVector = e.physics.grab.edgeVector;
    slot<oevent::SearchGrab>().notify(false);
    slot<oevent::EnterGrabMode>().notify();
  }
  void Controller::ActionClimb(const InputEvent&) noexcept
  {
    glm::vec3 v(_player.velocity());
    v.y = 1.5f * _settings->Vol.Player.jumpForce;
    _player.velocity(v);
    _velociness = _settings->Vol.Player.baseVelociness * 0.1f;
    _gravityFactor = 0.6f;
    _directionFollowness = 1.f;
    slot<oevent::Climb>().notify();
  }
}