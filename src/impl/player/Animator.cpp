/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Animator.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-18
 ///

#include "Animator.hpp"
#include <cmath>

namespace {

  using namespace player;

  std::unordered_map<AnimationHandler::Pose, AnimationHandler::PoseCycle> __Animations = {
    { AnimationHandler::Idle, AnimationHandler::PoseCycle{ "Armature|Idle", 8} },
    { AnimationHandler::Walk, AnimationHandler::PoseCycle{ "Armature|Walk", 4} },
    { AnimationHandler::Fly,  AnimationHandler::PoseCycle{ "Armature|Fly", 2}  },
    { AnimationHandler::Grab, AnimationHandler::PoseCycle{ "Armature|Grab", 1} },
    { AnimationHandler::Jump, AnimationHandler::PoseCycle{ "Armature|Jump", 2} }
  };

  std::pair<std::string, std::pair<size_t, size_t>> RegAnimation(AnimationHandler::Pose p) noexcept
  {
    return { __Animations[p].name, {static_cast<size_t>(p), __Animations[p].length} };
  }

  class WalkAnimator : public AnimationHandler::AAnimator {
  public:

    static WalkAnimator* get(const AnimationHandler& h) noexcept
    {
      static WalkAnimator _instance(h);
      return &_instance;
    }

    void setFootstepSound(const std::shared_ptr<audio::SoundBunch>& sound)
    {
      _footstepsound = sound;
    }

    WalkAnimator(const AnimationHandler& h) :
      AAnimator(h),
      _lastInterpolator(h.pose(AnimationHandler::Idle, 0)),
      _nextInterpolator(h.pose(AnimationHandler::Idle, 0)),
      _interpolator(h.pose(AnimationHandler::Idle, 0)),
      _nextFrame(0),
      _time(0), _transitionSpeed(0.1f)
    {
    }
    void init(const model::IPose* start) noexcept override
    {
      _lastInterpolator = model::PoseInterpolator(start);
      _nextInterpolator = model::PoseInterpolator(_handler.pose(AnimationHandler::Pose::Idle, 0));
      _interpolator = model::PoseInterpolator(&_lastInterpolator, &_nextInterpolator, 0);
      _nextFrame = 0;
      _time = 0;
      _transitionSpeed = 0.1f;
    }
    const model::IPose& update(float dt) noexcept override
    {
      glm::vec3 v(_handler.player().velocity());
      glm::vec2 vxz(v.x, v.z);
      float vel(glm::length(vxz));
      float velFactor(_handler.ctrl().targetVelocity());
      _time += std::max(_transitionSpeed, (vel * dt) / float(M_PI * 2.f * 0.5f));
      if (1.f < _time) {
        _time = fmod(_time, 1.f);
        _nextFrame = (_nextFrame + 1) % __Animations[AnimationHandler::Walk].length;
        _lastInterpolator = _nextInterpolator;
        _transitionSpeed = 0.02f;
        if (_nextFrame % 2 == 0) {
          _footstepsound->play(-1, 0, velFactor * 0.2f);
        }
      }
      _nextInterpolator = model::PoseInterpolator(
        _handler.pose(AnimationHandler::Idle, _nextFrame),
        _handler.pose(AnimationHandler::Walk, _nextFrame),
        velFactor);
      _interpolator = model::PoseInterpolator(&_lastInterpolator, &_nextInterpolator, _time);
      return _interpolator;
    }
    std::optional<glm::mat4> getKey(size_t boneID) const noexcept override
    {
      return _interpolator.getKey(boneID);
    }
    std::optional<glm::vec3> translation(size_t boneID) const noexcept override
    {
      return _interpolator.translation(boneID);
    }
    std::optional<glm::quat> rotation(size_t boneID) const noexcept override
    {
      return _interpolator.rotation(boneID);
    }
  private:

    std::shared_ptr<audio::SoundBunch> _footstepsound;
    model::PoseInterpolator _lastInterpolator;
    model::PoseInterpolator _nextInterpolator;
    model::PoseInterpolator _interpolator;
    size_t _nextFrame;
    float _time, _transitionSpeed;
  };

  class BasicAnimator : public AnimationHandler::AAnimator {
  public:

    template <AnimationHandler::Pose pose>
    static BasicAnimator* get(const AnimationHandler& h) noexcept
    {
      static BasicAnimator _instance(h, pose);
      return &_instance;
    }

    BasicAnimator(const AnimationHandler& h, AnimationHandler::Pose pose) :
      AAnimator(h),
      _pose(pose),
      _startPose(h.pose(pose, 0)), _nextPose(h.pose(pose, 0)),
      _interpolator(h.pose(pose, 0)),
      _nextFrame(0),
      _time(0)
    {
    }
    void init(const model::IPose* start) noexcept override
    {
      _startPose = start;
      _nextPose = _handler.pose(_pose, 0);
      _interpolator = model::PoseInterpolator(_startPose, _nextPose, 0);
      _nextFrame = 0;
      _time = 0;
    }
    const model::IPose& update(float dt) noexcept override
    {
      _time += dt * 5;
      if (1.f < _time) {
        _time = fmod(_time, 1.f);
        _startPose = _handler.pose(_pose, _nextFrame);
        if (_nextFrame + 1 < __Animations[_pose].length) {
          _nextFrame = (_nextFrame + 1);
          _nextPose = _handler.pose(_pose, _nextFrame);
        }
      }
      _interpolator = model::PoseInterpolator(_startPose, _nextPose, _time);
      return _interpolator;
    }
    std::optional<glm::mat4> getKey(size_t boneID) const noexcept override
    {
      return _interpolator.getKey(boneID);
    }
    std::optional<glm::vec3> translation(size_t boneID) const noexcept override
    {
      return _interpolator.translation(boneID);
    }
    std::optional<glm::quat> rotation(size_t boneID) const noexcept override
    {
      return _interpolator.rotation(boneID);
    }
  private:

    AnimationHandler::Pose _pose;
    const model::IPose* _startPose;
    const model::IPose* _nextPose;
    model::PoseInterpolator _interpolator;
    size_t _nextFrame;
    float _time;
  };
}

namespace player {

  AnimationHandler::AAnimator::AAnimator(
    const AnimationHandler& h) noexcept :
    _handler(h)
  {
  }

  std::unordered_map<std::string, std::pair<size_t, size_t>>
    AnimationHandler::neededPoses() noexcept
  {
    std::unordered_map<std::string, std::pair<size_t, size_t>> result;
    result.emplace(RegAnimation(Pose::Idle));
    result.emplace(RegAnimation(Pose::Walk));
    result.emplace(RegAnimation(Pose::Fly));
    result.emplace(RegAnimation(Pose::Grab));
    result.emplace(RegAnimation(Pose::Jump));
    return result;
  }

  AnimationHandler::AnimationHandler(
    Controller& ctrl,
    const Player& player,
    const model::Skeleton& baseSkeleton,
    std::unordered_map<size_t, std::vector<model::KeyPose>>&& poses) noexcept :
    _controller(ctrl),
    _player(player),
    _baseSkeleton(baseSkeleton),
    _poses(poses),
    _sounds({
      {Sound::FootStepSand, std::make_shared<audio::SoundBunch>(
        "../res/sounds/footstep/sand", ".ogg", 4)},
      {Sound::FootStepWall, std::make_shared<audio::SoundBunch>(
        "../res/sounds/footstep/wall", ".ogg", 6)}
      }),
    _lastPose(),
    _currentAnimator(nullptr)
  {
    auto anim = WalkAnimator::get(*this);
    anim->setFootstepSound(_sounds.at(Sound::FootStepSand));
    _currentAnimator = anim;

    ctrl.slot<oevent::EnterFloorMode>().addSubscriber(
      [this](Controller& ctrl, oevent::EnterFloorMode&) -> void {
        auto anim = WalkAnimator::get(*this);
        if (physics::Material::Sand == ctrl.currentFloorMaterial())
          anim->setFootstepSound(_sounds.at(Sound::FootStepSand));  
        else
          anim->setFootstepSound(_sounds.at(Sound::FootStepWall));  
        setAnimator(anim);
      });
    ctrl.slot<oevent::EnterWallMode>().addSubscriber(
      [this](Controller& ctrl, oevent::EnterWallMode&) -> void {
        auto anim = WalkAnimator::get(*this);
        if (physics::Material::Sand == ctrl.currentFloorMaterial())
          anim->setFootstepSound(_sounds.at(Sound::FootStepSand));  
        else
          anim->setFootstepSound(_sounds.at(Sound::FootStepWall));  
        setAnimator(anim);
      });
    ctrl.slot<oevent::EnterFallMode>().addSubscriber(
      [this](Controller&, oevent::EnterFallMode&) -> void {
        setAnimator(BasicAnimator::get<Pose::Jump>(*this));
      });
    ctrl.slot<oevent::EnterFlyMode>().addSubscriber(
      [this](Controller&, oevent::EnterFlyMode&) -> void {
        setAnimator(BasicAnimator::get<Pose::Fly>(*this));
      });
    ctrl.slot<oevent::EnterGrabMode>().addSubscriber(
      [this](Controller&, oevent::EnterGrabMode&) -> void {
        setAnimator(BasicAnimator::get<Pose::Grab>(*this));
      });
    ctrl.slot<oevent::Jump>().addSubscriber(
      [this](Controller&, oevent::Jump&) -> void {
        setAnimator(BasicAnimator::get<Pose::Jump>(*this));
      });
    ctrl.slot<oevent::Climb>().addSubscriber(
      [this](Controller&, oevent::Climb&) -> void {
        setAnimator(BasicAnimator::get<Pose::Jump>(*this));
      });
  }

  const model::IPose& AnimationHandler::update(float dt) noexcept
  {
    return _currentAnimator->update(dt);
  }

  void AnimationHandler::setAnimator(AAnimator* animator) noexcept
  {
    if (animator != _currentAnimator) {
      _lastPose = model::KeyPose(*_currentAnimator, _baseSkeleton);
      _currentAnimator = animator;
      _currentAnimator->init(&_lastPose);
    }
  }

  const model::KeyPose* AnimationHandler::pose(Pose p, size_t frame) const noexcept
  {
    return &_poses.at(p)[frame];
  }
  const Player& AnimationHandler::player() const noexcept
  {
    return _player;
  }
  const Controller& AnimationHandler::ctrl() const noexcept
  {
    return _controller;
  }
}