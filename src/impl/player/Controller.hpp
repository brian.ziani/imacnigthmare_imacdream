/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Controller.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-17
 ///

#ifndef _SRC_IMPL_PLAYER_CONTROLLER_H
#define _SRC_IMPL_PLAYER_CONTROLLER_H

#include "Flowchart.hpp"
#include "Player.hpp"
#include <impl/Settings.hpp>
#include <impl/controller/InputHandler.hpp>
#include <libs/camera/impl/TrackballCamera.hpp>
#include <libs/physics/MotionSolver.hpp>
#include <libs/physics/Material.hpp>
#include <libs/utils/Observer.hpp>
#include <memory>

namespace player {

  class Controller :
    public Observer::Subject <
    Controller,
    oevent::EnterFloorMode,
    oevent::EnterWallMode,
    oevent::EnterFallMode,
    oevent::EnterFlyMode,
    oevent::EnterGrabMode,
    oevent::SearchGrab,
    oevent::Jump,
    oevent::Climb
    > {
  public:

    template <typename E>
    Subject<Controller,E>& slot() noexcept {
      return static_cast<Observer::Subject<Controller,E>&>(*this);
    }

    Controller(
      const conf::SettingsPtr& settings,
      ctrl::InputHandler& inputs,
      gui::TrackballCamera& cam,
      Player& player);

    void update(unsigned long int time, float dt) noexcept;

    void handleMotion(const physics::motion::Solution& motion, float dt) noexcept;
    void grabbableFound(const glm::vec3& origin, const glm::vec3& vector) noexcept;

    void loadFlowchart(const std::string& path) noexcept;

    float targetVelocity() const noexcept;
    float gravityFactor() const noexcept;

    physics::Material currentFloorMaterial() const noexcept;

  private:

    std::pair<glm::vec2, glm::vec2> XZAxis() const noexcept;

    void updateOrientation(float dt) noexcept;
    void rotateCameraFree(float dt) noexcept;
    void rotateCameraFlight(float dt) noexcept;
    void rotateCameraGrab(float dt) noexcept;

    void upadteVelocity(float dt) noexcept;
    void moverFlight(float dt) noexcept;
    void moverWalk(float dt) noexcept;
    void moverGrab(float dt) noexcept;

    ctrl::InputHandler::KeyMap& __ControllerX360KeyMap() noexcept;
    ctrl::InputHandler::KeyMap& __KeyboardMouseKeyMap() noexcept;
    const player::FlowchartHandler::TriggersTemplatesTable& __TriggerEvents() noexcept;
    const player::FlowchartHandler::ActionsTable& __ActionTable() noexcept;

    void ActionFloorJump(const InputEvent& e) noexcept;
    void ActionWallJump(const InputEvent& e) noexcept;

    void ActionHitGround(const InputEvent& e) noexcept;
    void ActionHitWall(const InputEvent& e) noexcept;
    void ActionEnterFlyMode(const InputEvent& e) noexcept;
    void ActionStartFalling(const InputEvent& e) noexcept;

    void ActionSetSearchGrab(bool search, const InputEvent& e) noexcept;
    void ActionEnterGrabMode(const InputEvent& e) noexcept;
    void ActionClimb(const InputEvent& e) noexcept;

    conf::SettingsPtr _settings;

    ctrl::InputHandler& _inputHandler;
    FlowchartHandler _flowchart;

    SDL_GameController* _joystick;

    gui::TrackballCamera& _camera;
    Player& _player;

    std::function<void(float)> _mover;
    std::function<void(float)> _rotater;

    glm::vec3 _wallNormal;
    glm::vec3 _grabEdgeOrigin;
    glm::vec3 _grabEdgeVector;

    float _cameraYRotationSpeed;  ///< Rotation around player
    float _cameraXZRotationSpeed; ///< Rotation up / down
    float _playerTargetForward;   ///< Movement in camera forward direction
    float _playerTargetRightward; ///< Movement in camera left / right direciton

    float _velociness; ///< Factor constraining how much controller can change player velocity
    float _gravityFactor; ///< Scale factor applied to gravity
    float _directionFollowness;

    physics::Material _floorMaterial;
  };
}

#endif /* _SRC_IMPL_PLAYER_CONTROLLER_H */