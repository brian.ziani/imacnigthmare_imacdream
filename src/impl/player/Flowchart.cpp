/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Controller.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-22
 ///

#include "Controller.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <libs/utils/Math.hpp>
#include <libs/utils/log.hpp>
#include <libs/glm/Helpers.hpp>
#include <unordered_map>

namespace player {

  void FlowchartHandler::update(unsigned long int time)
  {
    if (_timer.has_value()) {
      const auto& [event, transition] = _timer.value();
      if (event.time.timestamp <= time) {
        follow(event, transition);
      }
    }
  }
  void FlowchartHandler::handle(const InputEvent& event)
  {
    if (auto itr = _triggers.find(event); itr != _triggers.end()) {
      follow(event, itr->second);
    }
  }
  void FlowchartHandler::follow(const InputEvent& trigger, const FSM::Transition& t) noexcept
  {
    std::optional<FSM::Transition> epsilon;
    _state = _state.follow(t, trigger);
    /* clear old waited events */
    _triggers.clear();
    _timer = std::nullopt;
    /* bind new waited events */
    for (auto itr = _state.begin(); itr != _state.end(); ++itr) {
      if (InputEvent::Kind::Epsilon == itr->first.kind) {
        epsilon = std::make_optional(FSM::Transition(itr));
        break;
      }
      else if (InputEvent::Kind::Time == itr->first.kind) {
        _timer = std::make_optional(std::make_pair<InputEvent, FSM::Transition>(
          InputEvent(itr->first.time.delay, _timestamp + itr->first.time.delay),
          FSM::Transition(itr)));
      }
      else {
        _triggers.emplace(itr->first, itr);
      }
    }
    /* call the new state */
    _state.call();
    /* if an epsilon transition exist, follow it */
    if (epsilon.has_value()) {
      follow(epsilon.value()->first, epsilon.value());
    }
  }

  void FlowchartHandler::load(
    const std::string& path,
    const TriggersTemplatesTable& triggersTemplates,
    const ActionsTable& actionsTable)
  {
    _automaton.clear();
    std::unordered_map<std::string, FSM::StateID> loadedStatesTable;
    const FSM::StateID invalidState(std::numeric_limits<FSM::StateID>::max());
    InputEvent::Kind currentKind(InputEvent::Kind::Invalid);
    utils::parseDotFile(
      path,
      [&](const std::string& name,
        const std::string& shape) -> void {
          if ("ellipse" == shape) {
            loadedStatesTable[name] = _automaton.addState();
          }
          else if ("diamond" == shape) {
            auto itr(actionsTable.find(name));
            if (actionsTable.end() == itr) {
              throw std::runtime_error("Unkown Action : " + name);
            }
            else {
              loadedStatesTable[name] = _automaton.addState(actionsTable.at(name));
            }
          }
          else if ("plain" == shape) {
            if ("ControllerEvent" == name) {
              currentKind = InputEvent::Kind::Controller;
            }
            else if ("PhysicsEvent" == name) {
              currentKind = InputEvent::Kind::Physics;
            }
            else if ("TimedEvent" == name) {
              currentKind = InputEvent::Kind::Time;
            }
            else if ("Epsilon" == name) {
              currentKind = InputEvent::Kind::Epsilon;
            }
            else {
              throw std::runtime_error(
                "Invalid Transition Type Flag : "
                + shape);
            }
            loadedStatesTable[name] = invalidState;
          }
          LOG::debug("'" + name + "'");
      },
      [&](
        const std::string& src,
        const std::string& dest,
        const std::optional<std::string>& label) -> void {

          auto tmp(loadedStatesTable.find(src));
          if (loadedStatesTable.end() == tmp)
            throw std::runtime_error("Unkown State : " + src);
          FSM::StateID srcID = tmp->second;

          tmp = loadedStatesTable.find(dest);
          if (loadedStatesTable.end() == tmp)
            throw std::runtime_error("Unkown State : " + dest);
          FSM::StateID destID = tmp->second;

          if (invalidState != srcID && invalidState != destID) {
            if (label.has_value() || InputEvent::Kind::Epsilon == currentKind) {
              InputEvent event;
              switch (currentKind) {
              case InputEvent::Kind::Controller:
              case InputEvent::Kind::Physics:
                if (auto itr = triggersTemplates.find(label.value()); itr != triggersTemplates.end()) {
                  event = itr->second;
                }
                else {
                  throw std::runtime_error("Invalid Event" + label.value());
                }
                break;
              case InputEvent::Kind::Time:
                event = InputEvent(std::atoi(label.value().c_str()));
                break;
              case InputEvent::Kind::Epsilon:
                event = InputEvent(InputEvent::Epsilon {});
                break;
              default:
                assert(false && "Invalid Transition Mode");
              };
              _automaton.addTransition(srcID, destID, event);
            }
            else {
              LOG::warning("Ignoring Incomplete Edge : ", src, " -> ", dest);
            }
          }
      });
    auto itr = loadedStatesTable.find("ENTRY");
    if (loadedStatesTable.end() == itr) {
      throw std::runtime_error("No Entry State specified");
    }
    _automaton.setEntry(itr->second);
    _state = _automaton.begin();
    follow(InputEvent(InputEvent::Epsilon{}), _state.begin());
  }
}