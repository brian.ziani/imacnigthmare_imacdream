/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Events.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-17
 ///

#include "Events.hpp"
#include <cassert>

namespace player {

  namespace ievents {

    CtrlEvent::CtrlEvent(Kind k) noexcept :
      kind(k)
    {
    }
    PhxEvent::PhxEvent(Kind k) noexcept :
      kind(k)
    {
    }
  }

  InputEvent::InputEvent() noexcept : kind(Kind::Invalid) {}
  InputEvent::InputEvent(Epsilon) noexcept : kind(Kind::Epsilon) {}
  InputEvent::InputEvent(const ievents::CtrlEvent& ctrl) :
    kind(Kind::Controller), ctrl(ctrl)
  {
  }
  InputEvent::InputEvent(const ievents::PhxEvent& phx) :
    kind(Kind::Physics), physics(phx)
  {
  }
  InputEvent::InputEvent(unsigned long int delay, unsigned long int timestamp) :
    kind(Kind::Time), time{ delay, timestamp }
  {
  }

  size_t InputEvent::KindHasher::operator() (const InputEvent& e) const noexcept
  {
    size_t m = static_cast<size_t>(e.kind);
    size_t k = 0;
    switch (e.kind) {
    case Kind::Controller:
      k = static_cast<size_t>(e.ctrl.kind);
      break;
    case Kind::Physics:
      k = static_cast<size_t>(e.physics.kind);
      break;
    default:
      k = 0;
    }
    return ((m << 32) | (m >> 32)) ^ (k);
  }
  bool InputEvent::KindEquals::operator() (const InputEvent& a, const InputEvent& b) const noexcept
  {
    return a.kind == b.kind
      && (Kind::Controller != a.kind || a.ctrl.kind == b.ctrl.kind)
      && (Kind::Physics != a.kind || a.physics.kind == b.physics.kind);
  }

  bool InputEvent::TimeEventOrder::operator() (const InputEvent& a, const InputEvent& b) const noexcept
  {
    assert(Kind::Time == a.kind);
    assert(Kind::Time == b.kind);
    return a.time.timestamp < b.time.timestamp;
  }

  std::ostream& operator<< (std::ostream& s, const InputEvent& e) noexcept
  {
    switch (e.kind) {
    case InputEvent::Kind::Invalid:
      return s << "Invalid";
    case InputEvent::Kind::Controller:
      return s << "Controller[" << static_cast<size_t>(e.ctrl.kind) << "]";
    case InputEvent::Kind::Physics:
      return s << "Physics[" << static_cast<size_t>(e.physics.kind) << "]";
    case InputEvent::Kind::Time:
      return s << "Time[" << e.time.delay << "]";
    case InputEvent::Kind::Epsilon:
      return s << "Epsilon";
    default:
      assert(0 && "Invalid Event Kind");
      return s << "ERROR";
    }
  };
}