/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Animator.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-18
 ///

#ifndef _SRC_IMPL_PLAYER_ANIMATOR_H
#define _SRC_IMPL_PLAYER_ANIMATOR_H

#include "Player.hpp"
#include "Controller.hpp"
#include <impl/model/Animation.hpp>
#include <impl/sound/SoundBunch.hpp>
#include <unordered_map>
#include <string>

namespace player {

  class AnimationHandler {
  public:

    enum Pose {
      Idle, Walk, Fly, Grab, Jump
    };

    struct PoseCycle {
      std::string name;
      size_t length;
    };

    class AAnimator : public model::IPose {
    public:

      AAnimator(const AnimationHandler& h) noexcept;
      virtual void init(const model::IPose* start) noexcept = 0;
      virtual const model::IPose& update(float dt) noexcept = 0;

    protected:

      const AnimationHandler& _handler;
    };

    static std::unordered_map<std::string, std::pair<size_t, size_t>> neededPoses() noexcept;

    AnimationHandler(
      Controller& ctrl,
      const Player& player,
      const model::Skeleton& baseSkeleton,
      std::unordered_map<size_t, std::vector<model::KeyPose>>&& poses) noexcept;
    ~AnimationHandler() noexcept = default;

    const model::IPose& update(float dt) noexcept;

    const model::KeyPose* pose(Pose p, size_t frame) const noexcept;
    const Player& player() const noexcept;
    const Controller& ctrl() const noexcept;

  private:

    enum class Sound {
      FootStepSand, FootStepWall
    };

    void setAnimator(AAnimator* animator) noexcept;

    const Controller& _controller;
    const Player& _player;
    const model::Skeleton& _baseSkeleton;
    std::unordered_map<size_t, std::vector<model::KeyPose>> _poses;
    std::unordered_map<Sound, std::shared_ptr<audio::SoundBunch>> _sounds;
    model::KeyPose _lastPose;
    AAnimator* _currentAnimator;
  };
}

#endif /* _SRC_IMPL_PLAYER_ANIMATOR_H */