/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Controller.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-22
 ///

#ifndef _SRC_IMPL_PLAYER_FLOWCHART_H
#define _SRC_IMPL_PLAYER_FLOWCHART_H

#include "Events.hpp"
#include <libs/utils/Automaton.hpp>
#include <functional>
#include <optional>
#include <unordered_map>
#include <set>

namespace player {

  class FlowchartHandler {
  public:

    using Action = std::function<void(const InputEvent&)>;
    using FSM = utils::Automaton<Action, InputEvent>;
    using TriggersTemplatesTable = std::unordered_map<std::string, InputEvent>;
    using ActionsTable = std::unordered_map<std::string, Action>;

    void update(unsigned long int time);
    void handle(const InputEvent& event);

    void load(
      const std::string& path,
      const TriggersTemplatesTable& triggersTemplates,
      const ActionsTable& actionsTable);

  private:

    void follow(const InputEvent& trigger, const FSM::Transition& t) noexcept;

    using TriggersTable = std::unordered_map<InputEvent, FSM::Transition, InputEvent::KindHasher, InputEvent::KindEquals>;

    FSM _automaton;
    FSM::State _state;
    TriggersTable _triggers;
    std::optional<std::pair<InputEvent, FSM::Transition>> _timer = std::nullopt;
    unsigned long int _timestamp = 0;
  };
}

#endif /* _SRC_IMPL_PLAYER_FLOWCHART_H */