/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Settings.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#ifndef _SRC_IMPL_GLOBAL_SETTINGS_H
#define _SRC_IMPL_GLOBAL_SETTINGS_H

#include <libs/glm/Helpers.hpp>
#include <libs/utils/Observer.hpp>
#include <libs/utils/Serial.hpp>
#include <memory>
#include <string>

namespace conf {

  /// \brief Settings loaded once at program start and then never changed
  struct HardSettings {
    struct {

      struct {

      } Generator;

      struct {
        int tilesPerChunk;
        int tileSize;
        int idmod;
      } Chunk;

      struct {
        int tileSize;
        int viewDistance;
      } Void;

    } World;

    struct {

      struct {
        std::string name;
        int width;
        int height;
      } Window;

    } Gui;

    struct {
      int framerate;
      int FPSSampleTime;
    } Global;

    static const utils::serial::SerialisationTable& serialTable() noexcept;
  };

  /// \brief Settings for wich changes requires extra work to sanitise
  ///   program state
  struct SoftSettings {

    struct {

      struct {
        int preloadDistance;
        int maxLOD;
        int minLOD;
      } Chunk;

      struct {
        int resolution;
        int amplitude;
      } Render;

    } World;

    static const utils::serial::SerialisationTable& serialTable() noexcept;
  };

  /// \brief Settings that might change without notification
  struct VolatileSettings {
    struct {

      struct {

        struct {
          float red, green, blue;
          glm::vec3 color() const noexcept { return helpers::rgb(red, green, blue); }
        } Dark;

        struct {
          float red, green, blue;
          glm::vec3 color() const noexcept { return helpers::rgb(red, green, blue); }
        } Bright;

        float shinyness;

      } Render;

    } World;


    struct {
      float baseGravity;
      float baseVelociness;

      float walkSpeed;
      float grabSpeed;
      float jumpForce;
      float wallJumpForce;

      float followVelocityDirectionSpeed;
      float standUpCorrectionSpeed;

      float cameraRotSpeedY;
      float cameraTorSpeedXZ;
      float flightCameraAutolockForce;

      float flightPitchSpeed;
      float flightRollSpeed;
      float flightSpeedBoost;
      float flightMass;
    } Player;

    static const utils::serial::SerialisationTable& serialTable() noexcept;
  };

  struct SoftSettingsChanged {};

  struct GlobalSettings :
    public Observer::Subject<GlobalSettings, SoftSettingsChanged> {

    HardSettings Hard;
    SoftSettings Soft;
    VolatileSettings Vol;

    Subject<GlobalSettings, SoftSettingsChanged>& slot() noexcept
    {
      return static_cast<Subject<GlobalSettings, SoftSettingsChanged>&>(*this);
    }
  };

  using SettingsPtr = std::shared_ptr<GlobalSettings>;
}

#endif /* _SRC_IMPL_GLOBAL_SETTINGS_H */