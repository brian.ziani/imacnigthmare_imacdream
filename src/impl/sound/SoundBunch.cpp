/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file SoundBunch.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-30
 ///

#include "SoundBunch.hpp"
#include <cassert>

namespace audio {

  SoundBunch::SoundBunch(
    const std::string& path,
    const std::string& ext,
    size_t count) :
    _sounds(count, nullptr),
    _generator(0),
    _distrib(0, count - 1),
    _currentChannel(0)
  {
    for (size_t i = 0; i < count; ++i) {
      std::string file = path + std::to_string(i) + ext;
      Mix_Chunk* tmp(Mix_LoadWAV(file.c_str()));
      if (!tmp) {
        for (auto chunk : _sounds) Mix_FreeChunk(chunk);
        throw MissingFile{ file + " : " + Mix_GetError() };
      }
      _sounds[i] = tmp;
    }
  }
  SoundBunch::~SoundBunch()
  {
    for (auto chunk : _sounds) {
      Mix_FreeChunk(chunk);
    }
  }

  int SoundBunch::play(int channel, int loop, float volume) noexcept
  {
    _currentChannel = Mix_PlayChannel(channel, _sounds[_distrib(_generator)], loop);
    Mix_Volume(_currentChannel, 128 * volume);
    return _currentChannel;
  }
  void SoundBunch::stop() noexcept
  {
    Mix_FadeOutChannel(_currentChannel, 100);
  }

  int SoundBunch::channel() const noexcept
  {
    return _currentChannel;
  }
  int SoundBunch::size() const noexcept
  {
    return _sounds.size();
  }
}