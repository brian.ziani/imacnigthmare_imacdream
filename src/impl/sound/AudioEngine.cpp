/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file AudioEngine.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-29
 ///

#include "AudioEngine.hpp"

#include <SDL2/SDL_mixer.h>

namespace audio {

  AudioEngine::AudioEngine()
  {
    if (MIX_INIT_OGG != Mix_Init(MIX_INIT_OGG)) {
      throw MixerInitFailure{ Mix_GetError() };
    }
    if (0 != Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 1024)) {
      throw OpenAudioFailure{ Mix_GetError() };
    }
    Mix_AllocateChannels(64);
  }

  AudioEngine::~AudioEngine()
  {
    Mix_CloseAudio();
    Mix_Quit();
  }
}