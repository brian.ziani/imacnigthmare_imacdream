/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Music.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-30
 ///

#include "Music.hpp"

namespace audio {

  Music::Music(const std::string& path) :
    _music(Mix_LoadMUS(path.c_str()))
  {
    if (nullptr == _music)
      throw FileNotFound{ path + " : " + Mix_GetError() };
  }
  Music::~Music() noexcept
  {
    Mix_FreeMusic(_music);
  }

  Music::Music(Music&& m) noexcept : _music(m._music)
  {
    m._music = nullptr;
  }
  Music& Music::operator= (Music&& m) noexcept
  {
    _music = m._music;
    m._music = nullptr;
    return *this;
  }

  int Music::tryPlay(int loops) noexcept
  {
    if (MIX_FADING_OUT == Mix_FadingMusic()) {
      return -1;
    }
    return Mix_PlayMusic(_music, loops);
  }
}