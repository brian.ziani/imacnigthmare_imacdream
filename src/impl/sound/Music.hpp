/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Music.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-30
 ///

#ifndef _SRC_IMPL_SOUND_MUSIC_H
#define _SRC_IMPL_SOUND_MUSIC_H

#include <SDL2/SDL_mixer.h>
#include <string>

namespace audio {

  class Music {
  public:

    struct FileNotFound { std::string what; };

    Music(const std::string& path);
    ~Music() noexcept;

    Music(Music&& m) noexcept;
    Music& operator= (Music&& m) noexcept;

    Music(const Music&) = delete;
    Music& operator= (const Music&) = delete;

    int tryPlay(int loops) noexcept;

  private:

    Mix_Music* _music;
  };
}

#endif /* _SRC_IMPL_SOUND_MUSIC_H */