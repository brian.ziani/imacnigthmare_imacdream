/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file InputHandler.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-13
 ///

#include "InputHandler.hpp"
#include <stdexcept>

namespace ctrl {

  void InputHandler::KeyMap::addCallback(
    const SDL_Event& filter,
    const Callback& callback) noexcept
  {
    _callbacks.emplace_back(filter, callback);
  }

  InputHandler::Binding InputHandler::bindCallback(
    const SDL_Event& filter,
    const Callback& callback)
  {
    if (!EventMatcher()(filter, filter)) {
      throw std::invalid_argument("Unsupported Event" + std::to_string(filter.type));
    }
    auto itr(_callbacks.find(filter));
    if (_callbacks.end() == itr) {
      itr = _callbacks.emplace_hint(itr, filter, CallbackList());
    }
    itr->second.push_front(callback);
    return std::make_pair<SDL_Event, CallbackList::const_iterator>(
      SDL_Event(filter),
      itr->second.cbegin());
  }
  void InputHandler::unbindCallback(const Binding& link) noexcept
  {
    _callbacks.at(link.first).erase(link.second);
  }

  void InputHandler::bindKeyMap(KeyMap& map)
  {
    map._bindings.reserve(map._callbacks.size());
    for (const auto& [filter, callback] : map._callbacks) {
      map._bindings.emplace_back(bindCallback(filter, callback));
    }
  }
  void InputHandler::unbindKeyMap(KeyMap& map) noexcept
  {
    for (const auto& binding : map._bindings) {
      unbindCallback(binding);
    }
    map._bindings.clear();
  }

  void InputHandler::update() const noexcept
  {
    mut_eventsStack.clear();
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      if (auto itr = _callbacks.find(event); itr != _callbacks.cend()) {
        mut_eventsStack.insert_or_assign(event, itr);
      }
    }
    for (const auto& [event, match] : mut_eventsStack) {
      for (const auto& callback : match->second) {
        callback(event);
      }
    }
  }

  SDL_Event InputHandler::GenericFilter(uint32_t type, uint8_t flag) noexcept
  {
    SDL_Event filter;
    filter.type = type;
    switch (filter.type) {

    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
      filter.button.button = flag;
      break;

    case SDL_CONTROLLERAXISMOTION:
      filter.caxis.axis = flag;
      break;

    case SDL_CONTROLLERBUTTONDOWN:
    case SDL_CONTROLLERBUTTONUP:
      filter.cbutton.button = flag;
      break;
    }
    return filter;
  }
  SDL_Event InputHandler::KeyBoardFilter(uint32_t type, SDL_Keycode key) noexcept
  {
    SDL_Event filter;
    filter.type = type;
    filter.key.keysym.sym = key;
    return filter;
  }

  bool InputHandler::EventMatcher::operator() (
    const SDL_Event& filter,
    const SDL_Event& event) const noexcept
  {
    if (filter.type != event.type) {
      return false;
    }
    else {
      switch (event.type) {

      case SDL_QUIT:
        return true;

      case SDL_MOUSEMOTION:
      case SDL_MOUSEWHEEL:
        return true;

      case SDL_MOUSEBUTTONDOWN:
      case SDL_MOUSEBUTTONUP:
        return filter.button.button == event.button.button;

      case SDL_KEYDOWN:
      case SDL_KEYUP:
        return filter.key.keysym.sym == event.key.keysym.sym;

      case SDL_CONTROLLERAXISMOTION:
        return filter.caxis.axis == event.caxis.axis;

      case SDL_CONTROLLERBUTTONDOWN:
      case SDL_CONTROLLERBUTTONUP:
        return filter.cbutton.button == event.cbutton.button;

      default:
        return false;
      }
    }
  }
  size_t InputHandler::EventHasher::operator() (const SDL_Event& e) const noexcept
  {
    size_t x(static_cast<size_t>(e.type));
    return (x << 5) | (x >> 5);
  }
}