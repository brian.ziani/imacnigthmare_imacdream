/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file InputHandler.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-13
 ///

#ifndef _SRC_IMPL_CONTROLLER_INPUT_HANDLER_H
#define _SRC_IMPL_CONTROLLER_INPUT_HANDLER_H

#include <SDL2/SDL_events.h>
#include <unordered_map>
#include <functional>
#include <vector>
#include <list>

namespace ctrl {

  /// \brief Class used to allow dynamic binding between SDL_Events and callbacks
  class InputHandler {
  public:

    using Callback = std::function<void(const SDL_Event&)>;
    using CallbackList = std::list<Callback>;
    using Binding = std::pair<SDL_Event, CallbackList::const_iterator>;

    /// \brief Utility object used to bind and unbind a set of keys in a single shot
    class KeyMap {
    public:
      /// \see InputHandler::registerCallback
      void addCallback(const SDL_Event& filter, const Callback& callback) noexcept;
    private:
      friend InputHandler;
      std::vector<std::pair<SDL_Event, Callback>> _callbacks; ///< List of filters
      std::vector<Binding> _bindings; ///< List of active bindings if keymap is binded
    };

    ///
    /// \brief Add a new callback associated with given event
    ///   Events are matched according to their types and button / key if relevant
    ///   Supported events are :
    ///
    ///   SDL_QUIT
    ///   SDL_MOUSEMOTION, SDL_MOUSEWHEEL
    ///
    ///   SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP           (+button)
    ///   SDL_KEYDOWN, SDL_KEYUP                           (+keysym)
    ///   SDL_CONTROLLERAXISMOTION                         (+axis)
    ///   SDL_CONTROLLERBUTTONDOWN, SDL_CONTROLLERBUTTONUP (+button)
    ///
    /// \return an handle usefull to later remove this binding
    /// \throw invalid_argument if filter event is not supported
    ///
    Binding bindCallback(const SDL_Event& filter, const Callback& callback);

    /// \brief Remove a previously added callback
    void unbindCallback(const Binding& link) noexcept;

    /// \brief Usefull to load a set of callbacks in a single call
    void bindKeyMap(KeyMap& map);
    /// \brief Usefull to unload a set of callbacks in a single call
    void unbindKeyMap(KeyMap& map) noexcept;

    /// \brief Must be called at each tick, poll SDL_Events queue
    ///   then call matched bindigs.
    ///   If a binding is matched several time, only the last event is called
    void update() const noexcept;

    static SDL_Event GenericFilter(uint32_t type, uint8_t flag = 0) noexcept;
    static SDL_Event KeyBoardFilter(uint32_t type, SDL_Keycode key) noexcept;

  private:

    /// \brief Functor that return true if both events are of the same type
    ///   and the same button / key if relevant
    struct EventMatcher {
      bool operator() (const SDL_Event& filter, const SDL_Event& event) const noexcept;
    };
    /// \brief Functor that hash an event according to it's type
    struct EventHasher {
      size_t operator() (const SDL_Event& e) const noexcept;
    };

    using CallbacksTable = std::unordered_map<
      SDL_Event,
      CallbackList,
      EventHasher,
      EventMatcher>;

    using Match = CallbacksTable::const_iterator;
    using MatchedEventsList = std::unordered_map<
      SDL_Event,
      Match,
      EventHasher,
      EventMatcher>;

    /// Table of active bindings 
    CallbacksTable _callbacks;
    /// Tmp table use to prevent multiple match of the same event
    ///   used to avoid allocation at each cicle
    mutable MatchedEventsList mut_eventsStack;
  };
}

#endif /* _SRC_IMPL_CONTROLLER_INPUT_HANDLER_H */