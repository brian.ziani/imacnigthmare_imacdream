/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Animation.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-06
 ///

#ifndef _SRC_IMPL_RENDER_MODEL_ANIMATION_H
#define _SRC_IMPL_RENDER_MODEL_ANIMATION_H

#include "Pose.hpp"
#include "Skeleton.hpp"

#include <unordered_set>
#include <unordered_map>
#include <optional>

namespace model {

  ///////////////////////////////////////////////////////////////////////

  /// \class Basic Concrete IPose implementation
  class KeyPose : public IPose {
  public:

    KeyPose() noexcept;
    KeyPose(const IPose& pose, const Skeleton& skeleton) noexcept;
    virtual ~KeyPose() noexcept = default;

    std::optional<glm::mat4> getKey(size_t boneID) const noexcept override;
    std::optional<glm::vec3> translation(size_t boneID) const noexcept override;
    std::optional<glm::quat> rotation(size_t boneID) const noexcept override;

    /// \brief Add translation key for given bone and timestamp
    /// \warning Keys must be added in growing timestamp order
    void addKey(size_t boneID, const glm::vec3& translation);
    /// \brief Add rotation key for given bone and timestamp
    /// \warning Keys must be added in growing timestamp order
    void addKey(size_t boneID, const glm::quat& rotation);

  private:

    std::unordered_map<size_t, glm::vec3> _translationKeys;
    std::unordered_map<size_t, glm::quat> _rotationKeys;
  };

  ///////////////////////////////////////////////////////////////////////

  class PoseInterpolator : public IPose {
  public:

    PoseInterpolator(const IPose* A, const IPose* B = nullptr, float t = 0.f) noexcept;
    virtual ~PoseInterpolator() noexcept = default;

    /// \brief Perform a linear interpolation between pose A and B for bone 'index'
    ///   with 't' the interpolation parameter in [0,1]
    ///   t == 0 returns A, t == 1 returns B
    std::optional<glm::mat4> getKey(size_t boneID) const noexcept override;
    std::optional<glm::vec3> translation(size_t boneID) const noexcept override;
    std::optional<glm::quat> rotation(size_t boneID) const noexcept override;

  private:

    const IPose* _A;
    const IPose* _B;
    float _t;
    bool _unary;
  };

  ///////////////////////////////////////////////////////////////////////
}

#endif /* _SRC_IMPL_RENDER_MODEL_ANIMATION_H */