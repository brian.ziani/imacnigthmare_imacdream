/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Skeleton.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-06
 ///

#include "Skeleton.hpp"
#include <libs/glm/Helpers.hpp>
#include <libs/utils/log.hpp>
#include <cassert>

namespace model {

  Skeleton::BoneNode::BoneNode(size_t index, const glm::mat4& transform, const glm::mat4& offset) noexcept :
    index(index), transform(transform), offset(offset), childs()
  {
  }

  Skeleton::Skeleton() noexcept :
    _root(nullptr),
    _initialTransform(glm::identity<glm::mat4>()),
    _rootInverseTransform(glm::identity<glm::mat4>()),
    _bonesTable()
  {
  }
  Skeleton::~Skeleton() noexcept
  {
    delete _root;
  }

  Skeleton::Skeleton(Skeleton&& s) noexcept :
    _root(s._root),
    _initialTransform(s._initialTransform),
    _rootInverseTransform(s._rootInverseTransform),
    _bonesTable(std::move(s._bonesTable))
  {
    s._root = nullptr;
  }
  Skeleton& Skeleton::operator= (Skeleton&& s) noexcept
  {
    _root = s._root;
    _initialTransform = s._initialTransform;
    _rootInverseTransform = s._rootInverseTransform;
    _bonesTable = std::move(s._bonesTable);

    s._root = nullptr;
    return *this;
  }

  Skeleton::BoneNode::~BoneNode() noexcept
  {
    for (auto child : childs) {
      delete child;
    }
  }

  void Skeleton::setInitialTransforms(const glm::mat4& baseTransform, const glm::mat4& rootInverse) noexcept
  {
    _initialTransform = baseTransform;
    _rootInverseTransform = rootInverse;
  }

  void Skeleton::addBone(size_t parent, size_t index, const glm::mat4& transform, const glm::mat4& offset) noexcept
  {
    if (nullptr == _root) {
      _root = new BoneNode(index, transform, offset);
      _bonesTable.emplace(index, _root);
    }
    else {
      BoneNode* tmp(new BoneNode(index, transform, offset));
      _bonesTable.at(parent)->childs.emplace_back(tmp);
      _bonesTable.emplace(index, tmp);
    }
  }

  void Skeleton::computeTransforms(const IPose& pose, std::vector<glm::mat4>& o_transforms) const noexcept
  {
    processNode(_root, _initialTransform, pose, o_transforms);
  }

  void Skeleton::processNode(const BoneNode* node, const glm::mat4& parentTransform, const IPose& pose, std::vector<glm::mat4>& o_transforms) const noexcept
  {
    assert(nullptr != node && "Invalid Node");

    glm::mat4 nodeTransformation(node->transform);
    if (const auto key = pose.getKey(node->index); key.has_value()) {
      nodeTransformation = key.value();
    }
    nodeTransformation = parentTransform * nodeTransformation;

    o_transforms[node->index] = _rootInverseTransform * nodeTransformation * node->offset;

    for (const auto& child : node->childs) {
      processNode(child, nodeTransformation, pose, o_transforms);
    }
  }

  Skeleton::BonesList::const_iterator Skeleton::begin() const noexcept
  {
    return _bonesTable.cbegin();
  }
  Skeleton::BonesList::const_iterator Skeleton::end() const noexcept
  {
    return _bonesTable.cend();
  }

  Skeleton::BonesList::const_iterator begin(const Skeleton& s) noexcept
  {
    return s.begin();
  }
  Skeleton::BonesList::const_iterator end(const Skeleton& s) noexcept
  {
    return s.end();
  }
}