/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Pose.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-06
 ///

#include "Pose.hpp"
#include <libs/glm/Helpers.hpp>

namespace model {


  const PoseEmpty* PoseEmpty::Get() noexcept
  {
    static const PoseEmpty _instance;
    return &_instance;
  }

  std::optional<glm::mat4> PoseEmpty::getKey(size_t) const noexcept
  {
    return std::nullopt;
  }
  std::optional<glm::vec3> PoseEmpty::translation(size_t) const noexcept
  {
    return std::nullopt;
  }
  std::optional<glm::quat> PoseEmpty::rotation(size_t) const noexcept
  {
    return std::nullopt;
  }

}