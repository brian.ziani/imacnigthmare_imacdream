/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Loader.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-11
 ///

#ifndef _SRC_IMPL_MODEL_LOADER_H
#define _SRC_IMPL_MODEL_LOADER_H

#include "Mesh.hpp"
#include "Pose.hpp"
#include "Animation.hpp"
#include "Skeleton.hpp"

#include <libs/physics/Primitives.hpp>

#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>

#include <string>
#include <memory>
#include <future>
#include <unordered_map>
#include <initializer_list>

namespace model {

  struct LoadResult {
    std::vector<std::shared_ptr<AMesh>> meshes;
    std::shared_ptr<AModel> prototype;
    std::unordered_map<size_t, std::vector<KeyPose>> poses;
  };

  struct LoadRequest {
    std::string path;
    std::unordered_map<std::string, std::pair<size_t,size_t>> poses;
    std::shared_ptr<glhelp::Texture<uint8_t>> defaultTexture;
    std::shared_ptr<Program> forcedShader;
    unsigned int flags;
    bool allowWeirdMeshes;
  };

  class Loader {
  public:

    LoadResult loadModel(const LoadRequest& request);
    std::vector<physics::Hexahedron> loadHitbox(const std::string& file);

  protected:

    enum class Kind {
      Static,
      Rigged
    };

    struct Material {

      Material() noexcept = default;

      size_t diffuse, shinyness;
    };

    struct PartialMesh {
      std::shared_ptr<AMesh> mesh;
      Material material;
      PartialMesh(const std::shared_ptr<AMesh>& me, const Material& ma) noexcept;
    };

    void init(const LoadRequest& request);
    LoadResult buildResult();
    LoadResult buildStatic();
    LoadResult buildRigged();

    void processNode(const aiNode* node, const glm::mat4& transform);
    void processMesh(const aiMesh* mesh);
    void processRiggedMesh(const aiMesh* mesh);
    void processStaticMesh(const aiMesh* mesh);

    void readSkeleton(const aiNode* node, size_t parentID);
    void loadPoses();

    Material processMaterial(const aiMaterial* material);
    size_t readTexture(const aiMaterial* material, aiTextureType type);

    Assimp::Importer _importer;
    const aiScene* _scene;

    const LoadRequest* _request;

    Kind _kind;

    std::vector<PartialMesh> _meshes;
    std::unordered_map<size_t, std::vector<KeyPose>> _poses;

    std::vector<std::shared_future<std::shared_ptr<glhelp::Texture<uint8_t>>>> _textures;
    std::unordered_map<std::string, size_t> _texturesTable;

    bool _armatureRootFound;
    const aiNode* _skeletonRoot;
    std::vector<glm::mat4> _bonesOffsets;
    std::unordered_map<std::string, size_t> _bonesTable;
    std::shared_ptr<Skeleton> _skeleton;
  };

}

#endif /* _SRC_IMPL_MODEL_LOADER_H */