/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Mesh.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-10
 ///

#ifndef _SRC_IMPL_RENDER_MODEL_MODEL_H
#define _SRC_IMPL_RENDER_MODEL_MODEL_H

#include <libs/glwrap/Mesh.hpp>
#include <libs/glwrap/Texture.hpp>
#include <libs/glwrap/Program.hpp>

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#include <memory>

namespace model {

  class Loader;

  ////////////////////////////////////////////////////////////////

  class Program : protected glhelp::Program {
  public:

    enum class DrawMode {
      Default = 0,
      Depth = 1
    };

    static const std::shared_ptr<Program>& Get() noexcept;
    static void load(const std::string& vs, const std::string& fs);

    Program(const std::string& vs, const std::string& fs);
    virtual ~Program() noexcept = default;

    virtual void bind() const;

    void setModelMatrix(const glm::mat4& m) const;
    void setNomalMatrix(const glm::mat4& m) const;
    void setMVPMatrix(const glm::mat4& m) const;

    void setDiffuseTexture(GLuint id) const;
    void setShinynessTexture(GLuint id) const;

    void setDrawMode(DrawMode mode) const;

    virtual void unbind() const;

  private:

    static std::shared_ptr<Program> _instance;

    GLuint sampler;
    GLuint uMmatrix, uNormalMatrix, uMVPmatrix;
    GLuint uDiffuse, uShinyness;
    GLuint uMode;
  };

  ////////////////////////////////////////////////////////////////

  class AMesh {
  public:

    class IInstance {
    public:

      virtual ~IInstance() noexcept = default;

      virtual void bind(const glm::mat4& VP) const = 0;
      virtual void unbind() const = 0;
      virtual std::shared_ptr<AMesh> mesh() const noexcept = 0;

      virtual void setModelMatrix(const glm::mat4& M) noexcept = 0;
    };

    AMesh(const std::shared_ptr<Program>& program) noexcept;
    virtual ~AMesh() noexcept = default;

    void draw(const glm::mat4& VP, const IInstance& I) const;

    virtual void bind(Program::DrawMode mode) const;
    virtual void unbind() const;

  protected:

    virtual void drawCall() const noexcept = 0;

    friend class Loader;

    std::shared_ptr<Program> _program;
    std::shared_ptr<glhelp::Texture<uint8_t>> _diffuse;
    std::shared_ptr<glhelp::Texture<uint8_t>> _shinyness;
  };

  ////////////////////////////////////////////////////////////////

  class AModel {
  public:

    using PartList = std::vector<std::shared_ptr<AMesh::IInstance>>;

    virtual std::shared_ptr<AModel> clone() const = 0;

    template <typename T>
    std::shared_ptr<T> cloneAs() const;

    virtual void setModelMatrix(const glm::mat4& M) noexcept;

    PartList::const_iterator begin() const noexcept;
    PartList::const_iterator end() const noexcept;

  protected:

    std::vector<std::shared_ptr<AMesh::IInstance>> _parts;
  };

  AModel::PartList::const_iterator begin(const AModel& m) noexcept;
  AModel::PartList::const_iterator end(const AModel& m) noexcept;
}

//////////////////////////////////////////////////////////

namespace model {

  template <typename T>
  std::shared_ptr<T> AModel::cloneAs() const
  {
    return std::static_pointer_cast<T>(this->clone());
  }
}

#endif /* _SRC_IMPL_RENDER_MODEL_MODEL_H */