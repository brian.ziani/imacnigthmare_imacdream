/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Skeleton.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-06
 ///

#ifndef _SRC_IMPL_RENDER_MODELS_SKELETON_H
#define _SRC_IMPL_RENDER_MODELS_SKELETON_H

#include "Pose.hpp"
#include <unordered_map>
#include <vector>

namespace model {

  /// \class Class used to represent a Mesh's skeleton and compute deformations
  class Skeleton {
  public:

    struct BoneNode {

      BoneNode(size_t index, const glm::mat4& transform, const glm::mat4& offset) noexcept;
      ~BoneNode() noexcept;

      size_t index;
      glm::mat4 transform;
      glm::mat4 offset;
      std::vector<BoneNode*> childs;
    };

    using BonesList = std::unordered_map<size_t, BoneNode*>;

    Skeleton() noexcept;
    ~Skeleton() noexcept;

    Skeleton(Skeleton&& s) noexcept;
    Skeleton& operator= (Skeleton&& s) noexcept;

    Skeleton(const Skeleton&) noexcept = delete;
    Skeleton& operator= (const Skeleton&) noexcept = delete;

    void setInitialTransforms(const glm::mat4& baseTransform, const glm::mat4& rootInverse) noexcept;
    void addBone(size_t parent, size_t index, const glm::mat4& transform, const glm::mat4& offset) noexcept;

    void computeTransforms(const IPose& pose, std::vector<glm::mat4>& o_transforms) const noexcept;

    BonesList::const_iterator begin() const noexcept;
    BonesList::const_iterator end() const noexcept;

  private:

    void processNode(const BoneNode* node, const glm::mat4& parentTransform, const IPose& pose, std::vector<glm::mat4>& o_transforms) const noexcept;

    BoneNode* _root;
    glm::mat4 _initialTransform;
    glm::mat4 _rootInverseTransform;
    std::unordered_map<size_t, BoneNode*> _bonesTable;
  };

  Skeleton::BonesList::const_iterator begin(const Skeleton& s) noexcept;
  Skeleton::BonesList::const_iterator end(const Skeleton& s) noexcept;
}

#endif /* _SRC_IMPL_RENDER_MODELS_SKELETON_H */