/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Mesh.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-10
 ///

#include "Mesh.hpp"

namespace model {

  std::shared_ptr<Program> Program::_instance = nullptr;

  const std::shared_ptr<Program>& Program::Get() noexcept
  {
    return _instance;
  }
  void Program::load(const std::string& vs, const std::string& fs)
  {
    _instance = std::make_shared<Program>(vs, fs);
  }

  Program::Program(const std::string& vs, const std::string& fs) :
    glhelp::Program(vs, fs),

    sampler(0),

    uMmatrix(uniform("uMmatrix")),
    uNormalMatrix(uniform("uNormalMatrix")),
    uMVPmatrix(uniform("uMVPmatrix")),

    uDiffuse(uniform("uDiffuse")),
    uShinyness(uniform("uSpecular")),

    uMode(uniform("uMode"))
  {
    glGenSamplers(1, &sampler);
    glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glhelp::checkGLError();
  }

  void Program::bind() const
  {
    use();

    glBindSampler(0, sampler);
    setInt(uDiffuse, 0);

    glBindSampler(1, sampler);
    setInt(uShinyness, 1);

    glhelp::checkGLError();
  }

  void Program::setModelMatrix(const glm::mat4& m) const
  {
    setMat4(uMmatrix, m);
    glhelp::checkGLError();
  }
  void Program::setNomalMatrix(const glm::mat4& m) const
  {
    setMat4(uNormalMatrix, m);
    glhelp::checkGLError();
  }
  void Program::setMVPMatrix(const glm::mat4& m) const
  {
    setMat4(uMVPmatrix, m);
    glhelp::checkGLError();
  }

  void Program::setDiffuseTexture(GLuint id) const
  {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, id);
    glhelp::checkGLError();
  }
  void Program::setShinynessTexture(GLuint id) const
  {
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, id);
    glhelp::checkGLError();
  }
  void Program::setDrawMode(DrawMode mode) const
  {
    setInt(uMode, static_cast<int32_t>(mode));
  }

  void Program::unbind() const
  {
    for (unsigned int i = 0; i < 3; ++i) {
      glActiveTexture(GL_TEXTURE0 + i);
      glBindTexture(GL_TEXTURE_2D, 0);
      glBindSampler(i, 0);
    }
    glhelp::checkGLError();
  }

  ////////////////////////////////////////////////////////////////

  AMesh::AMesh(const std::shared_ptr<Program>& program) noexcept :
    _program(program), _diffuse(nullptr), _shinyness(nullptr)
  {
  }

  void AMesh::draw(const glm::mat4& VP, const IInstance& I) const
  {
    I.bind(VP);
    drawCall();
    I.unbind();
  }

  void AMesh::bind(Program::DrawMode mode) const
  {
    _program->bind();
    _program->setDiffuseTexture(_diffuse->glid());
    _program->setShinynessTexture(_shinyness->glid());
    _program->setDrawMode(mode);
  }
  void AMesh::unbind() const
  {
    _program->unbind();
  }

  ////////////////////////////////////////////////////////////////

  AModel::PartList::const_iterator begin(const AModel& m) noexcept
  {
    return m.begin();
  }
  AModel::PartList::const_iterator end(const AModel& m) noexcept
  {
    return m.end();
  }
  AModel::PartList::const_iterator AModel::begin() const noexcept
  {
    return _parts.begin();
  }
  AModel::PartList::const_iterator AModel::end() const noexcept
  {
    return _parts.end();
  }

  void AModel::setModelMatrix(const glm::mat4& M) noexcept
  {
    for (auto& instance : _parts) {
      instance->setModelMatrix(M);
    }
  }

}