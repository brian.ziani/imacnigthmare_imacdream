/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Loader.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-11
 ///

#include "Loader.hpp"

#include "impl/Rigged.hpp"
#include "impl/Static.hpp"

#include <libs/glm/Helpers.hpp>
#include <libs/assimp/Helpers.hpp>
#include <libs/utils/log.hpp>
#include <glm/gtc/quaternion.hpp>

#include <regex>

namespace model {

  Loader::PartialMesh::PartialMesh(
    const std::shared_ptr<AMesh>& me, const Material& ma) noexcept :
    mesh(me), material(ma)
  {
  }


  LoadResult Loader::loadModel(const LoadRequest& request)
  {
    init(request);
    /* Load Scene */
    _scene = _importer.ReadFile(_request->path, _request->flags);
    if (
      !_scene
      || _scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE
      || !_scene->mRootNode
      ) {
      throw std::runtime_error(_request->path + ":" + _importer.GetErrorString());
    }
    /* add the error texture */
    _textures.emplace_back( 
      std::async(
        std::launch::deferred,
        [](auto t) -> auto {return t;},
        _request->defaultTexture));
    /* process the scene tree */
    processNode(_scene->mRootNode, glm::identity<glm::mat4>());
    if (nullptr != _skeletonRoot && _kind == Kind::Rigged) {
      readSkeleton(_skeletonRoot, 0);
      loadPoses();
    }
    /* done */
    return buildResult();
  }
  std::vector<physics::Hexahedron> Loader::loadHitbox(const std::string& file)
  {
    std::vector<physics::Hexahedron> primitives;
    _scene = _importer.ReadFile(file, aiProcess_JoinIdenticalVertices | aiProcess_FlipWindingOrder);
    if (
      !_scene
      || _scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE
      || !_scene->mRootNode
      ) {
      throw std::runtime_error(_request->path + ":" + _importer.GetErrorString());
    }
    primitives.assign(_scene->mNumMeshes, physics::Hexahedron{});
    for (size_t m = 0; m < _scene->mNumMeshes; ++m) {
      const aiMesh& mesh(*_scene->mMeshes[m]);
      if (mesh.mNumFaces != 6 || mesh.mNumVertices != 8) {
        throw std::runtime_error("Invalid hitbox mesh :" + file);
      }
      for (int v = 0; v < 8; ++v) {
        primitives[m].vertices[v] = helpers::assimpToGlmVec3(mesh.mVertices[v]);
      }
      for (int f = 0;f < 6; ++f) {
        for (int i = 0; i < 4; ++i) {
          if (4 != mesh.mFaces[f].mNumIndices) {
            throw std::runtime_error("Invalid hitbox format :" + file);
          }
          primitives[m].faces[f][i] = mesh.mFaces[f].mIndices[i];
        }
      }
    }
    return primitives;
  }

  void Loader::init(const LoadRequest& request)
  {
    _scene = nullptr;

    _request = &request;

    _kind = 0 < _request->poses.size() ? Kind::Rigged : Kind::Static;

    _meshes.clear();
    _poses.clear();

    _textures.clear();
    _texturesTable.clear();

    _armatureRootFound = false;
    _skeletonRoot = nullptr;
    _bonesOffsets.clear();
    _bonesTable.clear();
    _skeleton.reset();
  }

  LoadResult Loader::buildResult()
  {
    /* wait for texture loading ends */
    for (auto& futureTexture : _textures) {
      futureTexture.wait();
      futureTexture.get()->build();
    }
    /* build result */
    switch (_kind) {
    case Kind::Rigged:
      return buildRigged();
      break;
    case Kind::Static:
      return buildStatic();
      break;
    default:
      assert(0 && "Invalid Kind");
      return LoadResult{};
    }
  }

  LoadResult Loader::buildRigged()
  {
    LoadResult result;
    auto model(std::make_shared<impl::RiggedModel>());

    model->_pose = std::make_shared<std::vector<glm::mat4>>(
      _bonesTable.size(), glm::identity<glm::mat4>());
    _skeleton->computeTransforms(*PoseEmpty::Get(), *model->_pose);

    for (auto& mesh : _meshes) {
      mesh.mesh->_diffuse = _textures[mesh.material.diffuse].get();
      mesh.mesh->_shinyness = _textures[mesh.material.shinyness].get();
      auto tmp(static_pointer_cast<impl::RiggedMesh>(mesh.mesh));
      tmp->_skeleton = _skeleton;
      auto part(std::make_shared<impl::RiggedMesh::Instance>(
        model->_pose, tmp));
      result.meshes.emplace_back(tmp);
      model->_parts.emplace_back(part);
    }
    result.prototype = model;
    result.poses = std::move(_poses);
    return result;
  }

  LoadResult Loader::buildStatic()
  {
    LoadResult result;
    auto model(std::make_shared<impl::StaticModel>());
    for (auto& mesh : _meshes) {
      mesh.mesh->_diffuse = _textures[mesh.material.diffuse].get();
      mesh.mesh->_shinyness = _textures[mesh.material.shinyness].get();
      auto tmp(static_pointer_cast<impl::StaticMesh>(mesh.mesh));
      auto part(std::make_shared<impl::StaticMesh::Instance>(tmp));
      result.meshes.emplace_back(tmp);
      model->_parts.emplace_back(part);
    }
    result.prototype = model;
    return result;
  }

  void Loader::processNode(const aiNode* node, const glm::mat4& transform)
  {
    LOG::debug("Node :", node->mName, " : ", node->mNumMeshes, node->mTransformation);
    if (0 < node->mNumMeshes) {
      for (GLuint i = 0; i < node->mNumMeshes; i++) {
        processMesh(_scene->mMeshes[node->mMeshes[i]]);
      }
    }
    else if (!_armatureRootFound && 0 == strcmp("Armature", node->mName.C_Str())) {
      _armatureRootFound = true;
    }
    else if (_armatureRootFound) {
      _skeletonRoot = node;
      _skeleton = std::make_shared<Skeleton>();
      _skeleton->setInitialTransforms(
        transform * helpers::assimpToGlmMatrix(node->mTransformation),
        glm::inverse(transform * helpers::assimpToGlmMatrix(node->mTransformation)));
      return; // We keep this processing for later
    }
    for (GLuint i = 0; i < node->mNumChildren; i++) {
      processNode(node->mChildren[i], transform * helpers::assimpToGlmMatrix(node->mTransformation));
    }
    LOG::debug("-");
  }

  void Loader::processMesh(const aiMesh* mesh)
  {
    switch (_kind) {
    case Kind::Rigged:
      processRiggedMesh(mesh);
      break;
    case Kind::Static:
      processStaticMesh(mesh);
      break;
    default:
      assert(0 && "Invalid Kind");
    }
  }

  void Loader::processRiggedMesh(const aiMesh* mesh)
  {
    std::shared_ptr<Program> pgm;
    if (nullptr != _request->forcedShader) {
      pgm = _request->forcedShader;
    }
    else {
      pgm = impl::RiggedProgram::Get();
    }
    auto tmp(std::make_shared<impl::RiggedMesh>(pgm));
    tmp->_mesh = std::make_shared<glhelp::Mesh<impl::RiggedVertex>>(
      mesh->mNumVertices, mesh->mNumFaces * 3);
    /* vertices */
    for (size_t i = 0; i < mesh->mNumVertices; ++i) {
      impl::RiggedVertex v;
      v.position = helpers::assimpToGlmVec3(mesh->mVertices[i]);
      v.normal = helpers::assimpToGlmVec3(mesh->mNormals[i]);
      if (mesh->mTextureCoords[0]) {
        v.uv = helpers::assimpToGlmVec3(mesh->mTextureCoords[0][i]);
      }
      else {
        v.uv = glm::vec2(0.f);
      }
      for (size_t i = 0; i < 4; ++i) {
        v.boneIDs[i] = 0;
        v.boneWeights[i] = 0.f;
      }
      tmp->_mesh->vertices.emplace_back(v);
    }
    /* bones */
    for (size_t i = 0; i < mesh->mNumBones; ++i) {
      const aiBone* bone(mesh->mBones[i]);
      std::string boneName(bone->mName.C_Str());
      auto itr(_bonesTable.find(boneName));
      if (_bonesTable.end() == itr) {
        size_t index(_bonesOffsets.size());
        _bonesOffsets.emplace_back(helpers::assimpToGlmMatrix(bone->mOffsetMatrix));
        itr = _bonesTable.emplace_hint(itr, boneName, index);
      }
      for (size_t w = 0; w < bone->mNumWeights; ++w) {
        tmp->_mesh->vertices[bone->mWeights[w].mVertexId].tryAddBone(itr->second, bone->mWeights[w].mWeight);
      }
    }
    /* normalize bones weights */
    for (auto& v : tmp->_mesh->vertices) {
      float sum(0.f);
      for (size_t i = 0; i < 4; ++i) {
        sum += v.boneWeights[i];
      }
      if (sum != 0.f) {
        for (size_t i = 0; i < 4; ++i) {
          v.boneWeights[i] /= sum;
        }
      }
    }
    /* indices */
    for (size_t i = 0; i < mesh->mNumFaces; ++i) {
      if (int n = mesh->mFaces[i].mNumIndices; !_request->allowWeirdMeshes && n != 3) {
        throw std::runtime_error("Invalid Mesh Format : " + std::to_string(n));
      }
      for (size_t k = 0; k < 3; ++k) {
        tmp->_mesh->indices.emplace_back(mesh->mFaces[i].mIndices[k]);
      }
    }
    tmp->_mesh->build();
    /* materials */
    Material material = processMaterial(_scene->mMaterials[mesh->mMaterialIndex]);
    /* done */
    _meshes.emplace_back(tmp, material);
  }
  void Loader::processStaticMesh(const aiMesh* mesh)
  {
    std::shared_ptr<Program> pgm;
    if (nullptr != _request->forcedShader) {
      pgm = _request->forcedShader;
    }
    else {
      pgm = Program::Get();
    }
    auto tmp(std::make_shared<impl::StaticMesh>(pgm));
    tmp->_mesh = std::make_shared<glhelp::Mesh<impl::StaticVertex>>(
      mesh->mNumVertices, mesh->mNumFaces * 3);
    /* vertices */
    for (size_t i = 0; i < mesh->mNumVertices; ++i) {
      impl::StaticVertex v;
      v.position = helpers::assimpToGlmVec3(mesh->mVertices[i]);
      v.normal = helpers::assimpToGlmVec3(mesh->mNormals[i]);

      if (mesh->mTextureCoords[0]) {
        v.uv = helpers::assimpToGlmVec3(mesh->mTextureCoords[0][i]);
      }
      else {
        v.uv = glm::vec2(0.f);
      }
      tmp->_mesh->vertices.emplace_back(v);
    }
    /* indices */
    for (size_t i = 0; i < mesh->mNumFaces; ++i) {
      if (int n = mesh->mFaces[i].mNumIndices; !_request->allowWeirdMeshes && n != 3) {
        throw std::runtime_error("Invalid Mesh Format : " + std::to_string(n));
      }
      for (size_t k = 0; k < 3; ++k) {
        tmp->_mesh->indices.emplace_back(mesh->mFaces[i].mIndices[k]);
      }
    }
    tmp->_mesh->build();
    /* materials */
    Material material = processMaterial(_scene->mMaterials[mesh->mMaterialIndex]);
    /* done */
    _meshes.emplace_back(tmp, material);
  }

  void Loader::readSkeleton(const aiNode* node, size_t parentID)
  {
    std::string nodeName(node->mName.C_Str());
    auto itr(_bonesTable.find(nodeName));
    if (_bonesTable.end() == itr) {
      size_t index(_bonesOffsets.size());
      _bonesOffsets.emplace_back(glm::identity<glm::mat4>());
      itr = _bonesTable.emplace_hint(itr, nodeName, index);
    }
    size_t boneID(itr->second);
    _skeleton->addBone(parentID, boneID, helpers::assimpToGlmMatrix(node->mTransformation), _bonesOffsets[boneID]);
    for (size_t i = 0; i < node->mNumChildren; ++i) {
      readSkeleton(node->mChildren[i], boneID);
    }
  }
  void Loader::loadPoses()
  {
    for (size_t i = 0; i < _scene->mNumAnimations; ++i) {
      const aiAnimation* animation(_scene->mAnimations[i]);
      const std::string animName(animation->mName.C_Str());
      if (auto itr(_request->poses.find(animName)); itr != _request->poses.end()) {

        auto [poseID, framesCount] = itr->second;
        std::vector<KeyPose> poseStack;
        poseStack.reserve(framesCount);

        for (size_t f = 0; f < framesCount; ++f) {

          KeyPose tmp;
          for (size_t k = 0; k < animation->mNumChannels; ++k) {

            const aiNodeAnim* channel(animation->mChannels[k]);
            if (
              auto itr = _bonesTable.find(channel->mNodeName.C_Str());
              itr != _bonesTable.end()) {

              if (size_t n = channel->mNumPositionKeys; 0 < n) {
                if (1 < framesCount)
                  n = std::min(n - 1, (f * (n - 1)) / (framesCount - 1));
                else
                  n = n - 1;
                tmp.addKey(
                  itr->second,
                  helpers::assimpToGlmVec3(channel->mPositionKeys[n].mValue)
                );
              }

              if (size_t n = channel->mNumRotationKeys; 0 < n) {
                if (1 < framesCount)
                  n = std::min(n - 1, (f * (n - 1)) / (framesCount - 1));
                else
                  n = n - 1;
                tmp.addKey(
                  itr->second,
                  helpers::assimpToGlmQuat(channel->mRotationKeys[n].mValue)
                );
              }

            }
          }
          poseStack.emplace_back(std::move(tmp));
        }

        if (poseStack.size() != framesCount) {
          throw std::runtime_error(
            _request->path
            + "Failed to load requested frame count :"
            + std::to_string(poseStack.size())
            + std::to_string(framesCount));
        }
        _poses.emplace_hint(_poses.end(), itr->second.first, std::move(poseStack));
      }
      else {
        LOG::warning(_request->path, ": Unkonwn animation :", animName, "... Ignored");
      }
    }
    for (const auto& [name, ID] : _request->poses) {
      if (auto itr(_poses.find(ID.first)); itr == _poses.end()) {
        throw std::runtime_error(_request->path + " : Missing Pose : " + name);
      }
    }
  }

  Loader::Material Loader::processMaterial(const aiMaterial* material)
  {
    Material tmp;
    for (size_t i = 0; i < material->mNumProperties; ++i) {
      LOG::debug(" :", material->mProperties[i]->mKey, material->mProperties[i]->mType, material->mProperties[i]->mDataLength);
    }
    tmp.diffuse = readTexture(material, aiTextureType_DIFFUSE);
    tmp.shinyness = readTexture(material, aiTextureType_SHININESS);
    return tmp;
  }

  template <typename T>
  std::tuple<float, float, float> readColors(const char* datas) noexcept
  {
    const T* array(reinterpret_cast<const T*>(datas));
    return { array[0] * 255.f, array[1] * 255.f, array[2] * 255.f };
  }

  size_t Loader::readTexture(const aiMaterial* material, aiTextureType type)
  {
    if (0 == material->GetTextureCount(type)) {
      if (aiTextureType_DIFFUSE != type) {
        return 0;
      }
      else {
        bool colorfound(false);
        std::function<std::tuple<float, float, float>()> colors;
        for (size_t i = 0; !colorfound && i < material->mNumProperties; ++i) {
          const aiMaterialProperty* prop(material->mProperties[i]);
          std::string name(prop->mKey.C_Str());
          if (0 != strcmp("$clr.diffuse", name.c_str()))
            continue;
          if (aiPropertyTypeInfo::aiPTI_Float == prop->mType) {
            colors = std::bind_front(readColors<float>, prop->mData);
          }
          else if (aiPropertyTypeInfo::aiPTI_Double == prop->mType) {
            colors = std::bind_front(readColors<double>, prop->mData);
          }
          else {
            throw std::runtime_error("Unsuported color format");
          }
          colorfound = true;
          break;
        }
        if (!colorfound)
          return 0;
        auto [red, blue, green] = colors();
        size_t index(_textures.size());
        _textures.emplace_back(
          std::async(
            std::launch::async,
            [](float r, float g, float b) -> auto {
          auto texture(std::make_shared<glhelp::Texture<uint8_t>>(1, 1, 3));
          texture->pixels.emplace_back(r);
          texture->pixels.emplace_back(g);
          texture->pixels.emplace_back(b);
          return texture;
        }, red, blue, green));
        return index;
      }
    }
    else {
      aiString tmp;
      material->GetTexture(type, 0, &tmp);
      const std::string path(tmp.C_Str());
      LOG::debug("Texture :", path);
      auto itr(_texturesTable.find(path));
      if (_texturesTable.end() == itr) {
        size_t index(_textures.size());
        /* asynchronous texture loading */
        std::string fullpath;
        if (path[0] == '/') {
          std::regex fileregex(
            ".*(\\/[a-z_0-9]*\\.[a-z]*).*",
            std::regex_constants::icase);
          std::smatch match;
          if (std::regex_match(path, match, fileregex)) {
            fullpath = "../res/models" + std::string(match[1]);
          }
          else {
            LOG::warning("Absolute Texture path cannot be resolved... :", path);
            return 0;
          }
        }
        else {
          fullpath = "../res/models/" + path;
        }
        _textures.emplace_back(
          std::async(
            std::launch::async,
            [](const std::string& path) -> auto {
          return std::make_shared<glhelp::Texture<uint8_t>>(
            glhelp::readTextureFromFile(path));
        }, fullpath));
        itr = _texturesTable.emplace_hint(itr, path, index);
      }
      return itr->second;
    }
  }
}