/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Pose.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-06
 ///

#ifndef _SRC_IMPL_RENDER_MODELS_POSE_H
#define _SRC_IMPL_RENDER_MODELS_POSE_H

#include <glm/vec3.hpp>
#include <glm/gtx/quaternion.hpp>
#include <optional>
#include <ostream>

namespace model {

  /// \class Interface used to represent a skeleton's pose
  ///   A pose is a set of keys for each bones
  class IPose {
  public:

    virtual ~IPose() noexcept = default;

    /// \brief Must return pose key for given bone if such exist
    virtual std::optional<glm::mat4> getKey(size_t index) const noexcept = 0;
    virtual std::optional<glm::vec3> translation(size_t boneID) const noexcept = 0;
    virtual std::optional<glm::quat> rotation(size_t boneID) const noexcept = 0;
  };

  class PoseEmpty : public IPose {
  public:

    /// \brief Return Empty pose singleton
    static const PoseEmpty* Get() noexcept;

    virtual ~PoseEmpty() noexcept = default;

    /// \brief Must return pose key for given bone if such exist
    std::optional<glm::mat4> getKey(size_t) const noexcept override;
    std::optional<glm::vec3> translation(size_t) const noexcept override;
    std::optional<glm::quat> rotation(size_t) const noexcept override;
  };

}

#endif /* _SRC_IMPL_RENDER_MODELS_POSE_H */