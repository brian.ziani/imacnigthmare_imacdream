/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Animation.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-06
 ///

#include "Animation.hpp"
#include <glm/gtc/quaternion.hpp>
#include <algorithm>
#include <stdexcept>

namespace {
  template <typename T>
  std::optional<T> getIfExist(
    size_t boneID,
    const std::unordered_map<size_t, T>& keys)
  {
    auto itr(keys.find(boneID));
    if (keys.end() == itr) {
      return std::nullopt;
    }
    else {
      return std::make_optional<T>(itr->second);
    }
  }
  std::optional<glm::mat4> interpolatedTranslation(const std::optional<glm::vec3>& tmp) noexcept
  {
    if (tmp.has_value()) {
      return std::make_optional<glm::mat4>(
        glm::translate(glm::identity<glm::mat4>(), tmp.value())
        );
    }
    else {
      return std::nullopt;
    }
  }
  std::optional<glm::mat4> interpolatedRotation(const std::optional<glm::quat>& tmp) noexcept
  {
    if (tmp.has_value()) {
      return std::make_optional<glm::mat4>(
        glm::toMat4(tmp.value())
        );
    }
    else {
      return std::nullopt;
    }
  }
}

namespace model {

  //////////////////////////////////////////////////////////////////

  KeyPose::KeyPose() noexcept : _translationKeys(), _rotationKeys()
  {
  }
  KeyPose::KeyPose(const IPose& pose, const Skeleton& skeleton) noexcept :
    KeyPose()
  {
    for (const auto& [id, _] : skeleton) {
      if (auto T = pose.translation(id); T.has_value()) {
        addKey(id, T.value());
      }
      if (auto R = pose.rotation(id); R.has_value()) {
        addKey(id, R.value());
      }
    }
  }

  void KeyPose::addKey(size_t boneID, const glm::vec3& translation)
  {
    auto res(_translationKeys.emplace(boneID, translation));
    if (!res.second) {
      throw std::runtime_error("Duplicated Bone T Key : " + std::to_string(boneID));
    }
  }
  void KeyPose::addKey(size_t boneID, const glm::quat& rotation)
  {
    auto res(_rotationKeys.emplace(boneID, rotation));
    if (!res.second) {
      throw std::runtime_error("Duplicated Bone R Key : " + std::to_string(boneID));
    }
  }
  std::optional<glm::mat4> KeyPose::getKey(size_t boneID) const noexcept
  {
    auto T(interpolatedTranslation(translation(boneID)));
    auto R(interpolatedRotation(rotation(boneID)));

    if (!T.has_value() && !R.has_value()) {
      return std::nullopt;
    }
    else {
      return std::make_optional<glm::mat4>(
        T.value_or(glm::identity<glm::mat4>())
        * R.value_or(glm::identity<glm::mat4>())
        );
    }
  }
  std::optional<glm::vec3> KeyPose::translation(size_t boneID) const noexcept
  {
    return getIfExist(boneID, _translationKeys);
  }
  std::optional<glm::quat> KeyPose::rotation(size_t boneID) const noexcept
  {
    return getIfExist(boneID, _rotationKeys);
  }

  //////////////////////////////////////////////////////////////////

  PoseInterpolator::PoseInterpolator(const IPose* A, const IPose* B, float t) noexcept :
    _A(A), _B(B ? B : A), _t(t), _unary(A == B)
  {
  }

  std::optional<glm::mat4> PoseInterpolator::getKey(size_t boneID) const noexcept
  {
    if (_unary) {
      return _A->getKey(boneID);
    }
    else {
      auto T(interpolatedTranslation(translation(boneID)));
      auto R(interpolatedRotation(rotation(boneID)));

      if (!T.has_value() && !R.has_value()) {
        return std::nullopt;
      }
      else {
        return std::make_optional<glm::mat4>(
          T.value_or(glm::identity<glm::mat4>())
          * R.value_or(glm::identity<glm::mat4>())
          );
      }
    }
  }
  std::optional<glm::vec3> PoseInterpolator::translation(size_t boneID) const noexcept
  {
    auto a(_A->translation(boneID));
    auto b(_B->translation(boneID));

    if (!a.has_value() && !b.has_value()) {
      return std::nullopt;
    }
    else {
      glm::vec3 tmp;
      if (!a.has_value()) {
        tmp = b.value();
      }
      else if (!b.has_value()) {
        tmp = a.value();
      }
      else {
        tmp = (1.f - _t) * a.value() + _t * b.value();
      }
      return std::make_optional<glm::vec3>(tmp);
    }
  }
  std::optional<glm::quat> PoseInterpolator::rotation(size_t boneID) const noexcept
  {
    auto a(_A->rotation(boneID));
    auto b(_B->rotation(boneID));

    if (!a.has_value() && !b.has_value()) {
      return std::nullopt;
    }
    else if (!a.has_value()) {
      return b.value();
    }
    else if (!b.has_value()) {
      return a.value();
    }
    else {
      return std::make_optional<glm::quat>(
        glm::slerp(a.value(), b.value(), _t)
        );
    }
  }

  //////////////////////////////////////////////////////////////////
}