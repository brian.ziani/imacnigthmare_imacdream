/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Renderer.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#include "Renderer.hpp"
#include <libs/glwrap/Helpers.hpp>

namespace model {

  void Renderer::registerMesh(const std::vector<std::shared_ptr<model::AMesh>>& meshes)
  {
    for (auto& mesh : meshes) {
      if (auto itr(_renderableList.find(mesh)); itr == _renderableList.end()) {
        itr = _renderableList.emplace_hint(itr, mesh, InstanceList());
      }
      else {
        throw DuplicatedMesh();
      }
    }
  }
  void Renderer::addDrawable(const std::shared_ptr<model::AModel>& obj)
  {
    for (const auto& part : *obj) {
      auto [itr, result] = _renderableList.at(part->mesh()).emplace(part);
      if (!result) {
        throw DuplicatedDrawable();
      }
    }
  }
  void Renderer::removeDrawable(const std::shared_ptr<model::AModel>& obj)
  {
    for (const auto& part : *obj) {
      _renderableList.at(part->mesh()).erase(part);
    }
  }

  void Renderer::drawCall(model::Program::DrawMode mode, const glm::mat4& VP) const
  {
    for (const auto& [model, instanceList] : _renderableList) {
      model->bind(mode);
      for (const auto& instance : instanceList) {
        model->draw(VP, *instance);
        glhelp::checkGLError();
      }
      model->unbind();
    }
  }
}