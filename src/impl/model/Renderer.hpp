/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Renderer.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#ifndef _SRC_IMPL_MODEL_RENDERER_H
#define _SRC_IMPL_MODEL_RENDERER_H

#include "Mesh.hpp"

#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <vector>

namespace model {

  class Renderer {
  public:

    struct DuplicatedMesh {};
    struct DuplicatedDrawable {};

    void registerMesh(const std::vector<std::shared_ptr<model::AMesh>>& meshes);
    void addDrawable(const std::shared_ptr<model::AModel>& obj);
    void removeDrawable(const std::shared_ptr<model::AModel>& obj);

    void drawCall(model::Program::DrawMode mode, const glm::mat4& VP) const;

  private:

    using MeshPtr = std::shared_ptr<model::AMesh>;
    using InstanceList = std::unordered_set<std::shared_ptr<model::AMesh::IInstance>>;
    using RenderableList = std::unordered_map<MeshPtr, InstanceList>;

    RenderableList _renderableList;
  };
}

#endif /* _SRC_IMPL_MODEL_RENDERER_H */