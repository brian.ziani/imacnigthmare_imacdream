/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Static.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-11
 ///

#ifndef _SRC_IMPL_MODEL_MODELS_STATIC_H
#define _SRC_IMPL_MODEL_MODELS_STATIC_H

#include "Base.hpp"

#include <libs/glwrap/Mesh.hpp>
#include <glm/mat4x4.hpp>
#include <memory>

namespace model {
  namespace impl {

    /// \brief VBO datas associated with static mesh
    struct StaticVertex {
      glm::vec3 position;
      glm::vec3 normal;
      glm::vec2 uv;

      static void bindAttributes();
    };

    class StaticMesh : public BaseMesh<StaticMesh, StaticVertex, Program> {
    public:

      using Base = BaseMesh<StaticMesh, StaticVertex, Program>;

      StaticMesh(const std::shared_ptr<Program>& pgm = Program::Get()) noexcept;
      virtual ~StaticMesh() noexcept = default;

    private:

      friend class ::model::Loader;
    };

    class StaticModel : public AModel {
    public:

      std::shared_ptr<AModel> clone() const override;

    protected:

      friend class ::model::Loader;
    };
  }
}

#endif /* _SRC_IMPL_MODEL_MODELS_STATIC_H */