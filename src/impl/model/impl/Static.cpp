/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Static.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-11
 ///

#include "Static.hpp"
#include <glm/gtc/matrix_transform.hpp>

namespace model {
  namespace impl {

    void StaticVertex::bindAttributes()
    {
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (GLvoid*)offsetof(StaticVertex, position));
      glhelp::checkGLError();
      glEnableVertexAttribArray(1);
      glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (GLvoid*)offsetof(StaticVertex, normal));
      glhelp::checkGLError();
      glEnableVertexAttribArray(2);
      glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (GLvoid*)offsetof(StaticVertex, uv));
      glhelp::checkGLError();
    }

    StaticMesh::StaticMesh(const std::shared_ptr<Program>& pgm) noexcept :
      Base(pgm)
    {
    }


    std::shared_ptr<AModel> StaticModel::clone() const
    {
      auto model(std::make_shared<StaticModel>());
      model->_parts.reserve(_parts.size());
      for (const auto& part : _parts) {
        model->_parts.emplace_back(
          std::make_shared<StaticMesh::Instance>(
            *std::static_pointer_cast<StaticMesh::Instance>(part)));
      }
      return model;
    }
  }
}