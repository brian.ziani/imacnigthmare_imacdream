/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Rigged.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-11
 ///

#ifndef _SRC_IMPL_MODEL_MODELS_RIGGED_H
#define _SRC_IMPL_MODEL_MODELS_RIGGED_H

#include "Base.hpp"
#include "../Skeleton.hpp"
#include "../Pose.hpp"

#include <vector>
#include <memory>

namespace model {
  namespace impl {

    /// \brief Program used to draw rigged meshes
    class RiggedProgram : public Program {
    public:

      static constexpr const size_t MaxBonesCount = 128;

      static const std::shared_ptr<RiggedProgram>& Get() noexcept;
      static void load(const std::string& vs, const std::string& fs);

      RiggedProgram(const std::string& vs, const std::string& fs);
      virtual ~RiggedProgram() noexcept = default;

      void setBone(size_t index, const glm::mat4& m) const;

    private:

      static std::shared_ptr<RiggedProgram> _instance;

      GLuint uBoneTransforms[MaxBonesCount];
    };

    /// \brief VBO datas associated with static mesh
    struct RiggedVertex {
      glm::vec3 position;
      glm::vec3 normal;
      glm::vec2 uv;

      int32_t boneIDs[4];
      float boneWeights[4];

      void tryAddBone(int32_t bone, float weight);

      static void bindAttributes();
    };

    /// \brief Rigged meshes
    class RiggedMesh : public BaseMesh<RiggedMesh, RiggedVertex, RiggedProgram> {
    public:

      using Base = BaseMesh<RiggedMesh, RiggedVertex, RiggedProgram>;

      /// \brief Instance of a static mesh
      class Instance : public Base::Instance {
      public:

        Instance(
          const std::shared_ptr<std::vector<glm::mat4>>& pose,
          const std::shared_ptr<RiggedMesh>& mesh) noexcept;
        virtual ~Instance() noexcept = default;

        void bind(const glm::mat4& VP) const override;

      private:

        std::shared_ptr<std::vector<glm::mat4>> _pose;
      };

      RiggedMesh(const std::shared_ptr<Program>& pgm = RiggedProgram::Get()) noexcept;
      virtual ~RiggedMesh() noexcept = default;

      const Skeleton& skeleton() const noexcept;

    protected:

      friend class ::model::Loader;

      std::shared_ptr<Skeleton> _skeleton;
    };

    class RiggedModel : public AModel {
    public:

      std::shared_ptr<AModel> clone() const override;

      void setPose(const IPose& pose) noexcept;

      const Skeleton& skeleton() const noexcept;

    protected:

      friend class ::model::Loader;
      std::shared_ptr<std::vector<glm::mat4>> _pose;
    };
  }
}

#endif /* _SRC_IMPL_MODEL_MODELS_RIGGED_H */