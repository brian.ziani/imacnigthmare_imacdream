/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Base.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-11
 ///

#ifndef _SRC_IMPL_MODEL_IMPL_BASE_H
#define _SRC_IMPL_MODEL_IMPL_BASE_H

#include "../Mesh.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <memory>

namespace model {
  namespace impl {

    template <typename Mesh, typename VertexT, typename ProgramT>
    class BaseMesh : public AMesh {
    public:

      class Instance : public AMesh::IInstance {
      public:

        Instance(const std::shared_ptr<Mesh>& mesh) noexcept;
        Instance(const Instance& o) noexcept;

        void bind(const glm::mat4& VP) const override;
        void unbind() const override;
        std::shared_ptr<AMesh> mesh() const noexcept override;

        void setModelMatrix(const glm::mat4& M) noexcept override;

      protected:

        std::shared_ptr<Mesh> _mesh;
        glm::mat4 _modelMatrix;
      };

      BaseMesh(const std::shared_ptr<Program>& program) noexcept;
      virtual ~BaseMesh() noexcept = default;

      std::shared_ptr<glhelp::Mesh<VertexT>> rawMesh() const noexcept;

    protected:

      void drawCall() const noexcept override;

      friend class ::model::Loader;

      std::shared_ptr<glhelp::Mesh<VertexT>> _mesh;
    };
  }
}

////////////////////////////////////////////////////////////////

namespace model {
  namespace impl {

    template <typename Mesh, typename VertexT, typename ProgramT>
    BaseMesh<Mesh, VertexT, ProgramT>::Instance::Instance(
      const std::shared_ptr<Mesh>& mesh) noexcept :

      _mesh(mesh), _modelMatrix(glm::identity<glm::mat4>())
    {
    }

    template <typename Mesh, typename VertexT, typename ProgramT>
    BaseMesh<Mesh, VertexT, ProgramT>::Instance::Instance(
      const Instance& o) noexcept :
      _mesh(o._mesh), _modelMatrix(o._modelMatrix)
    {
    }

    template <typename Mesh, typename VertexT, typename ProgramT>
    void BaseMesh<Mesh, VertexT, ProgramT>::Instance::bind(const glm::mat4& VP) const
    {
      _mesh->_program->setModelMatrix(_modelMatrix);
      _mesh->_program->setNomalMatrix(glm::transpose(glm::inverse(_modelMatrix)));
      _mesh->_program->setMVPMatrix(VP * _modelMatrix);
    }
    template <typename Mesh, typename VertexT, typename ProgramT>
    void BaseMesh<Mesh, VertexT, ProgramT>::Instance::unbind() const
    {
    }
    template <typename Mesh, typename VertexT, typename ProgramT>
    std::shared_ptr<AMesh> BaseMesh<Mesh, VertexT, ProgramT>::Instance::mesh() const noexcept
    {
      return _mesh;
    }

    template <typename Mesh, typename VertexT, typename ProgramT>
    void BaseMesh<Mesh, VertexT, ProgramT>::Instance::setModelMatrix(const glm::mat4& M) noexcept
    {
      _modelMatrix = M;
    }

    template <typename Mesh, typename VertexT, typename ProgramT>
    BaseMesh<Mesh, VertexT, ProgramT>::BaseMesh(
      const std::shared_ptr<Program>& program) noexcept :

      AMesh(std::static_pointer_cast<Program>(program)),
      _mesh(nullptr)
    {
    }

    template <typename Mesh, typename VertexT, typename ProgramT>
    void BaseMesh<Mesh, VertexT, ProgramT>::drawCall() const noexcept
    {
      _mesh->draw();
    }

    template <typename Mesh, typename VertexT, typename ProgramT>
    std::shared_ptr<glhelp::Mesh<VertexT>>
      BaseMesh<Mesh, VertexT, ProgramT>::rawMesh() const noexcept
    {
      return _mesh;
    }
  }
}

#endif /* _SRC_IMPL_MODEL_IMPL_BASE_H */