/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Rigged.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-11
 ///

#include "Rigged.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <libs/glm/Helpers.hpp>
#include <libs/utils/log.hpp>

namespace model {
  namespace impl {

    ////////////////////////////////////////////////////////////////

    std::shared_ptr<RiggedProgram> RiggedProgram::_instance = nullptr;

    const std::shared_ptr<RiggedProgram>& RiggedProgram::Get() noexcept
    {
      return _instance;
    }
    void RiggedProgram::load(const std::string& vs, const std::string& fs)
    {
      _instance = std::make_shared<RiggedProgram>(vs, fs);
    }

    RiggedProgram::RiggedProgram(const std::string& vs, const std::string& fs) :
      Program(vs, fs)
    {
      for (size_t i = 0; i < MaxBonesCount; ++i) {
        uBoneTransforms[i] = uniform("uBoneTransforms[" + std::to_string(i) + "]");
      }
    }
    void RiggedProgram::setBone(size_t index, const glm::mat4& m) const
    {
      assert(index < MaxBonesCount && "Invalid Bone index");
      setMat4(uBoneTransforms[index], m);
      glhelp::checkGLError();
    }

    ////////////////////////////////////////////////////////////////

    void RiggedVertex::tryAddBone(int32_t bone, float weight)
    {
      ssize_t insert(-1);
      /* find correct position */
      for (ssize_t i = 0; i < 4; ++i) {
        if (boneWeights[i] <= weight) {
          insert = i;
        }
        else {
          break;
        }
      }
      /* insert new weigth */
      if (0 <= insert) {
        for (ssize_t i = 0; i < insert; ++i) {
          boneWeights[i] = boneWeights[i + 1];
          boneIDs[i] = boneIDs[i + 1];
        }
        boneWeights[insert] = weight;
        boneIDs[insert] = bone;
      }
    }

    void RiggedVertex::bindAttributes()
    {
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(RiggedVertex), (GLvoid*)offsetof(RiggedVertex, position));
      glhelp::checkGLError();
      glEnableVertexAttribArray(1);
      glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(RiggedVertex), (GLvoid*)offsetof(RiggedVertex, normal));
      glhelp::checkGLError();
      glEnableVertexAttribArray(2);
      glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(RiggedVertex), (GLvoid*)offsetof(RiggedVertex, uv));
      glhelp::checkGLError();
      glEnableVertexAttribArray(3);
      glVertexAttribIPointer(3, 4, GL_INT, sizeof(RiggedVertex), (GLvoid*)offsetof(RiggedVertex, boneIDs));
      glhelp::checkGLError();
      glEnableVertexAttribArray(4);
      glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(RiggedVertex), (GLvoid*)offsetof(RiggedVertex, boneWeights));
      glhelp::checkGLError();
    }

    ////////////////////////////////////////////////////////////////

    RiggedMesh::Instance::Instance(
      const std::shared_ptr<std::vector<glm::mat4>>& pose,
      const std::shared_ptr<RiggedMesh>& mesh) noexcept :

      Base::Instance(mesh),
      _pose(pose)
    {
    }

    void RiggedMesh::Instance::bind(const glm::mat4& VP) const
    {
      Base::Instance::bind(VP);
      for (size_t i = 0; i < _pose->size(); ++i) {
        std::static_pointer_cast<RiggedProgram>(_mesh->_program)->setBone(i, (*_pose)[i]);
      }
    }

    ////////////////////////////////////////////////////////////////

    RiggedMesh::RiggedMesh(const std::shared_ptr<Program>& pgm) noexcept :

      Base(pgm),
      _skeleton(nullptr)
    {
    }

    const Skeleton& RiggedMesh::skeleton() const noexcept
    {
      return *_skeleton;
    }

    std::shared_ptr<AModel> RiggedModel::clone() const
    {
      auto model(std::make_shared<RiggedModel>());

      model->_pose = std::make_shared<std::vector<glm::mat4>>();
      model->_pose->reserve(_pose->size());
      for (const auto& M : *_pose) {
        model->_pose->emplace_back(M);
      }

      model->_parts.reserve(_parts.size());
      for (const auto& part : _parts) {
        model->_parts.emplace_back(
          std::make_shared<RiggedMesh::Instance>(
            model->_pose,
            std::static_pointer_cast<RiggedMesh>(part->mesh())));
      }

      return model;
    }

    void RiggedModel::setPose(const IPose& pose) noexcept
    {
      std::static_pointer_cast<RiggedMesh>(
        std::static_pointer_cast<RiggedMesh::Instance>(_parts[0])->mesh())
        ->skeleton().computeTransforms(pose, *_pose);
    }
    const Skeleton& RiggedModel::skeleton() const noexcept
    {
      return std::static_pointer_cast<RiggedMesh>(
        std::static_pointer_cast<RiggedMesh::Instance>(_parts[0])->mesh())
        ->skeleton();
    }

  }
}