/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file GBuffer.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-26
 ///

#ifndef _SRC_IMPL_RENDER_GBUFFER_H
#define _SRC_IMPL_RENDER_GBUFFER_H

#include <impl/Settings.hpp>
#include <libs/glwrap/Framebuffer.hpp>
#include <libs/glwrap/Sampler.hpp>
#include <libs/glwrap/Mesh.hpp>
#include <glm/vec3.hpp>

namespace render {

  namespace gbuffer {

    enum Buffers {
      PositionBuffer = 0,
      NormalBuffer = 1,
      AmbiantBuffer = 2,
      DiffuseBuffer = 3,
      SpecularBuffer = 4,
      EmissiveBuffer = 5,
      DepthBuffer,
      TotalBuffsCount
    };

    struct ScreenMeshVertex {
      glm::vec3 position;
      ScreenMeshVertex(const glm::vec3& p) noexcept : position(p) {}
      static void bindAttributes();
    };
  }

  class GBuffer : public glhelp::Framebuffer<gbuffer::TotalBuffsCount> {
  public:

    using Mesh = glhelp::Mesh<gbuffer::ScreenMeshVertex>;

    GBuffer(const conf::SettingsPtr& settings);
    ~GBuffer() noexcept = default;

    const glhelp::Sampler& sampler() const noexcept;
    const Mesh& screenMesh() const noexcept;

    void drawBuffers() const;

  private:

    Mesh _mesh;
    glhelp::Sampler _sampler;
    conf::SettingsPtr _settings;
  };
}

#endif /* _SRC_IMPL_RENDER_GBUFFER_H */