/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file RenderingEngine.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-27
 ///

#include "../GBuffer.hpp"
#include "../Stage.hpp"

#include <impl/Settings.hpp>
#include <libs/window/Window.hpp>

#include <initializer_list>
#include <functional>
#include <memory>
#include <list>

namespace render {

  class RenderingEngine {
  public:

    struct Error { std::string name; };
    struct SDLInitFailure : public Error {};
    struct GlewInitFailure : public Error {};
    struct ConfigurationFailure : public Error {};

    using Pipeline = std::list<std::shared_ptr<IStage>>;
    using Key = Pipeline::const_iterator;

    RenderingEngine(const conf::SettingsPtr& settings);
    ~RenderingEngine() noexcept;

    void setPipeline(
      std::initializer_list<
      std::shared_ptr<IStage>>&& pipeline);
    void swapPipelines(Pipeline& other);

    void drawCall() const;

  private:

    void configureOpenGL();

    std::unique_ptr<gui::Window> _window;
    std::unique_ptr<GBuffer> _gbuffer;
    Pipeline _pipeline;
  };
}