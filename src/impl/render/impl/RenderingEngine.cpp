/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file RenderingEngine.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-27
 ///

#include <GL/glew.h>

#include "RenderingEngine.hpp"

#include <stdexcept>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>

namespace render {

  RenderingEngine::RenderingEngine(const conf::SettingsPtr& settings) :
    _window(nullptr), _gbuffer(nullptr), _pipeline()
  {
    /* Initialize the library */
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
      throw SDLInitFailure{ SDL_GetError() };
    }
    if (IMG_INIT_PNG != IMG_Init(IMG_INIT_PNG)) {
      throw std::runtime_error(IMG_GetError());
    }

    configureOpenGL();

    _window = std::make_unique<gui::Window>(
      settings->Hard.Gui.Window.name,
      settings->Hard.Gui.Window.width,
      settings->Hard.Gui.Window.height);

    GLenum err = glewInit();
    if (err != GL_NO_ERROR) {
      throw GlewInitFailure{
        reinterpret_cast<const char*>(gluErrorString(err)) };
    }

    _gbuffer = std::make_unique<GBuffer>(settings);
  }
  RenderingEngine::~RenderingEngine() noexcept
  {
    SDL_Quit();
  }
  void RenderingEngine::configureOpenGL()
  {
    /* Set OpenGL version */
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3))
      throw ConfigurationFailure{ SDL_GetError() };
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3))
      throw ConfigurationFailure{ SDL_GetError() };
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE))
      throw ConfigurationFailure{ SDL_GetError() };
    if (SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8))
      throw ConfigurationFailure{ SDL_GetError() };
    if (SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8))
      throw ConfigurationFailure{ SDL_GetError() };
    if (SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8))
      throw ConfigurationFailure{ SDL_GetError() };
    if (SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8))
      throw ConfigurationFailure{ SDL_GetError() };
    if (SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16))
      throw ConfigurationFailure{ SDL_GetError() };
    if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8))
      throw ConfigurationFailure{ SDL_GetError() };
    if (SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1))
      throw ConfigurationFailure{ SDL_GetError() };
  }

  void RenderingEngine::setPipeline(
    std::initializer_list<
    std::shared_ptr<IStage>>&& pipeline)
  {
    _pipeline = pipeline;
  }
  void RenderingEngine::swapPipelines(Pipeline& other)
  {
    std::swap(_pipeline, other);
  }

  void RenderingEngine::drawCall() const
  {
    glViewport(0, 0, _gbuffer->width(), _gbuffer->height());
    for (const auto& stage : _pipeline) {
      stage->drawCall(*_gbuffer);
    }
    SDL_GL_SwapWindow(_window->window);
    glhelp::checkGLError();
  }
}