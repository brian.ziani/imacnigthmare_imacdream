/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file DumpGBuffer.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#ifndef _SRC_IMPL_RENDER_STAGE_DEBUG_GBUFFER_H
#define _SRC_IMPL_RENDER_STAGE_DEBUG_GBUFFER_H

#include <GL/glew.h>
#include <impl/render/Stage.hpp>

namespace render {

  namespace utils {

    template <typename Functor>
    class FunctorStage : public IStage {
    public:
      FunctorStage(Functor&& f) noexcept;
      virtual ~FunctorStage() noexcept = default;

      void drawCall(
        const GBuffer& gbuffer) const override;
    private:
      Functor _func;
    };

    using WrapperStage = FunctorStage<std::function<void(const GBuffer&)>>;

    class ClearGBufferStage : public IStage {
    public:
      void drawCall(
        const GBuffer& gbuffer) const override;
    };

    class ClearTargetBufferStage : public IStage {
    public:
      void drawCall(
        const GBuffer&) const override;
    };

    class CopyDepthStencilStage : public IStage {
    public:
      void drawCall(
        const GBuffer& gbuffer) const override;
    };
  }

  namespace debug {

    class DumpGBufferStage : public IStage {
    public:
      void drawCall(
        const GBuffer& gbuffer) const override;
    };
  }
}

//////////////////////////////////////////////////////

namespace render {
  namespace utils {

    template <typename Functor>
    FunctorStage<Functor>::FunctorStage(Functor&& f) noexcept :
      _func(std::forward<Functor>(f))
    {
    }
    template <typename Functor>
    void FunctorStage<Functor>::drawCall(
      const GBuffer& gbuffer) const
    {
      _func(gbuffer);
    }

    void ClearGBufferStage::drawCall(const GBuffer& gbuffer) const
    {
      gbuffer.bindOn(GL_DRAW_FRAMEBUFFER);
      glDepthMask(0xFF);
      glStencilMask(0xFF);
      glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
      glhelp::checkGLError();
    }

    void ClearTargetBufferStage::drawCall(const GBuffer&) const
    {
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      glClear(GL_COLOR_BUFFER_BIT);
    }

    void CopyDepthStencilStage::drawCall(const GBuffer& gbuffer) const
    {
      gbuffer.bindOn(GL_READ_FRAMEBUFFER);

      glEnable(GL_BLEND);
      glBlendEquation(GL_FUNC_ADD);
      glBlendFunc(GL_ONE, GL_ZERO);
      glhelp::checkGLError();

      glDepthMask(GL_FALSE);
      glClear(GL_COLOR_BUFFER_BIT);
      glhelp::checkGLError();

      glBlitFramebuffer(
        0, 0, gbuffer.width(), gbuffer.height(),
        0, 0, gbuffer.width(), gbuffer.height(),
        GL_DEPTH_BUFFER_BIT, GL_NEAREST);
      glBlitFramebuffer(
        0, 0, gbuffer.width(), gbuffer.height(),
        0, 0, gbuffer.width(), gbuffer.height(),
        GL_STENCIL_BUFFER_BIT, GL_NEAREST);
    }
  }

  namespace debug {

    void DumpGBufferStage::drawCall(const GBuffer& gbuffer) const
    {
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      gbuffer.drawBuffers();
    }

  }
}

#endif /* _SRC_IMPL_RENDER_STAGE_DEBUG_GBUFFER_H */