/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file BloomProgram.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-22
 ///

#include "BloomProgram.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <stdexcept>

namespace {
  float BorderColor[] = { 0.0f, 0.0f, 0.0f, 1.0f };
}

namespace render {
  namespace gfx {

    BloomProgram::BloomProgram(
      int width, int height,
      const std::string& vs,
      const std::string& fs) :

      IStage(),
      Program(vs, fs),

      gDiffuse(uniform("gDiffuse")),
      gEmission(uniform("gEmission")),
      gBlurIn(uniform("gBlurIn")),

      uMode(uniform("uMode")),
      uExposure(0),//uniform("uExposure")),
      uRadius(uniform("uRadius")),
      sampler({
        {GL_TEXTURE_MIN_FILTER, GL_LINEAR},
        {GL_TEXTURE_MAG_FILTER, GL_LINEAR},
        {GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER},
        {GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER},
        {GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER},
        {GL_TEXTURE_BORDER_COLOR, BorderColor}
        }),
      fbos{
        glhelp::Framebuffer<1>(
          width, height, {{0, {GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE}}}),
        glhelp::Framebuffer<1>(
          width, height, {{0, {GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE}}}) },
      _depth(10),
      _radius(1.f)
    {
    }

    void BloomProgram::drawCall(const GBuffer& gbuffer) const
    {
      use();
      gbuffer.bindOn(GL_READ_FRAMEBUFFER);
      gbuffer.bindTextureOn(0, gDiffuse, gbuffer.sampler().glid(), gbuffer::DiffuseBuffer);
      gbuffer.bindTextureOn(1, gEmission, gbuffer.sampler().glid(), gbuffer::EmissiveBuffer);

      fbos[1].bindOn(GL_DRAW_FRAMEBUFFER);
      glClear(GL_COLOR_BUFFER_BIT);
      fbos[0].bindOn(GL_DRAW_FRAMEBUFFER);
      glClear(GL_COLOR_BUFFER_BIT);

      glDisable(GL_BLEND);
      glDisable(GL_STENCIL_TEST);
      glDisable(GL_DEPTH_TEST);

      setInt(uMode, 0);
      setFloat(uRadius, _radius);
      glhelp::checkGLError();
      gbuffer.screenMesh().draw();
      glhelp::checkGLError();

      for (size_t i = 0; i < _depth; ++i) {

        fbos[0].bindOn(GL_READ_FRAMEBUFFER);
        fbos[1].bindOn(GL_DRAW_FRAMEBUFFER);

        fbos[0].bindTextureOn(2, gBlurIn, sampler.glid(), 0);
        setInt(uMode, 1);

        gbuffer.screenMesh().draw();


        fbos[0].bindOn(GL_DRAW_FRAMEBUFFER);

        fbos[1].bindTextureOn(2, gBlurIn, sampler.glid(), 0);
        setInt(uMode, 2);

        gbuffer.screenMesh().draw();
        glhelp::checkGLError();

      }

      // setFloat(uExposure, 1.f);
      fbos[0].bindOn(GL_READ_FRAMEBUFFER);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      fbos[0].bindTextureOn(2, gBlurIn, gbuffer.sampler().glid(), 0);
      setInt(uMode, 3);
      glhelp::checkGLError();

      glEnable(GL_BLEND);
      glBlendEquation(GL_FUNC_ADD);
      glBlendFunc(GL_ONE, GL_ONE);
      glhelp::checkGLError();

      gbuffer.screenMesh().draw();
      glhelp::checkGLError();

      // fbos[0].bindOn(GL_READ_FRAMEBUFFER);
      // glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      // glReadBuffer(GL_COLOR_ATTACHMENT0);
      // glBlitFramebuffer(
      //   0, 0, gbuffer.width(), gbuffer.height(),
      //   0, 0, gbuffer.width(), gbuffer.height(),
      //   GL_COLOR_BUFFER_BIT, GL_LINEAR);
      // glhelp::checkGLError();
    }

    void BloomProgram::setRadius(float r) noexcept
    {
      _radius = r;
    }
    void BloomProgram::setDepth(size_t d) noexcept
    {
      _depth = d;
    }
  }
}