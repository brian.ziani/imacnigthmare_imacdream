/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file GlobalLighting.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-05
 ///

#include "GlobalLighting.hpp"
#include <libs/utils/log.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <cmath>

namespace {
  float I(float a, float b, float t) noexcept
  {
    return (1.f - t) * a + t * b;
  }
}

namespace render {
  namespace light {

    GlobalLight GlobalLight::interpolate(
      const GlobalLight& a, const GlobalLight& b, float t) noexcept
    {
      return GlobalLight {
        {I(a.Direction.x, b.Direction.x, t), I(a.Direction.y, b.Direction.y, t), I(a.Direction.z, b.Direction.z, t)},
        {I(a.Intensity.r, b.Intensity.r, t), I(a.Intensity.g, b.Intensity.g, t), I(a.Intensity.b, b.Intensity.b, t)},
        {I(a.Ambiant.r, b.Ambiant.r, t), I(a.Ambiant.g, b.Ambiant.g, t), I(a.Ambiant.b, b.Ambiant.b, t)},
        {I(a.Sky.r, b.Sky.r, t), I(a.Sky.g, b.Sky.g, t), I(a.Sky.b, b.Sky.b, t)}
      };
    }

    const ::utils::serial::SerialisationTable& GlobalLight::serialTable() noexcept
    {
      using namespace ::utils::serial;
      static ::utils::serial::SerialisationTable _instance{
        {"GLOBAL_LIGHT_DIRECTION_X", {offsetof(GlobalLight, Direction.x), Entry::Type::Float}},
        {"GLOBAL_LIGHT_DIRECTION_Y", {offsetof(GlobalLight, Direction.y), Entry::Type::Float}},
        {"GLOBAL_LIGHT_DIRECTION_Z", {offsetof(GlobalLight, Direction.z), Entry::Type::Float}},

        {"GLOBAL_LIGHT_INTENSITY_R", {offsetof(GlobalLight, Intensity.r), Entry::Type::Float}},
        {"GLOBAL_LIGHT_INTENSITY_G", {offsetof(GlobalLight, Intensity.g), Entry::Type::Float}},
        {"GLOBAL_LIGHT_INTENSITY_B", {offsetof(GlobalLight, Intensity.b), Entry::Type::Float}},

        {"GLOBAL_LIGHT_AMBIANT_R", {offsetof(GlobalLight, Ambiant.r), Entry::Type::Float}},
        {"GLOBAL_LIGHT_AMBIANT_G", {offsetof(GlobalLight, Ambiant.g), Entry::Type::Float}},
        {"GLOBAL_LIGHT_AMBIANT_B", {offsetof(GlobalLight, Ambiant.b), Entry::Type::Float}},

        {"GLOBAL_LIGHT_SKY_R", {offsetof(GlobalLight, Sky.r), Entry::Type::Float}},
        {"GLOBAL_LIGHT_SKY_G", {offsetof(GlobalLight, Sky.g), Entry::Type::Float}},
        {"GLOBAL_LIGHT_SKY_B", {offsetof(GlobalLight, Sky.b), Entry::Type::Float}}
      };
      return _instance;
    }

    GlobalLightStage::GlobalLightStage(
      const std::string& vs,
      const std::string& fs) :

      IStage(),

      _pgm(vs, fs),

      _light(),

      gWorldPos(_pgm.uniform("gWorldPos")),
      gNormal(_pgm.uniform("gNormal")),
      gAmbiamt(_pgm.uniform("gAmbiamt")),
      gDiffuse(_pgm.uniform("gDiffuse")),
      gSpecular(_pgm.uniform("gSpecular")),

      uEyePosition_w(_pgm.uniform("uEyePosition_w")),
      uHorizon(_pgm.uniform("uHorizon")),

      uLightDir_w(_pgm.uniform("uLightDir_w")),
      uLightColor(_pgm.uniform("uLightColor")),
      uAmbiant(_pgm.uniform("uAmbiant")),
      uSkyColor(_pgm.uniform("uSkyColor")),

      uMode(_pgm.uniform("uMode")),

      _eyes(0.f), _horizon(0.f)
    {
    }

    void GlobalLightStage::setEyes(
      const glm::vec3& eyes, float FOV,
      float H_W_ratio, float lookVerticalAngle) noexcept
    {
      _eyes = eyes;
      float viewAngularHeight = H_W_ratio * FOV * 2.0f;
      float midScreenToHorizon = -lookVerticalAngle + 0.1f;
      float bottomToHorizon = viewAngularHeight * 0.5f - midScreenToHorizon;
      _horizon = bottomToHorizon / viewAngularHeight;
    }
    void GlobalLightStage::setGlobalLight(const GlobalLight& sun) noexcept
    {
      _light = sun;
    }

    void GlobalLightStage::drawCall(const GBuffer& gbuffer) const
    {
      _pgm.use();

      glDisable(GL_BLEND);

      gbuffer.bindOn(GL_READ_FRAMEBUFFER);
      gbuffer.bindTextureOn(0, gWorldPos, gbuffer.sampler().glid(), gbuffer::PositionBuffer);
      gbuffer.bindTextureOn(1, gNormal, gbuffer.sampler().glid(), gbuffer::NormalBuffer);
      gbuffer.bindTextureOn(2, gAmbiamt, gbuffer.sampler().glid(), gbuffer::AmbiantBuffer);
      gbuffer.bindTextureOn(3, gDiffuse, gbuffer.sampler().glid(), gbuffer::DiffuseBuffer);
      gbuffer.bindTextureOn(4, gSpecular, gbuffer.sampler().glid(), gbuffer::SpecularBuffer);

      _pgm.setVec3(uEyePosition_w, _eyes);
      _pgm.setFloat(uHorizon, _horizon);

      _pgm.setVec3(uLightDir_w, _light.Direction.vect());
      _pgm.setVec3(uLightColor, _light.Intensity.rgb());
      _pgm.setVec3(uAmbiant, _light.Ambiant.rgb());
      _pgm.setVec3(uSkyColor, _light.Sky.rgb());

      /* Draw Far Objects */

      _pgm.setInt(uMode, 0);

      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

      glEnable(GL_STENCIL_TEST);
      glStencilFunc(GL_EQUAL, 1, 0xFF);
      glStencilMask(0x00);
      glhelp::checkGLError();

      gbuffer.screenMesh().draw();
      glhelp::checkGLError();

      /* Draw World Objects */

      _pgm.setInt(uMode, 1);

      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

      glEnable(GL_STENCIL_TEST);
      glStencilFunc(GL_EQUAL, 2, 0xFF);
      glStencilMask(0x00);
      glhelp::checkGLError();

      gbuffer.screenMesh().draw();
      glhelp::checkGLError();

      /* Draw Sky */

      _pgm.setInt(uMode, 2);

      glEnable(GL_BLEND);
      glBlendEquation(GL_MAX);
      glBlendFunc(GL_ONE, GL_ONE);
      glhelp::checkGLError();

      glEnable(GL_STENCIL_TEST);
      glStencilFunc(GL_GEQUAL, 1, 0xFF);
      glStencilMask(0x00);
      glhelp::checkGLError();

      gbuffer.screenMesh().draw();
    }
  }
}