/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file PointLight.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-25
 ///

#include "PointLight.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <cmath>

namespace {
  constexpr const size_t MaxLightsCount = 8192;
}

namespace render {
  namespace light {

    PointLightProgram::PointLightProgram(
      const std::shared_ptr<model::AModel>& prototype,
      const std::string& vs,
      const std::string& fs) :
      IStage(),
      Program(vs, fs),

      gWorldPos(uniform("gWorldPos")),
      gNormal(uniform("gNormal")),
      gAmbiamt(uniform("gAmbiamt")),
      gDiffuse(uniform("gDiffuse")),
      gSpecular(uniform("gSpecular")),
      // gZbuffer(uniform("gZbuffer")),

      ucst(uniform("ucst")),
      ulin(uniform("ulin")),
      uquad(uniform("uquad")),

      uVPmatrix(uniform("uVPmatrix")),
      uEyePosition_w(uniform("uEyePosition_w")),
      uScreenSize(uniform("uScreenSize")),

      _enabledLightsCount(0),

      /// \todo THIS IS UGLY, VERY UGLY
      _sphereMesh(
        std::static_pointer_cast<model::impl::StaticMesh>(
          prototype->begin()->get()->mesh())->rawMesh()),
      _volumes(),
      _eyes()
    {
      _volumes.reserve(MaxLightsCount);

      glGenBuffers(1, &vbo);
      glBindBuffer(GL_ARRAY_BUFFER, vbo);
      glBufferData(GL_ARRAY_BUFFER, MaxLightsCount * sizeof(PrecomputedLight), nullptr, GL_STREAM_DRAW);
      glBindBuffer(GL_ARRAY_BUFFER, 0);
      // Bind things //
      glBindVertexArray(_sphereMesh->vaoid());
      glBindBuffer(GL_ARRAY_BUFFER, vbo);
      glVertexAttribDivisor(0, 0);
      // Position //
      glEnableVertexAttribArray(8);
      glVertexAttribPointer(8, 3, GL_FLOAT, GL_FALSE, sizeof(PrecomputedLight), (GLvoid*)offsetof(PrecomputedLight, center));
      glVertexAttribDivisor(8, 1);
      // Color //
      glEnableVertexAttribArray(9);
      glVertexAttribPointer(9, 3, GL_FLOAT, GL_FALSE, sizeof(PrecomputedLight), (GLvoid*)offsetof(PrecomputedLight, intensity));
      glVertexAttribDivisor(9, 1);
      // Radius //
      glEnableVertexAttribArray(10);
      glVertexAttribPointer(10, 1, GL_FLOAT, GL_FALSE, sizeof(PrecomputedLight), (GLvoid*)offsetof(PrecomputedLight, radius));
      glVertexAttribDivisor(10, 1);
      // END //
      glBindBuffer(GL_ARRAY_BUFFER, 0);
      glBindVertexArray(0);
      glhelp::checkGLError();
    }

    void PointLightProgram::addSource(const PointLight& v)
    {
      float c = 1.f;
      float l = 0.7f;
      float q = 2.5f;
      float max = std::max({ v.intensity.r, v.intensity.g, v.intensity.b });
      float radius = (-l + std::sqrt(l * l - 4 * q * (c - (256.0 / 5.0) * max))) / (2 * q);
      _volumes.emplace_back(PrecomputedLight{ v.center, v.intensity, radius });
    }
    void PointLightProgram::setEyes(const glm::vec3& eyes) noexcept
    {
      _eyes = eyes;
    }
    void PointLightProgram::setVP(const glm::mat4& VP)
    {
      _VP = VP;
    }
    void PointLightProgram::enableNewLight(size_t count) noexcept
    {
      _enabledLightsCount = std::min(
        { MaxLightsCount, _volumes.size(), _enabledLightsCount + count });
    }
    void PointLightProgram::shutdownLights() noexcept
    {
      _enabledLightsCount = 0;
    }

    void PointLightProgram::drawCall(const GBuffer& gbuffer) const
    {
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      use();
      gbuffer.bindOn(GL_READ_FRAMEBUFFER);
      gbuffer.bindTextureOn(0, gWorldPos, gbuffer.sampler().glid(), gbuffer::PositionBuffer);
      gbuffer.bindTextureOn(1, gNormal, gbuffer.sampler().glid(), gbuffer::NormalBuffer);
      gbuffer.bindTextureOn(2, gAmbiamt, gbuffer.sampler().glid(), gbuffer::AmbiantBuffer);
      gbuffer.bindTextureOn(3, gDiffuse, gbuffer.sampler().glid(), gbuffer::DiffuseBuffer);
      gbuffer.bindTextureOn(4, gSpecular, gbuffer.sampler().glid(), gbuffer::SpecularBuffer);
      //gbuffer.bindTextureOn(5, gZbuffer, gbuffer.sampler().glid(), gbuffer::DepthBuffer);

      glDisable(GL_DEPTH_TEST);
      glDepthMask(0x00);
      glStencilMask(0x00);

      glDisable(GL_STENCIL_TEST);

      glEnable(GL_BLEND);
      glBlendEquation(GL_FUNC_ADD);
      glBlendFunc(GL_ONE, GL_ONE);
      glhelp::checkGLError();

      glEnable(GL_CULL_FACE);
      glCullFace(GL_FRONT);

      setFloat(ucst, 1.f);
      setFloat(ulin, 0.7f);
      setFloat(uquad, 2.5f);

      setVec3(uEyePosition_w, _eyes);
      setVec2(uScreenSize, glm::vec2(gbuffer.width(), gbuffer.height()));
      setMat4(uVPmatrix, _VP);
      glhelp::checkGLError();

      glBindVertexArray(_sphereMesh->vaoid());
      glDrawElementsInstanced(GL_TRIANGLES, _sphereMesh->indices.size(), GL_UNSIGNED_INT, 0, _enabledLightsCount);
      glBindVertexArray(0);

      glDisable(GL_CULL_FACE);

      glhelp::checkGLError();
    }

    void PointLightProgram::sendLights() const
    {
      glBindBuffer(GL_ARRAY_BUFFER, vbo);
      glBufferData(GL_ARRAY_BUFFER, MaxLightsCount * sizeof(PrecomputedLight), nullptr, GL_STREAM_DRAW);
      glBufferSubData(GL_ARRAY_BUFFER, 0, _volumes.size() * sizeof(PrecomputedLight), _volumes.data());
      glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
  }
}