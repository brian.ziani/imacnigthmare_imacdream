/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file PointLight.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-25
 ///

#ifndef _SRC_IMPL_RENDER_POSTPROCESS_LIGHT_H
#define _SRC_IMPL_RENDER_POSTPROCESS_LIGHT_H

#include <impl/render/Stage.hpp>
#include <impl/model/impl/Static.hpp>
#include <libs/glwrap/Program.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <vector>
#include <memory>

namespace render {

  namespace light {

    struct PointLight {
      glm::vec3 center;
      glm::vec3 intensity;
    };

    class PointLightProgram :
      public IStage, private glhelp::Program {
    public:

      PointLightProgram(
        const std::shared_ptr<model::AModel>& prototype,
        const std::string& vs,
        const std::string& fs);

      void addSource(const PointLight& v);
      void setEyes(const glm::vec3& eyes) noexcept;
      void setVP(const glm::mat4& VP);

      void drawCall(const GBuffer& gbuffer) const override;

      void sendLights() const;

      void enableNewLight(size_t count=1) noexcept;
      void shutdownLights() noexcept;

    private:

      GLuint gWorldPos, gNormal, gAmbiamt, gDiffuse, gSpecular, gZbuffer;
      GLuint ucst, ulin, uquad;
      GLuint uVPmatrix, uEyePosition_w, uScreenSize;

      GLuint vbo;

      struct PrecomputedLight {
        glm::vec3 center;
        glm::vec3 intensity;
        float radius;
      };

      size_t _enabledLightsCount;

      std::shared_ptr<glhelp::Mesh<model::impl::StaticVertex>> _sphereMesh;
      std::vector<PrecomputedLight> _volumes;
      glm::vec3 _eyes;
      glm::mat4 _VP;
    };
  }

}

#endif /* _SRC_IMPL_RENDER_POSTPROCESS_LIGHT_H */