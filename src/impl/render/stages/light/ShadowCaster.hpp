/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file ShadowCastStage.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-24
 ///

#ifndef _SRC_IMPL_RENDER_SHADOW_CASTER_H
#define _SRC_IMPL_RENDER_SHADOW_CASTER_H

#include <GL/glew.h>
#include <impl/render/Stage.hpp>
#include <impl/model/Mesh.hpp>
#include <libs/glwrap/Framebuffer.hpp>
#include <memory>
#include <vector>
#include <map>

namespace render {
  namespace light {

    struct ShadowCastParam {
      int width, height;
      float left, right;
      float bottom, top;
      float distance;
      float near;
      float far;
      float bias;
      glm::vec3 center;
      glm::vec3 light;
      std::vector<std::shared_ptr<model::AModel>> casters;
    };

    class ShadowCastStage : public IStage {
    public:

      ShadowCastStage(
        int width, int height,
        const std::string& vs, const std::string& fs);
      ~ShadowCastStage() noexcept = default;

      void drawCall(const GBuffer& gbuffer) const override;

      void createShadowCastingZone(
        int id,
        const ShadowCastParam& params);

      void updateZone(int id, const glm::vec3& center);

    private:

      using DepthFBO = glhelp::Framebuffer<1>;

      struct ShadowZone {
        glm::mat4 VP;
        ShadowCastParam params;
        std::shared_ptr<DepthFBO> fbo;
      };

      glhelp::Program _pgm;
      glhelp::Sampler _sampler;
      glhelp::Framebuffer<1> _shadowFBO;
      std::map<int, ShadowZone> _zones;

      GLuint gWorldPos, gNormal, gShadowMap;
      GLuint uLightMatrix, uLightDir;
      GLuint uBias;
      GLuint uMode;

    };

  }
}

#endif /* _SRC_IMPL_RENDER_SHADOW_CASTER_H */