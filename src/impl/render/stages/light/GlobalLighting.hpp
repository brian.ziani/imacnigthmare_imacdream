/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file GlobalLighting.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-05
 ///

#ifndef _SRC_IMPL_RENDER_SUN_PGM_H
#define _SRC_IMPL_RENDER_SUN_PGM_H

#include <impl/render/Stage.hpp>
#include <impl/model/Mesh.hpp>
#include <libs/glwrap/Program.hpp>
#include <libs/utils/Serial.hpp>
#include <libs/glm/Helpers.hpp>
#include <glm/vec3.hpp>
#include <unordered_map>
#include <memory>

namespace render {

  namespace light {

    struct GlobalLight {
      struct {
        float x, y, z;
        glm::vec3 vect() const noexcept { return glm::vec3(x, y, z); }
      } Direction;
      struct {
        float r, g, b;
        glm::vec3 rgb() const noexcept { return helpers::rgb(r, g, b); }
      } Intensity;
      struct {
        float r, g, b;
        glm::vec3 rgb() const noexcept { return glm::vec3(r, g, b); }
      } Ambiant;
      struct {
        float r, g, b;
        glm::vec3 rgb() const noexcept { return helpers::rgb(r, g, b); }
      } Sky;

      static GlobalLight interpolate(
        const GlobalLight& a, const GlobalLight& b, float t) noexcept;

      static const ::utils::serial::SerialisationTable& serialTable() noexcept;
    };

    class GlobalLightStage : public IStage {
    public:

      GlobalLightStage(
        const std::string& vs,
        const std::string& fs);

      void drawCall(const GBuffer& gbuffer) const override;

      void setGlobalLight(const GlobalLight& sun) noexcept;

      void setEyes(
        const glm::vec3& eyes, float FOV,
        float H_W_ratio, float lookVerticalAngle) noexcept;

    private:

      glhelp::Program _pgm;

      GlobalLight _light;

      GLuint gWorldPos, gNormal, gAmbiamt, gDiffuse, gSpecular;
      GLuint uEyePosition_w, uHorizon;
      GLuint uLightDir_w, uLightColor, uAmbiant, uSkyColor;
      GLuint uMode;

      glm::vec3 _eyes;
      float _horizon;
    };

  }
}

#endif /* _SRC_IMPL_RENDER_SUN_PGM_H */