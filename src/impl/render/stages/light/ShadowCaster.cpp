/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file ShadowCastStage.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-24
 ///

#include "ShadowCaster.hpp"
#include <libs/glwrap/Helpers.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdexcept>

namespace {
  float BorderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
}

namespace render {
  namespace light {

    ShadowCastStage::ShadowCastStage(
      int width, int height,
      const std::string& vs, const std::string& fs) :

      _pgm(vs, fs),
      _sampler({
        {GL_TEXTURE_MIN_FILTER, GL_NEAREST},
        {GL_TEXTURE_MAG_FILTER, GL_NEAREST},
        {GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER},
        {GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER},
        {GL_TEXTURE_BORDER_COLOR, BorderColor}
        }),
      _shadowFBO(width, height, { {0, { GL_R8, GL_RED, GL_UNSIGNED_BYTE }} }),
      _zones(),

      gWorldPos(_pgm.uniform("gWorldPos")),
      gNormal(_pgm.uniform("gNormal")),

      gShadowMap(_pgm.uniform("gShadowMap")),
      uLightMatrix(_pgm.uniform("uLightMatrix")),
      uLightDir(_pgm.uniform("uLightDir")),

      uBias(_pgm.uniform("uBias")),
      uMode(_pgm.uniform("uMode"))
    {
    }

    void ShadowCastStage::drawCall(const GBuffer& gbuffer) const
    {
      _pgm.use();

      gbuffer.bindOn(GL_READ_FRAMEBUFFER);
      gbuffer.bindTextureOn(0, gWorldPos, gbuffer.sampler().glid(), gbuffer::PositionBuffer);
      gbuffer.bindTextureOn(1, gNormal, gbuffer.sampler().glid(), gbuffer::NormalBuffer);

      _shadowFBO.bindOn(GL_DRAW_FRAMEBUFFER);
      glClear(GL_COLOR_BUFFER_BIT);

      glDisable(GL_STENCIL_TEST);
      glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
      glStencilMask(0x00);

      glEnable(GL_BLEND);
      glBlendEquation(GL_MAX);
      glBlendFunc(GL_ONE, GL_ONE);

      _pgm.setInt(uMode, 0);
      glhelp::checkGLError();

      for (const auto& [_, Zone] : _zones) {
        Zone.fbo->bindOn(GL_READ_FRAMEBUFFER);
        Zone.fbo->bindTextureOn(2, gShadowMap, _sampler.glid(), 0);
        _pgm.setMat4(uLightMatrix, Zone.VP);
        _pgm.setVec3(uLightDir, Zone.params.light);
        _pgm.setFloat(uBias, Zone.params.bias);

        gbuffer.screenMesh().draw();
        glhelp::checkGLError();
      }

      _pgm.setInt(uMode, 1);
      _shadowFBO.bindOn(GL_READ_FRAMEBUFFER);
      _shadowFBO.bindTextureOn(2, gShadowMap, gbuffer.sampler().glid(), 0);
      gbuffer.bindOn(GL_DRAW_FRAMEBUFFER);

      glEnable(GL_BLEND);
      glBlendEquation(GL_FUNC_ADD);
      glBlendFunc(GL_ZERO, GL_ONE_MINUS_SRC_COLOR);
      glhelp::checkGLError();

      gbuffer.screenMesh().draw();
      glhelp::checkGLError();
    }

    void ShadowCastStage::createShadowCastingZone(
      int id,
      const ShadowCastParam& params)
    {
      if (_zones.contains(id)) {
        assert(false && "Duplicated zone id");
      }
      DepthFBO fbo(params.width, params.height, {},
        { {GL_DEPTH_ATTACHMENT, {GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_FLOAT}} });
      _zones.emplace(id, ShadowZone{
        glm::mat4(1.f), params, std::make_shared<DepthFBO>(std::move(fbo)) });
    }

    void ShadowCastStage::updateZone(int id, const glm::vec3& center)
    {
      auto& Zone = _zones.at(id);
      Zone.params.center = center;
      Zone.fbo->bindOn(GL_DRAW_FRAMEBUFFER);

      glDisable(GL_STENCIL_TEST);
      glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
      glStencilMask(0x00);

      glEnable(GL_DEPTH_TEST);
      glDepthMask(0xFF);
      glEnable(GL_CULL_FACE);
      glCullFace(GL_BACK);

      glClear(GL_DEPTH_BUFFER_BIT);

      glViewport(0, 0, Zone.params.width, Zone.params.height);
      glhelp::checkGLError();

      glm::mat4 lightProjection = glm::ortho(
        Zone.params.left, Zone.params.right,
        Zone.params.bottom, Zone.params.top,
        Zone.params.near, Zone.params.far);
      glm::mat4 lightView = glm::lookAt(
        Zone.params.center - Zone.params.light * Zone.params.distance,
        Zone.params.center,
        glm::vec3(0.0f, 1.0f, 0.0f));
      Zone.VP = lightProjection * lightView;

      for (const auto& caster : Zone.params.casters) {
        for (const auto& part : *caster) {
          part->mesh()->bind(model::Program::DrawMode::Depth);
          part->mesh()->draw(Zone.VP, *part);
          part->mesh()->unbind();
          glhelp::checkGLError();
        }
      }

      glDepthMask(0x00);
      glDisable(GL_DEPTH_TEST);
      glDisable(GL_CULL_FACE);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      glhelp::checkGLError();
    }

  }
}