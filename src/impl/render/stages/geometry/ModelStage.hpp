/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file ModelStage.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#ifndef _SRC_IMPL_RENDER_STAGE_MODEL_H
#define _SRC_IMPL_RENDER_STAGE_MODEL_H

#include <impl/model/Renderer.hpp>
#include <impl/render/Stage.hpp>
#include <libs/camera/ICamera.hpp>
#include <functional>
#include <memory>

namespace render {
  namespace geom {

    class ModelStage : public IStage {
    public:

      ModelStage(
        const std::shared_ptr<model::Renderer>& rdr,
        const std::shared_ptr<gui::ICamera>& cam,
        std::function<void(void)>&& prepare) noexcept;

      void drawCall(
        const GBuffer&) const override;

    private:
      std::shared_ptr<model::Renderer> _renderer;
      std::shared_ptr<gui::ICamera> _camera;
      std::function<void(void)> _preparator;
    };
  }
}

////////////////////////////////////////////////////////////

namespace render {
  namespace geom {

    ModelStage::ModelStage(
      const std::shared_ptr<model::Renderer>& rdr,
      const std::shared_ptr<gui::ICamera>& cam,
      std::function<void(void)>&& prepare) noexcept :
      _renderer(rdr), _camera(cam), _preparator(prepare)
    {
    }

    void ModelStage::drawCall(
      const GBuffer&) const
    {
      _preparator();
      _renderer->drawCall(
        model::Program::DrawMode::Default,
        _camera->VPMatrix());
    }
  }
}

#endif /* _SRC_IMPL_RENDER_STAGE_MODEL_H */