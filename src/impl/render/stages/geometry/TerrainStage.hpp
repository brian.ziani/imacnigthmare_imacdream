/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file TerrainStage.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#ifndef _SRC_IMPL_RENDER_STAGE_TERRAIN_H
#define _SRC_IMPL_RENDER_STAGE_TERRAIN_H

#include <impl/world/Renderer.hpp>
#include <impl/render/Stage.hpp>
#include <libs/camera/ICamera.hpp>
#include <memory>

namespace render {
  namespace geom {
    class TerrainStage : public IStage {
    public:

      TerrainStage(
        const std::shared_ptr<world::Renderer>& rdr,
        const std::shared_ptr<gui::ICamera>& cam) noexcept;

      void drawCall(
        const GBuffer&) const override;

    private:
      std::shared_ptr<world::Renderer> _renderer;
      std::shared_ptr<gui::ICamera> _camera;
    };
  }
}

////////////////////////////////////////////////////////////

namespace render {
  namespace geom {

    TerrainStage::TerrainStage(
      const std::shared_ptr<world::Renderer>& rdr,
        const std::shared_ptr<gui::ICamera>& cam) noexcept :
      _renderer(rdr), _camera(cam)
    {
    }

    void TerrainStage::drawCall(const GBuffer&) const
    {
      _renderer->renderTerrain(_camera->VPMatrix());
    }
  }
}

#endif /* _SRC_IMPL_RENDER_STAGE_TERRAIN_H */