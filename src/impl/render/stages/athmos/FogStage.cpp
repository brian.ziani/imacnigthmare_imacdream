/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file FogStage.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#include "FogStage.hpp"

namespace {
  float I(float a, float b, float t) noexcept
  {
    return (1.f - t) * a + t * b;
  }
}

namespace render {
  namespace athmos {

    AthmosphericSettings AthmosphericSettings::interpolate(
      const AthmosphericSettings& a, const AthmosphericSettings& b, float t) noexcept
    {
      return AthmosphericSettings{
        {I(a.Fog.r, b.Fog.r, t), I(a.Fog.g, b.Fog.g, t), I(a.Fog.b, b.Fog.b, t)}
      };
    }

    const ::utils::serial::SerialisationTable& AthmosphericSettings::serialTable() noexcept
    {
      using namespace ::utils::serial;
      static ::utils::serial::SerialisationTable _instance{
        {"FOG_COLOR_R", {offsetof(AthmosphericSettings, Fog.r), Entry::Type::Float}},
        {"FOG_COLOR_G", {offsetof(AthmosphericSettings, Fog.g), Entry::Type::Float}},
        {"FOG_COLOR_B", {offsetof(AthmosphericSettings, Fog.b), Entry::Type::Float}},
      };
      return _instance;
    }

    FogStage::FogStage(const std::string& vs, const std::string& fs) :
      IStage(),

      _pgm(vs, fs),
      _athmos(),

      gWorldPos(_pgm.uniform("gWorldPos")),
      gZbuffer(_pgm.uniform("gZbuffer")),

      uEyePosition_w(_pgm.uniform("uEyePosition_w")),
      uHorizon(_pgm.uniform("uHorizon")),
      uNear(_pgm.uniform("uNear")),
      uFar(_pgm.uniform("uFar")),

      uFogColor(_pgm.uniform("uFogColor")),
      uFogHeight(_pgm.uniform("uFogHeight")),

      _horizon(0),
      _near(0), _far(0)
    {
    }

    void FogStage::setFog(const AthmosphericSettings& settings) noexcept
    {
      _athmos = settings;
    }
    void FogStage::setClipPlanes(float near, float far) noexcept
    {
      _near = near;
      _far = far;
    }
    void FogStage::setEyes(
      const glm::vec3& eyes, float FOV,
      float H_W_ratio, float lookVerticalAngle) noexcept
    {
      _eyes = eyes;
      float viewAngularHeight = H_W_ratio * FOV * 2.0f;
      float midScreenToHorizon = -lookVerticalAngle + 0.1f + eyes.y / _far;
      float bottomToHorizon = viewAngularHeight * 0.5f - midScreenToHorizon;
      _horizon = bottomToHorizon / viewAngularHeight;
    }

    void FogStage::drawCall(const GBuffer& gbuffer) const
    {
      _pgm.use();

      gbuffer.bindOn(GL_READ_FRAMEBUFFER);
      gbuffer.bindTextureOn(0, gWorldPos, gbuffer.sampler().glid(), gbuffer::PositionBuffer);
      gbuffer.bindTextureOn(1, gZbuffer, gbuffer.sampler().glid(), gbuffer::DepthBuffer);

      _pgm.setVec3(uEyePosition_w, _eyes);
      _pgm.setFloat(uHorizon, _horizon);
      _pgm.setVec3(uFogColor, _athmos.Fog.rgb());
      _pgm.setFloat(uFogHeight, 100.f);

      _pgm.setFloat(uNear, _near);
      _pgm.setFloat(uFar, _far);

      glDisable(GL_STENCIL_TEST);
      glDisable(GL_DEPTH_TEST);

      glEnable(GL_BLEND);
      glBlendEquation(GL_FUNC_ADD);
      glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA);

      glhelp::checkGLError();

      gbuffer.screenMesh().draw();
    }
  }
}