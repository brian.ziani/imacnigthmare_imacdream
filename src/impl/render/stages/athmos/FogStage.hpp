/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file FogSkyStage.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#ifndef _SRC_IMPL_RENDER_STAGE_FOG_SKY_H
#define _SRC_IMPL_RENDER_STAGE_FOG_SKY_H

#include <GL/glew.h>
#include <impl/render/Stage.hpp>
#include <libs/glwrap/Program.hpp>
#include <libs/utils/Serial.hpp>
#include <libs/glm/Helpers.hpp>
#include <string>

namespace render {
  namespace athmos {

    struct AthmosphericSettings {
      struct {
        float r, g, b;
        glm::vec3 rgb() const noexcept { return helpers::rgb(r, g, b); }
      } Fog;

      static AthmosphericSettings interpolate(
        const AthmosphericSettings& a, const AthmosphericSettings& b, float t) noexcept;

      static const ::utils::serial::SerialisationTable& serialTable() noexcept;
    };

    class FogStage : public IStage {
    public:

      FogStage(const std::string& vs, const std::string& fs);

      void drawCall(const GBuffer& gbuffer) const override;

      void setFog(const AthmosphericSettings& settings) noexcept;
      void setClipPlanes(float near, float far) noexcept;
      void setEyes(
        const glm::vec3& eyes, float FOV,
        float H_W_ratio, float lookVerticalAngle) noexcept;

    private:

      glhelp::Program _pgm;
      AthmosphericSettings _athmos;
      GLuint gWorldPos, gZbuffer;
      GLuint uEyePosition_w, uHorizon, uNear, uFar;
      GLuint uFogColor, uFogHeight;
      glm::vec3 _eyes;
      float _horizon;
      float _near, _far;
    };
  }
}

#endif /* _SRC_IMPL_RENDER_STAGE_FOG_SKY_H */