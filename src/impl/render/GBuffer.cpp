/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file GBuffer.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-26
 ///

#include "GBuffer.hpp"

#include <libs/glwrap/Helpers.hpp>
#include <string>
#include <stdexcept>
#include <map>

namespace render {

  namespace gbuffer {
    void ScreenMeshVertex::bindAttributes()
    {
      glEnableVertexAttribArray(0);
      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(ScreenMeshVertex), (GLvoid*)offsetof(ScreenMeshVertex, position));
    }
  }

  GBuffer::GBuffer(const conf::SettingsPtr& settings) :
    glhelp::Framebuffer<gbuffer::TotalBuffsCount>(
      settings->Hard.Gui.Window.width, settings->Hard.Gui.Window.height,
      {
        {gbuffer::PositionBuffer, {GL_RGB32F, GL_RGB, GL_FLOAT}},
        {gbuffer::NormalBuffer, {GL_RGB32F, GL_RGB, GL_FLOAT}},
        {gbuffer::AmbiantBuffer, {GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE}},
        {gbuffer::DiffuseBuffer, {GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE}},
        {gbuffer::SpecularBuffer, {GL_R32F, GL_RED, GL_FLOAT}},
        {gbuffer::EmissiveBuffer, {GL_R8, GL_RED, GL_UNSIGNED_BYTE}} },
        {
          {GL_DEPTH_STENCIL_ATTACHMENT, {
            GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8 }}
        }),
    _mesh(4, 6),
    _sampler(
      {
      {GL_TEXTURE_MIN_FILTER, GL_NEAREST},
      {GL_TEXTURE_MAG_FILTER, GL_NEAREST} }
      ),
    _settings(settings)
  {
    _mesh.vertices.emplace_back(glm::vec3(-1, 1, 0));
    _mesh.vertices.emplace_back(glm::vec3(1, 1, 0));
    _mesh.vertices.emplace_back(glm::vec3(1, -1, 0));
    _mesh.vertices.emplace_back(glm::vec3(-1, -1, 0));

    _mesh.indices.emplace_back(0);
    _mesh.indices.emplace_back(1);
    _mesh.indices.emplace_back(3);

    _mesh.indices.emplace_back(1);
    _mesh.indices.emplace_back(2);
    _mesh.indices.emplace_back(3);

    _mesh.build();
    glhelp::checkGLError();
  }

  const glhelp::Sampler& GBuffer::sampler() const noexcept
  {
    return _sampler;
  }
  const GBuffer::Mesh& GBuffer::screenMesh() const noexcept
  {
    return _mesh;
  }

  void GBuffer::drawBuffers() const
  {
    bindOn(GL_READ_FRAMEBUFFER);
    glhelp::checkGLError();

    int QuarterWidth = width() / 4;
    int HalfHeight = height() / 2;

    glReadBuffer(GL_COLOR_ATTACHMENT0 + render::gbuffer::PositionBuffer);
    glBlitFramebuffer(
      0, 0, width(), height(),
      0, 0, QuarterWidth, HalfHeight,
      GL_COLOR_BUFFER_BIT, GL_LINEAR);
    glhelp::checkGLError();

    glReadBuffer(GL_COLOR_ATTACHMENT0 + render::gbuffer::NormalBuffer);
    glBlitFramebuffer(
      0, 0, width(), height(),
      0, HalfHeight, QuarterWidth, height(),
      GL_COLOR_BUFFER_BIT, GL_LINEAR);
    glhelp::checkGLError();

    glReadBuffer(GL_COLOR_ATTACHMENT0 + render::gbuffer::AmbiantBuffer);
    glBlitFramebuffer(
      0, 0, width(), height(),
      QuarterWidth, HalfHeight, 2 * QuarterWidth, height(),
      GL_COLOR_BUFFER_BIT, GL_LINEAR);
    glhelp::checkGLError();

    glReadBuffer(GL_COLOR_ATTACHMENT0 + render::gbuffer::DiffuseBuffer);
    glBlitFramebuffer(
      0, 0, width(), height(),
      QuarterWidth, 0, 2 * QuarterWidth, HalfHeight,
      GL_COLOR_BUFFER_BIT, GL_LINEAR);
    glhelp::checkGLError();

    glReadBuffer(GL_COLOR_ATTACHMENT0 + render::gbuffer::SpecularBuffer);
    glBlitFramebuffer(
      0, 0, width(), height(),
      2 * QuarterWidth, 0, 3 * QuarterWidth, HalfHeight,
      GL_COLOR_BUFFER_BIT, GL_LINEAR);
    glhelp::checkGLError();

    glReadBuffer(GL_COLOR_ATTACHMENT0 + render::gbuffer::EmissiveBuffer);
    glBlitFramebuffer(
      0, 0, width(), height(),
      2 * QuarterWidth, HalfHeight, 3 * QuarterWidth, height(),
      GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glhelp::checkGLError();
  }

}