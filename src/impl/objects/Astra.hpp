/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Astra.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-29
 ///

#ifndef _SRC_IMPL_OBJECTS_ASTRA_H
#define _SRC_IMPL_OBJECTS_ASTRA_H

#include <impl/model/Mesh.hpp>

namespace astra {

  class Program : public model::Program {
  public:

    Program(
      float phase,
      const std::string& vs, const std::string& fs);
    virtual ~Program() noexcept = default;

    void bind() const override;
    void unbind() const override;

    void startAnimation(float amplitudeanchor=0.3f) noexcept;
    void stopAnimation() noexcept;
    void addTime(float t) noexcept;

    float transitionTime() const noexcept;
    bool animating() const noexcept;

  private :

    GLuint uTime;
    GLuint uAnimMode, uSubTime, uAnimTime;
    GLuint uSize, uAmplitude;

    float _time;
    float _subTime, _animTime;
    float _speed, _targetSpeed, _anchor;
    float _phaseSign, _size;
    float _amplitude;
    int _animMode;
  };
}

#endif /* _SRC_IMPL_OBJECTS_ASTRA_H */