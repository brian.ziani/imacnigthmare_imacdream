/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Astra.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-29
 ///

#include "Astra.hpp"
#include <libs/utils/log.hpp>
#include <libs/utils/Math.hpp>

namespace astra {

  Program::Program(
    float phaseSign,
    const std::string& vs, const std::string& fs) :

    model::Program(vs, fs),

    uTime(uniform("uTime")),
    uAnimMode(0),//uniform("uAnimMode")),
    uSubTime(0),//uniform("uSubTime")),
    uAnimTime(uniform("uAnimTime")),
    uSize(uniform("uSize")),
    uAmplitude(uniform("uAmplitude")),

    _time(0.f),
    _subTime(0.f), _animTime(0.f),
    _speed(0.f), _targetSpeed(0.f), _anchor(0.02f),
    _phaseSign(phaseSign),
    _size(_phaseSign < 0 ? 2.f : 0.f),
    _amplitude(0.02f),
    _animMode(0)
  {
  }

  void Program::bind() const
  {
    model::Program::bind();
    setFloat(uTime, _time);

    // setInt(uAnimMode, _animMode);
    // setFloat(uSubTime, _subTime);
    setFloat(uAnimTime, 2.f * _animTime);
    setFloat(uSize, math::smootherstep(std::max(0.f, std::min(_size, 1.f))));
    setFloat(uAmplitude, _amplitude);

    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    // glLineWidth(6.f);
    glhelp::checkGLError();
  }
  void Program::unbind() const
  {
    // glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glhelp::checkGLError();
  }

  void Program::startAnimation(float amplitudeanchor) noexcept
  {
    _animMode = +1;
    _speed = 0.f;
    _animTime = 0.f;
    _subTime = 0.f;
    _size = _phaseSign < 0 ? 2.f : 0.f;
    _amplitude = 0.02f;
    _anchor = amplitudeanchor;
  }
  void Program::stopAnimation() noexcept
  {
    _animMode = 0;
    _targetSpeed = 0.f;
  }
  void Program::addTime(float t) noexcept
  {
    _amplitude += (_anchor - _amplitude) * t;
    if (_amplitude > 0.25f) {
      _targetSpeed = 0.5f;
    }
    else {
      _targetSpeed = 0.f;
    }
    _time = fmod(_time + t, 1.f);
    _speed += (_targetSpeed - _speed) * t;
    _subTime = fmod(_subTime + t * _speed, 1.f);
    _animTime = _animTime + t * _speed;
    _size = _size + _phaseSign * t * _speed * 2.f;
    if (1.f <= _animTime) {
      _animMode = 0.f;
      _anchor = 0.02f;
    }
    if (0 == _animMode) {
    }
    else {
    }
  }
  float Program::transitionTime() const noexcept
  {
    return fmod(_animTime, 1.f);
  }
  bool Program::animating() const noexcept
  {
    return 0 != _animMode;
  }
}