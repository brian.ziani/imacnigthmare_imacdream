/*
 * Copyright (C) 2021 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Portal.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2021-01-02
 ///

#ifndef _SRC_IMPL_OBJECTS_PORTAL_H
#define _SRC_IMPL_OBJECTS_PORTAL_H

#include <impl/render/Stage.hpp>

namespace portal {

  class PortalStage : public render::IStage {
  public:

    void drawCall(const render::GBuffer& gbuffer) const override;

  private:
  };
}

#endif /* _SRC_IMPL_OBJECTS_PORTAL_H */