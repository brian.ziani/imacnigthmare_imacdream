
#include <impl/Settings.hpp>

#include <impl/controller/InputHandler.hpp>
#include <impl/player/Controller.hpp>
#include <impl/player/Animator.hpp>

#include <impl/model/Loader.hpp>
#include <impl/model/Renderer.hpp>
#include <impl/model/impl/Static.hpp>
#include <impl/model/impl/Rigged.hpp>

#include <impl/world/World.hpp>
#include <impl/world/void/VoidMesh.hpp>
#include <impl/world/Renderer.hpp>

#include <impl/objects/Astra.hpp>

#include <impl/particles/Sand.hpp>

#include <impl/render/impl/RenderingEngine.hpp>
#include <impl/render/stages/utils/Utils.hpp>
#include <impl/render/stages/geometry/ModelStage.hpp>
#include <impl/render/stages/geometry/TerrainStage.hpp>
#include <impl/render/stages/light/ShadowCaster.hpp>
#include <impl/render/stages/light/GlobalLighting.hpp>
#include <impl/render/stages/light/PointLight.hpp>
#include <impl/render/stages/athmos/FogStage.hpp>
#include <impl/render/stages/gfx/BloomProgram.hpp>

#include <impl/sound/AudioEngine.hpp>
#include <impl/sound/SoundBunch.hpp>
#include <impl/sound/Music.hpp>

#include <libs/window/Window.hpp>
#include <libs/camera/impl/TrackballCamera.hpp>

#include <libs/glwrap/Mesh.hpp>
#include <libs/glwrap/Helpers.hpp>
#include <libs/glwrap/Program.hpp>
#include <libs/glwrap/Texture.hpp>

#include <libs/generator/Common.hpp>
#include <libs/generator/Simplex.hpp>

#include <libs/physics/Primitives.hpp>
#include <libs/physics/MotionSolver.hpp>

#include <libs/utils/FPSGovernor.hpp>
#include <libs/utils/Math.hpp>
#include <libs/utils/log.hpp>

#include <glm/glm.hpp>
#include <glm/gtx/polar_coordinates.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <map>
#include <set>
#include <iostream>
#include <fstream>
#include <sstream>
#include <random>
#include <vector>

int main(void)
{
  auto _GlobalSettings = std::make_shared<conf::GlobalSettings>();

  {
    auto result = utils::serial::readFile("../res/conf/global/Hard");
    utils::serial::deserialise(result, conf::HardSettings::serialTable(),
      &_GlobalSettings->Hard);
  }
  {
    auto result = utils::serial::readFile("../res/conf/global/Soft");
    utils::serial::deserialise(result, conf::SoftSettings::serialTable(),
      &_GlobalSettings->Soft);
  }
  {
    auto result = utils::serial::readFile("../res/conf/global/Volatile");
    utils::serial::deserialise(result, conf::VolatileSettings::serialTable(),
      &_GlobalSettings->Vol);
  }

  auto _GameRenderer = std::make_shared<render::RenderingEngine>(_GlobalSettings);

  /* Initialise things */

  auto _camera = std::make_shared<gui::TrackballCamera>();

  _camera->target = glm::vec3(16, 5, 16);
  _camera->distance = 10;
  _camera->anglex = M_PI / 8.0f;
  _camera->angley = 0;
  _camera->fov = M_PI / 2.5f;
  _camera->near = 0.5f;
  _camera->far = 4096.0f * 4.f;
  _camera->width = _GlobalSettings->Hard.Gui.Window.width;
  _camera->height = _GlobalSettings->Hard.Gui.Window.height;

  /* World */

  gen::Settings noiseSettings;
  noiseSettings.seed = 0;
  noiseSettings.frequency = 12.f;
  noiseSettings.scale = 0.11f / 128.f;
  noiseSettings.noiseAmplitude = 800.f;
  noiseSettings.samplefieldsize = 32;
  gen::SimplexNoise noise(noiseSettings);

  _camera->far =
    float(M_SQRT2)
    * _GlobalSettings->Hard.World.Chunk.tilesPerChunk
    * _GlobalSettings->Hard.World.Chunk.tileSize
    * (_GlobalSettings->Soft.World.Chunk.preloadDistance / 2);

  auto _portalCamera = std::make_shared<gui::TrackballCamera>(*_camera);

  auto _farCamera = std::make_shared<gui::TrackballCamera>(*_camera);
  _farCamera->far = _camera->far * 1500.f;
  _farCamera->near = _camera->far * 100.f;

  auto _world = std::make_shared<world::World>(
    _GlobalSettings, noise);

  auto _worldRenderer = std::make_shared<world::Renderer>(
    _world, _GlobalSettings);


  render::light::GlobalLight _currentLighting;
  render::athmos::AthmosphericSettings _currentAthmos;
  std::string _currentAthmosFile = "../res/conf/misc/Day";
  auto reloadAthmos = [&](void) -> void {
    auto result = utils::serial::readFile(_currentAthmosFile);
    utils::serial::deserialise(result, render::light::GlobalLight::serialTable(),
      &_currentLighting);
    utils::serial::deserialise(result, render::athmos::AthmosphericSettings::serialTable(),
      &_currentAthmos);
  };
  reloadAthmos();

  render::light::GlobalLight startLight, targetLight;
  render::athmos::AthmosphericSettings startAthmos, targetAthmos;
  {
    auto result = utils::serial::readFile("../res/conf/misc/Day");
    utils::serial::deserialise(result, render::light::GlobalLight::serialTable(),
      &startLight);
    utils::serial::deserialise(result, render::athmos::AthmosphericSettings::serialTable(),
      &startAthmos);

    result = utils::serial::readFile("../res/conf/misc/Night");
    utils::serial::deserialise(result, render::light::GlobalLight::serialTable(),
      &targetLight);
    utils::serial::deserialise(result, render::athmos::AthmosphericSettings::serialTable(),
      &targetAthmos);
  }

  auto _objectsRenderer = std::make_shared<model::Renderer>();
  auto _backgroundObjectsRenderer = std::make_shared<model::Renderer>();

  /* Audio */

  audio::AudioEngine _AudioEngine;
  audio::Music _dayMusic("../res/sounds/music/day.ogg");
  audio::Music _nightMusic("../res/sounds/music/night.ogg");
  audio::Music _voidMusic("../res/sounds/music/void.ogg");

  /* Bind Things */

  // _GameRenderer->setCamera(_camera);
  // _GameRenderer->setWorldRenderer(_worldRenderer);

  auto _shadowStage = std::make_shared<render::light::ShadowCastStage>(
    _GlobalSettings->Hard.Gui.Window.width, _GlobalSettings->Hard.Gui.Window.height,
    "../res/shaders/postprocess/postprocess.vs",
    "../res/shaders/postprocess/light/shadow.fs");

  auto _globalLightStage = std::make_shared<render::light::GlobalLightStage>(
    "../res/shaders/postprocess/postprocess.vs",
    "../res/shaders/postprocess/light/global.fs");

  auto _athmosphericStage = std::make_shared<render::athmos::FogStage>(
    "../res/shaders/postprocess/postprocess.vs",
    "../res/shaders/postprocess/athmos/fog.fs");

  auto _bloomStage = std::make_shared<render::gfx::BloomProgram>(
    _GlobalSettings->Hard.Gui.Window.width, _GlobalSettings->Hard.Gui.Window.height,
    "../res/shaders/postprocess/postprocess.vs",
    "../res/shaders/postprocess/gfx/bloom.fs");
  _bloomStage->setRadius(2.f);

  /* Dirty things */

  model::Program::load(
    "../res/shaders/geom/default.vs",
    "../res/shaders/geom/default.fs");
  model::impl::RiggedProgram::load(
    "../res/shaders/geom/rigged.vs",
    "../res/shaders/geom/rigged.fs");
  auto _luminumProgram(std::make_shared<astra::Program>(
    -1.f,
    "../res/shaders/geom/astra.vs",
    "../res/shaders/geom/astra.fs"));
  auto _nocturumProgram(std::make_shared<astra::Program>(
    +1.f,
    "../res/shaders/geom/astra.vs",
    "../res/shaders/geom/astra.fs"));
  auto _zevoidProgram(std::make_shared<astra::Program>(
    +1.f,
    "../res/shaders/geom/astra.vs",
    "../res/shaders/geom/zevoid.fs"));
  _worldRenderer->loadTerrainProgram(
    "../res/shaders/geom/terrain.vs",
    "../res/shaders/geom/terrain.fs");

  auto _defaultTexture = std::make_shared<glhelp::Texture<uint8_t>>(1, 1, 3);
  _defaultTexture->pixels.emplace_back(0);
  _defaultTexture->pixels.emplace_back(128);
  _defaultTexture->pixels.emplace_back(128);
  _defaultTexture->build();

  model::LoadRequest loadr;
  loadr.path = "../res/models/cube.fbx";
  loadr.flags = aiProcess_Triangulate | aiProcess_GenNormals | aiProcess_FlipUVs;
  loadr.defaultTexture = _defaultTexture;

  model::Loader _modelLoader;

  auto result = _modelLoader.loadModel(loadr);
  std::shared_ptr<model::AModel> cubePrototype = result.prototype;
  _objectsRenderer->registerMesh(result.meshes);

  loadr.path = "../res/models/sphere.obj";
  result = _modelLoader.loadModel(loadr);
  std::shared_ptr<model::AModel> spherePrototype = result.prototype;
  _objectsRenderer->registerMesh(result.meshes);

  loadr.path = "../res/models/castle.obj";
  result = _modelLoader.loadModel(loadr);
  std::shared_ptr<model::AModel> ruinPrototype = result.prototype;
  _objectsRenderer->registerMesh(result.meshes);

  loadr.path = "../res/models/sun.obj";
  loadr.forcedShader = _luminumProgram;
  loadr.allowWeirdMeshes = true;
  result = _modelLoader.loadModel(loadr);
  std::shared_ptr<model::AModel> sunPrototype = result.prototype;
  _backgroundObjectsRenderer->registerMesh(result.meshes);

  loadr.path = "../res/models/moon.obj";
  loadr.forcedShader = _nocturumProgram;
  result = _modelLoader.loadModel(loadr);
  std::shared_ptr<model::AModel> moonPrototype = result.prototype;
  _backgroundObjectsRenderer->registerMesh(result.meshes);

  loadr.path = "../res/models/Zevoide.obj";
  loadr.forcedShader = _zevoidProgram;
  loadr.allowWeirdMeshes = true;
  result = _modelLoader.loadModel(loadr);
  std::shared_ptr<model::AModel> zevoidPrototype = result.prototype;
  _backgroundObjectsRenderer->registerMesh(result.meshes);

  loadr.path = "../res/models/explorantin.fbx";
  loadr.allowWeirdMeshes = false;
  loadr.flags = aiProcess_Triangulate | aiProcess_FlipUVs;
  loadr.poses = player::AnimationHandler::neededPoses();
  loadr.forcedShader = nullptr;
  result = _modelLoader.loadModel(loadr);
  auto playerPrototype =
    std::static_pointer_cast<model::impl::RiggedModel>(result.prototype);
  auto playerPoses = std::move(result.poses);
  _objectsRenderer->registerMesh(result.meshes);

  glm::vec3 playerPos(-50.f, 200.f, -400.f);
  player::Player player(playerPos);

  auto portalRawMesh = std::static_pointer_cast<model::impl::StaticMesh>(
    cubePrototype->begin()->get()->mesh())->rawMesh();
  auto nightPortalTransform = std::make_shared<glm::mat4>();
  auto voidPortalTransform = std::make_shared<glm::mat4>();

  auto _pointLightStage = std::make_shared<render::light::PointLightProgram>(
    cubePrototype,
    "../res/shaders/postprocess/light/point.vs",
    "../res/shaders/postprocess/light/point.fs");

  auto _beginDrawCall = std::make_shared<render::utils::ClearGBufferStage>();

  auto _drawBackground = std::make_shared<render::geom::ModelStage>(
    _backgroundObjectsRenderer,
    _farCamera,
    []() -> void {
      glEnable(GL_DEPTH_TEST);
      glDepthFunc(GL_LESS);
      glDepthRangef(0.f, 1.f);
      glDisable(GL_BLEND);
      glhelp::checkGLError();

      glEnable(GL_STENCIL_TEST);
      glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
      glStencilFunc(GL_ALWAYS, 1, 0xFF);
      glStencilMask(0xFF);
    });

  auto _drawEntities = std::make_shared<render::geom::ModelStage>(
    _objectsRenderer,
    _camera,
    []() -> void {
      glClear(GL_DEPTH_BUFFER_BIT);
      glStencilFunc(GL_ALWAYS, 2, 0xFF);
    });

  auto _drawTerrain = std::make_shared<render::geom::TerrainStage>(
    _worldRenderer,
    _camera);

  auto _endGeometryPass = std::make_shared<render::utils::WrapperStage>(
    [](const render::GBuffer&) -> void {
      glStencilMask(0x00);

      glDepthMask(GL_FALSE);
      glDisable(GL_DEPTH_TEST);
      glhelp::checkGLError();

      glDisable(GL_STENCIL_TEST);
      glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    });

  auto _clearTargetBuffer = std::make_shared<render::utils::ClearTargetBufferStage>();
  auto _copyDepthStencil = std::make_shared<render::utils::CopyDepthStencilStage>();

  auto _nightPortalPass = std::make_shared <render::utils::WrapperStage>(
    [&]
  (const render::GBuffer&) -> void {
      auto pgm = model::Program::Get();

      glEnable(GL_DEPTH_TEST);
      glEnable(GL_STENCIL_TEST);
      glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
      glStencilFunc(GL_ALWAYS, 3, 0xFF);
      glStencilMask(0xFF);

      pgm->bind();
      pgm->setDrawMode(model::Program::DrawMode::Depth);
      pgm->setMVPMatrix(_camera->VPMatrix() * *nightPortalTransform);
      portalRawMesh->draw();
      pgm->unbind();

      glDisable(GL_DEPTH_TEST);
      glStencilOp(GL_KEEP, GL_KEEP, GL_DECR);
      glStencilFunc(GL_EQUAL, 3, 0xFF);

      _worldRenderer->renderTerrain(
        _portalCamera->VPMatrix(),
        world::ChunkSpan(
          _world,
          world::Chunk::ID(-4, -4),
          world::Chunk::ID(+4, +4)));

      glDisable(GL_STENCIL_TEST);
      glStencilMask(0x00);
    });

  auto _voidFloorMesh = std::make_shared<world::voiddim::VoidMesh>(
    world::voiddim::generateRawMesh(*_GlobalSettings));
  auto voidFloorProgram = std::make_shared<world::voiddim::FloorProgram>(
    "../res/shaders/geom/voidfloor.vs",
    "../res/shaders/geom/voidfloor.fs");

  auto _drawVoidFloor = std::make_shared<render::utils::WrapperStage>(
    [&](const render::GBuffer&) -> void {
      float tile = _GlobalSettings->Hard.World.Void.tileSize;
      float view = _GlobalSettings->Hard.World.Void.viewDistance / 2;
      glm::vec2 off(-tile * view);
      glm::vec3 playerpos(player.lowHitbox().position);
      glm::vec3 pos(playerpos);
      pos.x = pos.x - fmod(pos.x, tile) + off.x;
      pos.z = pos.z - fmod(pos.z, tile) + off.y;
      pos.y += 5.f;
      voidFloorProgram->use();
      voidFloorProgram->setMVPMatrix(_camera->VPMatrix());
      voidFloorProgram->setColor(glm::vec3(2.f, 0.f, 2.f));
      voidFloorProgram->setCenter(playerpos);
      voidFloorProgram->setTranslate(pos);
      voidFloorProgram->sendTime();

      glClear(GL_DEPTH_BUFFER_BIT);
      glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
      glPointSize(6.f);
      _voidFloorMesh->draw();
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    });
  auto _voidDimensionPipelineBegin = std::make_shared<render::utils::WrapperStage>(
    [&](const render::GBuffer&) -> void {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      glLineWidth(1.f);
    });
  auto _voidPortalPass = std::make_shared<render::utils::WrapperStage>(
    [&](const render::GBuffer&) -> void {
      auto pgm = model::Program::Get();

      glEnable(GL_DEPTH_TEST);
      glEnable(GL_STENCIL_TEST);
      glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
      glStencilFunc(GL_ALWAYS, 3, 0xFF);
      glStencilMask(0xFF);

      pgm->bind();
      pgm->setDrawMode(model::Program::DrawMode::Depth);
      pgm->setMVPMatrix(_camera->VPMatrix() * *nightPortalTransform);
      portalRawMesh->draw();
      pgm->unbind();

      glDisable(GL_DEPTH_TEST);
      glStencilOp(GL_KEEP, GL_KEEP, GL_DECR);
      glStencilFunc(GL_EQUAL, 3, 0xFF);

      glDisable(GL_STENCIL_TEST);
      glStencilMask(0x00);
    });
  auto _endVoidGeometryPass = std::make_shared<render::utils::WrapperStage>(
    [](const render::GBuffer&) -> void {
      glStencilMask(0x00);

      glDepthMask(GL_FALSE);
      glDisable(GL_DEPTH_TEST);
      glhelp::checkGLError();

      glDisable(GL_STENCIL_TEST);
      glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    });

  auto _dumpGBuffer = std::make_shared<render::debug::DumpGBufferStage>();

  _GameRenderer->setPipeline({
    _beginDrawCall,
    _drawBackground,
    _drawEntities,
    _drawTerrain,
    _nightPortalPass,
    _endGeometryPass,
    _clearTargetBuffer,
    _copyDepthStencil,
    _shadowStage,
    _globalLightStage,
    _pointLightStage,
    _athmosphericStage,
    _bloomStage
    });

  std::list<std::shared_ptr<render::IStage>> nightPipeline = {
    _beginDrawCall,
    _drawBackground,
    _drawEntities,
    _drawTerrain,
    _voidPortalPass,
    _endGeometryPass,
    _clearTargetBuffer,
    _copyDepthStencil,
    _shadowStage,
    _globalLightStage,
    _pointLightStage,
    _athmosphericStage,
    _bloomStage
  };
  std::list<std::shared_ptr<render::IStage>> voidPipeline = {
    _beginDrawCall,
    _voidDimensionPipelineBegin,
    _drawBackground,
    _drawVoidFloor,
    _endVoidGeometryPass,
    _clearTargetBuffer,
    _copyDepthStencil,
    _globalLightStage,
    _bloomStage
  };

  /* Controller */

  ctrl::InputHandler _inputHandler;

  /* Player */

  player::Controller playerCtrl(_GlobalSettings, _inputHandler, *_camera, player);
  player::AnimationHandler playerAnimator(
    playerCtrl,
    player,
    playerPrototype->skeleton(),
    std::move(playerPoses));
  world::Chunk::ID _oldPlayerChunk = world::chunkAt(player.lowHitbox().position, *_GlobalSettings);
  _world->updateLoadedChunks(player.lowHitbox().position);

  playerCtrl.loadFlowchart("../res/behaviours/player.dot");
  auto playerInstance = playerPrototype->cloneAs<model::impl::RiggedModel>();
  _objectsRenderer->addDrawable(playerInstance);

  audio::SoundBunch _hitsand("../res/sounds/hit/sand", ".ogg", 1);
  audio::SoundBunch _hitwall("../res/sounds/hit/wall", ".ogg", 1);
  audio::SoundBunch _flyingsound("../res/sounds/fly/fly", ".ogg", 1);

  bool flying(false);
  playerCtrl.slot<player::oevent::EnterFloorMode>().addSubscriber(
    [&](player::Controller& ctrl, player::oevent::EnterFloorMode&) -> void {
      if (physics::Material::Sand == ctrl.currentFloorMaterial())
        _hitsand.play(-1, 0, abs(player.velocity().y));
      else
        _hitwall.play(-1, 0, abs(player.velocity().y));
      if (flying) _flyingsound.stop();
      flying = false;
    });
  playerCtrl.slot<player::oevent::EnterWallMode>().addSubscriber(
    [&](player::Controller&, player::oevent::EnterWallMode&) -> void {
      _hitwall.play(-1, 0, 0.8f);
      if (flying) _flyingsound.stop();
      flying = false;
    });
  playerCtrl.slot<player::oevent::EnterGrabMode>().addSubscriber(
    [&](player::Controller&, player::oevent::EnterGrabMode&) -> void {
      _hitwall.play(-1, 0, 0.8f);
      if (flying) _flyingsound.stop();
      flying = false;
    });
  playerCtrl.slot<player::oevent::EnterFallMode>().addSubscriber(
    [&](player::Controller&, player::oevent::EnterFallMode&) -> void {
      if (flying) _flyingsound.stop();
      flying = false;
    });
  playerCtrl.slot<player::oevent::EnterFlyMode>().addSubscriber(
    [&](player::Controller&, player::oevent::EnterFlyMode&) -> void {
      _flyingsound.play(-1, -1, 1.f);
      flying = true;
    });
  playerCtrl.slot<player::oevent::Jump>().addSubscriber(
    [&](player::Controller& ctrl, player::oevent::Jump&) -> void {
      if (physics::Material::Sand == ctrl.currentFloorMaterial())
        _hitsand.play(-1, 0, abs(player.velocity().y));
    });

  /* Box */
  glm::vec3 cratePos(-50.f, 0.f, -400.f);
  std::vector<std::shared_ptr<model::AModel>> castingZone0;
  cratePos.y = noise.noiseAt3(cratePos);
  physics::Cube crate(cratePos, glm::vec3(50, 25, 5));
  {
    auto tmp = cubePrototype->clone();
    tmp->setModelMatrix(crate.transformation());
    _objectsRenderer->addDrawable(tmp);
    castingZone0.emplace_back(tmp);
  }

  physics::Cube crate2(cratePos, glm::vec3(5, 20, 50));
  {
    auto tmp = cubePrototype->clone();
    tmp->setModelMatrix(crate2.transformation());
    _objectsRenderer->addDrawable(tmp);
    castingZone0.emplace_back(tmp);
  }

  physics::Cube crate3(cratePos + glm::vec3(-30.f, 25.f, 0.f), glm::vec3(5, 12, 50));
  {
    auto tmp = cubePrototype->clone();
    tmp->setModelMatrix(crate3.transformation());
    _objectsRenderer->addDrawable(tmp);
    castingZone0.emplace_back(tmp);
  }
  _shadowStage->createShadowCastingZone(0, {
    512, 512,
    -80, 80,
    -80, 80,
    100, 10, 200, 0.009f,
    cratePos,
    _currentLighting.Direction.vect(),
    std::move(castingZone0) });
  _shadowStage->updateZone(0, cratePos);


  _shadowStage->createShadowCastingZone(1, {
    512, 512,
    -3, 3,
    -3, 3,
    5, 1, 100, 0.001f,
    player.lowHitbox().position,
    _currentLighting.Direction.vect(),
    {playerInstance} });

  physics::Cube nightPortalHitBox;
  physics::Cube voidPortalHitBox;
  std::vector<physics::Cube> castleHitbox;
  {
    auto tmp = ruinPrototype->clone();
    glm::vec3 pos(0.f, 0.f, -4000.f);
    pos.y = noise.noiseAt3(pos);
    physics::Cube hitbox(pos, glm::vec3(20.f));
    tmp->setModelMatrix(hitbox.transformation());
    _objectsRenderer->addDrawable(tmp);

    nightPortalHitBox = physics::Cube(pos, glm::vec3(100.f, 10.f, 100.f));
    *nightPortalTransform = nightPortalHitBox.transformation();

    voidPortalHitBox = physics::Cube(pos, glm::vec3(100.f, 10.f, 100.f));
    *voidPortalTransform = voidPortalHitBox.transformation();

    castleHitbox.emplace_back(pos + glm::vec3(0.f, 0.f, +206.f), glm::vec3(400.f, 53.7f, 152.f));

    // _shadowStage->createShadowCastingZone(0, {
    //   2048, 2048,
    //   -300, 300,
    //   -300, 300,
    //   5000, 10, 1000, 0.009f,
    //   pos, _currentLighting.Direction.vect(),
    //   {tmp} });
    // _shadowStage->updateZone(0, pos);
  }

  std::vector<physics::APrimitive*> portalHitboxList;
  portalHitboxList.emplace_back(&nightPortalHitBox);

  std::vector<physics::APrimitive*> primitives;
  for (auto& box : castleHitbox) {
    primitives.emplace_back(&box);
  }
  primitives.emplace_back(&crate);
  primitives.emplace_back(&crate2);
  primitives.emplace_back(&crate3);

  std::vector<physics::Edge> grabablesEdges;
  grabablesEdges.reserve(primitives.size() * 4);

  for (const physics::APrimitive* p : primitives) {
    const physics::Cube& cube(static_cast<const physics::Cube&>(*p));
    std::array<glm::vec3, 4> vertices = {
      glm::vec3(cube.position.x - cube.size.x, cube.position.y + cube.size.y, cube.position.z - cube.size.z),
      glm::vec3(cube.position.x + cube.size.x, cube.position.y + cube.size.y, cube.position.z - cube.size.z),
      glm::vec3(cube.position.x + cube.size.x, cube.position.y + cube.size.y, cube.position.z + cube.size.z),
      glm::vec3(cube.position.x - cube.size.x, cube.position.y + cube.size.y, cube.position.z + cube.size.z) };
    std::array<glm::vec3, 4> edges = {
      glm::vec3(vertices[1] - vertices[0]),
      glm::vec3(vertices[2] - vertices[1]),
      glm::vec3(vertices[3] - vertices[2]),
      glm::vec3(vertices[0] - vertices[3]) };
    for (size_t i = 0; i < 4; ++i) {
      grabablesEdges.emplace_back(physics::Edge{ vertices[i], edges[i] });
    }
  }


  std::function<float(const glm::vec3&)> floorHeightFunc;
  floorHeightFunc = std::bind_front(&gen::SimplexNoise::noiseAt3, &noise);

  physics::Ellipsoid sunPosition(glm::vec3(0.f), glm::vec3(1.f));
  auto sunModel = sunPrototype->clone();
  sunModel->setModelMatrix(sunPosition.transformation());
  auto moonModel = moonPrototype->clone();
  moonModel->setModelMatrix(sunPosition.transformation());
  auto zevoidModel = zevoidPrototype->clone();
  physics::Ellipsoid zevoidTransform(sunPosition);
  zevoidTransform.size = glm::vec3(1.f);
  zevoidModel->setModelMatrix(zevoidTransform.transformation());

  _backgroundObjectsRenderer->addDrawable(sunModel);

  std::mt19937 gen(0);
  std::uniform_real_distribution<float> dislightx(-1000, 1000);
  std::uniform_real_distribution<float> dislighty(100, -11000);
  for (size_t i = 0; i < 2048; ++i) {
    glm::vec3 pos(dislightx(gen), 0.f, dislighty(gen));
    pos.y = noise.noiseAt3(pos) + 0.1f;
    _pointLightStage->addSource({ pos, 20.f * glm::vec3(0.f, 0.3f, 1.9f) });
  }
  _pointLightStage->sendLights();

  bool _doSearchgrabbable(false);
  playerCtrl.slot<player::oevent::SearchGrab>().addSubscriber(
    [&](player::Controller&, player::oevent::SearchGrab& e) -> void {
      _doSearchgrabbable = e.doSearch;
    });

  /* setup controller */

  render::RenderingEngine::Pipeline debugPipeline = {
    _beginDrawCall,
    _drawBackground,
    _drawEntities,
    _drawTerrain,
    _nightPortalPass,
    _shadowStage,
    _dumpGBuffer
  };
  _inputHandler.bindCallback(
    ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_F1),
    [&](const SDL_Event& e) -> void {
      if (!e.key.repeat) {
        _GameRenderer->swapPipelines(debugPipeline);
      }
    });

  _inputHandler.bindCallback(
    ctrl::InputHandler::KeyBoardFilter(SDL_KEYDOWN, SDLK_F3),
    [&](const SDL_Event& e) -> void {
      if (!e.key.repeat) {

        auto result = utils::serial::readFile("../res/conf/global/Volatile");
        utils::serial::deserialise(result, conf::VolatileSettings::serialTable(),
          &_GlobalSettings->Vol);

        reloadAthmos();
      }
    });

  bool transitionAnimationEnded(false);
  enum class TimelinePosition {
    Day, Night, Void
  } _currentTimelinePosition(TimelinePosition::Day);
  unsigned long long int _voidEnter(0), _voidTimer(0);
  bool startEndOnce(false);

  _dayMusic.tryPlay(1);

  /* Main Loop */
  utils::FPSGovernor fpsgov(
    _GlobalSettings->Hard.Global.framerate,
    _GlobalSettings->Hard.Global.FPSSampleTime);
  bool run(true);
  _inputHandler.bindCallback(
    ctrl::InputHandler::GenericFilter(SDL_QUIT),
    [&run](const SDL_Event&) -> void { run = false; });
  unsigned long int _time = 0;
  while (run) {
    fpsgov.capTick([&](long deltaT) -> void {
      _time += deltaT;
      _voidTimer += deltaT;
      /* handle inputs */
      _inputHandler.update();

      /* update player */
      float dt = deltaT / 1000.f;
      playerCtrl.update(_time, dt);
      glm::vec3 gravity = glm::vec3(0, -1.f, 0) * _GlobalSettings->Vol.Player.baseGravity;
      player.velocity(player.velocity() + gravity * playerCtrl.gravityFactor() * dt);
      /* check collision against cube */
      physics::motion::Solution sol = physics::motion::solveEllipsoidMovement(
        player.lowHitbox(), player.velocity() * dt,
        primitives.cbegin(), primitives.cend(),
        floorHeightFunc,
        5);
      physics::motion::Solution trigger = physics::motion::solveEllipsoidMovement(
        player.lowHitbox(), player.velocity() * dt,
        portalHitboxList.cbegin(), portalHitboxList.cend(),
        floorHeightFunc,
        1);

      playerCtrl.handleMotion(sol, dt);

      if (_doSearchgrabbable) {
        /* result */
        physics::Edge contactEdge;
        physics::TimeInterval interval(-100.f, 100.f);
        bool contact(false);
        /* computation constants */
        const physics::Ellipsoid& hitbox(player.highHitbox());
        glm::vec3 base(glm::vec3(1.f, 1.f, 1.f) / hitbox.size);
        glm::vec3 eposition(base * hitbox.position);
        glm::vec3 evelocity(base * player.velocity());
        /* loop */
        for (auto& edge : grabablesEdges) {
          glm::vec3 evertex(base * edge.vertex);
          glm::vec3 eedge(base * edge.edge);
          glm::vec3 eposToEdge(eposition - evertex);
          eposToEdge -= glm::normalize(eedge) * glm::dot(eposToEdge, glm::normalize(eedge));
          if (glm::length2(eposToEdge) < 1.f) {
            /* check if player is facing the edge */
            glm::vec3 front(glm::normalize(base * player.frontDirection()));
            eposToEdge = glm::normalize(eposToEdge);
            float facingQty(glm::dot(eposToEdge, front));
            if (-1.f <= facingQty && facingQty <= -sqrt(3.f) / 2.f) {
              contact = true;
              contactEdge = edge;
            }
          }
        }
        if (contact) {
          playerCtrl.grabbableFound(contactEdge.vertex, contactEdge.edge);
        }
      }

      if (TimelinePosition::Void != _currentTimelinePosition) {
        world::Chunk::ID newPlayerChunk(world::chunkAt(player.lowHitbox().position, *_GlobalSettings));
        if (newPlayerChunk != _oldPlayerChunk) {
          _world->updateLoadedChunks(player.lowHitbox().position);
        }
        _oldPlayerChunk = newPlayerChunk;
        _world->update();

        playerInstance->setPose(playerAnimator.update(deltaT / 1000.f));
        playerInstance->setModelMatrix(player.transformation());
      }

      if (trigger.hasContact && physics::Material::Hard == trigger.contactMaterial) {
        if (TimelinePosition::Day == _currentTimelinePosition) {
          _currentTimelinePosition = TimelinePosition::Night;
          _luminumProgram->startAnimation();
          _nocturumProgram->startAnimation();
          _pointLightStage->shutdownLights();
          transitionAnimationEnded = false;
          portalHitboxList.clear();
          portalHitboxList.emplace_back(&voidPortalHitBox);
          _GameRenderer->swapPipelines(nightPipeline);
          _backgroundObjectsRenderer->addDrawable(moonModel);
          _nightMusic.tryPlay(1);
          _currentAthmosFile = "../res/conf/misc/Night";
        }
        else if (TimelinePosition::Night == _currentTimelinePosition) {
          _currentTimelinePosition = TimelinePosition::Void;
          _GameRenderer->swapPipelines(voidPipeline);
          _pointLightStage->shutdownLights();
          portalHitboxList.clear();
          primitives.clear();
          floorHeightFunc = [](const glm::vec3&) -> float { return 0.f; };
          transitionAnimationEnded = false;
          _backgroundObjectsRenderer->removeDrawable(moonModel);
          _backgroundObjectsRenderer->removeDrawable(sunModel);
          _backgroundObjectsRenderer->addDrawable(zevoidModel);
          _camera->fov = M_PI / 4.f;
          _bloomStage->setRadius(2.f);
          _bloomStage->setDepth(5);
          _currentAthmosFile = "../res/conf/misc/Void";
          reloadAthmos();
          _voidMusic.tryPlay(1);
          _voidEnter = _voidTimer;
        }
        player.position(glm::vec3(0, 3000.f, 0.f));
      }

      /* update camera */
      if (TimelinePosition::Void != _currentTimelinePosition) {
        if (_luminumProgram->animating()) {
          float t = _luminumProgram->transitionTime();
          t = pow(t, 0.1f);
          _currentLighting = render::light::GlobalLight::interpolate(
            startLight, targetLight, t);
          _currentAthmos = render::athmos::AthmosphericSettings::interpolate(
            startAthmos, targetAthmos, t);
          if (0.7f < t) {
            transitionAnimationEnded = true;
          }
        }
        if (transitionAnimationEnded) {
          _pointLightStage->enableNewLight(8);
        }
      }
      else {
        if (!startEndOnce && _voidEnter + 10000 < _voidTimer) {
          _zevoidProgram->startAnimation(4.f);
          startEndOnce = true;
        }
        // if (_voidEnter + 78000 < _voidTimer) {
        //   run = false;
        // }
      }

      _camera->target = player.lowHitbox().position + glm::vec3(0, 5.f, 0);

      glm::vec2 dxz(-sin(_camera->angley), cos(_camera->angley));
      glm::vec2 dyy = glm::vec2(cos(_camera->anglex), -sin(_camera->anglex));
      for (_camera->distance = 10; 0 < _camera->distance; _camera->distance -= 0.2f) {
        glm::vec3 delta = _camera->distance * glm::vec3(dyy.x * dxz.x, -dyy.y, dyy.x * dxz.y);
        glm::vec3 campos = _camera->target + delta;
        float floor(noise.noiseAt3(campos));
        if (floor < campos.y) {
          break;
        }
      }

      if (TimelinePosition::Void != _currentTimelinePosition) {
        sunPosition.position = _camera->target - _currentLighting.Direction.vect() * _camera->far * 500.f;

        sunPosition.size = glm::vec3(1.f) * _camera->far * 150.f;
        sunModel->setModelMatrix(sunPosition.transformation());
        moonModel->setModelMatrix(sunPosition.transformation());
      }
      else {
        sunPosition.position = _camera->target - _currentLighting.Direction.vect() * _camera->far * 800.f;
        sunPosition.size = glm::vec3(1.f) * _camera->far * 500.f;
        zevoidModel->setModelMatrix(sunPosition.transformation());
      }

      _globalLightStage->setEyes(
        _camera->target,
        _camera->fov,
        float(_camera->height) / _camera->width,
        _camera->anglex);
      _globalLightStage->setGlobalLight(_currentLighting);

      _shadowStage->updateZone(1, player.lowHitbox().position);

      ::new(_farCamera.get()) gui::TrackballCamera(*_camera);
      _farCamera->far = _camera->far * 1500.f;
      _farCamera->near = _camera->far * 100.f;
      ::new(_portalCamera.get()) gui::TrackballCamera(*_camera);
      _portalCamera->target = glm::vec3(0.f, 3000.f, 0.f);

      _portalCamera->update();
      _farCamera->update();
      _camera->update();

      glm::mat3 cameraY(
        glm::rotate(
          glm::identity<glm::mat4>(),
          -_camera->angley + float(M_PI_2),
          glm::vec3(0, 1, 0)));
      _worldRenderer->updateVisibleSpan(
        player.lowHitbox().position,
        cameraY[0],
        _camera->fov);

      _pointLightStage->setEyes(_camera->target);
      _pointLightStage->setVP(_camera->VPMatrix());

      _athmosphericStage->setEyes(
        _camera->target,
        _camera->fov,
        float(_camera->height) / _camera->width,
        _camera->anglex);
      _athmosphericStage->setClipPlanes(_camera->near, _camera->far);
      _athmosphericStage->setFog(_currentAthmos);

      _luminumProgram->addTime(deltaT / 5000.f);
      _nocturumProgram->addTime(deltaT / 5000.f);
      _zevoidProgram->addTime(deltaT / 1000.f);

      voidFloorProgram->addTime(deltaT / 400.f);

      _GameRenderer->drawCall();
      });
  }
  return 0;
}
