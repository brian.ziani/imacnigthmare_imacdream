/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Helpers.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-05
 ///

#include "Helpers.hpp"

std::ostream& operator<< (std::ostream& s, const aiMatrix4x4& mat) noexcept {
  s << "(" << mat.a1 << "," << mat.a2 << "," << mat.a3 << "," << mat.a4 << ")";
  s << "(" << mat.b1 << "," << mat.b2 << "," << mat.b3 << "," << mat.b4 << ")";
  s << "(" << mat.c1 << "," << mat.c2 << "," << mat.c3 << "," << mat.c4 << ")";
  s << "(" << mat.d1 << "," << mat.d2 << "," << mat.d3 << "," << mat.d4 << ")";
  return s;
}
std::ostream& operator<< (std::ostream& s, const aiVector3D& v) noexcept {
  return s << "(" << v.x << " " << v.y << " " << v.z << ")";
}
std::ostream& operator<< (std::ostream& s, const aiVector2D& v) noexcept {
  return s << "(" << v.x << " " << v.y << ")";
}
std::ostream& operator<< (std::ostream& s, const aiString& str) noexcept {
  return s << str.C_Str();
}