/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file AssimH.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-04
 ///

#ifndef _SRC_LIBS_ASSIMP_ASSIMP_HELPERS_H
#define _SRC_LIBS_ASSIMP_ASSIMP_HELPERS_H

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/glm.hpp>
#include <assimp/types.h>
#include <ostream>

namespace helpers {

  /// \brief Convert assimp matrix to glm matrix
  inline glm::mat4 assimpToGlmMatrix(const aiMatrix4x4& mat) noexcept;

  /// \brief Convert assimp vector to glm vector
  inline constexpr glm::vec3 assimpToGlmVec3(const aiVector3D& v) noexcept;
  inline constexpr glm::vec2 assimpToGlmVec2(const aiVector2D& v) noexcept;

  /// \brief Convert assimp quaternion to glm quaternion 
  inline constexpr glm::quat assimpToGlmQuat(const aiQuaternion& q) noexcept;
}

std::ostream& operator<< (std::ostream& s, const aiMatrix4x4& mat) noexcept;
std::ostream& operator<< (std::ostream& s, const aiVector3D& v) noexcept;
std::ostream& operator<< (std::ostream& s, const aiVector2D& v) noexcept;
std::ostream& operator<< (std::ostream& s, const aiString& str) noexcept;

/////////////////////////////////////////////////////////////////////////////////

namespace helpers {

  inline glm::mat4 assimpToGlmMatrix(const aiMatrix4x4& mat) noexcept
  {
    return glm::mat4(
      mat[0][0], mat[1][0], mat[2][0], mat[3][0],
      mat[0][1], mat[1][1], mat[2][1], mat[3][1],
      mat[0][2], mat[1][2], mat[2][2], mat[3][2],
      mat[0][3], mat[1][3], mat[2][3], mat[3][3]);
  }
  inline constexpr glm::vec3 assimpToGlmVec3(const aiVector3D& v) noexcept
  {
    return glm::vec3(v.x, v.y, v.z);
  }
  inline constexpr glm::vec2 assimpToGlmVec2(const aiVector2D& v) noexcept
  {
    return glm::vec2(v.x, v.y);
  }

  inline constexpr glm::quat assimpToGlmQuat(const aiQuaternion& q) noexcept
  {
    return glm::quat(q.w, q.x, q.y, q.z);
  }
}

#endif /* _SRC_LIBS_ASSIMP_ASSIMP_HELPERS_H */