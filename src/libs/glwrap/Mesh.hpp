/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 * Copyright (C) 2020 ZIANI Brian <brian.ziani@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Mesh.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-12
 ///

#ifndef _SRC_RENDER_MESH_H
#define _SRC_RENDER_MESH_H

#include <GL/glew.h>
#include <vector>
#include "Helpers.hpp"

/// \brief Namespace containing all stuff related to opengl meshes
namespace glhelp {

  template <typename VertexT>
  class Mesh {
  public:
    std::vector<VertexT> vertices;
    std::vector<uint32_t> indices;

    Mesh(size_t vcount = 0, size_t icount = 0) noexcept;
    ~Mesh() noexcept;

    Mesh(Mesh&& m) noexcept;
    Mesh& operator= (Mesh&& m) noexcept;

    Mesh(const Mesh& m) noexcept = delete;
    Mesh& operator= (const Mesh& m) noexcept = delete;

    void build();

    void draw() const;

    GLuint vaoid() const noexcept;

  private:
    GLuint vbo;
    GLuint ibo;
    GLuint vao;
  };
}

///////////////////////////////////////////////////////////////////////////////

namespace glhelp {

  template <typename VertexT>
  Mesh<VertexT>::Mesh(size_t vcount, size_t icount) noexcept :
    vertices(), indices(), vbo(0), ibo(0), vao(0)
  {
    vertices.reserve(vcount);
    indices.reserve(icount);
  }
  template <typename VertexT>
  Mesh<VertexT>::~Mesh() noexcept
  {
    if (vbo) glDeleteBuffers(1, &vbo);
    if (ibo) glDeleteBuffers(1, &ibo);
    if (vao) glDeleteVertexArrays(1, &vao);
  }

  template <typename VertexT>
  Mesh<VertexT>::Mesh(Mesh<VertexT>&& m) noexcept :
    vertices(std::move(m.vertices)), indices(std::move(m.indices)),
    vbo(m.vbo), ibo(m.ibo), vao(m.vao)
  {
    m.vbo = m.ibo = m.vao = 0;
  }
  template <typename VertexT>
  Mesh<VertexT>& Mesh<VertexT>::operator= (Mesh<VertexT>&& m) noexcept
  {
    vertices = std::move(m.vertices);
    indices = std::move(m.indices);
    vbo = m.vbo;
    ibo = m.ibo;
    vao = m.vao;
    m.vbo = m.ibo = m.vao = 0;
    return *this;
  }

  template <typename VertexT>
  void Mesh<VertexT>::build()
  {
    vbo = initGlBuffer(GL_ARRAY_BUFFER, vertices);
    ibo = initGlBuffer(GL_ELEMENT_ARRAY_BUFFER, indices);
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    VertexT::bindAttributes();

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    checkGLError();
  }

  template <typename VertexT>
  void Mesh<VertexT>::draw() const
  {
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    checkGLError();
  }

  template <typename VertexT>
  GLuint Mesh<VertexT>::vaoid() const noexcept {
    return vao;
  }
}

#endif /* _SRC_RENDER_MESH_H */