/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Framebuffer.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-27
 ///

#ifndef _SRC_LIBS_GLWRAP_FRAME_BUFFER_H
#define _SRC_LIBS_GLWRAP_FRAME_BUFFER_H

#include <GL/glew.h>
#include "Helpers.hpp"
#include <initializer_list>
#include <optional>
#include <array>
#include <tuple>
#include <string>
#include <cassert>

namespace glhelp {

  template <size_t N>
  class Framebuffer {
  public:

    using BuffersDescriptor = std::pair<size_t, std::tuple<size_t, size_t, size_t>>;
    using BufferInitList = std::initializer_list<BuffersDescriptor>;

    struct BuildFailure { std::string msg; };

    Framebuffer(
      int width, int height,
      BufferInitList&& buffers,
      std::optional<BuffersDescriptor>&& depthStencil = std::nullopt);
    ~Framebuffer() noexcept;

    Framebuffer(Framebuffer&& f) noexcept;
    Framebuffer& operator= (Framebuffer&& f) noexcept;

    Framebuffer(const Framebuffer&) = delete;
    Framebuffer& operator= (const Framebuffer&) = delete;

    void bindOn(GLenum target) const;
    void bindTextureOn(
      GLuint attachement,
      GLuint location,
      GLuint sampler,
      size_t texture) const;

    constexpr size_t size() const noexcept;
    int width() const noexcept;
    int height() const noexcept;

  private:
    std::array<GLuint, N> _buffers;
    std::optional<std::pair<GLuint, size_t>> _depthStencil;
    size_t _colorBuffersCount;
    GLuint _fbo;
    int _width, _height;
  };
}

/////////////////////////////////////////////////////////////////

namespace glhelp {

  template <size_t N>
  Framebuffer<N>::Framebuffer(
    int width, int height,
    BufferInitList&& buffers,
    std::optional<BuffersDescriptor>&& depthStencil) :
    _buffers(), _depthStencil(std::nullopt),
    _colorBuffersCount(buffers.size()),
    _fbo(0),
    _width(width), _height(height)
  {
    assert((depthStencil.has_value() && buffers.size() == N - 1)
      || (!depthStencil.has_value() && buffers.size() == N));

    _buffers.fill(0);
    glGenFramebuffers(1, &_fbo);
    glGenTextures(N, _buffers.data());
    glhelp::checkGLError();

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
    GLenum drawBuffers[buffers.size()];
    for (const auto& [index, descriptor] : buffers) {
      const auto& [internal, format, type] = descriptor;
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, _buffers[index]);
      glTexImage2D(
        GL_TEXTURE_2D, 0, internal,
        width, height, 0, format,
        type, nullptr);
      glFramebufferTexture2D(
        GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + index,
        GL_TEXTURE_2D, _buffers[index], 0);
      glBindTexture(GL_TEXTURE_2D, 0);
      glhelp::checkGLError();
      drawBuffers[index] = GL_COLOR_ATTACHMENT0 + index;
    }
    glDrawBuffers(_colorBuffersCount, drawBuffers);
    glhelp::checkGLError();

    if (depthStencil.has_value()) {
      const auto& [attachement, descriptor] = depthStencil.value();
      const auto& [internal, format, type] = descriptor;
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, _buffers[N - 1]);
      glTexImage2D(GL_TEXTURE_2D, 0, internal, width, height,
        0, format, type, nullptr);
      glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, attachement,
        GL_TEXTURE_2D, _buffers[N - 1], 0);
      glBindTexture(GL_TEXTURE_2D, 0);
      glhelp::checkGLError();
    }

    GLenum status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
    if (GL_FRAMEBUFFER_COMPLETE != status) {
      this->~Framebuffer();
      throw BuildFailure{ "Framebuffer build failure : " + std::to_string(status) };
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glhelp::checkGLError();
  }
  template <size_t N>
  Framebuffer<N>::~Framebuffer() noexcept
  {
    if (_fbo) glDeleteFramebuffers(1, &_fbo);
    for (GLuint t : _buffers) {
      if (t) glDeleteTextures(1, &t);
    }
  }

  template <size_t N>
  Framebuffer<N>::Framebuffer(Framebuffer<N>&& f) noexcept :
    _buffers(f._buffers),
    _depthStencil(f._depthStencil),
    _colorBuffersCount(f._colorBuffersCount),
    _fbo(f._fbo)
  {
    f._fbo = 0;
    f._buffers.fill(0);
    f._depthStencil.reset();
  }
  template <size_t N>
  Framebuffer<N>& Framebuffer<N>::operator= (Framebuffer<N>&& f) noexcept
  {
    this->~Framebuffer();
    _fbo = f._fbo;
    _buffers = f._buffers;
    _depthStencil = f._depthStencil;
    f._fbo = 0;
    f._buffers.fill(0);
    f._depthStencil.reset();
    return *this;
  }

  template <size_t N>
  void Framebuffer<N>::bindOn(GLenum target) const
  {
    glBindFramebuffer(target, _fbo);
    glhelp::checkGLError();
  }
  template <size_t N>
  void Framebuffer<N>::bindTextureOn(
    GLuint attachement,
    GLuint location,
    GLuint sampler,
    size_t texture) const
  {
    glActiveTexture(GL_TEXTURE0 + attachement);
    glBindTexture(GL_TEXTURE_2D, _buffers[texture]);
    glBindSampler(attachement, sampler);
    glUniform1i(location, attachement);
    glhelp::checkGLError();
  }

  template <size_t N>
  constexpr size_t Framebuffer<N>::size() const noexcept
  {
    return N;
  }
  template <size_t N>
  int Framebuffer<N>::width() const noexcept
  {
    return _width;
  }
  template <size_t N>
  int Framebuffer<N>::height() const noexcept
  {
    return _height;
  }
}

#endif /* _SRC_LIBS_GLWRAP_FRAME_BUFFER_H */