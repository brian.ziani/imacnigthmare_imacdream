/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Shader.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-11
 ///

#include "Program.hpp"
#include "Helpers.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <exception>
#include <fstream>
#include <sstream>

namespace {

  enum class ProgramType {
    Program,
    Shader
  };

  const std::string ProgramTypeStr(ProgramType t) noexcept
  {
    switch (t) {
    case ProgramType::Program: return "Program";
    case ProgramType::Shader: return "Shader";
    default:
      assert(false && "Invalid Type");
      return "ERROR";
    };
  }

  void checkCompileErrors(const std::string& path, GLuint shader, ProgramType type)
  {
    GLint success;
    GLchar infoLog[1024];
    switch (type) {
    case ProgramType::Program:
      glGetProgramiv(shader, GL_LINK_STATUS, &success);
      break;
    case ProgramType::Shader:
      glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
      break;
    default:
      assert(false && "Invalid Type");
    };
    if (!success) {
      glGetShaderInfoLog(shader, 1024, NULL, infoLog);
      throw std::runtime_error(path + ProgramTypeStr(type) + "\n" + infoLog);
    }
  }
}

namespace glhelp {

  GLuint Program::loadShader(const std::string& path, GLuint kind)
  {
    /* retrieve shader's code */
    std::string source;
    try {
      std::ifstream filestream;
      std::stringstream strstream;
      /* ensure ioerror will throw exceptions */
      filestream.exceptions(std::ifstream::failbit | std::ifstream::badbit);
      filestream.open(path);
      strstream << filestream.rdbuf();
      filestream.close();
      source = strstream.str();
    }
    catch (std::ifstream::failure& e) {
      throw std::runtime_error("Can't read file :" + path);
    }
    const char* ccode = source.c_str();
    GLuint shaderid = glCreateShader(kind);
    glShaderSource(shaderid, 1, &ccode, NULL);
    glCompileShader(shaderid);
    checkCompileErrors(path, shaderid, ProgramType::Shader);
    checkGLError();
    return shaderid;
  }

  Program::Program(const std::string& vertexPath, const std::string& fragmentPath) : id(0)
  {
    GLuint vertexShader(loadShader(vertexPath, GL_VERTEX_SHADER));
    GLuint fragmentShader(loadShader(fragmentPath, GL_FRAGMENT_SHADER));
    build(vertexShader, fragmentShader);
    // delete the shaders as they're linked into our program now and no longer necessery
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    checkGLError();
  }

  Program::Program() : id(0)
  {
  }
  Program::Program(Program&& s) noexcept : id(s.id)
  {
    s.id = 0;
  }

  Program& Program::operator= (Program&& s) noexcept
  {
    id = s.id;
    s.id = 0;
    return *this;
  }
  Program::~Program() noexcept
  {
    if (id) glDeleteProgram(id);
  }

  void Program::build(GLuint vertex, GLuint fragment)
  {
    id = glCreateProgram();
    glAttachShader(id, vertex);
    glAttachShader(id, fragment);
    glLinkProgram(id);
    checkCompileErrors("Linker : ", id, ProgramType::Program);
    checkGLError();
  }
  void Program::use() const noexcept
  {
    glUseProgram(id);
    checkGLError();
  }

  GLuint Program::uniform(const std::string& name) const
  {
    GLint result(glGetUniformLocation(id, name.c_str()));
#ifdef NDEBUG
    if (-1 == result) {
      throw std::runtime_error("Failed Found Uniform :" + name);
    }
#endif
    return result;
  }

  void Program::setBool(GLuint location, bool value) const noexcept
  {
    glUniform1i(location, (int)value);
  }
  void Program::setInt(GLuint location, int value) const noexcept
  {
    glUniform1i(location, value);
  }
  void Program::setFloat(GLuint location, float value) const noexcept
  {
    glUniform1f(location, value);
  }

  void Program::setVec2(GLuint location, const glm::vec2& value) const noexcept
  {
    glUniform2fv(location, 1, glm::value_ptr(value));
  }
  void Program::setVec2(GLuint location, float x, float y) const noexcept
  {
    glUniform2f(location, x, y);
  }

  void Program::setVec3(GLuint location, const glm::vec3& value) const noexcept
  {
    glUniform3fv(location, 1, glm::value_ptr(value));
  }
  void Program::setVec3(GLuint location, float x, float y, float z) const noexcept
  {
    glUniform3f(location, x, y, z);
  }

  void Program::setVec4(GLuint location, const glm::vec4& value) const noexcept
  {
    glUniform4fv(location, 1, glm::value_ptr(value));
  }
  void Program::setVec4(GLuint location, float x, float y, float z, float w) const noexcept
  {
    glUniform4f(location, x, y, z, w);
  }

  void Program::setMat2(GLuint location, const glm::mat2& mat, GLboolean transpose) const noexcept
  {
    glUniformMatrix2fv(location, 1, transpose, glm::value_ptr(mat));
  }
  void Program::setMat3(GLuint location, const glm::mat3& mat, GLboolean transpose) const noexcept
  {
    glUniformMatrix3fv(location, 1, transpose, glm::value_ptr(mat));
  }
  void Program::setMat4(GLuint location, const glm::mat4& mat, GLboolean transpose) const noexcept
  {
    glUniformMatrix4fv(location, 1, transpose, glm::value_ptr(mat));
  }
}