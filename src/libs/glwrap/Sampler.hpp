/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Sampler.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-27
 ///

#ifndef _SRC_LIBS_GLWRAP_SAMPLER_H
#define _SRC_LIBS_GLWRAP_SAMPLER_H

#include <GL/glew.h>
#include <initializer_list>
#include <variant>

namespace glhelp {
  class Sampler {
  public:

    Sampler(std::initializer_list<std::pair<GLenum, std::variant<int32_t, float, int32_t*, float*>>>&& parameters);
    ~Sampler() noexcept;

    Sampler(Sampler&& s) noexcept;
    Sampler& operator= (Sampler&& s) noexcept;

    Sampler(const Sampler&) = delete;
    Sampler& operator= (const Sampler&) = delete;

    void bindOn(GLuint index);
    GLuint glid() const noexcept;

  private:
    GLuint _id;
  };
}

#endif /* _SRC_LIBS_GLWRAP_SAMPLER_H */