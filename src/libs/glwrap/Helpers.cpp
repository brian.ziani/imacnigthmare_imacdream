/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 * Copyright (C) 2020 ZIANI Brian <brian.ziani@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file GLHelpers.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-21
 ///
#include "Helpers.hpp"
#include <stdexcept>
#include <sstream>

namespace glhelp {

  void checkGLError() {
    bool pgmerr(false);
    GLenum err(glGetError());
    std::stringstream sstr;
    while (err != GL_NO_ERROR) {
      switch (err) {
        case GL_INVALID_OPERATION : sstr << "[Invalid Operation]"; pgmerr = true; break;
        case GL_INVALID_ENUM : sstr << "[Invalid Enum]"; pgmerr = true; break;
        case GL_INVALID_VALUE : sstr << "[Invalid Value]"; pgmerr = true; break;
        case GL_OUT_OF_MEMORY : sstr << "[Out Of Memory]"; break;
        case GL_INVALID_FRAMEBUFFER_OPERATION : sstr << "[Invalid FBO op]"; pgmerr = true; break;
      }
      err = glGetError();
    }
    if (std::string errstr = sstr.str() ; 0 < errstr.length()) {
      if (pgmerr)
        throw std::invalid_argument(errstr.c_str());
      else 
        throw std::runtime_error(errstr.c_str());
    }
  }
}
