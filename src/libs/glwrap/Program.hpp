/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Shader.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-22
 ///
#pragma once
#ifndef _SRC_LIBS_GLWRAP_SHADER_H
#define _SRC_LIBS_GLWRAP_SHADER_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>

namespace glhelp {

  class Program {
  public:

    Program();
    ~Program() noexcept;

    Program(const std::string& vertexPath, const std::string& fragmentPath);

    Program(Program&& s) noexcept;
    Program& operator= (Program&& s) noexcept;

    Program(const Program& s) noexcept = delete;
    Program& operator= (const Program& s) noexcept = delete;

    void build(GLuint vertex, GLuint fragment);
    void use() const noexcept;

    static GLuint loadShader(const std::string& path, GLuint kind);

    GLuint uniform(const std::string& name) const;

    void setBool(GLuint location, bool value) const noexcept;
    void setInt(GLuint location, int value) const noexcept;
    void setFloat(GLuint location, float value) const noexcept;

    void setVec2(GLuint location, const glm::vec2& value) const noexcept;
    void setVec2(GLuint location, float x, float y) const noexcept;

    void setVec3(GLuint location, const glm::vec3& value) const noexcept;
    void setVec3(GLuint location, float x, float y, float z) const noexcept;

    void setVec4(GLuint location, const glm::vec4& value) const noexcept;
    void setVec4(GLuint location, float x, float y, float z, float w) const noexcept;

    void setMat2(GLuint location, const glm::mat2& mat, GLboolean transpose = GL_FALSE) const noexcept;
    void setMat3(GLuint location, const glm::mat3& mat, GLboolean transpose = GL_FALSE) const noexcept;
    void setMat4(GLuint location, const glm::mat4& mat, GLboolean transpose = GL_FALSE) const noexcept;

  private:

    uint id;
  };
}

#endif /* _SRC_LIBS_GLWRAP_SHADER_H */