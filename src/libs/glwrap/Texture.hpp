/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Texture.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-23
 ///

#ifndef _SRC_LIBS_GLWRAP_TEXTURE
#define _SRC_LIBS_GLWRAP_TEXTURE

#include <GL/glew.h>
#include "Helpers.hpp"
#include <vector>
#include <string>

namespace glhelp {

  template <typename PixelT = uint8_t>
  class Texture {
  public:

    std::vector<PixelT> pixels;

    Texture(int width = 0, int height = 0, int depth=1) noexcept;
    ~Texture() noexcept;

    Texture(Texture&& t) noexcept;
    Texture& operator= (Texture&& t) noexcept;

    Texture(const Texture& t) noexcept = delete;
    Texture& operator= (const Texture& t) noexcept = delete;

    GLuint glid() const noexcept;

    void build(
      GLuint internalFormat = GL_RGB8,
      GLuint format = GL_RGB,
      GLuint size = GL_UNSIGNED_BYTE);

  private:

    GLuint id;
    int width;
    int height;
    bool built;
  };

  Texture<uint8_t> readTextureFromFile(const std::string& path);
  Texture<uint8_t> buildTextureFromFile(const std::string& path);
}

//////////////////////////////////////////////////////////////////

namespace glhelp {

  template <typename PixelT>
  Texture<PixelT>::Texture(int width, int height, int depth) noexcept :
    pixels(), id(0), width(width), height(height), built(false)
  {
    pixels.reserve(width * height * depth);
  }
  template <typename PixelT>
  Texture<PixelT>::~Texture() noexcept
  {
    if (id) glDeleteTextures(1, &id);
  }
  template <typename PixelT>
  Texture<PixelT>::Texture(Texture<PixelT>&& t) noexcept :
    pixels(std::move(t.pixels)), id(t.id), width(t.width), height(t.height), built(t.built)
  {
    t.id = t.width = t.height = 0;
  }
  template <typename PixelT>
  Texture<PixelT>& Texture<PixelT>::operator= (Texture<PixelT>&& t) noexcept
  {
    id = t.id;
    width = t.width;
    height = t.height;
    pixels = std::move(t.pixels);
    built = t.built;
    t.id = t.width = t.height = 0;
    t.built = false;
    return *this;
  }

  template <typename PixelT>
  void Texture<PixelT>::build(GLuint internalFormat, GLuint format, GLuint size)
  {
    if (!built) {
      glGenTextures(1, &id);
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, id);
      glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, size, pixels.data());
      glBindTexture(GL_TEXTURE_2D, 0);
      checkGLError();
    }
    built = true;
  }

  template <typename PixelT>
  GLuint Texture<PixelT>::glid() const noexcept
  {
    return id;
  }

}


#endif /* _SRC_LIBS_GLWRAP_TEXTURE */