/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 * Copyright (C) 2020 ZIANI Brian <brian.ziani@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file GLHelpers.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-11
 ///

#ifndef _SRC_LIBS_HELPERS_GL
#define _SRC_LIBS_HELPERS_GL

#include <GL/glew.h>
#include <vector>

namespace glhelp {

  /// \brief Throw a runtime exception if a GLError has occured
  void checkGLError();

  /// \brief Create an opengl buffer from a vector
  template <typename T>
  GLuint initGlBuffer(GLuint dock, std::vector<T>& datas, GLenum usage=GL_STATIC_DRAW);
}

//////////////////////////////////////////////////////////////////////

namespace glhelp {
  template <typename T>
  GLuint initGlBuffer(GLuint dock, std::vector<T>& datas, GLenum usage) {
    GLuint id;
    glGenBuffers(1, &id);
    glBindBuffer(dock, id);
    glBufferData(dock, datas.size() * sizeof(T), datas.data(), usage);
    glBindBuffer(dock, 0);
    checkGLError();
    return id;
  }
}

#endif /* _SRC_LIBS_HELPERS_GL */