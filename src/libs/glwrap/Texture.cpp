/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Texture.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-23
 ///

#include "Texture.hpp"
#include <SDL2/SDL_image.h>
#include <functional>
#include <stdexcept>

namespace {

  glhelp::Texture<uint8_t> loadTexture(
    const std::string& path,
    std::function<void(
      std::vector<uint8_t>&,
      uint8_t, uint8_t, uint8_t, uint8_t)> loader,
    std::function<void(glhelp::Texture<uint8_t>&)> builder)
  {

    SDL_Surface* img(IMG_Load(path.c_str()));
    if (nullptr == img) {
      throw std::runtime_error(IMG_GetError());
    }
    SDL_LockSurface(img);
    glhelp::Texture<uint8_t> texture(img->w, img->h, 3);
    for (int i = 0; i < img->h; ++i) {
      for (int j = 0; j < img->w; ++j) {
        uint32_t pixel = static_cast<uint32_t*>(img->pixels)[i * img->w + j];
        uint8_t r, g, b, a;
        SDL_GetRGBA(pixel, img->format, &r, &g, &b, &a);
        loader(texture.pixels, r, g, b, a);
      }
    }
    SDL_UnlockSurface(img);
    SDL_FreeSurface(img);
    builder(texture);
    return texture;
  }
}

namespace glhelp {

  Texture<uint8_t> readTextureFromFile(const std::string& path)
  {
    return loadTexture(path,
      [](std::vector<uint8_t>& pixels, uint8_t r, uint8_t g, uint8_t b, uint8_t) -> void {
        pixels.emplace_back(r);
        pixels.emplace_back(g);
        pixels.emplace_back(b);
      },
      [](Texture<uint8_t>&) -> void {});
  }
  Texture<uint8_t> buildTextureFromFile(const std::string& path)
  {
    return loadTexture(path,
      [](std::vector<uint8_t>& pixels, uint8_t r, uint8_t g, uint8_t b, uint8_t) -> void {
        pixels.emplace_back(r);
        pixels.emplace_back(g);
        pixels.emplace_back(b);
      },
      [](Texture<uint8_t>& texture) -> void {
        texture.build();
      });
  }
}