/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Sampler.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-27
 ///

#include "Sampler.hpp"
#include "Helpers.hpp"
#include <functional>
#include <cassert>

namespace glhelp {


  Sampler::Sampler(std::initializer_list<std::pair<GLenum, std::variant<int32_t, float, int32_t*, float*>>>&& parameters) :
    _id(0)
  {
    glGenSamplers(1, &_id);
    glhelp::checkGLError();

    for (const auto& [pname, args] : parameters) {
      switch (args.index()) {
      case 0: glSamplerParameteri(_id, pname, std::get<0>(args)); break;
      case 1: glSamplerParameterf(_id, pname, std::get<1>(args)); break;
      case 2: glSamplerParameteriv(_id, pname, std::get<2>(args)); break;
      case 3: glSamplerParameterfv(_id, pname, std::get<3>(args)); break;
      default: assert(false && "WTF DODO");
      }
      glhelp::checkGLError();
    }
  }

  Sampler::~Sampler() noexcept
  {
    if (_id) glDeleteSamplers(1, &_id);
  }

  Sampler::Sampler(Sampler&& s) noexcept :
    _id(s._id)
  {
    s._id = 0;
  }
  Sampler& Sampler::operator= (Sampler&& s) noexcept
  {
    _id = s._id;
    s._id = 0;
    return *this;
  }

  void Sampler::bindOn(GLuint index)
  {
    glBindSampler(index, _id);
    glhelp::checkGLError();
  }
  GLuint Sampler::glid() const noexcept
  {
    return _id;
  }
}