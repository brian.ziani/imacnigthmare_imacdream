/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Perlin.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-15
 ///

#ifndef _SRC_GENERATOR_PERLIN_NOISE_H
#define _SRC_GENERATOR_PERLIN_NOISE_H

#include "Common.hpp"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <initializer_list>
#include <vector>

namespace gen {

  class PerlinNoise {
  public:

    /// \brief Create a Perlin Noise generator with given base frequency and harmonics
    PerlinNoise(
      const Settings& settings, 
      const std::initializer_list<Harmonic>& harmonics) noexcept;
    ~PerlinNoise() noexcept = default;
    /// \brief Return noise value at given position, invariant between calls
    float noiseAt(const glm::vec2& pos) const noexcept;
    float noiseAt3(const glm::vec3& pos) const noexcept;
    /// \brief Return floor's normal at given position
    struct Plan {
      glm::vec3 tangent;
      glm::vec3 bitangent;
    };
    Plan planAt(const glm::vec2& pos) const noexcept;

  private:

    /// \brief Return fondamental noise value at given point
    float sampleNoiseAt(const glm::vec2& pos) const noexcept;
    /// \brief Return gradiant value at position rounded to smallest integer
    glm::vec2 sampleGradiantAt(const glm::vec2& pos) const noexcept;

    std::vector<glm::vec2> _field;
    std::vector<Harmonic> _harmonics;
    float _frequency;
    float _noiseAmplitude;
    float _scale;
  };
}

#endif /* _SRC_GENERATOR_PERLIN_NOISE_H */