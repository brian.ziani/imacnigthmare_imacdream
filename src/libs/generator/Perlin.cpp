/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Perlin.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-15
 ///

#include "Perlin.hpp"

#include <libs/utils/Math.hpp>
#include <glm/glm.hpp>

namespace gen {

  PerlinNoise::PerlinNoise(
    const Settings& settings,
    const std::initializer_list<Harmonic>& harmonics) noexcept :
    _field(generateVectorField(settings.seed, settings.samplefieldsize)), 
    _harmonics(harmonics),
    _frequency(settings.frequency),
    _noiseAmplitude(settings.noiseAmplitude),
    _scale(settings.scale) {
    /* generate random vector field */
    /* precompute maximal amplitude */
    float harmonicsAmplitude = 0.f;
    for (auto& h : _harmonics) {
      harmonicsAmplitude += h.amplitude;
    }
    _noiseAmplitude /= harmonicsAmplitude;
  }

  float PerlinNoise::noiseAt(const glm::vec2& pos) const noexcept {
    float res(0);
    size_t i(0);
    glm::vec2 tmp(pos * _scale);
    for (auto& h : _harmonics) {
      glm::vec2 p(tmp + _field[i] * h.amplitude);
      res += h.amplitude * sampleNoiseAt(p * h.order * _frequency);
      i = (i + 1) % _harmonics.size();
    }
    return res * _noiseAmplitude;
  }
  float PerlinNoise::noiseAt3(const glm::vec3& pos) const noexcept {
    return noiseAt(glm::vec2(pos.x, pos.z));
  }

  PerlinNoise::Plan PerlinNoise::planAt(const glm::vec2& pos) const noexcept {
    glm::vec2 dx(0.01f, 0.f);
    glm::vec2 dz(0.f, 0.01f);
    glm::vec3 DX(2.f * dx.x, noiseAt(pos + dx) - noiseAt(pos - dx), 0.f);
    glm::vec3 DZ(0.f, noiseAt(pos + dz) - noiseAt(pos - dz), 2.f * dz.y);
    return Plan{
      .tangent = glm::normalize(DZ),
      .bitangent = glm::normalize(DX) };
  }

  float PerlinNoise::sampleNoiseAt(const glm::vec2& pos) const noexcept {
    glm::vec2 a(std::floor(pos.x), std::floor(pos.y));
    glm::vec2 b(a + glm::vec2(1, 0));
    glm::vec2 c(a + glm::vec2(0, 1));
    glm::vec2 d(a + glm::vec2(1, 1));

    glm::vec2 t(math::smootherstep(pos.x - a.x), math::smootherstep(pos.y - a.y));
    return math::interpolate(t.y,
      math::interpolate(t.x,
        fastdot(sampleGradiantAt(a), (pos - a)),
        fastdot(sampleGradiantAt(b), (pos - b))),
      math::interpolate(t.x,
        fastdot(sampleGradiantAt(c), (pos - c)),
        fastdot(sampleGradiantAt(d), (pos - d))));
  }
  glm::vec2 PerlinNoise::sampleGradiantAt(const glm::vec2& pos) const noexcept {
    int x(math::safemod(pos.x, 256));
    int y(math::safemod(pos.y, 256));
    return _field[Permutation[Permutation[(Permutation[x] + y)]] % _field.size()];
  }
}