/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Simplex.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-21
 ///

#ifndef _SRC_LIBS_GENERATOR_SIMPLEX_H
#define _SRC_LIBS_GENERATOR_SIMPLEX_H

#include "Common.hpp"
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vector>

namespace gen {

  class SimplexNoise {
  public:

    SimplexNoise(const Settings& settings) noexcept;
    ~SimplexNoise() noexcept;

    SimplexNoise(SimplexNoise&& n) noexcept;
    SimplexNoise& operator= (SimplexNoise&& n) noexcept;

    SimplexNoise(const SimplexNoise&) = delete;
    SimplexNoise& operator= (const SimplexNoise&) = delete;

    float noiseAt(const glm::vec2& pos) const noexcept;
    float noiseAt3(const glm::vec3& pos) const noexcept;

  private:

    float sampleNoiseAt(const glm::vec2& pos) const noexcept;

    std::vector<glm::vec2> _field;
    uint8_t* _lookTable;
    Settings _settings;
  };
}

#endif /* _SRC_LIBS_GENERATOR_SIMPLEX_H */