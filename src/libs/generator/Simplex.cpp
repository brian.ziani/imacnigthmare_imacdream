/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Simplex.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-22
 ///

#include "Simplex.hpp"
#include <libs/utils/Math.hpp>

namespace gen {

  SimplexNoise::SimplexNoise(const Settings& settings) noexcept :
    _field(generateVectorField(settings.seed, settings.samplefieldsize)),
    _lookTable(new uint8_t[512]),
    _settings(settings)
  {
    for (size_t i = 0; i < 512; ++i) {
      _lookTable[i] = Permutation[i] % settings.samplefieldsize;
    }
  }
  SimplexNoise::~SimplexNoise() noexcept
  {
    delete[] _lookTable;
  }

  SimplexNoise::SimplexNoise(SimplexNoise&& n) noexcept :
    _field(std::move(n._field)), _lookTable(n._lookTable), _settings(n._settings)
  {
    n._lookTable = nullptr;
  }
  SimplexNoise& SimplexNoise::operator= (SimplexNoise&& n) noexcept
  {
    _field = std::move(n._field);
    _lookTable = n._lookTable;
    _settings = n._settings;
    _lookTable = nullptr;
    return *this;
  }

  float SimplexNoise::noiseAt(const glm::vec2& pos) const noexcept
  {
    return (
      sampleNoiseAt(pos) * 0.4f
      + sampleNoiseAt(pos / 2.6f)
      + sampleNoiseAt(pos / 21.f) * 3.3f) / 4.7f;
  }
  float SimplexNoise::noiseAt3(const glm::vec3& pos) const noexcept
  {
    return noiseAt(glm::vec2(pos.x, pos.z));
  }

  float SimplexNoise::sampleNoiseAt(const glm::vec2& pos) const noexcept
  {
    float n0, n1, n2; // Noise contributions from the three corners
    // Skew the input space to determine which simplex cell we're in
    float xin = pos.x * _settings.scale;
    float yin = pos.y * _settings.scale;
    constexpr const float F2 = 0.366025404f;
    float s = (xin + yin) * F2; // Hairy factor for 2D
    int i = math::fastfloor(xin + s);
    int j = math::fastfloor(yin + s);
    constexpr const float G2 = 0.211324865f;
    float t = (i + j) * G2;
    float X0 = i - t; // Unskew the cell origin back to (x,y) space
    float Y0 = j - t;
    float x0 = xin - X0; // The x,y distances from the cell origin
    float y0 = yin - Y0;
    // For the 2D case, the simplex shape is an equilateral triangle.
    // Determine which simplex we are in.
    int i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
    if (x0 > y0) { i1 = 1; j1 = 0; } // lower triangle, XY order: (0,0)->(1,0)->(1,1)
    else { i1 = 0; j1 = 1; }      // upper triangle, YX order: (0,0)->(0,1)->(1,1)
    // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
    // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
    // c = (3-sqrt(3))/6
    float x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
    float y1 = y0 - j1 + G2;
    float x2 = x0 - 1.0f + 2.0f * G2; // Offsets for last corner in (x,y) unskewed coords
    float y2 = y0 - 1.0f + 2.0f * G2;
    // Work out the hashed gradient indices of the three simplex corners
    int ii = i & 255;
    int jj = j & 255;
    int gi0 = _lookTable[ii + Permutation[jj]];
    int gi1 = _lookTable[ii + i1 + Permutation[jj + j1]];
    int gi2 = _lookTable[ii + 1 + Permutation[jj + 1]];
    // Calculate the contribution from the three corners
    float t0 = 0.5f - x0 * x0 - y0 * y0;
    if (t0 < 0) n0 = 0.0f;
    else {
      t0 *= t0;
      n0 = t0 * t0 * fastdot(_field[gi0], x0, y0);  // (x,y) of grad3 used for 2D gradient
    }
    float t1 = 0.5f - x1 * x1 - y1 * y1;
    if (t1 < 0) n1 = 0.0f;
    else {
      t1 *= t1;
      n1 = t1 * t1 * fastdot(_field[gi1], x1, y1);
    }
    float t2 = 0.5f - x2 * x2 - y2 * y2;
    if (t2 < 0) n2 = 0.0f;
    else {
      t2 *= t2;
      n2 = t2 * t2 * fastdot(_field[gi2], x2, y2);
    }
    // Add contributions from each corner to get the final noise value.
    // The result is scaled to return values in the interval [-1,1].
    return _settings.noiseAmplitude * 70.0f * (n0 + n1 + n2);
  }
}