/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Serial.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#ifndef _SRC_LIBS_UTILS_SERIAL_H
#define _SRC_LIBS_UTILS_SERIAL_H

#include <fstream>
#include <string>
#include <tuple>
#include <unordered_map>

namespace utils {
  /// \brief Namespace containing utilities to serialize and deserialize simple datas
  namespace serial {

    /// \brief Struct describing an entry to serialise
    struct Entry {
      enum class Type {
        String, Int, Float
      };
      std::size_t offset; ///< Offset from begining of object
      Type type;          ///< Data type
    };

    /// \brief This table is used to store and load an object from a file
    using SerialisationTable = std::unordered_map<std::string, Entry>;

    /// \brief Temporary buffer usefull to store several objects in the same file
    ///   Contains all the (key,value) pairs found in plain text format
    using Readresult = std::unordered_map<std::string, std::string>;

    struct FileNotFound { std::string what; };

    /// \brief Error with parsing or openning of a file
    struct IOFailure { std::string what; };

    /// \brief Error with deserialisation of an object
    struct MissingEntry { std::string what; };

    /// \brief Read a file containing some serialised objects
    Readresult readFile(const std::string& path);

    /// \brief Read a specific object from a preparsed file
    /// \throw MissingEntry if strict == true and an entry is missing
    void deserialise(
      const Readresult& raw,
      const SerialisationTable& table,
      void* obj,
      bool strict = true);
  }
}

#endif /* _SRC_LIBS_UTILS_SERIAL_H */