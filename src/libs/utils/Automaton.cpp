/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Automaton.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-14
 ///

#include "Automaton.hpp"

#include <iostream>
#include <fstream>
#include <regex>

namespace utils {

  void parseDotFile(
    const std::string& path,
    std::function<void(
      const std::string& name,
      const std::string& shape)> nodeCallback,
    std::function<void(
      const std::string& src,
      const std::string& dest,
      const std::optional<std::string>& label)> edgeCallaback)
  {
    try {
      size_t lineNumber(0);
      std::string line;
      std::ifstream filestream;
      filestream.exceptions(std::ifstream::badbit);
      filestream.open(path);
      if (!filestream) {
        throw std::runtime_error("Failed open file : " + path);
      }

      std::regex nodeRegex(
        " *([a-z_]*) \\[shape = ([a-z_]*)\\];",
        std::regex_constants::icase);
      std::regex edgeRegex(
        " *([a-z_]*) -> ([a-z_]*).*;",
        std::regex_constants::icase);
      std::regex labelRegex(
        "\\[label = \"([a-z0-9_]*)\"\\]",
        std::regex_constants::icase);

      while (std::getline(filestream, line)) {
        std::smatch match;
        if (std::regex_match(line, match, nodeRegex)) {
          nodeCallback(match[1], match[2]);
        }
        else if (std::regex_match(line, match, edgeRegex)) {
          std::smatch labelMatch;
          std::optional<std::string> label(std::nullopt);
          if (std::regex_search(line, labelMatch, labelRegex)) {
            label = std::make_optional(labelMatch[1]);
          }
          edgeCallaback(match[1], match[2], label);
        }
        ++lineNumber;
      }
    }
    catch (std::ifstream::failure& e) {
      throw std::runtime_error("Can't read file :" + path);
    }
  }
}