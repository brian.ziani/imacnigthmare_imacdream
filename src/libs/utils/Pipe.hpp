/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Pipe.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-21
 ///

#ifndef _SRC_LIBS_UTILS_LOCK_FREE_QUEUE_H
#define _SRC_LIBS_UTILS_LOCK_FREE_QUEUE_H

#include <cstddef>
#include <cassert>

namespace utils {

  /// \brief Provide a lock free Pipe usefull to comunicate between two threads
  template <typename T>
  class Pipe {
  public:

    struct InvalidWrite {};
    struct InvalidRead {};

    Pipe(size_t maxlength) noexcept;
    ~Pipe() noexcept;

    template <typename ...Args>
    void write(Args&& ...args);
    T read();

    bool full() const noexcept;
    bool empty() const noexcept;

  private:

    T* _buffer;
    size_t _length;
    size_t _writeHead;
    size_t _readHead;
  };
}

//////////////////////////////////////////////////////////////////

namespace utils {

  template <typename T>
  Pipe<T>::Pipe(size_t maxlength) noexcept :
    _buffer(new T[maxlength]),
    _length(maxlength),
    _writeHead(0),
    _readHead(0)
  {
  }
  template <typename T>
  Pipe<T>::~Pipe() noexcept
  {
    delete[] _buffer;
  }

  template <typename T>
  template <typename ...Args>
  void Pipe<T>::write(Args&& ...args)
  {
    if (full()) throw InvalidWrite();
    ::new(_buffer + _writeHead) T(args...);
    _writeHead = (_writeHead + 1) % _length;
  }
  template <typename T>
  T Pipe<T>::read()
  {
    if (empty()) throw InvalidRead();
    T head(_buffer[_readHead]);
    _readHead = (_readHead + 1) % _length;
    return head;
  }

  template <typename T>
  bool Pipe<T>::full() const noexcept
  {
    return (_writeHead + 1) % _length == _readHead;
  }
  template <typename T>
  bool Pipe<T>::empty() const noexcept
  {
    return _writeHead == _readHead;
  }
}

#endif /* _SRC_LIBS_UTILS_LOCK_FREE_QUEUE_H */