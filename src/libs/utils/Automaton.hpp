/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Automaton.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-14
 ///

#ifndef _SRC_LIBS_AUTOMATON_AUTOMATON_H
#define _SRC_LIBS_AUTOMATON_AUTOMATON_H

#include <string>
#include <functional>
#include <optional>
#include <vector>
#include <utility>
#include <cassert>
#include <ostream>

namespace utils {

  template <typename ActionF, typename EventT>
  class Automaton {
  public:

    using StateID = std::size_t;
    using TransitionTable = std::vector<std::pair<EventT, StateID>>;
    using Transition = typename TransitionTable::const_iterator;

    class State {
    public:

      State(
        StateID id = 0,
        const Automaton* a = nullptr,
        const EventT& trigger = EventT()) noexcept;
      ~State() noexcept = default;

      State(const State&) noexcept = default;
      State& operator= (const State&) noexcept = default;

      State(State&&) noexcept = default;
      State& operator= (State&&) noexcept = default;

      State follow(const Transition& t, const EventT& triggger) const noexcept;

      Transition begin() const noexcept;
      Transition end() const noexcept;

      void call() const noexcept;

      StateID id() const noexcept;

    private:

      friend class Automaton;
      template <typename A, typename E>
      friend std::ostream& operator<< (std::ostream&, const typename Automaton<A, E>::State&) noexcept;

      const Automaton* _automaton;
      EventT _trigger;
      StateID _state;
    };

    void clear() noexcept;

    StateID addState(const std::optional<ActionF>& a = std::nullopt) noexcept;
    void addTransition(StateID src, StateID dest, const EventT& trigger) noexcept;

    void setEntry(StateID id) noexcept;

    State begin() const noexcept;

  private:

    template <typename A, typename E>
    friend std::ostream& operator<< (std::ostream&, const Automaton<A, E>&) noexcept;

    struct Node {

      Node(size_t i, const std::optional<ActionF>& a) noexcept;

      std::vector<std::pair<EventT, StateID>> outgoings;
      std::optional<ActionF> action;
      StateID id;
    };

    using NodeList = std::vector<Node>;

    NodeList _states;
    StateID _entry = 0;
    StateID _nextNodeID = 0;
  };

  void parseDotFile(
    const std::string& path,
    std::function<void(
      const std::string& name,
      const std::string& shape)> nodeCallback,
    std::function<void(
      const std::string& src,
      const std::string& dest,
      const std::optional<std::string>& label)> edgeCallaback);
}

////////////////////////////////////////////////////////////////////////

namespace utils {

  template <typename ActionF, typename EventT>
  Automaton<ActionF, EventT>::State::State(
    StateID id,
    const Automaton* a,
    const EventT& trigger) noexcept :
    _automaton(a), _trigger(trigger), _state(id)
  {
  }

  template <typename ActionF, typename EventT>
  typename Automaton<ActionF, EventT>::State Automaton<ActionF, EventT>::State::follow(
    const Transition& t, const EventT& trigger) const noexcept
  {
    assert(_automaton);
    return State(t->second, _automaton, trigger);
  }

  template <typename ActionF, typename EventT>
  typename Automaton<ActionF, EventT>::Transition Automaton<ActionF, EventT>::State::begin() const noexcept
  {
    assert(_automaton);
    return _automaton->_states[_state].outgoings.cbegin();
  }
  template <typename ActionF, typename EventT>
  typename Automaton<ActionF, EventT>::Transition Automaton<ActionF, EventT>::State::end() const noexcept
  {
    assert(_automaton);
    return _automaton->_states[_state].outgoings.cend();
  }

  template <typename ActionF, typename EventT>
  void Automaton<ActionF, EventT>::State::call() const noexcept
  {
    assert(_automaton);
    const auto& action(_automaton->_states[_state].action);
    if (action.has_value()) {
      action.value()(_trigger);
    }
  }
  template <typename ActionF, typename EventT>
  typename Automaton<ActionF, EventT>::StateID Automaton<ActionF, EventT>::State::id() const noexcept
  {
    return _state;
  }
  template <typename A, typename E>
  std::ostream& operator<< (std::ostream& s, const typename Automaton<A, E>::State& e) noexcept
  {
    return s << e._state;
  }

  //////////////////////////////////////////////////////////////

  template <typename ActionF, typename EventT>
  void Automaton<ActionF, EventT>::clear() noexcept
  {
    _states.clear();
    _nextNodeID = _entry = 0;
  }

  template <typename ActionF, typename EventT>
  typename Automaton<ActionF, EventT>::StateID Automaton<ActionF, EventT>::addState(
    const std::optional<ActionF>& a) noexcept
  {
    _states.emplace_back(_nextNodeID++, a);
    return _states.back().id;
  }
  template <typename ActionF, typename EventT>
  void Automaton<ActionF, EventT>::addTransition(
    StateID src, StateID dest, const EventT& trigger) noexcept
  {
    assert(src < _states.size() && "invalid state id");
    assert(dest < _states.size() && "invalid state id");
    _states[src].outgoings.emplace_back(trigger, dest);
  }
  template <typename ActionF, typename EventT>
  void Automaton<ActionF, EventT>::setEntry(StateID id) noexcept
  {
    assert(id < _states.size() && "invalid state id");
    _entry = id;
  }

  template <typename ActionF, typename EventT>
  typename Automaton<ActionF, EventT>::State Automaton<ActionF, EventT>::begin() const noexcept
  {
    assert(0 < _states.size() && "Empty Automaton");
    return State(_entry, this);
  }

  template <typename ActionF, typename EventT>
  std::ostream& operator<< (std::ostream& s, const Automaton<ActionF, EventT>& a) noexcept
  {
    s << "[Automaton] : Entry=" << a._entry << '\n';
    s << "  Nodes :" << '\n';
    for (const auto& state : a._states) {
      s << "    " << state.id << " : " << state.action.has_value();
      for (const auto& [trigger, dest] : state.outgoings) {
        s << "     " << trigger << " -> " << dest << '\n';
      }
    }
    return s << "==" << '\n';
  }

  /////////////////////////////////////////////////////////////////////////////

  template <typename ActionF, typename EventT>
  Automaton<ActionF, EventT>::Node::Node(size_t i, const std::optional<ActionF>& a) noexcept :
    outgoings(), action(a), id(i)
  {

  }
}

#endif /* _SRC_LIBS_AUTOMATON_AUTOMATON_H */