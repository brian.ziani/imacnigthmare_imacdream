/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Math.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-27
 ///

#include "Math.hpp"

namespace math {

  QuadraticSolution quadraticEquation(float a, float b, float c) noexcept
  {
    float delta(b * b - 4.f * a * c);
    if (delta < 0) {
      return QuadraticSolution(0);
    }
    if (fequals(delta, 0.f)) {
      float x = -b / (2.f * a);
      return QuadraticSolution(1, x, x);
    }
    delta = sqrt(delta);
    float denom(1.f / (2.f * a));
    return QuadraticSolution(
      2,
      (-b - delta) * denom,
      (-b + delta) * denom);
  }

  std::pair<bool, float> smallestSolutionInRange(float min, float max, const QuadraticSolution& sol) noexcept
  {
    if (sol.count == 0) {
      return std::make_pair(false, 0.f);
    }
    if (sol.count == 1) {
      return std::make_pair(min <= sol.x0 && sol.x0 <= max, sol.x0);
    }
    float x0 = std::min(sol.x0, sol.x1);
    float x1 = std::max(sol.x0, sol.x1);
    if (min <= x0 && x0 <= max) {
      return std::make_pair(true, x0);
    }
    if (min <= x1 && x1 <= max) {
      return std::make_pair(true, x1);
    }
    return std::make_pair(false, 0.f);
  }
}