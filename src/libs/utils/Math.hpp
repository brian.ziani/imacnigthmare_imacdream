/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Mesh.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-12
 ///

#ifndef MATH_H
#define MATH_H

#include <cmath>

namespace math {

  /// \brief Uniform rounding function that push halfvalues toward +inf
  ///
  ///   Default 'round' function in std push half values away from 0
  ///   but this behaviour leads to difference on negatives or positives
  ///   -0.5 and 0.5 will be rounded to -1 and 1 (resp) witch may cause
  ///   problems
  ///
  ///   This function will round -0.5 and 0.5 to 0 and 1 (resp)
  ///   which may be more stable for several operations
  inline constexpr int mrnd(float a) noexcept
  {
    return std::roundf(a) + ((a < 0) && (fabsf(a - std::roundf(a)) >= 0.5f));
  }
  /// \brief Fastest mrnd function, faster than mrnd by ~20%
  /// \see int math::mrnd(float a)
  inline constexpr int fastmrnd(float a) noexcept
  {
    return (int)(a + (a < -0.5f ? -0.5f : 0.5f))
      + (a < -0.5f && ((int)a - a) == 0.5f);
  }
  /// \brief Same as mrnd but faster
  /// \see int math::mrnd(float a)
  inline constexpr int fastmrnd2(float a) noexcept
  {
    return (int)(a + (a < -0.5f ? -0.5f + (((int)a - a) == 0.5f) : 0.5f));
  }
  inline constexpr int fastfloor(float x) noexcept
  {
    int xx(x);
    return x < xx ? xx - 1 : xx;
  }
  /// \brief Modulo that always return value between 0 and b
  inline constexpr int safemod(int a, int b) noexcept
  {
    if (a < 0)
      return b - ((-a) % b);
    return a % b;
  }
  /// \brief Smoothstep function, without clamping
  inline constexpr float smoothstep(float t) noexcept
  {
    return t * t * (3 - 2 * t);
  }
  /// \brief Smootherstep function, without clamping
  inline constexpr float smootherstep(float t) noexcept
  {
    return t * t * t * (t * (t * 6 - 15) + 10);
  }
  /// \brief compute linear interpolation between a and b with t in [0,1]
  inline constexpr float interpolate(float t, float a, float b) noexcept
  {
    return (1 - t) * a + t * b;
  }
  /// \brief Test for wide equality between two floats at given precision
  inline constexpr bool fequals(float a, float b, float precision = 0.00001f) noexcept
  {
    return fabs(a - b) <= precision;
  }
  /// \brief Round very small values to 0
  inline constexpr float nullifySmallValue(float a, float precision = 0.00001f) noexcept
  {
    return fequals(a, 0.f, precision) ? 0.f : a;
  }

  /// \brief solve given equation and return a pair containing number of poles
  ///   and the two poles
  struct QuadraticSolution {
    int count;
    float x0;
    float x1;
    inline QuadraticSolution(int c = 0, float x0 = 0, float x1 = 0) noexcept :
      count(c), x0(x0), x1(x1)
    {
    }
    inline explicit operator bool() const noexcept { return count != 0; }
  };
  QuadraticSolution quadraticEquation(float a, float b, float c) noexcept;
  std::pair<bool, float> smallestSolutionInRange(float min, float max, const QuadraticSolution& sol) noexcept;
}

#endif /* MATH_H */
