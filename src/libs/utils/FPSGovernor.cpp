/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file FPSGovernor.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-06
 ///

#include "FPSGovernor.hpp"
#include "log.hpp"
#include <SDL2/SDL_timer.h>

namespace utils {
  FPSGovernor::FPSGovernor(long fps, long sample) noexcept :
    start(SDL_GetTicks()), lastTime(start), period(1000 / fps),
    samplerStart(start), samplerLoad(0),
    samplerFrameCount(0), samplePeriod(sample) {
  }

  void FPSGovernor::tickBegin() noexcept {
    start = SDL_GetTicks();
  }
  void FPSGovernor::tickEnd() noexcept {
    ++samplerFrameCount;
    double sampletime = SDL_GetTicks() - samplerStart;
    if (sampletime >= samplePeriod) {
      LOG::info("AVG FPS :",
        std::setprecision(3), std::setw(3),
        samplerFrameCount * 1000.0 / sampletime,
        ": LOAD :", samplerLoad / samplerFrameCount, "%");
      samplerStart = SDL_GetTicks();
      samplerFrameCount = 0;
      samplerLoad = 0;
    }

    /* sleep until next tick */
    long ellapsed = SDL_GetTicks() - start;
    samplerLoad += ellapsed * 100. / period;
    if (ellapsed < period) {
      SDL_Delay(period - ellapsed);
    }
    lastTime = start;
  }
}