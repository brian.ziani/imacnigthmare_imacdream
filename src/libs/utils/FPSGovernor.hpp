/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file FPSGovernor.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-06
 ///

#ifndef _SRC_LIBS_FPS_CAPPER_H
#define _SRC_LIBS_FPS_CAPPER_H

namespace utils {

  /// \brief Object used govern game framerate
  class FPSGovernor {
  private:

    long start;   ///< Tick start time (ms)
    long lastTime;///< Begin of last tick (ms)
    long period;  ///< Duration of each frame (ms)

    double samplerStart;    ///< Time since last sample
    double samplerLoad;     ///< Sampler average load
    long samplerFrameCount; ///< Frames sinces last sample
    long samplePeriod;      ///< Time between two samples (ms)

  public:

    /// \brief Create a governor
    /// \param fps : Targeted fps
    /// \param sample : time in milliseconds between two load computation
    FPSGovernor(long fps, long sample) noexcept;
    /// \brief Destructor
    ~FPSGovernor() = default;

    /// \brief Compute one game tick and ensure correct framerate if possible
    /// \param tick : game tick callback
    template <typename Tick>
    void capTick(Tick&& tick) {
      tickBegin();
      tick(start - lastTime);
      tickEnd();
    }

  private:
    /// \brief Must be called at the begining of each tick
    void tickBegin() noexcept;
    /// \brief Must be called at the end of each tick
    ///  wait until the begining of next tick according to framerate
    void tickEnd() noexcept;
  };
}

#endif