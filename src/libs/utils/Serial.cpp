/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Serial.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-28
 ///

#include "Serial.hpp"
#include <iostream>
#include <cassert>
#include <regex>

namespace utils {
  namespace serial {

    Readresult readFile(const std::string& path)
    {
      std::ifstream filestream;
      filestream.exceptions(std::ifstream::badbit);
      filestream.open(path);
      if (!filestream) {
        throw FileNotFound{ "Failed open file : " + path };
      }
      size_t lineNumber(0);
      Readresult result;
      try {

        std::string line;

        std::regex lineregex(
          " *([a-z_]*) *= *([a-z_0-9\\-\\.]*) *",
          std::regex_constants::icase);

        while (std::getline(filestream, line)) {
          std::smatch match;
          if (std::regex_match(line, match, lineregex)) {
            auto [itr, success] = result.emplace(match[1], match[2]);
            if (!success) {
              throw IOFailure{
                path + " duplicated token at line : "
                + std::to_string(lineNumber) };
            }
          }
          ++lineNumber;
        }
      }
      catch (std::ifstream::failure& e) {
        filestream.close();
        throw IOFailure{
          "read failure : " + path
          + " at line : " + std::to_string(lineNumber) };
      }
      filestream.close();
      return result;
    }

    void deserialise(
      const Readresult& raw,
      const SerialisationTable& table,
      void* obj,
      bool strict)
    {
      uint8_t* rawobj = reinterpret_cast<uint8_t*>(obj);
      for (const auto& [key, entry] : table) {
        if (auto itr = raw.find(key); itr != raw.end()) {

          uint8_t* position = rawobj + entry.offset;
          const char* str = itr->second.c_str();

          switch (entry.type) {

          case Entry::Type::String:
            ::new(position) std::string(str);
            break;

          case Entry::Type::Int:
            ::new(position) int(std::atoi(str));
            break;

          case Entry::Type::Float:
            ::new(position) float(std::atof(str));
            break;

          default:
            assert(false && "Unkown Data Type");
          }
        }
        else if (strict) {
          throw MissingEntry{ key };
        }
      }
    }
  }
}