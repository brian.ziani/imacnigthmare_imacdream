/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file GLMHelpers.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-04
 ///

#ifndef _SRC_LIBS_GLM_HELPERS_H
#define _SRC_LIBS_GLM_HELPERS_H

#include <libs/utils/Math.hpp>
#include <glm/glm.hpp>
#include <ostream>

namespace helpers {

  /// \brief Return an unit length vector in same direction as v, or null vector if v is null
  template <int N, typename T, glm::qualifier Q>
  constexpr inline glm::vec<N, T, Q> safeNormalize(const glm::vec<N, T, Q>& v) noexcept;

  /// \brief Return the square of v's norm
  template <int N, typename T, glm::qualifier Q>
  constexpr inline float norm2(const glm::vec<N, T, Q>& v) noexcept;

  /// \brief Recursive closure for nullifySmallComponents
  template <typename T, glm::qualifier Q>
  constexpr inline glm::vec<1, T, Q> nullifySmallComponents(const glm::vec<1, T, Q>& v, float precision = 0.00001f) noexcept;

  /// \brief Function usefull to increase numeric stability of algorithms, by nullifying garbage values
  template <int N, typename T, glm::qualifier Q>
  constexpr inline glm::vec<N, T, Q> nullifySmallComponents(const glm::vec<N, T, Q>& v, float precision = 0.00001f) noexcept;

  /// \brief Convert an rgb value to a vec3 of floats
  constexpr inline glm::vec3 rgb(uint8_t r, uint8_t g, uint8_t b) noexcept;

  /// \brief Convert a angular rotation vector to a rotation matrix
  ///   A unit length vector result in an angle of 2pi
  glm::mat3 rotationMatrix(const glm::vec3& axis) noexcept;
}

namespace glm {

  /// \brief Make glm vectors streamable
  template <int N, typename T, qualifier Q>
  std::ostream& operator<< (std::ostream& s, const vec<N, T, Q>& v) noexcept;

  /// \brief Make glm matrices streamable
  template <int N, int M, typename T, qualifier Q>
  std::ostream& operator<< (std::ostream& s, const mat<N, M, T, Q>& v) noexcept;

  /// \brief Make glm Quaternions streamable
  template <typename T, qualifier Q>
  std::ostream& operator<< (std::ostream& s, const qua<T, Q>& q) noexcept;
}

///////////////////////////////////////////////////////////////////////////////////////////////

namespace helpers {

  template <int N, typename T, glm::qualifier Q>
  constexpr inline glm::vec<N, T, Q> safeNormalize(const glm::vec<N, T, Q>& v) noexcept {
    if (glm::length(v) == 0)
      return glm::vec<N, T, Q>(static_cast<T>(0));
    return glm::normalize(v);
  }

  template <int N, typename T, glm::qualifier Q>
  constexpr inline float norm2(const glm::vec<N, T, Q>& v) noexcept {
    return glm::dot(v, v);
  }

  template <typename T, glm::qualifier Q>
  constexpr inline glm::vec<1, T, Q> nullifySmallComponents(const glm::vec<1, T, Q>& v, float precision) noexcept {
    return glm::vec<1, T, Q>(math::nullifySmallValue(v[0], precision));
  }

  template <int N, typename T, glm::qualifier Q>
  constexpr inline glm::vec<N, T, Q> nullifySmallComponents(const glm::vec<N, T, Q>& v, float precision) noexcept {
    return glm::vec<N, T, Q>(
      nullifySmallComponents(glm::vec<N - 1, T, Q>(v), precision),
      math::nullifySmallValue(v[N - 1], precision)
      );
  }

  constexpr inline glm::vec3 rgb(uint8_t r, uint8_t g, uint8_t b) noexcept {
    return glm::vec3(r / 255.f, g / 255.f, b / 255.f);
  }
}

namespace glm {
  template <int N, typename T, qualifier Q>
  std::ostream& operator<< (std::ostream& s, const vec<N, T, Q>& v) noexcept {
    s << '(';
    for (size_t i = 0; i < N; ++i)
      s << v[i] << ' ';
    return s << ')';
  }

  template <int N, int M, typename T, qualifier Q>
  std::ostream& operator<< (std::ostream& s, const mat<N, M, T, Q>& v) noexcept {
    s << "M{" << v[0];
    for (size_t i = 1; i < N; ++i)
      s << v[i];
    return s << "}";
  }

  template <typename T, qualifier Q>
  std::ostream& operator<< (std::ostream& s, const qua<T, Q>& q) noexcept {
    return s << "Q(" << q.w << " " << q.x << " " << q.y << " " << q.z << ")";
  }
}

#endif /* _SRC_LIBS_GLM_HELPERS_H */