/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file GLMHelpers.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-04
 ///

#include "Helpers.hpp"
#include <glm/gtc/matrix_transform.hpp>

namespace helpers {

  glm::mat3 rotationMatrix(const glm::vec3& w) noexcept {
    float angle(2.f * M_PI * glm::length(w));
    glm::vec3 u(safeNormalize(w));
    glm::mat3 Q = glm::mat3(
      0.f, -u.z, u.y,
      u.z, 0.f, -u.x,
      -u.y, u.x, 0.f);
    glm::mat3 I = glm::identity<glm::mat3>();
    glm::mat3 R = I + (sinf(angle) * Q) + ((1.f - cosf(angle)) * Q * Q);
    return R;
  }
}