/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Helpers.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-27
 ///

#ifndef _SRC_LIBS_PHYSICS_PLAN_H
#define _SRC_LIBS_PHYSICS_PLAN_H

#include <glm/vec3.hpp>
#include <utility>
#include <tuple>

namespace physics {

  /// \brief Struct used to store datas about a single collision
  /// They can be ordered in a incraesing time order
  struct Contact {
    glm::vec3 contact;  ///< Contact point
    float timestamp;    ///< Normalised timestamp in [0,1]

    Contact(float t = 1.f, const glm::vec3& c = glm::vec3()) noexcept;
  };
  /// \brief defines a strong order relation on timestamp
  bool operator< (const Contact& a, const Contact& b) noexcept;

  /// \brief Represent the normalised time interval [t0,t1] included in [0,1]
  struct TimeInterval {
    float t0; ///< Interval begin
    float t1; ///< Interval end

    TimeInterval(float t0 = 0.f, float t1 = 1.f) noexcept;

    /// \brief Reduce given interval to [t0, contact.timestamp]
    void reduce(const Contact& contact) noexcept;
    /// \brief Reduce given interval to [t0, min(t1, I.t1)]
    void reduce(const TimeInterval& I) noexcept;
    /// \brief Return true if the two intervals intersects, and the intersection
    /// \warning at least one of the interval must be well formed (included in [0,1])
    ///   in order to return a well formed interval
    std::pair<bool, TimeInterval> intersection(const TimeInterval& I) const noexcept;
  };

  /// \brief Struct that represent an infinite plan with it's normal 
  ///   and it's constant
  struct Plan {
    glm::vec3 normal; ///< Plan's normal
    float constant;   ///< Plan's constant (the 'd' in : ax + by + cz + d = 0)

    /// \brief Construct a plan directly from a normal and a constant
    Plan(const glm::vec3& n = glm::vec3(), float d = 0.f) noexcept;
    /// \brief Construct a plan from a normal and a belonging point
    Plan(const glm::vec3& p, const glm::vec3& normal) noexcept;
    /// \brief Construct a plan from three points, in clockwise order
    Plan(const glm::vec3& a, const glm::vec3& b, const glm::vec3& c) noexcept;

    /// \brief Return signed distance to between plan and the point
    float signedDistanceTo(const glm::vec3& p) const noexcept;
  };

  /// \brief Test if given sphere collide with given plan
  /// \return (collision, embeded, interval) with
  /// collision : true if collision occured
  /// embeded : true if sphere is embeded in the plane
  /// interval : time interval when collision occurs
  std::tuple<bool, bool, TimeInterval> unitSphereVSPlaneCollisionTimestamp(
    const glm::vec3& sphere, const glm::vec3& velocity,
    const glm::vec3& planNormal, float planConstant,
    const TimeInterval& interval
  ) noexcept;

  /// \brief Check for a collision between a sphere and given square or triangle
  /// \return true and the collision it occurs in given interval
  std::pair<bool, Contact> unitSphereVSFaceCollision(
    const glm::vec3& A, const glm::vec3& B,
    const glm::vec3& C, const glm::vec3& D,
    const glm::vec3& normal,
    const glm::vec3& sphere, const glm::vec3& velocity,
    const TimeInterval& interval
  ) noexcept;

  /// \brief Check for a collision between a sphere and given vertex
  /// \return true and the collision it occurs in given interval
  std::pair<bool, Contact> unitSphereVSVertexCollision(
    const glm::vec3& vertex,
    const glm::vec3& sphere, const glm::vec3& velocity,
    float velocity2,
    const TimeInterval& interval
  ) noexcept;

  /// \brief Struct that represent a segment, 
  ///   with a starting point and a vector to se second one
  struct Edge {
    glm::vec3 vertex; ///< Base edge vertex
    glm::vec3 edge;   ///< Vector from base to the other vertex
  };

  /// \brief Check for a collision between a sphere and given edge
  /// \return true and the collision it occurs in given interval
  std::pair<bool, Contact> unitSphereVSEdgeCollision(
    const glm::vec3& vertex, const glm::vec3& edge,
    const glm::vec3& position, const glm::vec3& velocity,
    float velocity2,
    const TimeInterval& interval
  ) noexcept;
}

#endif /* _SRC_LIBS_PHYSICS_PLAN_H */