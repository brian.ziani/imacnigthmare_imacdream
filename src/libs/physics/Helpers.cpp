/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Types.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-27
 ///

#include "Helpers.hpp"

#include <libs/glm/Helpers.hpp>
#include <glm/geometric.hpp>
#include <libs/utils/Math.hpp>
#include <cassert>

namespace physics {

  Contact::Contact(float t, const glm::vec3& c) noexcept :
    contact(c), timestamp(t) {
  }

  bool operator< (const Contact& a, const Contact& b) noexcept {
    return a.timestamp < b.timestamp;
  }

  TimeInterval::TimeInterval(float t0, float t1) noexcept :
    t0(t0), t1(t1) {
  }

  void TimeInterval::reduce(const Contact& contact) noexcept {
    assert(t0 <= contact.timestamp && "Contact out of timeinterval");
    assert(contact.timestamp <= t1 && "Contact out of timeinterval");
    t1 = contact.timestamp;
  }
  void TimeInterval::reduce(const TimeInterval& I) noexcept {
    assert(t0 <= I.t1 && "Interval not left included");
    assert(I.t1 <= t1 && "Interval bigger than current");
    t1 = I.t1;
  }

  std::pair<bool, TimeInterval> TimeInterval::intersection(const TimeInterval& I) const noexcept {
    if (I.t1 < t0 || t1 < I.t0) {
      return std::make_pair(false, TimeInterval());
    }
    return std::make_pair(true, TimeInterval(std::max(t0, I.t0), std::min(t1, I.t1)));
  }

  Plan::Plan(const glm::vec3& n, float d) noexcept :
    normal(n), constant(d) {
  }
  Plan::Plan(const glm::vec3& p, const glm::vec3& normal) noexcept :
    normal(normal),
    constant(-glm::dot(p, normal)) {
  }
  Plan::Plan(const glm::vec3& a, const glm::vec3& b, const glm::vec3& c) noexcept :
    normal(glm::normalize(glm::cross(c - a, b - a))),
    constant(-glm::dot(a, normal)) {
  }

  float Plan::signedDistanceTo(const glm::vec3& p) const noexcept {
    return glm::dot(normal, p) + constant;
  }

  std::tuple<bool, bool, TimeInterval> unitSphereVSPlaneCollisionTimestamp(
    const glm::vec3& sphere, const glm::vec3& velocity,
    const glm::vec3& planNormal, float planConstant,
    const TimeInterval& interval
  ) noexcept {
    Plan plan(planNormal, planConstant);
    float distance(plan.signedDistanceTo(sphere));
    float normalVel(glm::dot(planNormal, velocity));
    bool embeded(fabs(distance) < 1.f);
    /* if mouvment is tangent to the plan */
    if (math::fequals(normalVel, 0.f)) {
      if (embeded) {
        return std::make_tuple(true, true, interval);
      }
      return std::make_tuple(false, false, TimeInterval());
    }
    /* else compute expected collision timestamps */
    float t0 = (1.f - distance) / (normalVel);
    float t1 = (-1.f - distance) / (normalVel);
    if (t1 < t0) {
      std::swap(t0, t1);
    }
    // a collision occurs if t0 or t1 is in interval [0,1] 
    //  we always return false on embeded flag, to ensure that the sphere
    //  cannot go forther inside the face
    auto [collide, result] = interval.intersection(TimeInterval(t0, t1));
    return std::make_tuple(collide, false, result);
  }

  std::pair<bool, Contact> unitSphereVSFaceCollision(
    const glm::vec3& A, const glm::vec3& B,
    const glm::vec3& C, const glm::vec3& D,
    const glm::vec3& normal,
    const glm::vec3& sphere, const glm::vec3& velocity,
    const TimeInterval& interval
  ) noexcept {
    glm::vec3 tmp(sphere - normal + interval.t0 * velocity);
    glm::vec3 AB(B - A), AD(D - A), CD(D - C), CB(B - C);
    glm::vec3 BAD(glm::cross(AB, AD));
    glm::vec3 DCB(glm::cross(CD, CB));
    // This work either for a planar quad and for a triangle
    if (0 <= glm::dot(BAD, glm::cross(AB, tmp - A))
      && 0 <= glm::dot(DCB, glm::cross(CD, tmp - C))
      && 0 <= glm::dot(-BAD, glm::cross(AD, tmp - A))
      && 0 <= glm::dot(-DCB, glm::cross(CB, tmp - C))) {
      return std::make_pair(true, Contact(interval.t0, tmp));
    }
    return std::make_pair(false, Contact());
  }

  std::pair<bool, Contact> unitSphereVSVertexCollision(
    const glm::vec3& vertex,
    const glm::vec3& sphere, const glm::vec3& velocity,
    float velocity2,
    const TimeInterval& interval
  ) noexcept {
    float a = velocity2;
    float b = 2.f * glm::dot(velocity, sphere - vertex);
    float c = helpers::norm2(vertex - sphere) - 1.f;
    if (math::QuadraticSolution sol = math::quadraticEquation(a, b, c)) {
      auto [valid, t] = math::smallestSolutionInRange(interval.t0, interval.t1, sol);
      return std::make_pair(valid, Contact(t, vertex));
    }
    return std::make_pair(false, Contact());
  }

  std::pair<bool, Contact> unitSphereVSEdgeCollision(
    const glm::vec3& vertex, const glm::vec3& edge,
    const glm::vec3& position, const glm::vec3& velocity,
    float velocity2,
    const TimeInterval& interval
  ) noexcept {
    glm::vec3 sphereToEdge(vertex - position);
    float edge2(helpers::norm2(edge));
    float edgeDotVelocity(glm::dot(edge, velocity));
    float edgeDotSphere(glm::dot(edge, sphereToEdge));
    float a = edge2 * (-velocity2) + edgeDotVelocity * edgeDotVelocity;
    float b = edge2 * 2.f * glm::dot(velocity, sphereToEdge) - 2.f * edgeDotVelocity * edgeDotSphere;
    float c = edge2 * (1.f - helpers::norm2(sphereToEdge)) + edgeDotSphere * edgeDotSphere;
    if (math::QuadraticSolution sol = math::quadraticEquation(a, b, c)) {
      auto [valid, t] = math::smallestSolutionInRange(interval.t0, interval.t1, sol);
      if (valid) {
        float f0 = (edgeDotVelocity * t - edgeDotSphere) / edge2;
        if (0.f <= f0 && f0 <= 1.f) {
          return std::make_pair(true, Contact(t, vertex + f0 * edge));
        }
      }
    }
    return std::make_pair(false, Contact());
  }

} // namespace physics
