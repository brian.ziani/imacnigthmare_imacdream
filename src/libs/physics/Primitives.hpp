/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Primitives.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-29
 ///

#ifndef _SRC_LIBS_PHYSICS_PRIMITIVES_H
#define _SRC_LIBS_PHYSICS_PRIMITIVES_H

#include "Helpers.hpp"

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <array>

namespace physics {

  struct Ellipsoid;
  struct Cube;

  /// \brief Base class for collidable physic primitives
  struct APrimitive {

    virtual ~APrimitive() noexcept = default;
    /// \brief Method that must return true if a collision occured in given interval
    virtual std::pair<bool, Contact> solveAgainstEllipsoid(
      const glm::vec3& eposition, const glm::vec3& evelocity,
      const glm::vec3& wsize, const glm::vec3& basis,
      const TimeInterval& interval) const noexcept = 0;
  };

  /* forward declare all primitives */

  struct Ellipsoid : public APrimitive {

    Ellipsoid(
      const glm::vec3& p = glm::vec3(0.f), 
      const glm::vec3& s = glm::vec3(0.f)) noexcept;
    virtual ~Ellipsoid() noexcept = default;

    /// \brief Method that must return true if a collision occured in given interval
    std::pair<bool, Contact> solveAgainstEllipsoid(
      const glm::vec3&, const glm::vec3&,
      const glm::vec3&, const glm::vec3&,
      const TimeInterval&) const noexcept override
    {
      assert(false && "Not implemented !!");
      return { false, Contact{} };
    }

    glm::mat4 transformation() const noexcept;

    glm::vec3 position; ///< Ellipsoid's center
    glm::vec3 size;     ///< Ellipsoid radiuses (x,y,z)
  };

  /// \brief A primitive that represent a parallelepiped
  struct Cube : public APrimitive {

    Cube(
      const glm::vec3& p = glm::vec3(0.f), 
      const glm::vec3& s = glm::vec3(0.f)) noexcept;
    virtual ~Cube() noexcept = default;

    /// \brief Method that must return true if a collision occured in given interval
    std::pair<bool, Contact> solveAgainstEllipsoid(
      const glm::vec3& eposition, const glm::vec3& evelocity,
      const glm::vec3& wsize, const glm::vec3& basis,
      const TimeInterval& interval) const noexcept override;

    glm::mat4 transformation() const noexcept;

    glm::vec3 position; ///< Cube's center
    glm::vec3 size;     ///< Cube's half dimensions (x,y,z)
  };

  /// \brief Define an irregular Hexahedron by it's vertices
  struct Hexahedron : public APrimitive {

    Hexahedron() noexcept = default;
    ~Hexahedron() noexcept = default;

    /// \brief Method that must return true if a collision occured in given interval
    std::pair<bool, Contact> solveAgainstEllipsoid(
      const glm::vec3& eposition, const glm::vec3& evelocity,
      const glm::vec3& wsize, const glm::vec3& basis,
      const TimeInterval& interval) const noexcept override;

    std::array<glm::vec3, 8> vertices;
    std::array<uint8_t[4], 6> faces;
  };
}

#endif /* _SRC_LIBS_PHYSICS_PRIMITIVES_H */