/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file MotionSolver.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-28
 ///

#ifndef _SRC_LIBS_PHYSICS_MOTION_SOLVER_H
#define _SRC_LIBS_PHYSICS_MOTION_SOLVER_H

#include "Helpers.hpp"
#include "Material.hpp"
#include "Primitives.hpp"

#include <libs/glm/Helpers.hpp>

#include <glm/vec3.hpp>
#include <glm/geometric.hpp>
#include <type_traits>
#include <functional>

namespace physics {
  namespace motion {

    /// \brief Represent the solution of a movement
    struct Solution {
      glm::vec3 finalPosition;  ///< Final object's position
      glm::vec3 contactNormal;  ///< Optional : smallest contact at final position
      Material contactMaterial; ///< Optional : Material of contact object
      bool hasContact;          ///< True if final position is in contact with an object
    };

    ///
    /// \brief Recursive function that compute movement for an unit sphere
    /// \return final position for the sphere, if initial position is invalid
    ///   final position might be invalid too (GIGO)
    ///  
    /// \param sphere : unit sphere's center
    /// \param velocity : unit sphere's velocity
    /// \param begin : iterator on begining of object that might be collided
    /// \param end : iterator on post last object that might be collided
    /// \param depth : maximal recursion depth for contact solving
    /// \warning WorldItr must be copy constructible
    /// \warning WorldItr must meets the requierements LegacyForwardIterator
    ///
    template <typename WorldItr>
    Solution solveEllipsoidMovement(
      const Ellipsoid& ellipse, const glm::vec3& velocity,
      const WorldItr& begin, const WorldItr& end,
      std::function<float(const glm::vec3&)> heightmap,
      size_t depth) noexcept;
  }
}

/////////////////////////////////////////////////////////////////////////////

namespace physics {
  namespace motion {

    template <typename WorldItr>
    Solution solveEllipsoidMovement(
      const Ellipsoid& ellipse, const glm::vec3& velocity,
      const WorldItr& begin, const WorldItr& end,
      std::function<float(const glm::vec3&)> heightmap,
      size_t depth) noexcept {
      /* type traits verifications (prevent compiler wtf error message) */
      static_assert(std::is_copy_constructible<WorldItr>::value, "WorldItr must be copy constructible");
      /* store temporary movement parameters */
      glm::vec3 currentBasis(glm::vec3(1.f, 1.f, 1.f) / ellipse.size);
      glm::vec3 currentPos(ellipse.position);
      glm::vec3 currentVelocity(velocity);
      /* final status */
      glm::vec3 finalContactNormal(0.f);
      Material finalContactMaterial(Material::Invalid);
      bool hasFinalContact(false);
      /* looped recursion */
      for (; 0 < depth; --depth) {
        /* collision context */
        Contact contact(1.f);
        TimeInterval interval(0.f, 1.f);
        bool collision(false);
        /* get smallest collision time */
        {
          glm::vec3 eposition(currentBasis * currentPos);
          glm::vec3 evelocity(currentBasis * currentVelocity);
            for (WorldItr itr(begin); itr != end; ++itr) {
              const auto& obj(*itr);
              auto [valid, tmp] = obj->solveAgainstEllipsoid(eposition, evelocity, ellipse.size, currentBasis, interval);
              if (valid && tmp < contact) {
                collision = true;
                contact = tmp;
                interval.reduce(tmp);
              }
            }
        }
        /* if no collision has occured, quick return else we have to recurse */
        if (!collision) {
          currentPos += currentVelocity;
          break;
        }
        /* compute substep */
        const float smallDistance = 0.001f;
        float distance = contact.timestamp * glm::length(currentVelocity);
        glm::vec3 originalTarget(currentPos + currentVelocity);
        glm::vec3 finalTarget(currentPos);
        /* always keep a small distance between objects */
        if (smallDistance <= distance) {
          glm::vec3 dir = glm::normalize(currentVelocity);
          finalTarget += dir * (distance - smallDistance);
          contact.contact -= smallDistance * dir;
        }
        /* sweep sphere against the tangent plan */
        glm::vec3 normal(finalTarget - contact.contact);
        normal = helpers::nullifySmallComponents(normal, smallDistance);
        Plan slidingp(contact.contact, glm::normalize(normal));
        glm::vec3 projectedTarget(originalTarget - slidingp.normal * slidingp.signedDistanceTo(originalTarget));
        currentPos = finalTarget;
        currentVelocity = projectedTarget - contact.contact;
        /* update collision status */
        hasFinalContact = true;
        finalContactNormal = slidingp.normal;
        finalContactMaterial = Material::Hard;
        /* if we are close enought to target we're done */
        if (glm::length(currentVelocity) <= smallDistance) {
          break;
        }
        /* here we go again */
      } /* depth recursion */
      /* adjust final position according to heightmap */
      float floor(ellipse.size.y + heightmap(currentPos));
      if (currentPos.y <= floor) {
        currentPos.y = floor;
        hasFinalContact = true;
        finalContactNormal = glm::vec3(0.f, 1.f, 0.f);
        finalContactMaterial = Material::Sand;
      }
      /* done */
      return Solution{
        .finalPosition = currentPos,
        .contactNormal = finalContactNormal,
        .contactMaterial = finalContactMaterial,
        .hasContact = hasFinalContact
      };
    }

  } /* namespace motion */
} /* namespace physics */

#endif /* _SRC_LIBS_PHYSICS_MOTION_SOLVER_H */