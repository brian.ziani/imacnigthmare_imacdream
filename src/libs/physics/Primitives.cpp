/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Primitives.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-29
 ///

#include "Primitives.hpp"

#include <libs/glm/Helpers.hpp>
#include <libs/utils/log.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/geometric.hpp>
#include <tuple>
#include <array>

namespace physics {

  glm::mat4 Ellipsoid::transformation() const noexcept
  {
    return glm::scale(
      glm::translate(
        glm::identity<glm::mat4>(), position
      ), size
    );
  }

  Ellipsoid::Ellipsoid(const glm::vec3& p, const glm::vec3& s) noexcept :
    position(p), size(s)
  {
  }

  Cube::Cube(const glm::vec3& p, const glm::vec3& s) noexcept :
    position(p), size(s)
  {
  }

  glm::mat4 Cube::transformation() const noexcept
  {
    return glm::scale(
      glm::translate(
        glm::identity<glm::mat4>(), position
      ), size
    );
  }

  static std::pair<bool, Contact> solveAgainstFacesSet(
    const glm::vec3& eposition, const glm::vec3& evelocity,
    const glm::vec3& wsize, const glm::vec3&,
    const TimeInterval& interval,
    const std::array<std::tuple<Plan, glm::vec3, glm::vec3, glm::vec3, glm::vec3>, 6>& facesplans) noexcept
  {
    bool collision(false);
    TimeInterval currentInterval(interval);
    Contact contact(1.f);

    float velocity2(helpers::norm2(evelocity));

    for (const auto& [plan, A, B, C, D] : facesplans) {
      glm::vec3 N = plan.normal;
      /* skip plans that do not face the velocity */
      if (0 < glm::dot(evelocity, N)) {
        continue;
      }
      auto [valid, embeded, time] = unitSphereVSPlaneCollisionTimestamp(
        eposition, evelocity, plan.normal, plan.constant, currentInterval);
      if (valid) {
        bool collideFace = false;
        bool validCollision = false;
        if (!embeded) {
          auto [valid, tmp] = unitSphereVSFaceCollision(
            A, B, C, D, N,
            eposition, evelocity,
            time);
          if (valid && tmp < contact) {
            collision = validCollision = collideFace = true;
            contact = tmp;
            time.reduce(tmp);
          }
        }
        if (!collideFace) {
          std::array<glm::vec3, 4> vertices = { A,B,C,D };
          std::array<glm::vec3, 4> edges = { B - A, C - B, D - C, A - D };
          for (const glm::vec3& vertex : vertices) {
            auto [valid, tmp] = unitSphereVSVertexCollision(
              vertex,
              eposition, evelocity,
              velocity2,
              time);
            if (valid && tmp < contact) {
              collision = validCollision = true;
              contact = tmp;
              time.reduce(tmp);
            }
          }
          for (size_t i = 0; i < 4; ++i) {
            const glm::vec3& vertex(vertices[i]);
            const glm::vec3& edge(edges[i]);
            auto [valid, tmp] = unitSphereVSEdgeCollision(
              vertex, edge,
              eposition, evelocity,
              velocity2,
              time);
            if (valid && tmp < contact) {
              collision = validCollision = true;
              contact = tmp;
              time.reduce(tmp);
            }
          } /* for each edge */
        } /* if not collide face */
        /* if a valid collision has occured, we can reduce the time interval for the following steps */
        if (validCollision) {
          currentInterval.reduce(time);
        }
      } /* if collide plan */
    } /* for each plan */
    /* convert result to world positions */
    contact.contact *= wsize;
    return std::make_pair(collision, contact);
  }

  std::pair<bool, Contact> Cube::solveAgainstEllipsoid(
    const glm::vec3& eposition, const glm::vec3& evelocity,
    const glm::vec3& wsize, const glm::vec3& basis,
    const TimeInterval& interval) const noexcept
  {
    glm::vec3 epos(basis * position);
    glm::vec3 esize(basis * size);
    /* naive method, check against all faces, vertices and edges */
    std::array<glm::vec3, 8> vertices = {
      glm::vec3(epos.x - esize.x, epos.y - esize.y, epos.z - esize.z),
      glm::vec3(epos.x + esize.x, epos.y - esize.y, epos.z - esize.z),
      glm::vec3(epos.x + esize.x, epos.y - esize.y, epos.z + esize.z),
      glm::vec3(epos.x - esize.x, epos.y - esize.y, epos.z + esize.z),

      glm::vec3(epos.x - esize.x, epos.y + esize.y, epos.z - esize.z),
      glm::vec3(epos.x + esize.x, epos.y + esize.y, epos.z - esize.z),
      glm::vec3(epos.x + esize.x, epos.y + esize.y, epos.z + esize.z),
      glm::vec3(epos.x - esize.x, epos.y + esize.y, epos.z + esize.z) };

    std::array<std::tuple<Plan, glm::vec3, glm::vec3, glm::vec3, glm::vec3>, 6> facesplans = {
      /* back faces are counter clockwise */
      std::make_tuple(
        Plan(vertices[0], vertices[3], vertices[2]), vertices[0], vertices[3], vertices[2], vertices[1]),
      {Plan(vertices[0], vertices[1], vertices[5]), vertices[0], vertices[1], vertices[5], vertices[4]},
      {Plan(vertices[0], vertices[4], vertices[7]), vertices[0], vertices[4], vertices[7], vertices[3]},
      /* front faces are clockwise */
      {Plan(vertices[6], vertices[7], vertices[4]), vertices[6], vertices[7], vertices[4], vertices[5]},
      {Plan(vertices[6], vertices[5], vertices[1]), vertices[6], vertices[5], vertices[1], vertices[2]},
      {Plan(vertices[6], vertices[2], vertices[3]), vertices[6], vertices[2], vertices[3], vertices[7]} };
    return solveAgainstFacesSet(eposition, evelocity, wsize, basis, interval, facesplans);
  } /* fn solveAgainstUnitSphere */


  std::pair<bool, Contact> Hexahedron::solveAgainstEllipsoid(
    const glm::vec3& eposition, const glm::vec3& evelocity,
    const glm::vec3& wsize, const glm::vec3& basis,
    const TimeInterval& interval) const noexcept
  {
    const std::array<glm::vec3, 8>& v(vertices);
    std::array<std::tuple<Plan, glm::vec3, glm::vec3, glm::vec3, glm::vec3>, 6> facesplans;
    for (int i = 0; i < 6; ++i) {
      const auto& face = faces[i];
      facesplans[i] = { Plan(v[face[0]], v[face[1]], v[face[2]]), 
        v[face[0]], v[face[1]], v[face[2]], v[face[3]] };
    }
    return solveAgainstFacesSet(eposition, evelocity, wsize, basis, interval, facesplans);
  }

} /* namespace physics */