/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file ICamera.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-12-10
 ///

#ifndef _SRC_LIBS_CAMERA_ICAMERA_H
#define _SRC_LIBS_CAMERA_ICAMERA_H

#include <glm/mat4x4.hpp>

namespace gui {

  class ICamera {
  public:

    virtual ~ICamera() noexcept = default;

    virtual const glm::mat4& viewMatrix() const noexcept = 0;
    virtual const glm::mat4& projectionMatrix() const noexcept = 0;
    virtual const glm::mat4& VPMatrix() const noexcept = 0;
    virtual const glm::vec3& eyesPosition() const noexcept = 0;
  };
}

#endif /* _SRC_LIBS_CAMERA_ICAMERA_H */