/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Camera.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-06
 ///

#ifndef _SRC_GUI_CAMERA_H
#define _SRC_GUI_CAMERA_H

#include "../ICamera.hpp"

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

namespace gui {

  struct TrackballCamera : public ICamera {

    glm::vec3 target; ///< Camera's target point
    float distance;   ///< Distance to target point
    float angley;     ///< Angle between z axis and camera
    float anglex;     ///< Angle between floor and camera

    float fov;  ///< Camera's FOV
    float near; ///< Near clip plan
    float far;  ///< Far clip plan

    int width;  ///< Viewport width
    int height; ///< Viewport height

    glm::mat4 V;
    glm::mat4 P;
    glm::mat4 VP;

    TrackballCamera() noexcept = default;
    ~TrackballCamera() noexcept = default;

    const glm::mat4& viewMatrix() const noexcept override;
    const glm::mat4& projectionMatrix() const noexcept override;
    const glm::mat4& VPMatrix() const noexcept override;
    const glm::vec3& eyesPosition() const noexcept override;

    void update() noexcept;
  };
}

#endif /* _SRC_GUI_CAMERA_H */