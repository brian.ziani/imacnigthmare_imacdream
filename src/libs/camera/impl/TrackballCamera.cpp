/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file TrackballCamera.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \date 2020-11-18
 ///

#include "TrackballCamera.hpp"

#include <glm/gtc/matrix_transform.hpp>

namespace gui {

  const glm::mat4& TrackballCamera::viewMatrix() const noexcept
  {
    return V;
  }

  const glm::mat4& TrackballCamera::projectionMatrix() const noexcept
  {
    return P;
  }
  const glm::mat4& TrackballCamera::VPMatrix() const noexcept
  {
    return VP;
  }
  const glm::vec3& TrackballCamera::eyesPosition() const noexcept
  {
    return target;
  }

  void TrackballCamera::update() noexcept
  {
    V = glm::translate(
      glm::rotate(
        glm::rotate(
          glm::translate(glm::identity<glm::mat4>(), glm::vec3(0, 0, -distance)),
          anglex, glm::vec3(1, 0, 0)),
        angley, glm::vec3(0, 1, 0)),
      -target);
    P = glm::perspectiveFov(fov, float(width), float(height), near, far);
    VP = P * V;
  }
}