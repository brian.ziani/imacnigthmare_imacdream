/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Window.cpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-12
 ///

#include <GL/glew.h>
#include "Window.hpp"

#include <stdexcept>

namespace gui {

  Window::Window(const std::string& name, int width, int height) : window(nullptr), glcontext(nullptr)
  {
    try {
      window = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED, width, height,
        SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
      if (!window)
        throw WindowCreationFailure{ SDL_GetError() };
      glcontext = SDL_GL_CreateContext(window);
      if (!glcontext)
        throw OpenGLInitFailure{ SDL_GetError() };
      if (SDL_GL_SetSwapInterval(1) < 0)
        throw SwapInitFailure{ SDL_GetError() };

      GLenum error = glGetError();
      if (error != GL_NO_ERROR) {
        throw OpenGLError{ std::to_string(error) };
      }
    }
    catch (...) {
      if (glcontext) SDL_GL_DeleteContext(glcontext);
      if (window) SDL_DestroyWindow(window);
      throw;
    }
  }
  Window::~Window() noexcept
  {
    if (glcontext) SDL_GL_DeleteContext(glcontext);
    if (window) SDL_DestroyWindow(window);
  }
}