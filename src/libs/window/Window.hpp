/*
 * Copyright (C) 2020 DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 ///
 ///  \file Window.hpp
 /// 
 ///  \author DAGO Kokri Esaïe <dago.esaie@protonmail.com>
 ///  \author ZIANI Brian <brian.ziani@gmail.com>
 ///  \date 2020-11-12
 ///


#ifndef _SRC_GUI_WINDOW_H_
#define _SRC_GUI_WINDOW_H_

#include <SDL2/SDL_video.h>
#include <string>

namespace gui {

  struct Window {
    SDL_Window* window;      ///< The main window (owned)
    SDL_GLContext glcontext; ///< Opaque handle to OpenGL context

    struct Error { std::string msg; };
    struct WindowCreationFailure : public Error {};
    struct OpenGLInitFailure : public Error {};
    struct SwapInitFailure : public Error {};
    struct OpenGLError : public Error {};

    /// \brief Create a window and associated OpenGL context
    /// \warning SDL must be started
    /// \throw runtime_error on failure
    Window(const std::string& name, int width, int height);
    ~Window() noexcept;
  };
} // namespace gui

#endif /* _SRC_GUI_WINDOW_H_ */